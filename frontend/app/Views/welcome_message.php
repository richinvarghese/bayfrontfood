<style>


	* {
	box-sizing: border-box;
	}

	.row > .column {
	padding: 0 8px;
	}

	.row:after {
	content: "";
	display: table;
	clear: both;
	}

	.column {
	float: left;
	width: 25%;
	}

	/* The Modal (background) */
	.modal {
	display: none;
	position: fixed;
	z-index: 1;
	padding-top: 100px;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	overflow: auto;
	background-color: black;
	}

	/* Modal Content */
	.modal-content {
	position: relative;
	background-color: #fefefe;
	margin: auto;
	padding: 0;
	width: 90%;
	max-width: 1200px;
	}

	/* The Close Button */
	.close {
	color: white;
	position: absolute;
	top: 10px;
	right: 25px;
	font-size: 35px;
	font-weight: bold;
	}

	.close:hover,
	.close:focus {
	color: #999;
	text-decoration: none;
	cursor: pointer;
	}

	.mySlides {
	display: none;
	}

	.cursor {
	cursor: pointer;
	}

	/* Next & previous buttons */
	.prev,
	.next {
	cursor: pointer;
	position: absolute;
	top: 50%;
	width: auto;
	padding: 16px;
	margin-top: -50px;
	color: white;
	font-weight: bold;
	font-size: 20px;
	transition: 0.6s ease;
	border-radius: 0 3px 3px 0;
	user-select: none;
	-webkit-user-select: none;
	}

	/* Position the "next button" to the right */
	.next {
	right: 0;
	border-radius: 3px 0 0 3px;
	}

	/* On hover, add a black background color with a little bit see-through */
	.prev:hover,
	.next:hover {
	background-color: rgba(0, 0, 0, 0.8);
	}

	/* Number text (1/3 etc) */
	.numbertext {
	color: #f2f2f2;
	font-size: 12px;
	padding: 8px 12px;
	position: absolute;
	top: 0;
	}

	img {
	margin-bottom: -4px;
	}

	.caption-container {
	text-align: center;
	background-color: black;
	padding: 2px 16px;
	color: white;
	}

	.demo {
	opacity: 0.6;
	}

	.active,
	.demo:hover {
	opacity: 1;
	}

	img.hover-shadow {
	transition: 0.3s;
	}

	.hover-shadow:hover {
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
	}
</style>
<!--================Banner Area =================-->
 <section class="banner_area">
            <div class="container">
                <div class="banner_content">
                    <h4>Our Gallery</h4>
                    <a href="#">Home</a>
                    <a class="active" href="gallery.html">Gallery</a>
                </div>
            </div>
        </section>
		<!--================End Banner Area =================-->  <!--================Our Gallery Area =================-->
		<section class="most_popular_item_area menu_list_page">
            <div class="container">
                <div class="popular_filter">
                    <ul>
                        <li class="active" data-filter="*" style='margin:5px;'><a href="">All</a></li>

                        <li data-filter=".Christmas" style='margin:5px;'><a href="" >Christmas</a></li>
                        <li data-filter=".MiniCatering" style='margin:5px;'><a href="" > Mini Catering</a></li>
                        <li data-filter=".RegularBuffet" style='margin:5px;'><a href="" >Regular Buffet</a></li>
                        <li data-filter=".PackedMeal" style='margin:5px;'><a href="" >Packed Meal</a></li>

                    </ul>
                </div>
                <section class="our_gallery_area">
                    <div class="container">
                        <div class="row our_gallery_ms_inner">
                            <div class="p_recype_item_main">
                                <div class="row p_recype_item_active">
                                    <div class="col-md-4 col-sm-6 break Christmas">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g2.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
                                            <div class="our_gallery_hover">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                                <a href="#"><h5>Fried Hong Kong Noodle</h5></a>
                                                <p>Fried Hong Kong Noodle 干炒香港面 | 🎄 Christmas </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break Christmas">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g3.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
                                            <div class="our_gallery_hover">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                                <a href="#"><h5>Vegetarian Luo Han Zai</h5></a>
                                                <p>Vegetarian Luo Han Zai 罗汉斋 | 🎄 Christmas</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g1.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
                                            <div class="our_gallery_hover">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                                <a href="#"><h5>Sweet and Sour Fish</h5></a>
                                                <p>Sweet and Sour Fish by Bayfront Catering Services </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break MiniCatering">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g8.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor"> 
                                            <div class="our_gallery_hover">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                                <a href="#"><h5>Yang Zhou Fried Rice</h5></a>
                                                <p>Yang Zhou Fried Rice 扬州炒饭 | Mini Catering </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break Christmas">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g5.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(5)" class="hover-shadow cursor">
                                            <div class="our_gallery_hover">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                                <a href="#"><h5>Full Buffet Catering Spread</h5></a>
                                                <p>Full Buffet Catering Spread | 🎄 Christmas</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break RegularBuffet">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g4.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(6)" class="hover-shadow cursor">
                                            <div class="our_gallery_hover">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                                <a href="#"><h5>Signature Curry Chicken w/ Potato</h5></a>
                                                <p>Signature Curry Chicken w/ Potato 招牌咖喱鸡马铃薯 | Regular Buffet </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break PackedMeal">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g12.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(7)" class="hover-shadow cursor">
                                            <div class="our_gallery_hover">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                                <a href="#"><h5>Broccoli w/Mushroom</h5></a>
                                                <p>Broccoli w/Mushroom | Packed Meal</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break RegularBuffet">
                                        <div class="our_gallery_item">
                                            <img id="myImg" src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g7.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(8)" class="hover-shadow cursor">
                                            <div class="our_gallery_hover">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                                <a href="#"><h5>Full Buffet Catering Setup</h5></a>
                                                <p>Full Buffet Catering Setup | Regular Buffet</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break PackedMeal">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g11.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(9)" class="hover-shadow cursor">
                                            <div class="our_gallery_hover">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                                <a href="#"><h5>Blackpepper Beef</h5></a>
                                                <p>Blackpepper Beef | Packed Meal</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break MiniCatering">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g9.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(10)" class="hover-shadow cursor">
                                            <div class="our_gallery_hover">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                                <a href="#"><h5>Thai Fish Cake</h5></a>
                                                <p>Thai Fish Cake 泰式鱼饼 | Mini Catering</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break PackedMeal">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g10.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(11)" class="hover-shadow cursor">
                                            <div class="our_gallery_hover">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                                <a href="#"><h5>Packed Meal</h5></a>
                                                <p>Packed Meal</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break RegularBuffet">
                                            <div class="our_gallery_item">
                                                <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g6.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(12)" class="hover-shadow cursor">
                                                <div class="our_gallery_hover">
                                                    <a href="#"><i class="fa fa-link"></i></a>
                                                    <a href="#"><h5>Bossam</h5></a>
                                                    <p> Regular Buffet</p>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>
<div class="row">
  <div class="column">
    <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g2.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g1.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g3.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
  </div>
  <div class="column">
    <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g4.jpg" alt="" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
  </div>
</div>

<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <div class="numbertext">1 / 11</div>
      <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g2.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">2 / 11</div>
      <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g3.jpg" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">3 / 11</div>
      <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g1.jpg" style="width:100%">
    </div>
    
    <div class="mySlides">
      <div class="numbertext">5 / 11</div>
      <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g8.jpg" style="width:100%">
    </div>
    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>


    <div class="column">
      <img class="demo cursor" src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g4.jpg" style="width:100%" onclick="currentSlide(1)" alt="Nature and sunrise">
    </div>
    <div class="column">
      <img class="demo cursor" src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g4.jpg" style="width:100%" onclick="currentSlide(2)" alt="Snow">
    </div>
    <div class="column">
      <img class="demo cursor" src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g4.jpg" style="width:100%" onclick="currentSlide(3)" alt="Mountains and fjords">
    </div>
    <div class="column">
      <img class="demo cursor" src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g4.jpg" style="width:100%" onclick="currentSlide(4)" alt="Northern Lights">
    </div>
  </div>
</div>

<script>
	function openModal() {
	document.getElementById("myModal").style.display = "block";
	}

	function closeModal() {
	document.getElementById("myModal").style.display = "none";
	}

	var slideIndex = 1;
	showSlides(slideIndex);

	function plusSlides(n) {
	showSlides(slideIndex += n);
	}

	function currentSlide(n) {
	showSlides(slideIndex = n);
	}

	function showSlides(n) {
	var i;
	var slides = document.getElementsByClassName("mySlides");
	var dots = document.getElementsByClassName("demo");
	var captionText = document.getElementById("caption");
	if (n > slides.length) {slideIndex = 1}
	if (n < 1) {slideIndex = slides.length}
	for (i = 0; i < slides.length; i++) {
		slides[i].style.display = "none";
	}
	for (i = 0; i < dots.length; i++) {
		dots[i].className = dots[i].className.replace(" active", "");
	}
	slides[slideIndex-1].style.display = "block";
	dots[slideIndex-1].className += " active";
	captionText.innerHTML = dots[slideIndex-1].alt;
	}
</script>
    

