
        
        <!--================Slider Area =================-->
        <section class="slider_area">
            <div class=slider_inner>
                <div class="rev_slider fullwidthabanner"  data-version="5.3.0.2" id="home-slider">
                    <ul> 
                        <li data-slotamount="7" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="600" data-rotate="0" data-saveperformance="off">
                            <!-- MAIN IMAGE -->
                            <img src="<?php echo base_url("public/assets") ?>/img/home-slider/slider-2.png"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="slider_text_box">
                               <div class="tp-caption bg_box" 
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap"
                                    data-x="center" 
                                    data-y="['350','350','300','250','0']"
                                    data-fontsize="['55']" 
                                    data-lineheight="['60']" 
                                    data-transform_idle="o:1;"
                                    data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:0px;" 
                                    data-mask_out="x:inherit;y:inherit;" 
                                    data-start="2000" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on">
                                </div>
                                <div class="tp-caption first_text" 
                                    data-x="center" 
                                    data-y="center" 
                                    data-voffset="['-20']"
                                    data-Hoffset="['0']"
                                    data-fontsize="['42','42','42','42','25']"
                                    data-lineheight="['52','52','52','52','35']"
                                    data-width="none"
                                    data-height="none"
                                    data-transform_idle="o:1;"
                                    data-whitespace="nowrap"
                                    data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1000" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    data-elementdelay="0.05" >Welcome To Our
                                </div>
                                <div class="tp-caption secand_text" 
                                    data-x="center" 
                                    data-y="center" 
                                    data-voffset="['45']"
                                    data-Hoffset="['0']"
                                    data-fontsize="['36']"
                                    data-lineheight="['42']"
                                    data-width="none"
                                    data-height="none"
                                    data-transform_idle="o:1;"
                                    data-whitespace="nowrap"
                                    data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1000" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    data-elementdelay="0.05" >Bayfront Catering
                                </div>
                                <div class="tp-caption third_text" 
                                    data-x="center" 
                                    data-y="center" 
                                    data-voffset="['100']"
                                    data-Hoffset="['0']"
                                    data-fontsize="['24','24','24','24','16']"
                                    data-lineheight="['34','34','34','34','26']"
                                    data-width="none"
                                    data-height="none"
                                    data-transform_idle="o:1;"
                                    data-whitespace="nowrap"
                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1200" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    data-elementdelay="0.05" >  SERVICES
                                </div>
                                <div class="tp-caption btn_text" 
                                    data-x="center" 
                                    data-y="center" 
                                    data-voffset="['180']"
                                    data-Hoffset="['0']"
                                    data-fontsize="['16.75']"
                                    data-lineheight="['26']"
                                    data-width="none"
                                    data-height="none"
                                    data-transform_idle="o:1;"
                                    data-whitespace="nowrap"
                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1200" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    data-elementdelay="0.05" ><a class="submit_btn_bg" href=<?php echo base_url("public/home/masterCategoies")?>>Order Buffet</a>
                                </div>
                            </div>
                        </li>
                        <li data-slotamount="7" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="600" data-rotate="0" data-saveperformance="off">
                            <!-- MAIN IMAGE -->
                            <img src="<?php echo base_url("public/assets") ?>/img/home-slider/g9.png"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="slider_text_box text_box2">
                               <div class="tp-caption bg_box" 
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap"
                                    data-x="center" 
                                    data-y="['350','350','300','250']"
                                    data-fontsize="['55']" 
                                    data-lineheight="['60']" 
                                    data-transform_idle="o:1;"
                                    data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:0px;" 
                                    data-mask_out="x:inherit;y:inherit;" 
                                    data-start="2000" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on">
                                </div>
                                <div class="tp-caption first_text" 
                                    data-x="center" 
                                    data-y="center" 
                                    data-voffset="['-20']"
                                    data-Hoffset="['0']"
                                    data-fontsize="['42','42','42','42','25']"
                                    data-lineheight="['52','52','52','52','35']"
                                    data-width="none"
                                    data-height="none"
                                    data-transform_idle="o:1;"
                                    data-whitespace="nowrap"
                                    data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1000" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    data-elementdelay="0.05" >Welcome To Our
                                </div>
                                <div class="tp-caption secand_text" 
                                    data-x="center" 
                                    data-y="center" 
                                    data-voffset="['45']"
                                    data-Hoffset="['0']"
                                    data-fontsize="['36']"
                                    data-lineheight="['42']"
                                    data-width="none"
                                    data-height="none"
                                    data-transform_idle="o:1;"
                                    data-whitespace="nowrap"
                                    data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1000" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    data-elementdelay="0.05" >Bayfront Catering
                                </div>
                                <div class="tp-caption third_text" 
                                    data-x="center" 
                                    data-y="center" 
                                    data-voffset="['100']"
                                    data-Hoffset="['0']"
                                    data-fontsize="['24','24','24','24','16']"
                                    data-lineheight="['34','34','34','34','26']"
                                    data-width="none"
                                    data-height="none"
                                    data-transform_idle="o:1;"
                                    data-whitespace="nowrap"
                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1200" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    data-elementdelay="0.05" >  SERVICES
                                </div>
                                <div class="tp-caption btn_text" 
                                    data-x="center" 
                                    data-y="center" 
                                    data-voffset="['180']"
                                    data-Hoffset="['0']"
                                    data-fontsize="['16.75']"
                                    data-lineheight="['26']"
                                    data-width="none"
                                    data-height="none"
                                    data-transform_idle="o:1;"
                                    data-whitespace="nowrap"
                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1200" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    data-elementdelay="0.05" ><a class="submit_btn_bg" href=<?php echo base_url("public/home/masterCategoies")?>>Order Buffet</a>
                                </div>
                            </div>
                        </li>
                    </ul> 
                </div><!-- END REVOLUTION SLIDER -->
            </div>
        </section>
        <!--================End Slider Area =================-->
        <section class="recent_bloger_area">
            <div class="container">
                <div class="s_black_title">
                    <h3>Bayfront Food</h3>
                    <h2>A PREMIUM RESTAURANT &<BR> CATERING SERVICES</h2>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="recent_blog_item">
                            <div class="blog_img">
                                <img src="<?php echo base_url("public/assets") ?>/img/blog/recent-blog/recent-blog-1.jpg" alt="">
                            </div>
                            <div class="recent_blog_text">
                                <div class="recent_blog_text_inner">
                                    <h6><a href="<?php echo base_url("public/home/about")?>">OUR STORY</a></h6>
                                <h5><a href="<?php echo base_url("public/home/about")?>">About us</a> </h5>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-md-4">
                        <div class="recent_blog_item">
                            <div class="blog_img">
                                <img src="<?php echo base_url("public/assets") ?>/img/blog/recent-blog/recent-blog-2.jpg" alt="">
                            </div>
                            <div class="recent_blog_text">
                                <div class="recent_blog_text_inner">
                                    <h6><a href="<?php echo base_url("public/home/assurance")?>">Our Assurance</a></h6>
                                    <h5><a href="<?php echo base_url("public/home/assurance")?>">Quality, Fresh & Safety</a></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-md-4">
                        <div class="recent_blog_item">
                            <div class="blog_img">
                                <img src="<?php echo base_url("public/assets") ?>/img/blog/recent-blog/recent-blog-3.jpg" alt="">
                            </div>
                            <div class="recent_blog_text">
                                <div class="recent_blog_text_inner">
                                    <h6><a href="<?php echo base_url("public/home/commitment")?>">Our Commitment</a></h6>
                                    <h5><a href="<?php echo base_url("public/home/commitment")?>">Unique & Memorable</a></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================Service Area =================-->
      <!--  <section class="service_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="service_item">
                            <img src="<?php echo base_url("public/assets") ?>/img/service-icon/service-1.png" alt="">
                            <h3>Pizzas</h3>
                            <p>Lorem ipsum dolor sit amet, cont tempor incididunt ut labore dolor adipiscing elit, sed do eiusmod et  magna aliquaquat officia.</p>
                            <a class="read_mor_btn" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="service_item">
                            <img src="<?php echo base_url("public/assets") ?>/img/service-icon/service-2.png" alt="">
                            <h3>Coffee</h3>
                            <p>Lorem ipsum dolor sit amet, cont tempor incididunt ut labore dolor adipiscing elit, sed do eiusmod et  magna aliquaquat officia.</p>
                            <a class="read_mor_btn" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="service_item">
                            <img src="<?php echo base_url("public/assets") ?>/img/service-icon/service-3.png" alt="">
                            <h3>Burgers</h3>
                            <p>Lorem ipsum dolor sit amet, cont tempor incididunt ut labore dolor adipiscing elit, sed do eiusmod et  magna aliquaquat officia.</p>
                            <a class="read_mor_btn" href="#">Read More</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="service_item">
                            <img src="<?php echo base_url("public/assets") ?>/img/service-icon/service-4.png" alt="">
                            <h3>Drinks</h3>
                            <p>Lorem ipsum dolor sit amet, cont tempor incididunt ut labore dolor adipiscing elit, sed do eiusmod et  magna aliquaquat officia.</p>
                            <a class="read_mor_btn" href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
        <!--================End Service Area =================-->
        
        <!--================Booking Table Area =================-->
        <section class="booking_table_area">
            <div class="container">
                <div class="s_white_title">
                    <h3>Get In Touch</h3>
                    <h2>DROP A LINE</h2>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name*">
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Your Email*">
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div>
                        <input type="text" class="form-control" id="subject" name="subject" placeholder="Write Message">


                        </div>
                    </div>
                    <div class="col-sm-3">
                        <button type="button" class="btn btn-default submit_btn">Send</button>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Booking Table Area =================-->
        
        <!--================Our feature Area =================-->
        <!--<section class="our_feature_area">
            <div class="container">
                <div class="s_black_title">
                    <h3>Book a</h3>
                    <h2>Table</h2>
                </div>
                <div class="feature_slider">
                    <div class="item">
                        <div class="feature_item">
                            <div class="feature_item_inner">
                                <img src="<?php echo base_url("public/assets") ?>/img/feature/feature-1.jpg" alt="">
                                <div class="icon_hover">
                                    <i class="fa fa-search"></i>
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                            </div>
                            <div class="title_text">
                                <div class="feature_left"><a href="table.html"><span>Grilled Chicken</span></a></div>
                                <div class="restaurant_feature_dots"></div>
                                <div class="feature_right">$32</div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="feature_item">
                            <div class="feature_item_inner">
                                <img src="<?php echo base_url("public/assets") ?>/img/feature/feature-2.jpg" alt="">
                                <div class="icon_hover">
                                    <i class="fa fa-search"></i>
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                            </div>
                            <div class="title_text">
                                <div class="feature_left"><a href="table"><span>Lasagne Pasta</span></a></div>
                                <div class="restaurant_feature_dots"></div>
                                <div class="feature_right">$16</div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="feature_item">
                            <div class="feature_item_inner">
                                <img src="<?php echo base_url("public/assets") ?>/img/feature/feature-3.jpg" alt="">
                                <div class="icon_hover">
                                    <i class="fa fa-search"></i>
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                            </div>
                            <div class="title_text">
                                <div class="feature_left"><a href="table"><span>Hamburger</span></a></div>
                                <div class="restaurant_feature_dots"></div>
                                <div class="feature_right">$25</div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="feature_item">
                            <div class="feature_item_inner">
                                <img src="<?php echo base_url("public/assets") ?>/img/feature/feature-1.jpg" alt="">
                                <div class="icon_hover">
                                    <i class="fa fa-search"></i>
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                            </div>
                            <div class="title_text">
                                <div class="feature_left"><a href="table"><span>Grilled Chicken.</span></a></div>
                                <div class="restaurant_feature_dots"></div>
                                <div class="feature_right">$32</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
        <!--================End Our feature Area =================-->
        
        <!--================End Our feature Area =================-->
        <section class="most_popular_item_area">
            <div class="container">
                <div class="s_white_title">
                    <h3>Buffet Master </h3>
                    <h2>Categories</h2>
                </div>
                <div class="popular_filter" style="display:none">
                    <ul>
                        <li class="active" data-filter="*"><a href="">All</a></li>
                        <li data-filter=".2019"><a href="">2019</a></li>
                        <li data-filter=".2018"><a href="">2018</a></li>
                        
                    </ul>
                </div>

               



                <div class="p_recype_item_main">
                    <div class="row p_recype_item_active">
                        <?php 
                        $i=0;
                            foreach($mcategory as $v_category):
                                $i++;
                                if($i==5){
                                break;
                                }
                                
                                if($v_category->master_cat_image==""){
                                    $imageurl = base_url("public/assets")."/img/recype/recype-7.jpg";
                                }
                                else{
                                    $imageurl  = "http://catering.bayfrontfood.sg/assets/images/master_cat/".$v_category->master_cat_image;
                                }
                            
                        ?>
                      

                        <div class="col-md-6 break 2019">
                            <div class="media">
                                <div class="media-left">
                                <img width="112" height="130" src = "<?php echo $imageurl ?>" />
 
                                </div>
                                <div class="media-body">
                                <a  target="_blank" href="http://catering.bayfrontfood.sg/frontend_controller/listmenu/index/<?php echo $v_category->master_cat_id?>"><h3>"<?php echo $v_category->master_cat_name; ?>" </h3></a>
                                    <h4></h4>
                                    <p> <?php  echo strip_tags($v_category->master_cat_descrip);?>
                              
                                </p>
                                <?php if($i%3==0){?>

<ul>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
   
</ul>
<?php  }
else if($i%2==0){
?>
        <ul>
        <li><a href="#"><i class="fa fa-star"></i></a></li>
        <li><a href="#"><i class="fa fa-star"></i></a></li>
        <li><a href="#"><i class="fa fa-star"></i></a></li>
        <li><a href="#"><i class="fa fa-star"></i></a></li>
      
    </ul>
<?php }

else {
?>
    <ul>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
   
</ul>
<?php }?>
                                </div>
                            </div>
                        </div>
                       
                        <?php 
                        endforeach;
                        ?>
                        
                       
                    </div>
                    <br><div style=" text-align: center;">
                    <a class="event_btn" href="<?php echo base_url("public/home/masterCategoies")?>" style="color:black">View More</a>
</div>
                   
                  
                        
                   
                </div>
                
            </div>
            
        </section>
        <!--================End Our feature Area =================-->
        
        <!--================Our Chefs Area =================-->
        <!--<section class="our_chefs_area">
            <div class="container">
                <div class="s_black_title">
                    <h3>Meet</h3>
                    <h2>OUR CHEFS</h2>
                </div>
                <div class="chefs_slider_active">
                    <div class="item">
                        <div class="chef_item_inner">
                            <div class="chef_img">
                                <img src="<?php echo base_url("public/assets") ?>/img/chef/chef-1.jpg" alt="">
                                <div class="chef_hover">
                                    <a href="#"><h4>Thomas Keller</h4></a>
                                    <h5>Chef</h5>
                                    <p>Lorem ipsum dolor sit amet et consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                </div>
                            </div>
                            <div class="chef_name">
                                <div class="name_chef_text">
                                    <h3>Suzanne Goin</h3>
                                    <h4>Chef</h4>
                                </div>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="chef_item_inner">
                            <div class="chef_img">
                                <img src="<?php echo base_url("public/assets") ?>/img/chef/chef-2.jpg" alt="">
                                <div class="chef_hover">
                                    <a href="#"><h4>Thomas Keller</h4></a>
                                    <h5>Chef</h5>
                                    <p>Lorem ipsum dolor sit amet et consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                </div>
                            </div>
                            <div class="chef_name">
                                <div class="name_chef_text">
                                    <h3>Suzanne Goin</h3>
                                    <h4>Chef</h4>
                                </div>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="chef_item_inner">
                            <div class="chef_img">
                                <img src="<?php echo base_url("public/assets") ?>/img/chef/chef-3.jpg" alt="">
                                <div class="chef_hover">
                                    <a href="#"><h4>Thomas Keller</h4></a>
                                    <h5>Chef</h5>
                                    <p>Lorem ipsum dolor sit amet et consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                </div>
                            </div>
                            <div class="chef_name">
                                <div class="name_chef_text">
                                    <h3>Paul Bocuse</h3>
                                    <h4>Chef</h4>
                                </div>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="chef_item_inner">
                            <div class="chef_img">
                                <img src="<?php echo base_url("public/assets") ?>/img/chef/chef-4.jpg" alt="">
                                <div class="chef_hover">
                                    <a href="#"><h4>Thomas Keller</h4></a>
                                    <h5>Chef</h5>
                                    <p>Lorem ipsum dolor sit amet et consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                </div>
                            </div>
                            <div class="chef_name">
                                <div class="name_chef_text">
                                    <h3>Giada Deen</h3>
                                    <h4>Chef</h4>
                                </div>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="chef_item_inner">
                            <div class="chef_img">
                                <img src="<?php echo base_url("public/assets") ?>/img/chef/chef-1.jpg" alt="">
                                <div class="chef_hover">
                                    <a href="#"><h4>Thomas Keller</h4></a>
                                    <h5>Chef</h5>
                                    <p>Lorem ipsum dolor sit amet et consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                                </div>
                            </div>
                            <div class="chef_name">
                                <div class="name_chef_text">
                                    <h3>Suzanne Goin</h3>
                                    <h4>Chef</h4>
                                </div>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
        <!--================End Our Chefs Area =================-->
        
        <!--================End Our Chefs Area =================-->
        <section class="next_event_area">
            <div class="container">
                <div class="s_white_red_title">
                    <h3>our</h3>
                    <h2>HAPPY GUESTS SAY</h2>
                </div>
                <div class="next_event_slider">
                   
                    <div class="item">
                    <div class=" col-sm-6">
                            <div class="service_item right_event_text">
                                <h3>Mardi Mad</h3>
                                <p>Nice place also.. The food was great...<br>we just choose n take. Staffs are very friendly</a></p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="service_item right_event_text">
                                <h3>NorKhalida Syafira</h3>
                                <p>Wide selection of meat and seafood. <br>Table is abit small and update the facebook page</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                    <div class=" col-sm-6">
                            <div class="service_item right_event_text">
                                <h3>Mardi Mad</h3>
                                <p>Nice place also.. The food was great...<br>we just choose n take. Staffs are very friendly</a></p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="service_item right_event_text">
                                <h3>NorKhalida Syafira</h3>
                                <p>Wide selection of meat and seafood.<br> Table is abit small and update the facebook page</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
      
       