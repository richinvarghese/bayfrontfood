<!--================Start of Footer Area =================-->
<footer class="footer_area">
            <div class="footer_widget_area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <aside class="f_widget about_widget">
                                <div class="f_w_title">
                                    <h4> Bayfront Catering</h4>
                                </div>
                                <p>We provide Catering for Corporate events, Small Group Parties and Home Functions.Prices are affordable and our Curry Chicken is to Die for.
                 <br>Just give us a call for Bayfront Catering.</p>
                                <ul>
                                    <li><a href="https://www.facebook.com/marinabaybbqsteamboatbuffet/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://www.instagram.com/marinabaybbqsteamboat/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-md-3">
                            <aside class="f_widget contact_widget">
                                <div class="f_w_title">
                                    <h4>CONTACT US</h4>
                                </div>
                                <p>Have questions, comments or just want to say hello:</p>
                                <ul>
                                    <li><a href="#"><i class="fa fa-envelope"></i> sales@bayfrontfood.sg</a></li>
                                    <li><a href="#"><i class="fa fa-phone"></i> (+65) 9119 9333</a></li>
                                    <li><a href="#"><i class="fa fa-map-marker"></i> 20 Opal Crescent, Singapore<br /> Singapore ,328415</a></li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-md-3">
                            <aside class="f_widget twitter_widget">
                                <div class="f_w_title">
                                    <h4>TESTIMONIALS</h4>
                                </div>
                                <ul>
                                    <li>
                                        <u><a href="#">Mardi Mad : </a></u> Nice place also.. The food was great...we just choose n take. Staffs are very friendly
                                    </li>
                                    <li>
                                        <u><a href="#">NorKhalida Syafira : </a></u> Wide selection of meat and seafood. Table is abit small and update the facebook page
                                    </li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-md-3">
                            <aside class="f_widget gallery_widget">
                                <div class="f_w_title">
                                    <a href='<?php echo base_url("public/home/gallery")?>' target='_blank'><h4>Our Gallery</h4></a>
                                </div>
                                <ul>
                                    <li><a href="<?php echo base_url("public/home/gallery")?>"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02435-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                    <li><a href="<?php echo base_url("public/home/gallery")?>"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02389-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                    <li><a href="<?php echo base_url("public/home/gallery")?>"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02409-1-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                    <li><a href="<?php echo base_url("public/home/gallery")?>"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02433-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                    <li><a href="<?php echo base_url("public/home/gallery")?>"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02402-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                    <li><a href="<?php echo base_url("public/home/gallery")?>"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02425-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                </ul>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copy_right_area">
                <div class="container">
                    <div class="pull-left">
                        <h5><p>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="http//akstech.com.sg" target="_blank">AKS TECH PTE. LTD.</a>
</p></h5>
                    </div>
                </div>
            </div>
        </footer>
        
        <!--================End of Footer Area =================-->  
        
        <input type = "hidden" value = "<?php echo base_url("public/assets") ?>/img/gallery/upload/" id="imagebase" />
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url("public/assets") ?>/js/jquery-2.1.4.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url("public/assets") ?>/js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="<?php echo base_url("public/assets") ?>/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <!-- Extra plugin js -->
        <script src="<?php echo base_url("public/assets") ?>/vendors/bootstrap-selector/bootstrap-select.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/bootatrap-date-time/bootstrap-datetimepicker.min.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/isotope/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/countdown/jquery.countdown.js"></script>
        <script src="<?php echo base_url("public/assets") ?>/vendors/js-calender/zabuto_calendar.min.js"></script>
        <!--gmaps Js-->
<!--        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>-->
<!--        <script src="js/gmaps.min.js"></script>-->
        
        
<!--        <script src="js/video_player.js"></script>-->
        <script src="<?php echo base_url("public/assets") ?>/js/theme.js"></script>
        <script>
   

   
  
   $('#myModal').on('show.bs.modal', function (event) {
     var button = $(event.relatedTarget); // Button that triggered the modal
     var image = button.data('image'); // Extract info from data-* attributes
     //var company = button.data('company'); // Extract info from data-* attributes
     // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
     // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
     var modal = $(this);
     var base = $('#imagebase').val();
     //alert(base+image);
     $('#image').attr("src",base+image);
     //modal.find('#img').val(image);
     
     //modal.find('#company').val(company);
   });

 
 
     </script>
    </body>
</html>