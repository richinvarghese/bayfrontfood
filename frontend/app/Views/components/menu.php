 <!--================End Footer Area =================-->
 <header class="main_menu_area">
            <nav class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"><img src="<?php echo base_url("public/assets") ?>/img/logo-1.png" alt="" width=200px; height=80px;></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href=<?php echo base_url("public/home") ?>>Home</a></li>
                            <li><a href=<?php echo base_url("public/home/about")?>> About Us</a></li>
                            <li><a href=<?php echo base_url("public/home/gallery")?>> Gallery</a></li>
                            <li><a href=<?php echo base_url("public/home/contact") ?>>Contact US</a></li>
                
                            <li style="display:none"><a href="http://catering.bayfrontfood.sg/frontend_controller/listmenu"><i class="fa fa-bus"></i></a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </header>
        <!--================End Footer Area =================-->
        