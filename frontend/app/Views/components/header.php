<!DOCTYPE html>
<html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="img/express-favicon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo $page_title;?></title>

        <!-- Icon css link -->
        <link href="<?php echo base_url("public/assets") ?>/vendors/material-icon/css/materialdesignicons.min.css" rel="stylesheet">
        <link href="<?php echo base_url("public/assets") ?>/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url("public/assets") ?>/vendors/linears-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo base_url("public/assets") ?>/css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Rev slider css -->
        <link href="<?php echo base_url("public/assets") ?>/vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="<?php echo base_url("public/assets") ?>/vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="<?php echo base_url("public/assets") ?>/vendors/revolution/css/navigation.css" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="<?php echo base_url("public/assets") ?>/vendors/bootstrap-selector/bootstrap-select.css" rel="stylesheet">
        <link href="<?php echo base_url("public/assets") ?>/vendors/bootatrap-date-time/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="<?php echo base_url("public/assets") ?>/vendors/owl-carousel/assets/owl.carousel.css" rel="stylesheet">
        
        <link href="<?php echo base_url("public/assets") ?>/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url("public/assets") ?>/css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
       
        <div id="preloader">
            <div class="loader absolute-center">
                <div class="loader__box"><b class="top"></b></div>
                <div class="loader__box"><b class="top"></b></div>
                <div class="loader__box"><b class="top"></b></div>
            </div>
        </div>
       
        <!--================ Frist hader Area =================-->
        <div class="first_header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="header_contact_details">
                            <a href="#"><i class="fa fa-phone"></i>+ (65) 9119 9333</a>
                            <a href="#"><i class="fa fa-envelope-o"></i>sales@bayfrontfood.sg</a>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="header_social">
                            <ul>
                                <li><a href="https://www.facebook.com/marinabaybbqsteamboatbuffet/"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.instagram.com/marinabaybbqsteamboat/ "><i class="fa fa-instagram"></i></a></li>
                                <!--<li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>-->
                                <a class="event_btn" href=<?php echo base_url("public/home/masterCategoies")?>><i class="fa fa-bus" aria-hidden="true"></i>Order Buffet</a>
                               
                            </ul>
                            
                        </div>
                    </div>
                    
                   <!-- <div class="col-md-4">
                        <div class="event_btn_inner">

                            <a class="event_btn" href="http://catering.bayfrontfood.sg/frontend_controller/listmenu"><i class="fa fa-bus" aria-hidden="true"></i>Order Buffet</a>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
        <!--================End Footer Area =================-->