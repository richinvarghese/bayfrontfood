<!--================Banner Area =================-->
<section class="banner_area">
            <div class="container">
                <div class="banner_content">
                    <h4>Buffet Master Categories</h4>
                    <a href=<?php echo base_url("public/home") ?>>Home</a>
                    <a class="active" href=<?php echo base_url("public/home/masterCategoies")?>>Master Categories</a>
                </div>
            </div>
        </section>
        <section class="most_popular_item_area">
            <div class="container">
                <div class="s_white_title">
                    <h3>Buffet Master </h3>
                    <h2>Categories</h2>
                </div>
                <div class="popular_filter" style="display:none">
                    <ul>
                        <li class="active" data-filter="*"><a href="">All</a></li>
                        <li data-filter=".2019"><a href="">2019</a></li>
                        <li data-filter=".2018"><a href="">2018</a></li>
                        
                    </ul>
                </div>

               



                <div class="p_recype_item_main">
                    <div class="row p_recype_item_active">
                        <?php 
                        $i=0;
                            foreach($mcategory as $v_category):
                                $i++;
                            
                        ?>
                      

                        <div class="col-md-6 break 2019">
                            <div class="media">
                                <div class="media-left">
                                <img src = "<?php echo base_url("public/assets") ?>/img/recype/recype-7.jpg" />
 
                                </div>
                                <div class="media-body">
                                <a  target="_blank" href="http://catering.bayfrontfood.sg/frontend_controller/listmenu/index/<?php echo $v_category->master_cat_id?>"><h3>"<?php echo $v_category->master_cat_name; ?>" </h3></a>
                                    <h4></h4>
                                    <p> <?php  echo strip_tags($v_category->master_cat_descrip);?>
                              
                                </p>
                                <?php if($i%3==0){?>

<ul>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
   
</ul>
<?php  }
else if($i%2==0){
?>
        <ul>
        <li><a href="#"><i class="fa fa-star"></i></a></li>
        <li><a href="#"><i class="fa fa-star"></i></a></li>
        <li><a href="#"><i class="fa fa-star"></i></a></li>
        <li><a href="#"><i class="fa fa-star"></i></a></li>
      
    </ul>
<?php }

else {
?>
    <ul>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
    <li><a href="#"><i class="fa fa-star"></i></a></li>
   
</ul>
<?php }?>
                                </div>
                            </div>
                        </div>
                       
                        <?php 
                        endforeach;
                        ?>
                        
                       
                    </div>
                    
                   
                </div>
            </div>
        </section>
        <!--================End Our feature Area =================-->
        