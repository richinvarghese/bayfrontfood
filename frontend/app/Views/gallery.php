
    
<!--================Banner Area =================-->
       <section class="banner_area">
            <div class="container">
                <div class="banner_content">
                    <h4>Our Gallery</h4>
                    <a href=<?php echo base_url("public/home") ?>>Home</a>
                    <a class="active" href=<?php echo base_url("public/home/gallery") ?>>Gallery</a>
                </div>
            </div>
        </section>
    <!--====End Banner Area =================-->  <!--================Our Gallery Area =================-->
      
  

<!-- The Modal -->

    <section class="most_popular_item_area menu_list_page">
            <div class="container">
                <div class="popular_filter">
                    <ul>
                        <li class="active" data-filter="*" style='margin:5px;'><a href="">All</a></li>

                        <li data-filter=".Christmas" style='margin:5px;'><a href="" >Christmas</a></li>
                        <li data-filter=".MiniCatering" style='margin:5px;'><a href="" > Mini Catering</a></li>
                        <li data-filter=".RegularBuffet" style='margin:5px;'><a href="" >Regular Buffet</a></li>
                        <li data-filter=".PackedMeal" style='margin:5px;'><a href="" >Packed Meal</a></li>

                    </ul>
                </div>
                
                <section class="our_gallery_area">
                    <div class="container">
                        <div class="row our_gallery_ms_inner">
                            <div class="p_recype_item_main">
                                <div class="row p_recype_item_active">
                                    <div class="col-md-4 col-sm-6 break Christmas">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g2.jpg" alt="">
                                            <div class="our_gallery_hover">
                                                <a href="#myModal" data-toggle="modal"  data-image="g2.jpg"><i class="fa fa-link"></i></a>
                                                <a href="#myModal" data-toggle="modal"  data-image="g2.jpg"><h5>Fried Hong Kong Noodle</h5></a>
                                                <p>Fried Hong Kong Noodle 干炒香港面 | 🎄 Christmas </p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-6 break Christmas">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g3.jpg" alt="">
                                            <div class="our_gallery_hover">
                                                <a href="#myModal"  data-toggle="modal"  data-image="g3.jpg"><i class="fa fa-link"></i></a>
                                                <a href="#myModal"  data-toggle="modal"  data-image="g3.jpg" ><h5>Vegetarian Luo Han Zai</h5></a>
                                                <p>Vegetarian Luo Han Zai 罗汉斋 | 🎄 Christmas</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g1.jpg" alt="">
                                            <div class="our_gallery_hover">
                                                <a data-toggle="modal" data-target="#myModal" data-image="g1.jpg"><i class="fa fa-link"></i></a>
                                                <a data-toggle="modal" data-target="#myModal" data-image="g1.jpg"><h5>Sweet and Sour Fish</h5></a>
                                                <p>Sweet and Sour Fish by Bayfront Catering Services </p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-6 break MiniCatering">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g8.jpg" alt="">
                                            <div class="our_gallery_hover">
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g8.jpg"><i class="fa fa-link"></i></a>
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g8.jpg"><h5>Yang Zhou Fried Rice</h5></a>
                                                <p>Yang Zhou Fried Rice 扬州炒饭 | Mini Catering </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break Christmas">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g5.jpg" alt="">
                                            <div class="our_gallery_hover">
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g5.jpg"><i class="fa fa-link"></i></a>
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g5.jpg"><h5>Full Buffet Catering Spread</h5></a>
                                                <p>Full Buffet Catering Spread | 🎄 Christmas</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break RegularBuffet">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g4.jpg" alt="">
                                            <div class="our_gallery_hover">
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g4.jpg"><i class="fa fa-link"></i></a>
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g4.jpg"><h5>Signature Curry Chicken w/ Potato</h5></a>
                                                <p>Signature Curry Chicken w/ Potato 招牌咖喱鸡马铃薯 | Regular Buffet </p>
                                          
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-6 break PackedMeal">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g12.jpg" alt="">
                                            <div class="our_gallery_hover">
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g12.jpg"><i class="fa fa-link"></i></a>
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g12.jpg"><h5>Broccoli w/Mushroom</h5></a>
                                                <p>Broccoli w/Mushroom | Packed Meal</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break RegularBuffet">
                                        <div class="our_gallery_item">
                                            <img id="myImg" src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g7.jpg" alt="">
                                            <div class="our_gallery_hover">
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g7.jpg"><i class="fa fa-link"></i></a>
                                                <a href="#myModal"  data-toggle="modal"  data-image="g7.jpg"><h5>Full Buffet Catering Setup</h5></a>
                                                <p>Full Buffet Catering Setup | Regular Buffet</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break PackedMeal">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g11.jpg" alt="">
                                            <div class="our_gallery_hover">
                                                <a href="#myModal"  data-toggle="modal"  data-image="g11.jpg"><i class="fa fa-link"></i></a>
                                                <a href="#myModal"  data-toggle="modal"  data-image="g11.jpg"><h5>Blackpepper Beef</h5></a>
                                                <p>Blackpepper Beef | Packed Meal</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break MiniCatering">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g9.jpg" alt="">
                                            <div class="our_gallery_hover">
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g9.jpg"><i class="fa fa-link"></i></a>
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g9.jpg"><h5>Thai Fish Cake</h5></a>
                                                <p>Thai Fish Cake 泰式鱼饼 | Mini Catering</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 break PackedMeal">
                                        <div class="our_gallery_item">
                                            <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g10.jpg" alt="">
                                            <div class="our_gallery_hover">
                                                <a  href="#myModal"  data-toggle="modal"  data-image="g10.jpg"><i class="fa fa-link"></i></a>
                                                <a href="#myModal"  data-toggle="modal"  data-image="g10.jpg"><h5>Packed Meal</h5></a>
                                                <p>Packed Meal</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                  <!--  <div class="col-md-4 col-sm-6 break RegularBuffet">
                                            <div class="our_gallery_item">
                                                <img src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g6.jpg" alt="">
                                                <div class="our_gallery_hover">
                                                    <a href="#"><i class="fa fa-link"></i></a>
                                                    <a href="#"><h5>Bossam</h5></a>
                                                    <p> Regular Buffet</p>
                                                </div>
                                            </div>
                                        </div> -->
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>
   <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="display:none">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="myModalLabel">Modal title</h4>

            </div>
            <div class="modal-body">
               
            <img id="image" style="width:100%" src="<?php echo base_url("public/assets") ?>/img/gallery/upload/g2.jpg" />
            </div>
            <div class="modal-footer" style="display:none">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

