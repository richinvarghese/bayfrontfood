  <!--================Banner Area =================-->
  <section class="banner_area">
            <div class="container">
                <div class="banner_content">
                    <h4>Assurance</h4>
                    <a href=<?php echo base_url("public/home") ?>>Home</a>
                    <a class="active" href=<?php echo base_url("public/home/assurance") ?>>Assurance</a>
                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->
        <section class="recent_bloger_area">
            <div class="container">
                <div class="s_black_title">
                    <h3>Our Assurance</h3><BR>
                    <h2>Quality, Taste & Safety. </h2>
                </div>
            </div>
        </section>
        <!--================Service Area =================-->
       <section class="service_area">
            <div class="container">
                
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="service_item">
                            <img src="<?php echo base_url("public/assets") ?>/img/service-icon/service-3.png" alt="">
                            <h3>Food Hygiene</h3>
                            <p>To maintain hygiene at the optimum level, we send food samples for monthly lab tests. Pest control professionals are also engaged for bi-monthly checks.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="service_item">
                            <img src="<?php echo base_url("public/assets") ?>/img/service-icon/service-2.png" alt="">
                            <h3>Logistics Team</h3>
                            <p>Daily cleanliness checks are done on our vehicles before delivery. Our captains are required to wear gloves when they handle food.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="service_item">
                            <img src="<?php echo base_url("public/assets") ?>/img/service-icon/service-1.png" alt="">
                            <h3>Culinary Team</h3>
                            <p>Fresh and quality Seafood items includes Crayfishes, Crabs, Scallops, Prawns, Bamboo Clams etc.
All ingredients and Vegetables are daily prepared.</p>
                        </div>
                    </div>
                   
                </div>
            </div>
        </section>
        <!--================End Service Area =================-->