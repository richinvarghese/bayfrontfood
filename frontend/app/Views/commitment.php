  <!--================Banner Area =================-->
  <section class="banner_area">
            <div class="container">
                <div class="banner_content">
                    <h4>Commitment</h4>
                    <a href=<?php echo base_url("public/home") ?>>Home</a>
                    <a class="active" href=<?php echo base_url("public/home/commitment") ?>>Commitment</a>
                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->
        
        <!--================About Us Content Area =================-->
        <section class="recent_bloger_area">
            <div class="container">
                <div class="s_black_title">
                    <h3>Our Commitment</h3><BR>
                    <h2>make your event unique and memorable </h2>
                </div>
            </div>
        </section>
        <section class="about_us_content">
            <div class="container">
               
                <div class="row about_inner_item">
                    <div class="col-md-6">
                        <div class="about_left_content">
                            <h4>Freshness</h4>
                            <p>To ensure the freshness of our food, we ensure that our process takes only a total of one hour. This includes the preparation of ingredients, to cooking and delivery to your venue.</p>
                            <ul>
                                <li><a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Fresh and quality Seafood items .</a></li>
                                <li><a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> All ingredients and Vegetables are daily prepared.</a></li>
                                <li><a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Catering wares are santitised in hot water before use</a></li>
                            </ul>

                        </div>
                    </div>
                   
                    <div class="col-md-6">
                        <div class="about_right_image">
                            <img src="<?php echo base_url("public/assets") ?>/img/about-item/about-item-2.jpg" alt="">
                        </div>
                    </div>
                    
                </div>
                <div class="row about_inner_item">
                    <div class="col-md-6">
                        <div class="about_right_image">
                            <img src="<?php echo base_url("public/assets") ?>/img/about-item/about-item-3.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="about_left_content">
                            <h4>Excellence</h4>
                            <p>Quality vs Quantity</p>
                            <p>From food ingredients to catering equipment like chafing dish and table skirting, only items of the best quality are curated. This is a tenet we will never compromise on, and one that has won us numerous accolades which has kept us at the forefront of the local food industry.</p>
                        </div>
                    </div>
                   
                </div>
                <div class="row about_inner_item">
                    <div class="col-md-6">
                        <div class="about_left_content">
                            <h4>Presentation:</h4>
                            <p>Top quality in taste and food presentation</p>
                            <p>We eat with our eyes first, so visual presentation of our food is of utmost importance.Each dish goes through a visual check on its arrangement and quantity before it leaves the kitchen</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="about_right_image">
                            <img src="<?php echo base_url("public/assets") ?>/img/about-item/about-item-4.jpg" alt="">
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!--================End About Us Content Area =================-->
        