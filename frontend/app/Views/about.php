  <!--================Banner Area =================-->
  <section class="banner_area">
            <div class="container">
                <div class="banner_content">
                    <h4>About Us</h4>
                    <a href=<?php echo base_url("public/home") ?>>Home</a>
                    <a class="active" href=<?php echo base_url("public/home/about") ?>>About Us</a>
                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->
        
        <!--================About Us Content Area =================-->
        <section class="about_us_content">
            <div class="container">
                <div class="row about_inner_item">
                    <div class="col-md-6">
                        <div class="about_left_content">
                            <h1 style="font-family:Bradley Hand ITC"><b>WHO WE ARE</b>&nbsp;<i class="fa fa-question " aria-hidden="true"></i></h1>
                            <p>
                            <span style="color:maroon; font-size:20px;">BayFront’s Humble Beginnings..<br></span>
                                <span style="color:maroon;">BAYFRONT CATERING SERVICES·FRIDAY, DECEMBER 6, 2019· </span> 
                            </p>
                            <p>   <span style="color:maroon; font-size:30px;justify-content:initial">O</span>ur Company came into existence in 2012 when a serial restaurateur found the lack of Quality in catering companies. Emboldened by his vision of quality food for functions he added on  this as parts of his portfolio. Since then the company has expanded rapidly with orders from big corporate clients such as DBS and Aviva.  
                                   <br/> Named after his famous Seafood Restaurant at MBS, Bayfront Catering Services is at the forefront of providing Asian Fusion Cuisine for your specific needs. We provide an array of dishes with our Islandwide famous Curry Chicken & Seafood Spread.
                            <span style="color:maroon; font-size:30px">C</span>all me now for a unforgettable experience. 
                            <span style="color:maroon; font-size:20px">CEO, Nicholas</span><br><br>
                            <a href="http://home.bayfrontfood.sg/public/home/contact"><span style="color:maroon; font-size:20px">Visit us now!! Our dedicated and friendly staffs are all ready to serve you with a smile <span style='font-size:20px;'>&#128522;</span></a></span></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="about_right_image">
                            <img src="<?php echo base_url("public/assets") ?>/img/about-item/about-item-1.jpg" alt  height="500" width="570" >
                        </div>
                    </div>
                    
                </div>
                
               <!-- <div class="row about_inner_item">
                    <div class="col-md-6">
                        <div class="about_right_image">
                            <img src="<?php echo base_url("public/assets") ?>/img/about-item/about-item-2.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="about_left_content">
                            <h4>About Our Menu</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typeting, remaining essentially unchanged.</p>
                            <ul>
                                <li><a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                                <li><a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> The standard chunk of Lorem Ipsum used.</a></li>
                                <li><a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Sed ut perspiciatis unde omnis iste natus error.</a></li>
                                <li><a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> At vero eos et accusamus et iusto odio dignissimos.</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="about_single_content">
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sture there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>
                        </div>
                    </div>
                </div>
                <div class="row about_inner_item">
                    <div class="col-md-6">
                        <div class="about_left_content">
                            <h4>Best Services</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typeting, remaining essentially unchanged.</p>
                            <ul>
                                <li><a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                                <li><a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> The standard chunk of Lorem Ipsum used.</a></li>
                                <li><a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Sed ut perspiciatis unde omnis iste natus error.</a></li>
                                <li><a href="#"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> At vero eos et accusamus et iusto odio dignissimos.</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="about_right_image">
                            <img src="<?php echo base_url("public/assets")?>/img/about-item/about-item-3.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="about_single_content">
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sture there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>
                        </div>
                    </div>
                </div>
                -->
                <!--================Service Area =================-->
   
        <!--================End Service Area =================-->
            </div>
        </section>
        <!--================End About Us Content Area =================-->
        