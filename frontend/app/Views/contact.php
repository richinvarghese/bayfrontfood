
        
        <!--================Banner Area =================-->
        <section class="banner_area">
            <div class="container">
                <div class="banner_content">
                    <h4>Contact Us</h4>
                    <a href=<?php echo base_url("public/home") ?>>Home</a>
                    <a class="active" href=<?php echo base_url("public/home/contact") ?>>Contact Us</a>
                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->
        
        <!--================Contact Area =================-->
        <section class="contact_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="contact_details">
                            <h3 class="contact_title">Contact Info</h3>
                            <p>
                            Here are a few ways to get in touch with us</p>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="media-body">
                                    <h4>Address</h4>
                                    <h5>
                                        20 Opal Crescent,
                                        Singapore</h5>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="media-body">
                                    <h4>Phone</h4>
                                    <h5>+65 9119 9333</h5>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <div class="media-body">
                                    <h4>Email</h4>
                                    <h5>sales@bayfrontfood.sg</h5>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <div class="media-body">
                                    <h4>Opening Times</h4>
                                    <h5>Monday-Friday 9am - 6pm | Saturday 10am - 6pm</h5>                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row contact_form_area">
                            <h3 class="contact_title">Send Message</h3>
                            <form action="contact_process.php" method="post" id="contactForm">
                                <div class="form-group col-md-12">
                                  <input type="text" class="form-control" id="name" name="name" placeholder="First Name*">
                                </div>
                                <div class="form-group col-md-12">
                                  <input type="text" class="form-control" id="last" name="last" placeholder="Last Name*">
                                </div>
                               
                                <div class="form-group col-md-12">
                                  <input type="email" class="form-control" id="email" name="email" placeholder="Your Email*">
                                </div>
                                 <div class="form-group col-md-12">
                                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
                                </div>
                                <div class="form-group col-md-12">
                                  <textarea class="form-control" id="message" name="message" placeholder="Write Message"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <button class="btn btn-default submit_btn" type="submit">Send Message</button>
                                 </div>
                            </form>
                            <div id="success">
                                <p>Your text message sent successfully!</p>
                            </div>
                            <div id="error">
                                <p>Sorry! Message not sent. Something went wrong!!</p>
                            </div>
                        </div>
                    </div>
                </div>
                </section><section>
            <div class="col-md-12">
                    
                    <div class="row map_contact">

                        <div class="map_area">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.746675275808!2d103.86498771475395!3d1.3279944990314325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da177f91777ce5%3A0xca047720cd1c164c!2s20%20Opal%20Cres%2C%20Singapore%20328415!5e0!3m2!1sen!2sin!4v1574970931758!5m2!1sen!2sin" width="100%" height="100%" frameborder="10" style="border:0;" allowfullscreen=""></iframe>

                        </div>
                    </div>
                   
                </div>
        </section>
        <!--================End Contact Area =================-->
        
                    
               