<?php namespace App\Controllers;

use App\Models\UserModel;

class Home extends BaseController
{
	public function index()
	{ 
		$data = ['page_title' => 'Bayfront Catering Services'];
		$db = \Config\Database::connect('default');
		$mcategory = $db->table('master_category');
		$query = $mcategory->get();
		$result['mcategory'] = $query->getResult();
		//print_r($result);die();
		$db->close();
		echo view('components/header', $data);
		echo view('components/menu');
		echo view('home', $result);
		echo view('components/footer');

		//return view('home',$data);
	}
	public function about()
	{ 
		$data = ['page_title' => 'About us - Bayfront Catering Services'];

		echo view('components/header',$data);
		echo view('components/menu');
		echo view('about');
		echo view('components/footer');
		//return view('home',$data);
	}
	public function contact()
	{ 
		$data = ['page_title' => 'Contact - Bayfront Catering Services'];

		echo view('components/header',$data);
		echo view('components/menu');
		echo view('contact');
		echo view('components/footer');
		//return view('home',$data);
	}
	public function gallery()
	{ 
		$data = ['page_title' => 'Gallery - Bayfront Catering Services'];

		echo view('components/header',$data);
		echo view('components/menu');
		echo view('gallery');
		echo view('components/footer');
		//return view('home',$data);
	}
	public function masterCategoies()
	{ 
		$data = ['page_title' => 'Master Categoies - Bayfront Catering Services'];
		$db = \Config\Database::connect('default');
		$mcategory = $db->table('master_category');
		$query = $mcategory->get();
		$result['mcategory'] = $query->getResult();
		//print_r($result);die();
		$db->close();
		echo view('components/header',$data);
		echo view('components/menu');
		echo view('master_categoies', $result);
		echo view('components/footer');
		//return view('home',$data);
	}
	public function assurance()
	{ 
		$data = ['page_title' => 'Assurance - Bayfront Catering Services'];

		echo view('components/header',$data);
		echo view('components/menu');
		echo view('assurance');
		echo view('components/footer');
		//return view('home',$data);
	}
	public function commitment()
	{ 
		$data = ['page_title' => 'Commitment-Bayfront Catering Services'];

		echo view('components/header',$data);
		echo view('components/menu');
		echo view('commitment');
		echo view('components/footer');
		//return view('home',$data);
	}
	public function test()
	{$data = ['page_title' => 'Gallery - Bayfront Catering Services'];

		echo view('components/header',$data);
		echo view('components/menu');
		echo view('welcome_message');
		echo view('components/footer');
	}

	//--------------------------------------------------------------------

}
