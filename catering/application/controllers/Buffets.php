<?php
defined('BASEPATH')OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Buffets extends CI_Controller{
    
    public function __construct(){
        
        parent::__construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Manage Buffets';
        
        $user_id=$_SESSION['user_id'];
        
        //Get Details from Database
        $this->load->model('Buffetsmodel');
        $return_buffets=$this->Buffetsmodel->get_buffets();
        $data['buffets']=$return_buffets->result();
        
        //Get Master Categories
        $this->load->model('Buffetsmodel');
        $data['master_categories']=$this->Buffetsmodel->get_master_categories()->result();
        
        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('manage_buffets', $data);
            $this->load->view('components/footer'); 
        }
        else{
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('manage_buffets', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function add_master_category(){
        
        
        $master_cat = array(
            
            'master_cat_name' => $_POST['master_cat_name'],
            'master_cat_descrip' => $_POST['master_cat_descrip'],
            'master_cat_status' => '1',
        );
        
        $this->form_validation->set_rules('master_cat_name', 'Master Category Name', 'required|is_unique[master_category.master_cat_name]');
        $this->form_validation->set_rules('master_cat_descrip', 'Master Category Description', 'required');
        
        //Image Upload
        $master_cat_image=$this->master_cat_display_upload();
        $master_cat['master_cat_image'] = $master_cat_image['upload_data']['file_name'];
        
        if($this->form_validation->run() == TRUE){
            
            //Update on Database
            $this->load->model('Buffetsmodel');
            $return=$this->Buffetsmodel->add_master_category($master_cat);
            
            if($return == TRUE){
                
                $this->session->set_flashdata('master-category-success', 'Successfully created Master Category');
                redirect('buffets/index');
            }
            else{
                
                $this->session->set_flashdata('master-category-error', 'Error occurred while Creating New Master Category. Please redo ..!');
                redirect('buffets/index');
            }
            
        }
        else{
            
            $errors = validation_errors();
            $this->session->set_flashdata('master-category-error', $errors);
            redirect('buffets/index');
        }
    }
    
    private function master_cat_display_upload(){
        
        $config['upload_path']          = 'assets/images/master_cat';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1024;
        $config['max_width']            = 300;
        $config['max_height']           = 300;
        $config['file_name']            = $_POST['master_cat_name'];
        $config['overwrite']            = TRUE;
        
        $this->load->library('upload', $config);
        
        if(!$this->upload->do_upload('master_cat_image')){
            $errors=$this->upload->display_errors();
            $this->session->set_flashdata('master-category-error', $errors);
            redirect('buffets/index');
            
        }
        else{
            $file_data = array('upload_data' => $this->upload->data());
            return $file_data;
            
        }
    }
    
    public function edit_master_category(){
        
        $this->form_validation->set_rules('master_cat_name', 'Category Name', 'required');
        $this->form_validation->set_rules('master_cat_descrip', 'Description', 'required');
        
        if($this->form_validation->run() == TRUE){
            
            $master_cat = array(
            
            'master_cat_id' => $_POST['master_cat_id'],
            'master_cat_name' => $_POST['master_cat_name'],
            'master_cat_descrip' => $_POST['master_cat_descrip'],
            );
            
            //Update Database
            $this->load->model('Buffetsmodel');
            $return=$this->Buffetsmodel->update_master_category($master_cat);
            
            if($return == TRUE){
                
                $this->session->set_flashdata('master-category-success', 'Successfully updated Master Category Details');
                redirect('buffets/index');
            }
            else{
                
                $this->session->set_flashdata('master-category-error', 'Error occurred while updating. Please redo..');
                redirect('buffets/index');
            }
        }
        else{
            
            $errors = validation_errors();
            $this->session->set_flashdata('master-category-error', $errors);
            redirect('buffets/index');
        }
        
    }
    
    public function edit_master_cat_image(){
        
        $master_cat = array(
            
            'master_cat_id' => $_POST['master_cat_id'],
            );

        $master_cat_image=$this->master_cat_edit_image($master_cat);
        $master_cat['master_cat_image'] =$master_cat_image['upload_data']['file_name'];

        //Update Database
        $this->load->model('Buffetsmodel');
        $return=$this->Buffetsmodel->update_master_cat_image($master_cat);

        if($return == TRUE){

            $this->session->set_flashdata('master-category-success', 'Successfully updated Master Category Details');
            redirect('buffets/index');
        }
        else{

            $this->session->set_flashdata('master-category-error', 'Error occurred while updating. Please redo..');
            redirect('buffets/index');
        }
        
    }
    
    private function master_cat_edit_image($master_cat){
        
        $config['upload_path']          = 'assets/images/master_cat';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1024;
        $config['max_width']            = 300;
        $config['max_height']           = 300;
        $config['file_name']            = $_POST['master_cat_image'];
        $config['overwrite']            = TRUE;
        
        $this->load->library('upload', $config);
        
        if(!$this->upload->do_upload('master_cat_image_file')){
            $errors=$this->upload->display_errors();
            $this->session->set_flashdata('master-category-error', $errors);
            redirect('buffets/index');
            
        }
        else{
            $file_data = array('upload_data' => $this->upload->data());
            return $file_data;
            
        }
    }
    
    
    
    public function delete_master_category(){
        
        $master_cat_id = $_POST['master_cat_delete_id'];
        
        //Check Usage
        $this->load->model('Buffetsmodel');
        $return=$this->Buffetsmodel->check_master_cat($master_cat_id);
        
        if($return->num_rows()=='0'){
            
            //Delete from Database
            $this->load->model('Buffetsmodel');
            $delete_return=$this->Buffetsmodel->delete_master_category($master_cat_id);
            
            if($delete_return == TRUE){
                
                $this->session->set_flashdata('master-category-success', 'Successfully Deleted the Master Category');
                redirect('buffets/index');
            }
            else{
                
                $this->session->set_flashdata('master-category-error', 'Error occurred while deleting the Category. Please redo.');
                redirect('buffets/index');
            }
        }
        else{
            
            $this->session->set_flashdata('master-category-error', 'This Category already in use. Cant be Deleted');
            redirect('buffets/index');
        }
    }
    
    public function add_buffet(){
        
        $buffet = array(
            
            'buffet_name' => $_POST['buffet_name'],
            'master_cat_id' => $_POST['master_cat_id'],
            'buffet_min_pax' => $_POST['buffet_min_pax'],
            'buffet_price' => $_POST['buffet_price'],
            'buffet_course_nos' => $_POST['buffet_course_nos'],
        );
        
        $banner=$this->upload_banner();
        $buffet['buffet_banner']=$banner['upload_data']['file_name'];

        //Update Database
        $this->load->model('Buffetsmodel');
        $return_buffet=$this->Buffetsmodel->add_buffet($buffet);
        
        if($return_buffet == TRUE){
            
            $this->session->set_flashdata('buffet-success', 'Successfully added New Buffet');
            redirect('buffets/index');
        }
        else{
            
            $this->session->set_flashdata('buffet-error', 'Error occurred while adding new Buffet. Please redo...!');
            redirect('buffets/index');
        }
    }
    
    private function upload_banner(){
        
        $config['upload_path']          = 'assets/images/menu/banners/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1024;
        $config['max_width']            = 450;
        $config['max_height']           = 150;
        $config['file_name']            = $_POST['buffet_name'];
        $config['overwrite']            = TRUE;
        
        $this->load->library('upload', $config);
        
        if(!$this->upload->do_upload('buffet_banner')){
            $errors=$this->upload->display_errors();
            $this->session->set_flashdata('buffet-error', $errors);
            redirect('buffets/index');
            
        }
        else{
            
            $file_data = array('upload_data' => $this->upload->data());
            return $file_data;
        }
    }
    
    public function edit_buffet(){
        
        $this->form_validation->set_rules('buffet_name', 'Buffet Title', 'required');
        $this->form_validation->set_rules('master_cat_id', 'Master Category', 'required');
        $this->form_validation->set_rules('buffet_min_pax', 'Buffet Minimum Pax', 'required|numeric');
        $this->form_validation->set_rules('buffet_price', 'Buffet Price per Pax', 'required');
        $this->form_validation->set_rules('buffet_course_nos', 'Number of courses', 'required|less_than[10]');
        
        if($this->form_validation->run() == TRUE){
            
            $buffet_id=$_POST['buffet_id'];
        
            $buffet = array(
                
                'master_cat_id' => $_POST['master_cat_id'],
                'buffet_name' => $_POST['buffet_name'],
                'buffet_min_pax' => $_POST['buffet_min_pax'],
                'buffet_price' => $_POST['buffet_price'],
                'buffet_course_nos' => $_POST['buffet_course_nos'],
            );

            //Update Database
            $this->load->model('Buffetsmodel');
            $return_buffet=$this->Buffetsmodel->edit_buffet($buffet, $buffet_id);

            if($return_buffet == TRUE){

                $this->session->set_flashdata('buffet-edit-success', 'Successfully updated Buffet');
                redirect('buffets/index');
            }
            else{

                $this->session->set_flashdata('buffet-edit-error', 'Error occurred while updating Buffet Details. Please redo..!');
                redirect('buffets/index');
            }
        }
        
        else{
            
            $errors=validation_errors();
            $this->session->set_flashdata('buffet-edit-error', $errors);
            redirect('buffets/index');
        }
        
    }
    
    public function edit_banner(){
        
        $buffet_id=$_POST['buffet_id'];
        
        //Get Buffet Details
        $this->load->model('Buffetsmodel');
        $return_buffet=$this->Buffetsmodel->get_buffet($buffet_id);
        $buffet_title=$return_buffet->row()->buffet_name;
        
        $return_banner=$this->update_banner($buffet_title);
        $banner['buffet_banner']=$return_banner['upload_data']['file_name'];
        
        $this->load->model('Buffetsmodel');
        $return_banner=$this->Buffetsmodel->update_banner($banner, $buffet_id);
        
        if($return_banner == TRUE){
            
            $this->session->set_flashdata('buffet-edit-success', 'Successfully updated Banner Image');
            redirect('buffets/index');
        }
        else{
            
            $this->session->set_flashdata('buffet-edit-error', 'Error occurred while updating new Banner. Please redo...!');
            redirect('buffets/index');
        }
        
    }
    
    private function update_banner($buffet_title){
        
        $config['upload_path']          = 'assets/images/menu/banners/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1024;
        $config['max_width']            = 450;
        $config['max_height']           = 150;
        $config['file_name']            = $buffet_title;
        $config['overwrite']            = TRUE;
        
        $this->load->library('upload', $config);
        
        if(!$this->upload->do_upload('buffet_banner')){
            $errors=$this->upload->display_errors();
            $this->session->set_flashdata('buffet-error', $errors);
            redirect('buffets/index');
            
        }
        else{
            
            $file_data = array('upload_data' => $this->upload->data());
            return $file_data;
        }
    }
    
    public function delete_buffet(){
        
        $buffet_id=$_POST['buffet_id'];
        
        $this->load->model('Buffetsmodel');
        $return_buffet=$this->Buffetsmodel->delete_buffet($buffet_id);
        
        if($return_buffet == TRUE){
            
            $this->session->set_flashdata('buffet-edit-success', 'Successfully Deleted Buffet Menu');
            redirect('buffets/index');
        }
        else{
            
            $this->session->set_flashdata('buffet-edit-error', 'Error occurred while delete Buffet Menu');
            redirect('buffets/index');
        }
    }
    
}

