<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Notifications extends CI_Controller{
    
    public function ajax_list_notif(){
        
        //Load Notifications on Header
        $this->load->model('Notificationsmodel');
        $notifications=$this->Notificationsmodel->get_notifications()->result();
        
        $x=0;
        foreach($notifications as $notif){
            
            $data[$x] = '<a href='.base_url().'notifications/process_notif/'.$notif->notification_id.'><i class="glyphicon glyphicon-eye-open pull-right"></i>'
                    . '<span class="line"><strong>'.$notif->description.'</strong></span>'
                    . '<span class="line small">'.date('D, Y-M-d H:m:A', strtotime($notif->created_on)).'</span>'
                    . '</a>';
            
            $x++;
        } 
        echo json_encode($data);
    }
    
    public function nos_notif(){
        
        
        //Load Number of Notifications from Database
        $this->load->model('Notificationsmodel');
        $noti_nos=$this->Notificationsmodel->get_notifications()->num_rows();
        echo json_encode($noti_nos);
    }
    
    public function process_notif($notif_id){
        
        $this->load->model('Notificationsmodel');
        $notif=$this->Notificationsmodel->process_notif($notif_id);
        
        
        if($notif->post_id == 0){
            
            //Mark as Seen
            $this->load->model('Notificationsmodel');
            $this->Notificationsmodel->update_notif ($notif_id);

            $redirect_url=base_url().$notif->path;
            redirect($redirect_url); 
        }
        else{
            
            //Mark as Seen
            $this->load->model('Notificationsmodel');
            $this->Notificationsmodel->update_notif($notif_id);
            
            $redirect_url=base_url().$notif->path.'/'.$notif->post_id;
            redirect($redirect_url);
        }
        
        
    }
    
}
