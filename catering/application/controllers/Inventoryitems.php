<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Inventoryitems extends CI_Controller{
    
    public function __construct() {
        
        parent::__construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Inventory Items';
        
        //Get Matric Units
        $this->load->model('Inventorymodel');
        $data['units']=$this->Inventorymodel->get_units();
        
        //Get Inventory Items
        $this->load->model('Inventorymodel');
        $data['items']=$this->Inventorymodel->get_items();

        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('inventory_items', $data);
            $this->load->view('components/footer'); 
        }
        else{
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('inventory_items', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function add_item(){
        
        $this->form_validation->set_rules('item_sku', 'SKU', 'is_unique');
        
        $item = array(
            'item_name' => $_POST['item_name'],
            'item_sku' => $_POST['item_sku'],
            'item_cost_price' => $_POST['item_cost_price'],
            'item_unit' => $_POST['item_unit'],
            'item_min_qty' => $_POST['item_min_qty'],
            'item_status' => '1',
        );
        
        //Update on Database
        $this->load->model('Inventorymodel');
        $item_id = $this->Inventorymodel->add_item($item);
        
        //Generate Barcode
        $this->load->model('Inventorymodel');
        $item=$this->Inventorymodel->get_sku($item_id);
        
        $item_sku = $item->item_sku;
        $item_name = $item->item_name; //Unable to render name on barcode
        
        $this->set_barcode($item_sku);
        
        $inventory = array(
            'item_id' => $item_id,
            'item_quantity' => '0',
            'location_id' => '0',
            'item_total_worth' => '0',
            );
            
        //Update on Inventory Database
        $this->load->model('Inventorymodel');
        $status=$this->Inventorymodel->add_inventory($inventory);
        
        
        if($status == TRUE){
            
            $this->session->set_flashdata('item-success', 'Successfully Added Unit');
            redirect('inventoryitems/index');
        }
        else{
            
            $this->session->set_flashdata('item-error', 'Error occurred while updating Unit. Please redo !');
            redirect('inventoryitems/index');
        }
    }
    
    public function edit_item(){
        
        $item_id = $_POST['item_id'];
        
        $item = array(
            
            'item_name' => $_POST['item_name'],
            'item_sku' => $_POST['item_sku'],
            'item_cost_price' => $_POST['item_cost_price'],
            'item_unit' => $_POST['item_unit'],
            'item_min_qty' => $_POST['item_min_qty'],
            'item_status' => $_POST['item_status']
        );
        
        
        //Update on Database
        $this->load->model('Inventorymodel');
        $status=$this->Inventorymodel->edit_item($item_id, $item);
        
        if($status == TRUE){
            
            $this->session->set_flashdata('item-edit-success', 'Successfully Updated Item Details');
            redirect('inventoryitems/index');
        }
        else{
            
            $this->session->set_flashdata('item-edit-error', 'Error occurred while updating Item Details. Please redo !');
            redirect('inventoryitems/index');
        }
    }
    
    public function delete_item(){
        
        $item_id = $_POST['item_id'];
        
        //Delete fom Database
        $this->load->model('Inventorymodel');
        $status=$this->Inventorymodel->delete_item($item_id);
        
        //Delete From Inventory
        $this->load->model('Inventorymodel');
        $this->Inventorymodel->delete_inventory_item($item_id);
        
        if($status == TRUE){
            
            $this->session->set_flashdata('item-edit-success', 'Successfully Deleted Item');
            redirect('inventoryitems/index');
        }
        
        else{
            
            $this->session->set_flashdata('item-edit-error', 'Error occurred while Deleting Item. Please redo !');
            redirect('inventoryitems/index');
        }
    }
	
    private function set_barcode($code){
        
        //load library
        $this->load->library('zend');
        //load in folder Zend
        $this->zend->load('Zend/Barcode');
        //generate barcode
        $file = Zend_Barcode::draw('code128', 'image', array('text'=>$code), array());

        $barcodeRealPath = FCPATH. '/assets/images/barcodes/'.$code.'.png';
        $barcodePath = FCPATH.'/assets/images/barcodes/';

        header('Content-Type: image/png');
        $store_image = imagepng($file,$barcodeRealPath);
        //return $barcodePath.$code.'.png';
        return TRUE;
    }
    
    public function print_barcodes($item_sku){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Print Barcodes';
        
        $data['item_sku'] = $item_sku;
        $data['nos_copies'] = $_POST['nos_copies'];
        
        $this->load->view('print_barcodes', $data);
    }
    
}

