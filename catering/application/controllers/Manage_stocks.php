<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Manage_stocks extends CI_Controller{
    
    public function __construct() {
        
        parent::__construct();
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        date_default_timezone_set($data['settings']->entity_timezone);
        
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Draw Stocks';
        
        //Get Order List
        $this->load->model('Stocksmodel');
        $data['orders']=$this->Stocksmodel->get_orders();
        
        //Get Inventory Items
        $this->load->model('Stocksmodel');
        $data['inventory_items']=$this->Stocksmodel->get_inventory_items();
        
        //Get Users List
        $this->load->model('Stocksmodel');
        $data['users']=$this->Stocksmodel->get_users();
        
        //Get Drawn Item List
        $this->load->model('Stocksmodel');
        $data['draws'] = $this->Stocksmodel->get_drawn_inventory();

        
        $i=0;
        foreach($data['draws'] as $draws){
            
            $item_id = $draws->item_id;
            $location_id = $draws->location_id;
            $order_id = $draws->order_id;
            $user_id = $draws->user_id;
            
            //Get Item Details
            $this->load->model('Stocksmodel');
            $data['draws'][$i]->item=$this->Stocksmodel->get_item($item_id);
            
            //Get Location Details
            $this->load->model('Stocksmodel');
            $data['draws'][$i]->location=$this->Stocksmodel->get_location($location_id);
            
            //Get Order Details
            $this->load->model('Stocksmodel');
            $data['draws'][$i]->order=$this->Stocksmodel->get_order($order_id);
            
            //Get User Details
            $this->load->model('Stocksmodel');
            $data['draws'][$i]->user=$this->Stocksmodel->get_user($user_id);
            
            $i++;
        };
        
        if($_SESSION['user_type']==1){

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('draw_stocks', $data);
            $this->load->view('components/footer');
        }
        else{

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('draw_stocks', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function ajax_location(){
        
        $item_id = $this->input->post('item_id');
        
        $this->load->model('Stocksmodel');
        $inventory = $this->Stocksmodel->get_items($item_id);
        
        $x=0;
        foreach($inventory as $invent){
            
            $location_id = $invent->location_id;
            $this->load->model('Stocksmodel');
            $inventory[$x]->location=$this->Stocksmodel->get_location($location_id);
            $x++;
        }
            
        $pro_select_box ='';
        foreach($inventory as $inven){
            $pro_select_box .= '<option value="'.$inven->location->location_id.'">'.$inven->location->location_name.'-'.$inven->location->location_stack.'</option>';
        }
        echo json_encode($pro_select_box);
    }
    
    public function draw_item(){
        
        
        $user_id = $_POST['user_id'];
        
        //Get User Details
        $this->load->model('Stocksmodel');
        $user_fullname=$this->Stocksmodel->get_user($user_id)->user_fullname;

        $drawn = array(
            
            'item_id' => $_POST['item_id'],
            'location_id' => $_POST['location_id'],
            'item_quantity' => $_POST['item_quantity'],
            'order_id' => $_POST['order_id'],
            'user_id' => $_POST['user_id'],
            'user_fullname' => $user_fullname,
            'drawn_date' => date('Y-m-d'),
        );
        
        $item_id = $_POST['item_id'];
        $location_id = $_POST['location_id'];
        
        
        //Get Inventory ID from  Database
        $this->load->model('Stocksmodel');
        $inventorydb = $this->Stocksmodel->get_inventorydb($item_id, $location_id);
        
        if($inventorydb->item_quantity < $drawn['item_quantity']){
            
            $this->session->set_flashdata('draw-error', 'Items left in the location is less than Requested Quantity');
            redirect('manage_stocks/index');
        }
        
        elseif($drawn['item_quantity'] <= '0'){
            
            $this->session->set_flashdata('draw-error', 'Min Quantity request has to be greater than "0"');
            redirect('manage_stocks/index');
        }
        
        elseif($inventorydb->item_quantity == $drawn['item_quantity']){
            
            $inventory_id = $inventorydb->inventory_id;
            
            $inventory = array(
                
                'item_quantity' => '0',
                'item_location' => '0',
                'item_total_worth' => '0'
            );
            
            //Update on Inventory Table
            $this->load->model('Stocksmodel');
            $status = $this->Stocksmodel->adjust_quantity($inventory_id, $inventory);
            
            if($status == TRUE){
                
                //Update on Inventory Draw Table
                $this->load->model('Stocksmodel');
                $this->Stocksmodel->add_draw_inventory($drawn);
                
                /********************************Item Draw To Notification Table Start****************************************/
        
                $notification=array(

                    'description' => 'Stock Drawn Request has been Placed',
                    'path' => 'manage_stocks/index',
                    'post_id'=> 'NULL',
                    'created_on' => date('Y-m-d H:i:s'),
                    'status' => '0',
                );

                //Update on Database
                $this->load->model('Notificationsmodel');
                $this->Notificationsmodel->add_notification($notification);


                /********************************Item Draw To Notification Table End****************************************/
                
                $this->session->set_flashdata('draw-success', 'Successfully updated Inventory as of Drawn Quantity');
                redirect('manage_stocks/index');
            }
           
        }
        
        else{
            
            $inventory_id = $inventorydb->inventory_id;
            
            //Get item Unit Price
            $this->load->model('Stocksmodel');
            $item=$this->Stocksmodel->get_item($item_id);
            
            $inventory = array(
                
                'item_quantity' => $inventorydb->item_quantity - $drawn['item_quantity'],
                'item_total_worth' => $inventorydb->item_total_worth - ($drawn['item_quantity'] * $item->item_cost_price),
            );
            
            //Update on Inventory Table
            $this->load->model('Stocksmodel');
            $status = $this->Stocksmodel->adjust_quantity($inventory_id, $inventory);
            
            if($status == TRUE){
                
                //Update on Inventory Draw Table
                $this->load->model('Stocksmodel');
                $this->Stocksmodel->add_draw_inventory($drawn);
                
                /********************************Item Draw To Notification Table Start****************************************/
        
                $notification=array(

                    'description' => 'Stock Drawn Request has been Placed',
                    'path' => 'manage_stocks/index',
                    'post_id'=> 'NULL',
                    'created_on' => date('Y-m-d H:i:s'),
                    'status' => '0',
                );

                //Update on Database
                $this->load->model('Notificationsmodel');
                $this->Notificationsmodel->add_notification($notification);


                /********************************Item Draw To Notification Table End****************************************/
                
                $this->session->set_flashdata('draw-success', 'Successfully updated Inventory as of Drawn Quantity');
                redirect('manage_stocks/index');
            }
            
        }
    }
    
    public function amend_order(){
        
        $order_id = $_POST['order_id'];
        $drawn_id = $_POST['drawn_id'];
        
        //Update on Database
        $this->load->model('Stocksmodel');
        $status=$this->Stocksmodel->amend_order($drawn_id, $order_id);
        
        if($status==TRUE){
            
            $this->session->set_flashdata('drawn-edit-success', 'Successfully Ammended Order');
            redirect('manage_stocks/index');
        }
        else{
            
            $this->session->set_flashdata('drawn-edit-error', 'Error occurred while amending Order. Please redo !');
            redirect('manage_stocks/index');
        }
    }
}
