<?php
defined('BASEPATH')OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Items extends CI_Controller{
    
    public function __construct(){
        
        parent::__construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Manage Items';
        
        //Get Items
        $this->load->model('Itemsmodel');
        $return_item_list=$this->Itemsmodel->get_items();
        $data['item_list']=$return_item_list->result();
        
        //Get Categories
        $this->load->model('Categorymodel');
        $return_categories=$this->Categorymodel->get_categories();
        $data['cats_rows']=$return_categories->num_rows();
        $data['categories']=$return_categories->result();
        
        if(isset($_POST['item_submit'])){
            
            $item=array(
                'item_eng' => $_POST['item_eng'],
                'item_chn' => $_POST['item_chn'],
                'item_price' => $_POST['item_price'],
                'category_id' => $_POST['category_id']
            );

            
            //Image Upload
            $item_image=$this->upload_item_image();
            $item['item_image']=$item_image['upload_data']['file_name'];
            
            //Update Database
            $this->load->model('Itemsmodel');
            $return_item=$this->Itemsmodel->add_item($item);
            
            if($return_item == TRUE){
                
                $this->session->set_flashdata('item-success', 'Successfully added Item');
                redirect('items/index');
            }
            
            else{
                
                $this->session->set_flashdata('item-error', 'Error occurred while adding Item. Please redo !');
                redirect('items/index');
            }
        }
        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('manage_items', $data);
            $this->load->view('components/footer'); 
        }
        else{
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('manage_items', $data);
            $this->load->view('components/footer');
        }
    }
    
    private function upload_item_image(){
        
        $config['upload_path']          = 'assets/images/menu/items/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1024;
        $config['max_width']            = 300;
        $config['max_height']           = 300;
        $config['file_name']            = $_POST['item_eng'];
        $config['overwrite']            = TRUE;
        
        $this->load->library('upload', $config);
        
        if(!$this->upload->do_upload('item_image')){
            $errors=$this->upload->display_errors();
            $this->session->set_flashdata('item-error', $errors);
            redirect('items/index');
            
        }
        else{
            $file_data = array('upload_data' => $this->upload->data());
            return $file_data;
            
        }
    }
    
    public function item_edit(){
        
        $this->form_validation->set_rules('item_eng', 'Item Name (English)', 'required');
        $this->form_validation->set_rules('item_chn', 'Item Name (Chinese)', 'required');
        $this->form_validation->set_rules('item_price', 'Item Price', 'required');
        $this->form_validation->set_rules('category_id', 'Category', 'required');
        
        if($this->form_validation->run() == TRUE){
            
            $item_id=$_POST['item_id'];
        
            $item = array(
                'item_eng' => $_POST['item_eng'],
                'item_chn' => $_POST['item_chn'],
                'item_price' => $_POST['item_price'],
                'category_id' => $_POST['category_id']
            );
            
            //Update Database
            $this->load->model('Itemsmodel');
            $return_item_edit=$this->Itemsmodel->item_edit($item, $item_id);

            if($return_item_edit == TRUE){

                $this->session->set_flashdata('item-edit-success', 'Successfully updated item');
                redirect('items/index');
            }
            else{

                $this->session->set_flashdata('item-edit-error', 'Error occurred while updating Item. Please redo..!');
                redirect('items/index');
            }
            
        }
        else{
            
            $errors=validation_errors();
            $this->session->set_flashdata('item-edit-error', $errors);
            redirect('items/index');
        }
        
    }
    
    public function item_edit_image(){
        

        $item_id=$_POST['item_id'];

        //Get Item Details
        $this->load->model('Itemsmodel');
        $return_item=$this->Itemsmodel->get_item($item_id);

        $item_image=$return_item->row()->item_image;

        $image['item_image']=$this->update_item_image($item_image)['upload_data']['file_name'];

        //Update Database
        $this->load->model('Itemsmodel');
        $return_image=$this->Itemsmodel->update_image_name($image, $item_id);

        if($return_image == TRUE){

            $this->session->set_flashdata('item-edit-success', 'Successfully updated Image');
            redirect('items/index');
        }
        else{

            $this->session->set_flashdata('item-edit-error', 'Error occurred while updating Image. Please redo..!');
            redirect('items/index');
        }
        
    }
    
    private function update_item_image($item_image){
        
        $config['upload_path']          = 'assets/images/menu/items/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1024;
        $config['max_width']            = 300;
        $config['max_height']           = 300;
        $config['file_name']            = $item_image;
        $config['overwrite']            = TRUE;
        
        $this->load->library('upload', $config);
        
        if(!$this->upload->do_upload('item_image')){
            $errors=$this->upload->display_errors();
            $this->session->set_flashdata('item-edit-error', $errors);
            redirect('items/index');
            
        }
        else{
            
            $file_data = array('upload_data' => $this->upload->data());
            return $file_data;
        }
    }
    
    public function delete_item(){
        
        $item_id=$_POST['item_delete'];
        
        //Update Database
        $this->load->model('Itemsmodel');
        $return_delete=$this->Itemsmodel->delete_item($item_id);
        
        if($return_delete == TRUE){
            
            $this->session->set_flashdata('item-edit-success', 'Successfully Deleted the Item');
            redirect('items/index');
        }
        else{
            
            $this->session->set_flashdata('item-edit-error', 'Error occurred while deleting the Item. Please redo..!');
            redirect('items/index');
        }
    }
    
    
}

