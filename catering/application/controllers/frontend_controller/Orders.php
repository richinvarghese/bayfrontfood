<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Orders extends CI_Controller{
    
    public function __construct(){
        
        parent::__construct();
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        date_default_timezone_set($data['settings']->entity_timezone);
    }
    
    public function confirm_order($buffet_id){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Order Confirmation';
        
        //Get Buffet Details
        $this->load->model('Ordermodel');
        $data['buffet']=$this->Ordermodel->get_buffet($buffet_id);
        
        //Get Course Details
        $this->load->model('Ordermodel');
        $courses=$this->Ordermodel->get_courses($buffet_id);
        $data['buffet']->courses=$courses;
        
        //Addon Details from Form Section
        $addon_ids = $_POST['addon_id'];
        $addon_pax = $_POST['addon_pax'];

        //print_r($data['nos_addons']);
        //die();
        
        $a=0;
        foreach($addon_ids as $addon_id){
            
            //Get Item Details from Database
            $this->load->model('Ordermodel');
            $addon_item=$this->Ordermodel->get_addon_item($addon_id);
            
            //$x=0;
            if($addon_pax[$a]==NULL){
                $addon_item->pax = 0;
                $order_addon[$a] = $addon_item;
                $a++;
            }
            else{
            $addon_item->pax = $addon_pax[$a];
            $order_addon[$a] = $addon_item;
            $a++;
            }
        }
            
        //print_r($data['total_null']);
        //die();
        /********************************Order Table Fields Calculations Start Here****************************************/
        
        //Buffet Price
        $buffet_std_price = $data['buffet']->buffet_price;
        $buffet_pax = $_POST['order_pax'];
        $buffet_std_total = ($buffet_std_price * $buffet_pax);
        
        //Addons Price
        $addon_total_price = 0;
        foreach($order_addon as $addon){
            
            $addon_pax = $addon->pax;
            $addon_price = $addon->addon_price;
            
            $addon_total_price = $addon_total_price + ($addon_pax * $addon_price);
        }
        
        //ORDER SUBTOTAL
        $order_stotal = ($buffet_std_total + $addon_total_price);
        
        //ORDER DISCOUNT
        $order_discount = 0;
        
       //ORDER GST
        if($data['settings']->entity_gst == 0){
            $order_gst =0;
        }
        
        else{
            $order_gst = ($order_stotal + $order_discount) *(7/100);
        }
        
        //ORDER GRAND TOTAL
        $order_gtotal = $order_stotal + $order_discount + $order_gst;
        
        /********************************Order Table Fields Calculations End Here****************************************/  

        
        
        /********************************Order Table Start****************************************/  
        $order_data = array(

            'order_buffet' => $data['buffet']->buffet_name,
            'order_date' => date('Y-m-d'),
            'order_buffet_price' => $data['buffet']->buffet_price,
            'dining_date' => date('Y-m-d', strtotime ($_POST['order_datetime'])),
            'dining_time' => date('H:i', strtotime ($_POST['order_datetime'])),
            'order_pax' => $_POST['order_pax'],
            'address_block' => $_POST['address_block'],
            'address_street' => $_POST['address_street'],
            'address_postal' => $_POST['address_postal'],
            'order_notes' => $_POST['order_additional_note'],
            'customer_name' => $_POST['customer_name'],
            'customer_email' => $_POST['customer_email'],
            'customer_contact' => $_POST['customer_contact'],
            'order_stotal' => $order_stotal,
            'order_delivery' => 0,
            'order_discount' => $order_discount,
            'order_gst' => $order_gst,
            'order_gtotal' => $order_gtotal,
            'order_status' => 0,
        );
        
        //Update On Database
        $this->load->model('Ordermodel');
        $order_id=$this->Ordermodel->add_order($order_data);
        
        /********************************Order Table End****************************************/
        
        
        /********************************Order To Notification Table Start****************************************/
        
        $notification=array(
            
            'description' => 'A new New Order Has been Placed by '.$_POST['customer_name'],
            'path' => 'manageorders/edit_order',
            'post_id'=>$order_id,
            'created_on' => date('Y-m-d H:i:s'),
            'status' => '0',
        );
        
        //Update on Database
        $this->load->model('Notificationsmodel');
        $this->Notificationsmodel->add_notification($notification);
        
        
        /********************************Order To Notification Table End****************************************/
        
        
        /********************************Order Items Starts Here****************************************/     
        $item_ids = $_POST['item_id'];//Item Ids from Form Input
        $i=0;
        foreach($data['buffet']->courses as $courses){
            
            //echo "<br/>".$courses->course_id."<br/>";
            
            $course_id = $courses->course_id;
            
            //Load Course Items
            $this->load->model('Ordermodel');
            $course_items = $this->Ordermodel->get_course_items($course_id);
            
            foreach($course_items as $cositems){
                
                $item_id = $cositems->item_id;
                
                $x=0;
                foreach($item_ids as $id){
                   if($item_id == $id){
                        
                        
                        //Get item Details from Database
                       $this->load->model('Ordermodel');
                       $itm=$this->Ordermodel->get_item($id);
                       
                        $data['buffet']->courses[$i]->course_items[$x] = $itm;
                    }
                    $x++;
                }   
            } 
        $i++;
        }
        
        //Order Courses and Course Items
        
        foreach($data['buffet']->courses as $cos){
        
            
            $order_course = array(
                
                'order_id' => $order_id,
                'course_id' => $cos->course_id,
                'order_course_name' => $cos->course_name,
            );
            
            //Update Databse
            $this->load->model('Ordermodel');
            $insert_course_id=$this->Ordermodel->add_order_course($order_course);
            
            foreach($cos->course_items as $items){
            
                
                $order_items = array(
                    
                    'order_courses_id' => $insert_course_id,
                    'order_items_eng' => $items->item_eng,
                );
                
                //Update Database
                $this->load->model('Ordermodel');
                $this->Ordermodel->add_order_items($order_items);
                
            }
        }
        
        /********************************Order Items Ends Here****************************************/
        
        /********************************Order Addon Items Start Here****************************************/
        
        //print_r($order_addon);
        
        foreach($order_addon as $addons){
            
            $order_addon_items = array(
            
                'order_id' => $order_id,
                'order_add_items_eng' => $addons->addon_eng,
                'order_add_items_pax' => $addons->pax,
                'order_add_items_price' => $addons->addon_price,
            );
            
            //Update Database
            $this->load->model('Ordermodel');
            $this->Ordermodel->add_order_addons($order_addon_items);
            
            $order_addon_items = array();
        }
        
        /********************************Order Addon Items End Here****************************************/   
        
        
        $this->send_email($order_id,$_POST['customer_email']);
        
        
        //Page View
        $this->load->view('components/frontcompo/header', $data);
        $this->load->view('order_confirmation', $data);
        $this->load->view('components/frontcompo/footer', $data);
    }
    
    
    public function send_email($order_id="", $bcc=""){
         $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings()->result();
        //print_r($return_settings[0]->entity_email);
        //die();
        
        $this->load->library('email');
        $config['protocol'] = 'sendmail';
$config['mailpath'] = '/usr/sbin/sendmail';
$config['charset'] = 'iso-8859-1';
$config['wordwrap'] = TRUE;
$config['mailtype'] = 'html';

$this->email->initialize($config);

        $this->email->from($return_settings[0]->entity_email, 'Bayfrontfood');
        $this->email->to('sales@bayfrontfood.sg');
       // $this->email->cc('another@another-example.com');
       if($bcc!=""){
        $this->email->bcc($bcc);
       }        
        $this->email->subject('New Order Received');
       // $this->email->mailtype('html');
        
    $base_url = base_url()."vieworder/view_order/".$order_id;
        
        $msg = '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #EEEEEE;border-top: 0;border-bottom: 0;">
 <tbody><tr>
 <td align="center" valign="top" style="padding-top: 30px;padding-right: 10px;padding-bottom: 30px;padding-left: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
 <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
 <tbody><tr>
 <td align="center" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
 <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;">
                                        <tbody><tr>
                                                <td valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="background-color: #33333e; max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">
                                                                        <tbody><tr>
                                                                                <td valign="top" style="padding-top: 40px;padding-right: 18px;padding-bottom: 40px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;">
                                                                                    <h2 style="display: block;margin: 0;padding: 0;font-family: Arial;font-size: 30px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: -1px;text-align: center;color: #ffffff !important;">Order Confirmation</h2>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">
                                                                        <tbody><tr>
                                                                                <td valign="top" style="padding-top: 20px;padding-right: 18px;padding-bottom: 0;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;">
                                                                                    Hello,<br>
                                                                                    We have received your order <br>
                                                                                    Thank you for your business cooperation.
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" style="padding-top: 10px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-top: 15px;padding-right: 0x;padding-bottom: 15px;padding-left: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate !important;border-radius: 2px;background-color: #00b393;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td align="center" valign="middle" style="font-family: Arial;font-size: 16px;padding: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                    <a href="'.$base_url.'" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;">Click to View the order</a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" style="padding-top: 0px;padding-right: 18px;padding-bottom: 10px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;"> 
                                                                                    
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" style="padding-top: 0px;padding-right: 18px;padding-bottom: 20px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #606060;font-family: Arial;font-size: 15px;line-height: 150%;text-align: left;">
  </td>
 </tr>
 </tbody>
  </table>
  </td>
  </tr>
  </tbody>
 </table>
  </td>
 </tr>
  </tbody>
  </table>
  </td>
 </tr>
 </tbody>
 </table>
 </td>
 </tr>
 </tbody>
  </table>';
  
  
        
        $this->email->message($msg);
        
        
        
        
        
        $this->email->send();
        
    }
    
}
