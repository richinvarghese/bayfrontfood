<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Listmenu extends CI_Controller{
    
    public function __construct(){
        
        parent:: __construct();
    }
    
    public function index($master_id){
        
                
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Buffet Lists';
        
        
        //Get Buffet Details
        $this->load->model('Listmenumodel');
        $data['buffets']=$this->Listmenumodel->get_buffets($master_id);
        
        
        //Page View
        $this->load->view('components/frontcompo/header', $data);
        $this->load->view('list_menu', $data);
        $this->load->view('components/frontcompo/footer', $data);
    }
    
    public function view_menu($buffet_id){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Buffet Lists';
        
        //Get Buffet Details
        $this->load->model('Listmenumodel');
        $data['buffet']=$this->Listmenumodel->get_buffet($buffet_id);
        
        //Get Addons
        $this->load->model('Listmenumodel');
        $data['addons']=$this->Listmenumodel->get_addons();
        
        //Get Courses
        $this->load->model('Listmenumodel');
        $data['courses']=$this->Listmenumodel->get_courses($buffet_id);
        
        $i=0;
        foreach($data['courses']as $cos){
            
            $course_id = $cos->course_id;
            
            //Get Item List for Each Course ID
            $this->load->model('Listmenumodel');
            //$data['course_items'][$i] = $this->Listmenumodel->get_courses_item($course_id);
            $course_items[$i] = $this->Listmenumodel->get_courses_item($course_id);
            
            $a=0;
            //foreach($data['course_items'][$i] as $item){
            foreach($course_items[$i] as $item){
                
                $item_id = $item->item_id;
                
                $this->load->model('Listmenumodel');
                $item = $this->Listmenumodel->get_item($item_id);
                
                $data['courses'][$i]->items[$a]=$item;
                $a++;
            }
            
            $i++;
        }
       
        //Page View
        $this->load->view('components/frontcompo/header', $data);
        $this->load->view('view_menu', $data);
        $this->load->view('components/frontcompo/footer', $data);
    }
}

