<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Vieworder extends CI_Controller{
    
    public function __construct(){
        
        parent::__construct();
        
    }
    

    
    public function view_order($order_id){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Manage Orders';
        
        //$user_id=$_SESSION['user_id'];
        
        
        //Get Order Details from Database
        $this->load->model('Manageordermodel');
        $data['order']=$this->Manageordermodel->get_order_details($order_id);
        
        //Get Buffet Details
        $buffet_name = $data['order']->order_buffet;
        $this->load->model('Manageordermodel');
        $data['buffet']=$this->Manageordermodel->get_buffet($buffet_name);
        
        //Get Order Courses
        $this->load->model('Manageordermodel');
        $data['order_courses']=$this->Manageordermodel->get_order_courses($order_id);
        
        //Get Course Items and append in array
        $a=0;
        foreach($data['order_courses'] as $odcos){
            
            $order_courses_id = $odcos->order_courses_id;
            
            //Get Items From Database
            $this->load->model('Manageordermodel');
            $order_items=$this->Manageordermodel->get_order_items($order_courses_id);
            
            $data['order_courses'][$a]->order_items=$order_items;
            $a++;
        }
        
        //Get Addon Items Details
        $this->load->model('Manageordermodel');
        $data['addons']=$this->Manageordermodel->get_addons($order_id);
        
        //Total number of Items
        $data['nos_addons']=count($data['addons']);
        
        //Total number of Nulls
        $i=0;
        $data['total_null']=0;
        foreach($data['addons'] as $addon){
            
            if($addon->order_add_items_pax == 0){
                
                $i++;
                $data['total_null']=$i;
            }
        }
        
      
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu_link');
            $this->load->view('view_order_link', $data);
            $this->load->view('components/footer');
        
    }
   
}
