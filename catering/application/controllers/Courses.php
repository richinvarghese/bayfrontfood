<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Courses extends CI_Controller{


    public function __construct(){

        parent:: __construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }

    public function index(){

        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Add Courses';

        $user_id=$_SESSION['user_id'];

        //Get Buffet Details from Database
        $this->load->model('Buffetsmodel');
        $return_buffets=$this->Buffetsmodel->get_buffets();
        $data['buffets']=$return_buffets->result();

        //Get Items Details form Database
        $this->load->model('Coursesmodel');
        $return_categories=$this->Coursesmodel->get_category();
        $catagory=$return_categories->result();
        $result = array();
        $i = 0;

        foreach($catagory as $cat){

            $result[$i]['category_name'] = $cat->category_name;
            $result[$i]['category_id'] = $cat->category_id;
            $result[$i]['category_description'] =$cat->category_description;
            $item = $this->Coursesmodel->get_items($cat->category_id)->result();
            $result[$i]['details'] = $item;
            $i++;
        }

        $data['categories'] = $result;

        if($_SESSION['user_type']==1){

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('add_courses', $data);
            $this->load->view('components/footer');
        }
        else{

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('add_courses', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function ajax_sort_id(){
        
        $buffet_id = $this->input->post('buffet_id');
        
        //Get Details from Database
        $this->load->model('Coursesmodel');
        $return_buffet=$this->Coursesmodel->get_sort_id($buffet_id);
        $sort_id=$return_buffet->row()->buffet_course_nos;
        
        echo json_encode($sort_id);
    }
    
    public function add_course(){
        
        $this->form_validation->set_rules('item_id[]', 'Item Selection', 'required', array('required' => 'Please Select atleast 1 Item from the Category List'));
        $this->form_validation->set_rules('buffet_id', 'Buffet Selection', 'required');
        $this->form_validation->set_rules('sort_id', 'Sort Id', 'required');
        
        if($this->form_validation->run() == TRUE){
            
            $buffet_id = $_POST['buffet_id'];
            $sort_id= $_POST['sort_id'];

            //Validate Sort ID
            $this->load->model('Coursesmodel');
            $return_course_sort_id=$this->Coursesmodel->validate_sort_id($buffet_id, $sort_id);

            if($return_course_sort_id->num_rows() !== 0){

                $this->session->set_flashdata('course-error', 'Sorting id already exists. Please choose another or Edit the same.');
                redirect('courses/index');
            }
            
            else{
                
               //Update Course Table
            
                $course = array(

                    'course_name' => $_POST['course_name'],
                    'buffet_id' => $_POST['buffet_id'],
                    'sort_id' => $_POST['sort_id'],
                    'course_allowed_selection' => $_POST['course_allowed_selection']
                );

                $this->load->model('Coursesmodel');
                $return_course=$this->Coursesmodel->add_course($course);

                if($return_course == TRUE){

                    $buffet_id = $_POST['buffet_id'];
                    $course_name = $_POST['course_name'];

                    //Get Course ID to Update Item Table
                    $this->load->model('Coursesmodel');
                    $return_course_id=$this->Coursesmodel->get_course_id($buffet_id, $course_name);
                    $course_id = $return_course_id->row()->course_id;

                    $rows=count($_POST['item_id']);

                    for($x=0; $x<$rows; $x++){

                        $items = array(

                            'course_id' => $course_id,
                            'item_id' => $_POST['item_id'][$x],
                        );
                        
                        //Update Course Items to Table
                        $this->load->model('Coursesmodel');
                        $course_items=$this->Coursesmodel->add_course_items($items);
                        
                        $items=array();
                    }
                    
                    if($course_items == TRUE){

                        $this->session->set_flashdata('course-success', 'Successfully Added New Course');
                        redirect('courses/index');
                    }

                }
                else{

                    $this->session->set_flashdata('course-error', 'Error occurred while adding new Course. Please redo !');
                    redirect('courses/index');
                } 
            }
            
            
        }
        else{
            
            $validation_errors = validation_errors();
            $this->session->set_flashdata('course-error', $validation_errors);
            redirect('courses/index');
        }
    }
    
}
