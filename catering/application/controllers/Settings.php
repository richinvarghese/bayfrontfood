<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Settings extends CI_Controller{
    
    public function __construct(){
        
        parent:: __construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Settings';
        
        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('settings', $data);
            $this->load->view('components/footer'); 
        }
        else{
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('settings', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function upload_logo(){
        
        $file = $this->upload_logo_file();
        $filename['entity_logo'] = ($file['upload_data']['file_name']);
        
        $this->load->model('Settingsmodel');
        $return_logo=$this->Settingsmodel->upload_logo($filename);
        
        if($return_logo == TRUE){
            
            $this->session->set_flashdata('logo-success', 'Successfully Updated Logo');
            redirect('settings/index');
        }
        else{
            
            $this->session->set_flashdata('logo-error', 'Error occurred while updating Logo');
            redirect('settings/index');
        }
        
        
    }
    
    private function upload_logo_file(){
            
        $config['upload_path']          = 'assets/images/app_settings/entity';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1024;
        $config['max_width']            = 500;
        $config['max_height']           = 500;
        $config['file_name']            = 'logo';
        $config['overwrite']            = TRUE;
        
        $this->load->library('upload', $config);
        
        if(!$this->upload->do_upload('logo')){
            
            $errors = array('errors' => $this->upload->display_errors());
            $this->session->set_flashdata('logo-error', $errors);
            redirect('settings/index');
        }
        else{
            
            $file_data = array('upload_data' => $this->upload->data());
            return $file_data;
        }
        
        
    }
    
    public function update_general(){
        
        $general = array(
            
            'entity' => $_POST['entity'],
            'entity_address' => $_POST['entity_address'],
            'entity_postal' => $_POST['entity_postal'],
            'entity_email' => $_POST['entity_email'],
            'entity_contact' => $_POST['entity_contact'],
            'entity_web' => $_POST['entity_web'],
        );
        
        $settings_id=$_POST['settings_id'];
        
        //Update Database
        $this->load->model('Settingsmodel');
        $return_general=$this->Settingsmodel->update_general($general, $settings_id);
        
        if($return_general == TRUE){
            
            $this->session->set_flashdata('general-success', 'Successfully Updated General Information');
            redirect('settings/index');
        }
        else{
            
            $this->session->set_flashdata('general-error', 'Error occurred while updating. Please Redo..!');
            redirect('settings/index');
        }
        

    }
    
    public function update_application(){
        
        $app_settings = array(
            
            'entity_timezone' => $_POST['entity_timezone'],
            'entity_currency' => $_POST['entity_currency'],
            'entity_currency_symbol' => $_POST['entity_currency_symbol'],
            'entity_gst' => $_POST['entity_gst'],
        );
        
        $settings_id=$_POST['settings_id'];
        
        //Update Database
        $this->load->model('Settingsmodel');
        $return_application=$this->Settingsmodel->update_application($app_settings);
        
        if($return_application == TRUE){
            
            $this->session->set_flashdata('application-success', 'Successfully updated Application Settings');
            redirect('settings/index');
        }
        else{
            
            $this->session->set_flashdata('application-error', 'Error occurred while updating. Please Redo..!');
            redirect('settings/index');
        }
        
        
    }
    
    public function update_invoice_terms(){
        
        $invoice_terms = array(
            
            'entity_invoice_prefix' => $_POST['entity_invoice_prefix'],
            'entity_invoice_terms' => $_POST['entity_invoice_terms'],
        );
        
        $settings_id=$_POST['settings_id'];
        
        //Update Database
        $this->load->model('Settingsmodel');
        $return_invoice_terms=$this->Settingsmodel->update_invoice_terms($invoice_terms);
        
        if($return_invoice_terms == TRUE){
            
            $this->session->set_flashdata('invoice_terms-success', 'Successfully Updated Invoice Terms');
            redirect('settings/index');
        }
        else{
            
            $this->session->set_flashdata('invoice_terms-error', 'Error occurred while updating. Please Redo..!');
            redirect('settings/index');
        }
        

    }
    
}
