<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Manage_courses extends CI_Controller{

    public function __construct(){

        parent::__construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }

    public function course($buffet_id){
        
        //Check whether the Course Exist
        $this->load->model('Coursemanage_model');
        $course_value=$this->Coursemanage_model->check_course_exist($buffet_id);

        
        if($course_value->num_rows() == '0'){
            
            $this->session->set_flashdata('buffet-error', 'No Courses have been assigned under this Buffet. Please add a new Course');
            redirect('buffets/index');
        }

        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Manage Courses';
        $data['settings']->buffet_id=$buffet_id;

        $user_id=$_SESSION['user_id'];

        //Get Buffet and Course Details
        $this->load->model('Coursemanage_model');
        $return_courses=$this->Coursemanage_model->get_courses($buffet_id);
        $data['course']=$return_courses->result();
        
        
        $x=0;
        
        //Get Items and Categories
        $this->load->model('Coursemanage_model');
        $data['categories'] = $this->Coursemanage_model->get_categories();
        
        $a=0;
        foreach($data['categories'] as $cats){
            
            $category_id = $cats->category_id;
            $this->load->model('Coursemanage_model');
            $items=$this->Coursemanage_model->get_items($category_id);
            
            $data['categories'][$a]->items=$items;
            
            //echo $category_id."<br>";
            //print_r($data['categories']);
            //echo "<br>";
            $a++;
        }
        //print_r($data['categories']);
        //die();
        
        //Get Course Items
        foreach($data['course'] as $courses){
            
            $course_id = $courses->course_id;
            
            //Get Course Items from Database
            $this->load->model('Coursemanage_model');
            $result_course_items=$this->Coursemanage_model->get_course_items($course_id);
            $items=$result_course_items->result();
            
            $i=0;
            foreach($items as $item){
                
                $item_id=$item->item_id;
                $this->load->model('Coursemanage_model');
                $return_item=$this->Coursemanage_model->get_item_data($item_id);
                $item_details=$return_item->result();
                
                $data['course'][$x]->items[$i]=$item_details;
                //print_r($data['course'][$x]);
                //echo "<br>";
                $i++;
            }
            //$data['course'][$x]->items=$items;
            //print_r($data['course'][$x]);
            //echo "<br>";
            $x++;
        }
        //print_r($data['course']);
        //die();
        
        if($_SESSION['user_type']==1){

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('explore_course', $data);
            $this->load->view('components/footer');
        }
        else{

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('explore_course', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function delete_course(){
        
        $course_id = $_POST['course_id'];
        $buffet_id = $_POST['buffet_id'];
        
        //echo $buffet_id."<br>";
        //echo $course_id;
        //die();
        
        //Delete Entire Course form Data
        $this->load->model('Coursemanage_model');
        $result_delete_course=$this->Coursemanage_model->delete_course($course_id);
        
        
        //Delete Course from Database
        if($result_delete_course == TRUE){
            
            $this->session->set_flashdata('course-success', 'Successfully deleted course and associated course items.');
            redirect('manage_courses/course/'.$buffet_id);
        }
        else{
            
            $this->session->set_flashdata('course-error', 'Error occurred while deleting course and its associated items. Please try again !');
            redirect('manage_courses/course/'.$buffet_id);
        }
    }
    
    public function delete_item(){
        
        $item_id = $_POST['item_id'];
        $course_id = $_POST['course_id'];
        $buffet_id = $_POST['buffet_id'];

        
        //Delete Item from Database
        $this->load->model('Coursemanage_model');
        $result_delete_item=$this->Coursemanage_model->delete_course_item($item_id, $course_id);
        
        if($result_delete_item == TRUE){
            
            $this->session->set_flashdata('course-success', 'Successfully deleted the item opted');
            redirect('manage_courses/course/'.$buffet_id);
        }
        
        else{
            
            $this->session->set_flashdata('course-error', 'Error occurred while deleting the item opted. Please redo !');
            redirect('manage_couses/course/'.$buffet_id);
        }
    }
    
    public function add_course_item(){
        
        $course_id = $_POST['course_id'];
        $item_id = $_POST['item_id'];
        $buffet_id = $_POST['buffet_id'];
        
        
        //Validate Item already exist
        $this->form_validation->set_rules('item_id', 'Select Item', 'required');
        
        if($this->form_validation->run()==TRUE){
            
            $this->load->model('Coursemanage_model');
            $result_check=$this->Coursemanage_model->check_items($course_id, $item_id);

            if($result_check->num_rows()==1){

                $this->session->set_flashdata('course-error', 'Course item already exists. Please select another one');
                redirect('manage_courses/course/'.$buffet_id);
            }

            else{
                
                $course_item = array(
                    'course_id' => $course_id,
                    'item_id' => $item_id,
                );
                
                $this->load->model('Coursemanage_model');
                $result=$this->Coursemanage_model->add_course_item($course_item);
                
                if($result==TRUE){
                    
                    $this->session->set_flashdata('course-success','Course Item Successfully added to the list.');
                    redirect('manage_courses/course/'.$buffet_id);
                }
                else{
                    
                  $this->session->set_flashdata('course-error','Error occurred while updating the Item to List. Please redo !');
                  redirect('manage_courses/course/'.$buffet_id);  
                }
                
            }
        }
        
        else{
            
            $this->session->set_flashdata('course-error', 'Please Select a Course Item to associate');
            redirect('manage_courses/course/'.$buffet_id);
        }
        
        
    }
}
