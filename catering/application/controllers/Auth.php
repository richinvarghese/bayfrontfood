<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    
	public function index(){
            
            //Get Basic Settings Details
            $this->load->model('Settingsmodel');
            $return_settings=$this->Settingsmodel->get_settings();
            $data['settings']=$return_settings->row();
            $data['settings']->pagetitle='Login';
            
            $this->load->view('components/header', $data);
            $this->load->view('login', $data);
            $this->load->view('components/footer');
	}
        
        public function login(){
            
            # Response Data Array
            $resp = array();


            // Fields Submitted
            $user = array(
              'username' => $_POST["username"],
              'password' => $_POST["password"],
            );
            


            // This array of data is returned for demo purpose, see assets/js/neon-forgotpassword.js
            $resp['submitted_data'] = $_POST;


            // Login success or invalid login data [success|invalid]
            // Your code will decide if username and password are correct
            $login_status = 'invalid';
            
            //Get Data From Model
            $this->load->model('Authmodel');
            $return_user=$this->Authmodel->get_user($user);
            $user_data=$return_user->row();
            
            if($return_user->num_rows()==1)
            {
                    $login_status = 'success';
                    $resp['login_status'] = $login_status;
            }
            
            // Login Success URL
            if($login_status == 'success')
            {
                // If you validate the user you may set the user cookies/sessions here
                #setcookie("logged_in", "user_id");
                #$_SESSION["logged_user"] = "user_id";

                $session = array(
                    'user_id' => $user_data->user_id,
                    'username' => $user_data->username,
                    'user_type' => $user_data->user_type,
                    'user_email' => $user_data->user_email,
                    'user_display' => $user_data->user_display,
                    'user_fullname' => $user_data->user_fullname,
                    'user_contact' => $user_data->user_contact,
                    'active' => TRUE,
                );

                $this->session->set_userdata($session);

                // Set the redirect url after successful login
                $url= base_url('dashboard/index');
                $resp['redirect_url'] = $url;
            }
            else{
                
                $login_status = 'invalid';
                $resp['login_status'] = $login_status;  
                }
            
            echo json_encode($resp);
        }
    
        public function logout(){
            
            session_destroy();
            redirect(base_url());
            $this->session->set_flashdata('success', 'Successfully Logged Out');
        }
    
}
