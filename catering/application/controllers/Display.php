<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Display extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Kitchen Display Screen';
        
        //Get order Lists
        $this->load->model('Displaymodel');
        $data['orders']=$this->Displaymodel->get_orders();
        
        $i=0;
        foreach($data['orders'] as $order){
            
            $order_id = $order->order_id;
            
            //Get Courses Details
            $this->load->model('Displaymodel');
            $data['orders'][$i]->order_courses=$this->Displaymodel->get_courses($order_id);
            
            $x=0;
            foreach($data['orders'][$i]->order_courses as $course){
                
                $order_course_id = $course->order_courses_id;
                
                $this->load->model('Displaymodel');
                $data['orders'][$i]->order_courses[$x]->course_items=$this->Displaymodel->get_course_items($order_course_id);
                $x++;
            }
            
            //Get Addons Details
            $this->load->model('Displaymodel');
            $data['orders'][$i]->addon_items=$this->Displaymodel->get_addons($order_id);
            
        $i++;   
        }
        
        
        $this->load->view('display_screen', $data);
    }
}
