<?php
defined('BASEPATH')OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Addons extends CI_Controller{
    
    public function __construct(){
        
        parent::__construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Buffet Addons';
        
        //Get Addon Items form Database
        $this->load->model('Addonmodel');
        $return_items=$this->Addonmodel->get_addon_items();
        $data['addon_items']=$return_items->result();
        
        if($_SESSION['user_type']==1){

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('buffet_addons', $data);
            $this->load->view('components/footer');
        }
        else{

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('buffet_addons', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function add_addon_item(){
        
        $this->form_validation->set_rules('addon_eng', 'Addon Item Name', 'required|is_unique[addons.addon_eng]');
        $this->form_validation->set_rules('addon_chn', 'Chinese Name', 'required|is_unique[addons.addon_chn]');
        
        if($this->form_validation->run()== TRUE){
            
            $addon_item = array(
                
                'addon_eng' => $_POST['addon_eng'],
                'addon_chn' => $_POST['addon_chn'],
                'addon_price' => $_POST['addon_price'],
            );
            
            //Update Item into Database
            $this->load->model('Addonmodel');
            $result = $this->Addonmodel->add_addon_item($addon_item);
            
            if($result == TRUE){
                
                $this->session->set_flashdata('addon-success', 'Succesfully updated Addon Item');
                redirect('addons/index');
            }
            
            else{
                
                $this->session->set_flashdata('addon-error', 'Error occurred while updating Addon Item. Please redo !');
                redirect('addons/index');
            }
        }
        
        else{
            
            $error = validation_errors();
            $this->session->set_flashdata('addon-error', $error);
            redirect('addons/index');
        }
    }
    
    public function update_addon_item(){
        
        $this->form_validation->set_rules('addon_eng', 'Item Name', 'required');
        $this->form_validation->set_rules('addon_chn', 'Chinese Name', 'required');
        $this->form_validation->set_rules('addon_price', 'Item Price', 'required');
        
        if($this->form_validation->run() == TRUE){
            
            $addon_id = $_POST['addon_id'];
            
            $addon_item = array(
                
                'addon_eng' => $_POST['addon_eng'],
                'addon_chn' => $_POST['addon_chn'],
                'addon_price' => $_POST['addon_price'],
            );
            
            //Update Database
            $this->load->model('Addonmodel');
            $result=$this->Addonmodel->update_addon_item($addon_id, $addon_item);
            
            if($result == TRUE){
                
                $this->session->set_flashdata('addon-edit-success', 'Successfully updated the Item Details');
                redirect('addons/index');
            }
            else{
                
                $this->session->set_flashdata('addon-edit-error', 'Error Occurred while Updating the Item. Please redo !');
                redirect('addons/index');
            }
        }
        
        else{
            
            $error = validation_errors();
            $this->session->set_flashdata('addon-edit-error', $error);
            redirect('addons/index');
        }
    }
    
    public function delete_addon_item(){
        
        $addon_id = $_POST['addon_id'];
        
        //Update on Database
        $this->load->model('Addonmodel');
        $result=$this->Addonmodel->delete_addon_item($addon_id);
        
        if($result == TRUE){
            
            $this->session->set_flashdata('addon-edit-success', 'Successfully Deleted the Item');
            redirect('addons/index');
        }
        else{
            
            $this->session->set_flashdata('addon-edit-success', 'Error occurred while deleting the Item. Please redo !');
            redirect('addons/index');
        }
    }
}
