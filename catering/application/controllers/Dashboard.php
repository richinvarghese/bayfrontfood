<?php

defined('BASEPATH')OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Dashboard extends CI_Controller{
    
    public function __construct(){
        
        parent::__construct();
        
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        date_default_timezone_set($data['settings']->entity_timezone);
        
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
        
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Dashboard';
        
        
        //Check Item Expiry Notifications
        
        $this->load->model('Notificationsmodel');
        $items=$this->Notificationsmodel->get_expiry_items();
        
        foreach($items as $item){
            
            $qty = $item->item_qty_supplied;
            $supply_date = date('D, d M', strtotime($item->supply_date));
            
            $exp = strtotime($item->item_expiry_date);
            $tdy = strtotime(date('d-m-Y'));
            
            $diff = $exp-$tdy;
            
            $days=abs(floor($diff/(60*60*24)));
            
            
            if($days <= '2' && $days > 0){
                
                $notification=array(

                'description' => $qty.' Items supplied on '.$supply_date.' is about to expire in '.$days.' days',
                'path' => 'stocks/index',
                'post_id' => 'NULL',
                'created_on' => date('Y-m-d H:i:s'),
                'status' => '0',
            );

            //Update on Database
            $this->load->model('Notificationsmodel');
            $this->Notificationsmodel->add_notification($notification);
            
            
            }
            
        }
        
        //Get this month Data
        $date = new DateTime();
        $month = $date->modify('first day of this month')->format('m-Y');
        $last_month = $date->modify('last day of previous month')->format('m-Y');
        $first_day = $date->modify('first day of this month')->format('d');
        $last_day = $date->modify('last day of this month')->format('d');
        
        
        /////////////////////////////// % more(Compared to Last month) -> Order Statistics & Average Orders Per day///////////////////////////////////////////////////
        $data['order_entries'] = array();
        
        for($i=1; $i<=$last_day; $i++){
            
            $day = $i;
            //Load Orders
            $this->load->model('Dashboardmodel');
            $return=$this->Dashboardmodel->get_orders($month, $day);
            
            if($return->num_rows() != '0'){
                
                $data['order_entries'][$i] = $return->num_rows();
            }
            
            else{
                
                $data['order_entries'][$i] = '0';
            }
            
        }
        
        //Get Total No: of Orders for This Month
        $this->load->model('Dashboardmodel');
        $data['nos_orders']=$this->Dashboardmodel->get_nos_orders($month)->num_rows();
        
        //Get Total Revenue for this month
        $rev_curr =0;
        
        $this->load->model('Dashboardmodel');
        $order_curr=$this->Dashboardmodel->get_orders_curr($month);
        
        foreach($order_curr as $order){
            
            $rev_curr = $rev_curr + $order->order_gtotal;
        }
        
  
        ////////////////////////Current Month Completed-Last Month Started////////////////////////////////////
        $rev_last =0;
        
        $this->load->model('Dashboardmodel');
        $order_last=$this->Dashboardmodel->get_orders_last($last_month);
        
        foreach($order_last as $order){
            
            $rev_last = $rev_last + $order->order_gtotal;
        }
        
        if($rev_last == '0'){
            
            $margin = $rev_curr;
            $data['eval_status'] = 'more';
            $data['eval'] = ($margin / 1) * 100;
        }
        
        else{
            
            if($rev_curr < $rev_last){
            
                $margin = $rev_last - $rev_curr;
                $data['eval_status'] = 'less';
                $data['eval'] = ($margin / $rev_last) * 100;
            }
            elseif($rev_curr > $rev_last){

                $margin = $rev_curr - $rev_last;
                $data['eval_status'] = 'more';
                $data['eval'] = ($margin / $rev_last) * 100;
            }
            elseif($rev_curr == $rev_last){

                $data['eval_status'] = 'change in';
                $data['eval'] = '0';
            }
        }
        
        
        
        /////////////////////////////// % more(Compared to Last month) -> Order Statistics & Average Orders Per day Completed///////////////////////////////////////////////////
        
        
        /////////////////////////////// % more(Compared to Last month) -> Supply Statistics///////////////////////////////////////////////////
        $data['supply_entries'] = array();
        
        for($i=1; $i<=$last_day; $i++){
            
            $day = $i;
            //Load Orders
            $this->load->model('Dashboardmodel');
            $return=$this->Dashboardmodel->get_supplies($month, $day);
            
            if($return->num_rows() != '0'){
                
                $data['supply_entries'][$i] = $return->num_rows();
            }
            
            else{
                
                $data['supply_entries'][$i] = '0';
            }
            
        }
        
        //Get Total Expenditure for this month
        
        $this->load->model('Dashboardmodel');
        $supply_curr=$this->Dashboardmodel->get_supplies_curr($month);
        
        $exp_curr = 0;
        
        foreach($supply_curr as $supply){
            
            $exp_curr = $exp_curr + $supply->supply_total;
        }
        ////////////////////////Current Month Completed-Last Month Started////////////////////////////////////
        
        $exp_last = 0;
        
        $this->load->model('Dashboardmodel');
        $supply_last=$this->Dashboardmodel->get_supplies_last($last_month);
        
        foreach($supply_last as $supply){
            
            $exp_last = $exp_last + $supply->supply_total;
        }
        
        ////////////////////////////Evaluvation of Current Month and Last Month//////////////////////////////

        /*
        echo 'Revenue Current Month  : $'.$rev_curr.'<br>';
        echo 'Revenue Last Month  : $'.$rev_last.'<br><br><br>';
        echo 'Expenses Current Month  : $'.$exp_curr.'<br>';
        echo 'Expenses Last Month  : $'.$exp_last.'<br>';
        die();*/
        
        
        if($exp_last == '0'){
            
            $margin = $exp_curr;
            $data['eval_status2'] = 'more';
            $data['eval2'] = ($margin / 1) * 100;
        }
        
        else{
            
            if($exp_curr < $exp_last){
            
                $margin = $rev_last - $rev_curr;
                $data['eval_status2'] = 'less';
                $data['eval2'] = ($margin / $exp_last) * 100;
            }
            elseif($exp_curr > $exp_last){

                $margin = $exp_curr - $exp_last;
                $data['eval_status2'] = 'more';
                $data['eval2'] = ($margin / $exp_last) * 100;
            }
            elseif($exp_curr == $exp_last){

                $data['eval_status2'] = 'change in';
                $data['eval2'] = '0';
            }
        }
        
        /////////////////////////////// % more(Compared to Last month) -> Supply Statistics///////////////////////////////////////////////////
        
        $data['income_curr'] = $rev_curr;
        $data['expense_curr'] = $exp_curr;
        
        /////////////////////////////// Comparison of Supply Expenditure and Order Revenue Completed///////////////////////////////////////////////////
        
        
        //Get Last 3 Customers made Order
        $this->load->model('Dashboardmodel');
        $data['customers']=$this->Dashboardmodel->get_customers();
        
        //Total Income So Far
        
        $this->load->model('Dashboardmodel');
        $all_orders=$this->Dashboardmodel->get_all_orders();
        
        $data['income'] = 0;
        foreach($all_orders as $order){
            
            $data['income'] = $data['income'] + $order->order_gtotal;
        }
        
        //Total Expenses So Far
        $this->load->model('Dashboardmodel');
        $all_supplies=$this->Dashboardmodel->get_all_supplies();
        
        $data['expense'] = 0;
        
        foreach($all_supplies as $supply){
            
            $data['expense'] = $data['expense'] + $supply->supply_total;
        }
        
         /////////////////////////////// Customers, Income and Expenditure Completed///////////////////////////////////////////////////
        
        $this->load->model('Dashboardmodel');
        $data['buffets']=$this->Dashboardmodel->get_buffet_orders();
        
        $i=0;
        foreach($data['buffets'] as $buffet){
            
            $order_buffet = $buffet->buffet_name;
            
            $this->load->model('Dashboardmodel');
            $data['buffets'][$i]->rows=$this->Dashboardmodel->get_nos_buffets($order_buffet)->num_rows();
            $orders = $this->Dashboardmodel->get_nos_buffets($order_buffet)->result();
            $order_total = 0;
                foreach($orders as $order){
                    
                    $order_total = $order_total + $order->order_gtotal;

                }
            $data['buffets'][$i]->order_gtotal = $order_total;
            $i++;
        }
        
        //For Events
        $this->load->model('Dashboardmodel');
        $data['orders_events']=$this->Dashboardmodel->getall_orders();
        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu', $data);
            $this->load->view('dashboard', $data);
            $this->load->view('components/footer'); 
        }
        else{
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu', $data);
            $this->load->view('dashboard', $data);
            $this->load->view('components/footer');
        }
    }
}