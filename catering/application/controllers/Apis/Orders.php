<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require(APPPATH . 'libraries/REST_Controller.php');
class Orders extends Rest_Controller{
    
    function __construct(){
        
        parent::__construct();
    }
    
    public function orders_post(){
        
        $api_key = md5('Mok_Nicholas');
        
        if($_POST['api_key'] == $api_key){
            
            $user_id = $_POST['user_id'];
            
            //Get Orders from Database
            $this->load->model('Ordermodel');
            $result=$this->Ordermodel->get_orders();
            
            if($result->num_rows() == '0'){
                
                $arrayName = array('value' => 'No Approved Orders' );
                $this->response(array('status'=>true,'message' =>'success', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
            }
            
            else{
                
                foreach($result->result() as $order){
                    
                    $arrayName[] = array(
                        'buffet' => $order->order_buffet,
                        'dining_date' => $order->dining_date,
                        'dining_time' => $order->dining_time,
                        'pax' => $order->order_pax,
                        'customer' => $order->customer_name,
                        'contact' => $order->customer_contact,
                        'address' => $order->address_block.','.$order->address_street,
                        'postal' => $order->address_postal,
                        'remarks' => $order->order_notes,
                    );
                }
                
                $this->response(array('status'=>true,'message' =>'success', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
            }
            
        }
        
        else{
            
            $arrayName = array('value' => 'Invalid Api Key. Please contact Administrator' );
            $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
        }
        
    }
    
    
}
