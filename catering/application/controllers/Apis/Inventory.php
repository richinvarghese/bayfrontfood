<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require(APPPATH . 'libraries/REST_Controller.php');

class Inventory extends REST_Controller{
    
    
    function __construct(){
        
        parent::__construct();
    }
    
    public function inventory_post(){
        
        $api_key = md5('Mok_Nicholas');
        $user_id = $_POST['user_id'];
        
        if($_POST['api_key'] == $api_key){
            
            //Get Inventory from Database
            $this->load->model('Inventorymodel');
            $result=$this->Inventorymodel->get_inventory();
            
            
            if($result->num_rows == '0'){
                
                $arrayName = array('value' => 'No Items Updated' );
                $this->response(array('status'=>true,'message' =>'success', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
            }
            else{
                
                //Get Data from Inventory Table
                $this->load->model('Stocksmodel');
                $data['stocks']=$this->Stocksmodel->get_stocks();

                //Get Locations
                $this->load->model('Stocksmodel');
                $data['locations']=$this->Stocksmodel->get_locations();
                
                //Populate Data On Datatable
                $x=0;
                foreach($data['stocks'] as $stocks){

                    $item_id = $stocks->item_id;
                    $location_id = $stocks->location_id;

                    //Get Item Details from Database
                    $this->load->model('Stocksmodel');
                    $item=$this->Stocksmodel->get_item($item_id);

                    //Get Location Details from Database
                    $this->load->model('Stocksmodel');
                    $location=$this->Stocksmodel->get_location($location_id);

                    $data['stocks'][$x]->item = $item;
                    $data['stocks'][$x]->location = $location;
                    $x++;
                }
                
                
                foreach($data['stocks'] as $stock){
                    
                    if($stock->location_id == '0'){
                        
                        $item_location = 'No Stocks In this Location';
                    }
                    else{
                        
                        $item_location = $stock->location->location_name.' - '.$stock->location->location_stack;
                    }
                    
                    $arrayName[] = array(
                        
                        'item_name' => $stock->item->item_name,
                        'item_sku' => $stock->item->item_sku,
                        'item_quantity' => $stock->item_quantity,
                        'item_location' => $item_location,
                    );
                  
                }
                
                $this->response(array('status'=>true,'message' =>'success', 'response'=>$arrayName ), REST_Controller::HTTP_OK);  
            }
            
            
        }
        else{
            
            $arrayName = array('value' => 'Invalid Api Key. Please contact Administrator' );
            $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
        }
    }
}

