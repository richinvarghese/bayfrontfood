<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */

require (APPPATH . 'libraries/REST_Controller.php');

class Auth extends REST_Controller{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

    }

    public function user_login_post(){

        $api_key = md5('Mok_Nicholas');
        
        
        if($_POST['api_key'] == $api_key){
            
            if($_POST['username'] == '' || $_POST['password'] == ''){
                
                $arrayName = array('value' => 'Username and Password are required. Please fill' );
                $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
                
            }
            
            else{
                
                $user = array(
                    
                    'username' => $_POST['username'],
                    'password' => $_POST['password']
                );
                
                //Check Database
                $this->load->model('Usersmodel');
                $result=$this->Usersmodel->check_login($user);
                
                if($result->num_rows() > '0'){
                    
                    $arrayName = array(
                        'value' => 'Successfully LoggedIn',
                        'user_id' => $result->row()->user_id,
                        'user_email' => $result->row()->user_email,
                        'user_fullname' => $result->row()->user_fullname,
                        'user_contact' => $result->row()->user_contact,
                        'user_status' => $result->row()->user_status,
                        'user_display'=>base_url().'assets/images/users/'.$result->row()->user_display,
                        
                        );
                    $this->response(array('status'=>true,'message' =>'success', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
                }
                else{
                    
                    $arrayName = array('value' => 'Incorrect Username or Password. Please Try again' );
                    $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
                }
                
                
            }
            
        }
        else{
            
            $arrayName = array('value' => 'Invalid Api Key. Please contact Administrator' );
            $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
        }
        
        
    }
    
    public function reset_password_post(){
        
        $api_key = md5('Mok_Nicholas');
        
        if($_POST['api_key'] == $api_key){
            
            if($_POST['current_password'] == '' || $_POST['new_password'] == ''){
            
            $arrayName = array('value' => 'Current Password and New Password are required Fields' );
            $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
            
            }
        
            else{

                $user_id = $_POST['user_id'];
                $current_password = $_POST['current_password'];
                $new_password = $_POST['new_password'];


                //Check Database
                $this->load->model('Usersmodel');
                $result=$this->Usersmodel->check_password($current_password, $user_id);

                if($result->num_rows() == '0'){

                    $arrayName = array('value' => 'Current Password doesnt match');
                    $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);

                }

                else{

                    //Update Database
                    $this->load->model('Usersmodel');
                    $result=$this->Usersmodel->new_password($user_id, $new_password);

                    if($result == TRUE){

                        $arrayName = array('value' => 'Successfully Updated new Password' );
                        $this->response(array('status'=>true,'message' =>'success', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
                    }

                    else{

                        $arrayName = array('value' => 'Error occurred while updating Password. Please try again' );
                        $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
                    }

                }

            }
            
            
        }
        
        else{
            
            $arrayName = array('value' => 'Invalid Api Key. Please contact Administrator' );
            $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
        }
        
        
    }



}
