<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require(APPPATH. 'libraries/REST_Controller.php');

class Draw_Inventory extends REST_Controller{
    
    function __construct(){
        
        parent::__construct();
    }
    
    public function get_item_post(){
        
        $api_key = md5('Mok_Nicholas');
        $user_id = $_POST['user_id'];
        
        if($_POST['api_key'] == $api_key){
            
            $item_sku = $_POST['item_sku'];
            
            //Get Item Id
            $this->load->model('Stocksmodel');
            $result_sku=$this->Stocksmodel->get_item_id($item_sku);
            
            if($result_sku->num_rows() != '1'){
                
                $arrayName = array('value' => 'Unable to find Item. Please contact Administrator' );
                $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
            }
            else{
                
                $item_id = $result_sku->row()->item_id;
                
                //Get User Details
                $this->load->model('Stocksmodel');
                $user_fullname=$this->Stocksmodel->get_user($user_id)->user_fullname;
                
                //Get Locations
                $this->load->model('Inventorymodel');
                $result_location=$this->Inventorymodel->inventory_locations($item_id);
                
                //Get Approved Orders
                $this->load->model('Stocksmodel');
                $orders=$this->Stocksmodel->get_draw_orders()->result();
                
                $arrayName = array(
                    
                    'item_id' => $result_sku->row()->item_id,
                    'item_name' => $result_sku->row()->item_name,
                    'item_unit' => $result_sku->row()->item_unit,
                    'item_location' => $result_location->result(),
                    'order' => $orders,
                    'user' => $user_fullname,
                    
                );
                $this->response(array('status'=>true,'message' =>'success', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
            }
            
        }
        
        else{
            
            $arrayName = array('value' => 'Invalid Api Key. Please contact Administrator' );
            $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
        }
    }
    
    public function draw_item_post(){
        
        $api_key = md5('Mok_Nicholas');
        $user_id = $_POST['user_id'];
        
        //Get User Details
        $this->load->model('Stocksmodel');
        $user_fullname=$this->Stocksmodel->get_user($user_id)->user_fullname;
        
        if($_POST['api_key'] == $api_key){
            
            $drawn = array(
            
                'item_id' => $_POST['item_id'],
                'location_id' => $_POST['location_id'],
                'item_quantity' => $_POST['item_quantity'],
                'order_id' => $_POST['order_id'],
                'user_id' => $_POST['user_id'],
                'user_fullname' => $user_fullname,
                'drawn_date' => date('Y-m-d'),
            );

            $item_id = $_POST['item_id'];
            $location_id = $_POST['location_id'];


            //Get Inventory ID from  Database
            $this->load->model('Stocksmodel');
            $inventorydb = $this->Stocksmodel->get_inventorydb($item_id, $location_id);

            if($inventorydb->item_quantity < $drawn['item_quantity']){

                $arrayName = array('value' => 'Items left in the location is less than Requested Quantity' );
                $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
            }

            elseif($drawn['item_quantity'] <= '0'){
                $arrayName = array('value' => 'Min Quantity request has to be greater than 0' );
                $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
            }

            elseif($inventorydb->item_quantity == $drawn['item_quantity']){

                $inventory_id = $inventorydb->inventory_id;

                $inventory = array(

                    'item_quantity' => '0',
                    'item_location' => '0',
                    'item_total_worth' => '0'
                );

                //Update on Inventory Table
                $this->load->model('Stocksmodel');
                $status = $this->Stocksmodel->adjust_quantity($inventory_id, $inventory);

                if($status == TRUE){

                    //Update on Inventory Draw Table
                    $this->load->model('Stocksmodel');
                    $this->Stocksmodel->add_draw_inventory($drawn);

                    /********************************Item Draw To Notification Table Start****************************************/

                    $notification=array(

                        'description' => 'Stock Drawn Request has been Placed',
                        'path' => 'manage_stocks/index',
                        'post_id'=> 'NULL',
                        'created_on' => date('Y-m-d H:i:s'),
                        'status' => '0',
                    );

                    //Update on Database
                    $this->load->model('Notificationsmodel');
                    $this->Notificationsmodel->add_notification($notification);


                    /********************************Item Draw To Notification Table End****************************************/

                    $arrayName = array('value' => 'Successfully updated Inventory as of Drawn Quantity' );
                    $this->response(array('status'=>true,'message' =>'success', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
                }

            }

            else{

                $inventory_id = $inventorydb->inventory_id;

                //Get item Unit Price
                $this->load->model('Stocksmodel');
                $item=$this->Stocksmodel->get_item($item_id);

                $inventory = array(

                    'item_quantity' => $inventorydb->item_quantity - $drawn['item_quantity'],
                    'item_total_worth' => $inventorydb->item_total_worth - ($drawn['item_quantity'] * $item->item_cost_price),
                );

                //Update on Inventory Table
                $this->load->model('Stocksmodel');
                $status = $this->Stocksmodel->adjust_quantity($inventory_id, $inventory);

                if($status == TRUE){

                    //Update on Inventory Draw Table
                    $this->load->model('Stocksmodel');
                    $this->Stocksmodel->add_draw_inventory($drawn);

                    /********************************Item Draw To Notification Table Start****************************************/

                    $notification=array(

                        'description' => 'Stock Drawn Request has been Placed',
                        'path' => 'manage_stocks/index',
                        'post_id'=> 'NULL',
                        'created_on' => date('Y-m-d H:i:s'),
                        'status' => '0',
                    );

                    //Update on Database
                    $this->load->model('Notificationsmodel');
                    $this->Notificationsmodel->add_notification($notification);


                    /********************************Item Draw To Notification Table End****************************************/
                    
                    $arrayName = array('value' => 'Successfully updated Inventory as of Drawn Quantity' );
                    $this->response(array('status'=>true,'message' =>'success', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
                }
            
            }


        }
        else{

            $arrayName = array('value' => 'Invalid Api Key. Please contact Administrator' );
            $this->response(array('status'=>true,'message' =>'error', 'response'=>$arrayName ), REST_Controller::HTTP_OK);
        }
    }
}
