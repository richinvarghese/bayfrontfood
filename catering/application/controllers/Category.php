<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Category extends CI_Controller{
    
    public function __contruct(){
        
        parent:: __construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function add_category(){
        
        $this->form_validation->set_rules('cat_name', 'Category Name', 'required|is_unique[category.category_name]');
        $this->form_validation->set_rules('cat_descrip', 'Category Description', 'required|max_length[35]');
        
        if($this->form_validation->run() == TRUE){
            
            $category = array(
            
                'category_name' => $_POST['cat_name'],
                'category_description' => $_POST['cat_descrip']
            );
            
            $this->load->model('Categorymodel');
            $return_category=$this->Categorymodel->add_category($category);
            
            if($return_category == TRUE){
                
                $this->session->set_flashdata('category-success', 'Successfully added new Category');
                redirect('items/index');
            }
            else{
                
                $this->session->set_flashdata('category-error', 'Error occurred while adding new Category');
                redirect('items/index');
            }
            
        }
        else{
            
            $errors=validation_errors();
            $this->session->set_flashdata('category-error', $errors);
            redirect('items/index');
        }
        
    }
    
    public function edit_category(){
        
        $category_id=$_POST['cat_id'];
        
        $category = array(
            
            'category_name' => $_POST['cat_name'],
            'category_description' => $_POST['cat_descrip'],
        );
        
        //Update Database
        $this->load->model('Categorymodel');
        $return_category=$this->Categorymodel->update_category($category_id, $category);

        if($return_category == TRUE){
                
            $this->session->set_flashdata('category-success', 'Successfully updated Category');
            redirect('items/index');
        }
        else{

            $this->session->set_flashdata('category-error', 'Error occurred while updating Category. Please redo..!');
            redirect('items/index');
        }
    }
    
    public function delete_category(){
        
        $category_id=$_POST['cat_id'];
        
        //Update on Database
        $this->load->model('Categorymodel');
        $return_category=$this->Categorymodel->delete_category($category_id);
        
        if($return_category == TRUE){
                
            $this->session->set_flashdata('category-success', 'Successfully Deleted Category');
            redirect('items/index');
        }
        else{

            $this->session->set_flashdata('category-error', 'Error occurred while deleting the Category. Please redo..!');
            redirect('items/index');
        }
    }
    
}
