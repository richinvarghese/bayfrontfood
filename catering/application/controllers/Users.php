<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Users extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Manage Users';
        
        //Get Users List
        $this->load->model('Usersmodel');
        $data['users']=$this->Usersmodel->get_users();
        
        
        if($_SESSION['user_type']==1){

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('manage_users', $data);
            $this->load->view('components/footer');
        }
        else{

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('manage_users', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function add_user(){
        
        $this->form_validation->set_rules('username', 'Username', 'is_unique[users.username]',array('is_unique' => 'Username has already taken. Please choose another'));
        $this->form_validation->set_rules('user_contact', 'Contact Information', 'is_unique[users.user_contact]');
        
        if($this->form_validation->run() == TRUE){
            
            $user = array(
                
                'user_fullname' => $_POST['user_fullname'],
                'username' => $_POST['username'],
                'user_email' => $_POST['user_email'],
                'user_contact' => $_POST['user_contact'],
                'user_type' => $_POST['user_type'],
                'user_status' => '1',
            );
            
            $display_pic=$this->upload_display();
            $user['user_display']=$display_pic['upload_data']['file_name'];
            
            //Update on Database
            $this->load->model('Usersmodel');
            $status = $this->Usersmodel->add_user($user);
            
            if($status == TRUE){
                
                $this->session->set_flashdata('user-success', 'Successfully added new user');
                redirect('users/index');
            }
            
            else{
                
                $this->session->set_flashdata('user-error', 'Error occurred while adding new User. Please try again !');
                redirect('users/index');
            }
        }
        else{
            
            $errors= validation_errors();
            $this->session->set_flashdata('user-error', $errors);
            redirect('users/index');
        }
    }
    
    private function upload_display(){
        
        $config['upload_path']          = 'assets/images/users/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1024;
        $config['max_width']            = 150;
        $config['max_height']           = 150;
        $config['file_name']            = $_POST['username'];
        $config['overwrite']            = TRUE;
        
        $this->load->library('upload', $config);
        
        if(!$this->upload->do_upload('user_display')){
            
            $errors=$this->upload->display_errors();
            $this->session->set_flashdata('user-error', $errors);
            redirect('users/index');
            
        }
        else{
            
            $file_data = array('upload_data' => $this->upload->data());
            return $file_data;
        }
    }
    
    public function edit_user(){
        
        $this->form_validation->set_rules('user_fullname', 'Fullname', 'required');
        $this->form_validation->set_rules('user_email', 'Email', 'required');
        $this->form_validation->set_rules('user_contact', 'Contact', 'required');
        
        if($this->form_validation->run() == TRUE){
            
            $user_id = $_POST['user_id'];
            
            $user = array(
                
                'user_fullname' => $_POST['user_fullname'],
                'user_email' => $_POST['user_email'],
                'user_contact' => $_POST['user_contact'],
                'user_type' => $_POST['user_type'],
                'user_status' => $_POST['user_status'],
            );
            
            //Update on Database
            $this->load->model('Usersmodel');
            $status = $this->Usersmodel->edit_user($user, $user_id);
            
            if($status == TRUE){
                
                $this->session->set_flashdata('user-edit-success', 'Successfully edited User Information');
                redirect('users/index');
            }
            
            else{
                
                $this->session->set_flashdata('user-edit-error', 'Error occurred while updating User Information. Please try again !');
                redirect('users/index');
            }
        }
        else{
            
            $error=validation_errors();
            $this->session->set_flashdata('user-edit-error', $error);
            redirect('users/index');
        }
    }
    
    public function reset_password(){
        
        $this->form_validation->set_rules('user_password', 'Password', 'required|min_length[6]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[user_password]');
        
        if($this->form_validation->run() == TRUE){
          
            $user_id=$_POST['user_id'];
            $password=$_POST['user_password'];
            
            //Update Password on Database
            $this->load->model('Usersmodel');
            $status=$this->Usersmodel->reset_password($user_id, $password);
            
            if($status == TRUE){
                
                $this->session->set_flashdata('user-edit-success', 'Successfully reset Password');
                redirect('users/index');
            }
            
            else{
                
                $this->session->set_flashdata('user-edit-error', 'Error occurred while resetting the Password. Please try again !');
                redirect('users/index');
            }
        }
        else{
            
            $errors=validation_errors();
            $this->session->set_flashdata('user-edit-error', $errors);
            redirect('users/index');
        }
        
    }
    
    public function edit_user_display(){
        
        $user_id =$_POST['user_id'];
        
        $display_pic=$this->edit_display();
        $user['user_display']=$display_pic['upload_data']['file_name'];
            
        //Update on Database
        $this->load->model('Usersmodel');
        $status = $this->Usersmodel->edit_user($user, $user_id);

        if($status == TRUE){

            $this->session->set_flashdata('user-edit-success', 'Successfully Updated User Display');
            redirect('users/index');
        }

        else{

            $this->session->set_flashdata('user-edit-error', 'Error occurred while updating User Display. Please try again !');
            redirect('users/index');
        }
    }
    
    private function edit_display(){
        
        $config['upload_path']          = 'assets/images/users/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1024;
        $config['max_width']            = 150;
        $config['max_height']           = 150;
        $config['file_name']            = $_POST['username'];
        $config['overwrite']            = TRUE;
        
        $this->load->library('upload', $config);
        
        if(!$this->upload->do_upload('user_display')){
            
            $errors=$this->upload->display_errors();
            $this->session->set_flashdata('user-edit-error', $errors);
            redirect('users/index');
            
        }
        else{
            
            $file_data = array('upload_data' => $this->upload->data());
            return $file_data;
        }
    }
    
    public function delete_user(){
        
        $user_id=$_POST['user_id'];
        
        //Update on Database
        $this->load->model('Usersmodel');
        $status=$this->Usersmodel->delete_user($user_id);
        
        if($status == TRUE){

            $this->session->set_flashdata('user-edit-success', 'Successfully Deleted User');
            redirect('users/index');
        }

        else{

            $this->session->set_flashdata('user-edit-error', 'Error occurred while deleting the User. Please try again !');
            redirect('users/index');
        }
    }
}
