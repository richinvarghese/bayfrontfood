<?php
defined('BASEPATH')OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Myaccount extends CI_Controller{
    
    
    public function __construct() {
        parent::__construct();
        
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='My Account';
        
        $user_id=$_SESSION['user_id'];
        
        //Retrive Details from Database
        $this->load->model('Myaccountmodel');
        $return_user=$this->Myaccountmodel->get_user($user_id);
        $data['user']=$return_user->row();
        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('my_account', $data);
            $this->load->view('components/footer'); 
        }
        else{
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('my_account', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function update_profile(){
        
        $user_id=$_POST['user_id'];
        
        $profile=array(
            
            'user_fullname' => $_POST['user_fullname'],
            'username' => $_POST['username'],
            'user_email' => $_POST['user_email'],
            'user_contact' => $_POST['user_contact']
        );
        
        //Updating Database
        $this->load->model('Myaccountmodel');
        $return_update_profile=$this->Myaccountmodel->update_profile($profile, $user_id);
        
        if($return_update_profile == TRUE){
            
            $this->session->set_flashdata('profile-success', 'Profile updated successfully');
            redirect('myaccount/index');
        }
        else{
            
            $this->session->set_flashdata('profile-error', 'Error occurred while updating your Profile. Please redo..!');
            redirect('myaccount/index');
        }
    }
    
    public function update_password(){
        
        $user_id=$_POST['user_id'];
        
        //Form Validation
        $this->form_validation->set_rules('new_password', 'New Password', 'min_length[8]');
        $this->form_validation->set_rules('repeat_password', 'Repeat Password', 'matches[new_password]');
        
        if($this->form_validation->run() == TRUE){
            
            $password=array(
                'current_password' => $_POST['current_password'],
                'new_password' => $_POST['new_password'],
            );
        
            //Verify Current Password
            $this->load->model('Myaccountmodel');
            $return_verify_password=$this->Myaccountmodel->verify_password($password);
            
            if($return_verify_password->num_rows() == 1){
                
                //Update Password
                $user['password']=$password['new_password'];
                
                $this->load->model('Myaccountmodel');
                $return_update_password=$this->Myaccountmodel->update_password($user);
                
                if($return_update_password == TRUE){
                    
                    $this->session->set_flashdata('password-success', 'Successfully Updated your Password');
                    redirect('myaccount/index');
                }
                else{
                    
                    $this->session->set_flashdata('password-error', 'Error occurred while updating password. Please try again..!');
                    redirect('myaccount/index');
                }
            }
            
            else{
                
                $this->session->set_flashdata('password-error', 'Password doesnt Match with your current password. Please try again..!');
                redirect('myaccount/index');
            }
        }
        else{
            
            $errors=validation_errors();
            $this->session->set_flashdata('password-error', $errors);
            redirect('myaccount/index');
        }
        
    }
    
    public function update_display(){
        
        $user_id=$_POST['user_id'];
        
        $file=$this->upload_display();
        $display['user_display'] = ($file['upload_data']['file_name']);
        
        $this->load->model('Myaccountmodel');
        $return_display=$this->Myaccountmodel->update_display($display, $user_id);
        
        if($return_display == TRUE){
            
            $this->session->set_flashdata('display-success', 'Successfully Updated Display Picture');
            redirect('myaccount/index');
        }
        else{
            
            $this->session->set_flashdata('display-error', 'Error occurred while updating Display Picture');
            redirect('myaccount/index');
        }
    }
    
    private function upload_display(){
        
        $config['upload_path']          = 'assets/images/users/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1024;
        $config['max_width']            = 500;
        $config['max_height']           = 500;
        $config['file_name']            = $_SESSION['username'];
        $config['overwrite']            = TRUE;
        
        $this->load->library('upload', $config);
        
        if(!$this->upload->do_upload('user_display')){
            
            $errors = array('errors' => $this->upload->display_errors());
            $this->session->set_flashdata('display-error', $errors);
            redirect('myaccount/index');
        }
        else{
            
            $file_data = array('upload_data' => $this->upload->data());
            return $file_data;
        }
        
    }
}

