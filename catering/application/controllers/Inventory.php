<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Inventory extends CI_Controller{
    
    public function __construct(){
        
        parent:: __construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        } 
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Inventory Settings';
        
        //Get Units
        $this->load->model('Inventorymodel');
        $data['units']=$this->Inventorymodel->get_units();
        
        //Get Locations from Database
        $this->load->model('Inventorymodel');
        $data['locations']=$this->Inventorymodel->get_locations();
        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('inventory_settings', $data);
            $this->load->view('components/footer'); 
        }
        else{
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('inventory_settings', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function add_unit(){
        
        $unit = array(
            
            'unit_name' => $_POST['unit_name'],
            'unit_short' => $_POST['unit_short'],
            'unit_status' => '1',
        );
        
        //Update on Database
        $this->load->model('Inventorymodel');
        $status=$this->Inventorymodel->add_unit($unit);
        
        if($status == TRUE){
            
            $this->session->set_flashdata('success-inventory-add', 'Successfully Added Unit');
            redirect('inventory/index');
        }
        else{
            
            $this->session->set_flashdata('error-inventory-add', 'Error occurred while updating Unit. Please redo !');
            redirect('inventory/index');
        }
    }
    
    public function edit_unit(){
        
        $this->form_validation->set_rules('unit_name', 'Unit Name', 'required');
        $this->form_validation->set_rules('unit_short', 'Unit Short Format', 'required');
        
        if($this->form_validation->run()==TRUE){
            
            $unit_id=$_POST['unit_id'];
            
            $unit = array(
                
                'unit_name' => $_POST['unit_name'],
                'unit_short' => $_POST['unit_short'],
                'unit_status' => $_POST['unit_status'],
            );
            
            //Update On Database
            $this->load->model('Inventorymodel');
            $status=$this->Inventorymodel->edit_unit($unit, $unit_id);
            
            if($status=TRUE){
                
                $this->session->set_flashdata('success-inventory-edit', 'Unit Updated Successfully');
                redirect('inventory/index');
            }
            else{
                
                $this->session->set_flashdata('error-inventory-edit', 'Error occurred while updating inventory. Please redo !');
                redirect('inventory/index');
            }
        }
        
        else{
            
            $errors = validation_errors();
            $this->session->set_flashdata('error-inventory-edit', $errors);
            redirect('inventory/index');
        }
        
    }
    
    
    public function delete_unit(){
        
        $unit_id = $_POST['unit_id'];
        
        //Update on Database
        $this->load->model('Inventorymodel');
        $status=$this->Inventorymodel->delete_unit($unit_id);
        
        if($status==TRUE){
            
            $this->session->set_flashdata('success-inventory-edit', 'Successfully Deleted the Unit');
            redirect('inventory/index');
        }
        else{
            
            $this->session->set_flashdata('error-inventory-edit', 'Error occurred while Deleting the Unit. Please try again !');
            redirect('inventory/index');
        }
    }
    
    public function add_location(){
        
        $location = array(

            'location_name' => $_POST['location_name'],
            'location_stack' => $_POST['location_stack'],
            'location_status' => '1',
        );
        
        //Update on database
        $this->load->model('Inventorymodel');
        $status=$this->Inventorymodel->add_location($location);
        
        if($status == TRUE){
            
            $this->session->set_flashdata('success-inventory-location', 'Successfully added new Location');
            redirect('inventory/index');
        }
        else{
            
            $this->session->set_flashdata('success-inventory-location', 'Error occurred while adding new Location. Please try again !');
            redirect('inventory/index');
        }
    }
    
    public function edit_location(){
        
        
        $this->form_validation->set_rules('location_name', 'Location Name', 'required');
        $this->form_validation->set_rules('location_stack', 'Location Stack', 'required');
        
        if($this->form_validation->run() == TRUE){
            
            $location_id = $_POST['location_id'];
            
            $location = array(
                
                'location_name' => $_POST['location_name'],
                'location_stack' => $_POST['location_stack'],
                'location_status' => $_POST['location_status'],
            );
            
            //Update on Database
            $this->load->model('Inventorymodel');
            $status=$this->Inventorymodel->edit_location($location, $location_id);
            
            if($status == TRUE){
                
                $this->session->set_flashdata('success-location-edit', 'Successfully edited Location Details');
                redirect('inventory/index');
            }
            else{
                
                $this->session->set_flashdata('error-location-edit', 'Error occurred while updating Location Details. Please redo !');
                redirect('inventory/index');
            }
        }
        else{
            
            $errors = validation_errors();
            $this->session->set_flashdata('error-location-edit', $errors);
            redirect('inventory/index');
        }
    }
    
    public function delete_location(){
        
        $location_id = $_POST['location_id'];
        
        //Check Inventory Table whether the Location has been used
        $this->load->model('Inventorymodel');
        $location_check=$this->Inventorymodel->check_location($location_id);
        
        if($location_check->num_rows() > 0){
            
            $this->session->set_flashdata('error-location-edit', 'Location cant be Deleted as it has used to allocate Inventory. Move Items to Other Locations inorder to Delete');
            redirect('inventory/index');
        }
        
        else{
            
            //Update on Database
            $this->load->model('Inventorymodel');
            $status=$this->Inventorymodel->delete_location($location_id);

            if($status == TRUE){

                $this->session->set_flashdata('success-location-edit', 'Successfully deleted the Location');
                redirect('inventory/index');
            }
            else{

                $this->session->set_flashdata('error-location-edit', 'Error occurred while deleting the Location. Please redo !');
                redirect('inventory/index');
            }
        }
        
    }
    
}