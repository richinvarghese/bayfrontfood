<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Supplier extends CI_Controller{
    
    public function __construct(){
        
        parent::__construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Manage Suppliers';
        
        //Get Suppliers Listing
        $this->load->model('Suppliermodel');
        $data['suppliers']=$this->Suppliermodel->get_suppliers();
        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('manage_suppliers', $data);
            $this->load->view('components/footer'); 
        }
        else{
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('manage_suppliers', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function add_supplier(){
        
        $supplier = array(
            
            'supplier_name' => $_POST['supplier_name'],
            'supplier_block' => $_POST['supplier_block'],
            'supplier_unit' => $_POST['supplier_unit'],
            'supplier_street' => $_POST['supplier_street'],
            'supplier_postal' => $_POST['supplier_postal'],
            'supplier_person' => $_POST['supplier_person'],
            'supplier_contact' => $_POST['supplier_contact'],
            'supplier_email' => $_POST['supplier_email'],
            'supplier_status' => '1',
        );
        
        //Update on Database
        $this->load->model('Suppliermodel');
        $status=$this->Suppliermodel->add_supplier($supplier);
        
        if($status == TRUE){
            
            $this->session->set_flashdata('success-supplier-add', 'Successfully added New Suplier');
            redirect('supplier/index');
        }
        else{
            
            $this->session->set_flashdata('error-supplier-add', 'Error occurred while adding New Supplier. Please redo !');
            redirect('supplier/index');
        }
    }
    
    public function edit_supplier(){
        
        $supplier_id = $_POST['supplier_id'];
        
        $supplier = array(
            'supplier_block' => $_POST['supplier_block'],
            'supplier_unit' => $_POST['supplier_unit'],
            'supplier_street' => $_POST['supplier_street'],
            'supplier_postal' => $_POST['supplier_postal'],
            'supplier_person' => $_POST['supplier_person'],
            'supplier_contact' => $_POST['supplier_contact'],
            'supplier_email' => $_POST['supplier_email'],
            'supplier_status' => $_POST['supplier_status'],
        );
        
        //Update on Database
        $this->load->model('Suppliermodel');
        $status=$this->Suppliermodel->edit_supplier($supplier_id, $supplier);
        
        if($status == TRUE){
            
            $this->session->set_flashdata('success-supplier-edit', 'Successfully updated Suplier Details');
            redirect('supplier/index');
        }
        else{
            
            $this->session->set_flashdata('error-supplier-edit', 'Error occurred while updating Supplier Details. Please redo !');
            redirect('supplier/index');
        }
    }
    
    public function delete_supplier(){
        
        $supplier_id = $_POST['supplier_id'];
        
        //Update in Database
        $this->load->model('Suppliermodel');
        $status=$this->Suppliermodel->delete_supplier($supplier_id);
        
        if($status == TRUE){
            
            $this->session->set_flashdata('success-supplier-edit', 'Successfully Deleted Supplier');
            redirect('supplier/index');
        }
        else{
            
            $this->session->set_flashdata('error-supplier-edit', 'Error occurred while Deleting the Supplier. Please redo !');
            redirect('supplier/index');
        }
    }
}
