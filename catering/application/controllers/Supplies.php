<?php
defined('BASEPATH') OR exit('No Direct Script access Allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Supplies extends CI_Controller{
    
    public function __construct(){
        
       parent::__construct();
       
       $this->load->model('Settingsmodel');
       $return_settings=$this->Settingsmodel->get_settings();
       $data['settings']=$return_settings->row();
       date_default_timezone_set($data['settings']->entity_timezone);
       
       if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Supplies';
        
        //Get Supplier Listings
        $this->load->model('Suppliesmodel');
        $data['suppliers']=$this->Suppliesmodel->get_suppliers();
        
        //Get Item Listings
        $this->load->model('Suppliesmodel');
        $data['items']=$this->Suppliesmodel->get_items();
        
        //Get Supplies Listing
        $this->load->model('Suppliesmodel');
        $data['supplies']=$this->Suppliesmodel->get_supplies();
        
        //Get Supplies Data This Month
        $this->load->model('Suppliesmodel');
        $return_this_month=$this->Suppliesmodel->get_this_month();
        $data['lmonth_supplies'] =$return_this_month->num_rows();
        
        $lmonth_cost ='0';
        
        foreach($return_this_month->result() as $supply_cost){
            
            $lmonth_cost = $lmonth_cost + ($supply_cost->supply_total);
        }
        $data['lmonth_cost']=$lmonth_cost;
        
        //Get Total Supplies Data
        $this->load->model('Suppliesmodel');
        $return_full_supplies=$this->Suppliesmodel->get_full_supplies();
        $data['full_supplies']=$return_full_supplies->num_rows();
        
        $full_cost='0';
        foreach($return_full_supplies->result() as $supply_cost){
            
            $full_cost = $full_cost + ($supply_cost->supply_total);
        }
        $data['full_cost']=$full_cost;
        
        //Get Storage Locations
        $this->load->model('Suppliesmodel');
        $data['locations']=$this->Suppliesmodel->get_locations();
        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('manage_supplies', $data);
            $this->load->view('components/footer'); 
        }
        else{
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('manage_supplies', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function add_supply(){
        
        $supplier_id = $_POST['supplier_id'];
        $item_id = $_POST['item_id'];
        
        //Get Supplier Name
        $this->load->model('Suppliesmodel');
        $supplier_name=$this->Suppliesmodel->get_supplier($supplier_id)->supplier_name;
        
        //Get Item Name
        $this->load->model('Suppliesmodel');
        $item_name=$this->Suppliesmodel->get_item($item_id)->item_name;
        $item_unit_cost=$this->Suppliesmodel->get_item($item_id)->item_cost_price;
        
        //Get Inventory
        $this->load->model('Suppliesmodel');
        $inventory=$this->Suppliesmodel->get_inventory($item_id);
        
        if(date('Y-m-d', strtotime ($_POST['item_expiry_date'])) == '1970-01-01'){
            
            $expiry_date = '0';
        }
        else{
            
            $expiry_date = date('Y-m-d', strtotime ($_POST['item_expiry_date']));
        }
                
        $supply = array(
            
            'supplier_id' => $_POST['supplier_id'],
            'supplier_name' => $supplier_name,
            'item_id' => $_POST['item_id'],
            'item_name' => $item_name,
            'item_qty_order' => $_POST['item_qty_order'],
            'item_qty_supplied' => $_POST['item_qty_supplied'],
            'supply_date' => date('Y-m-d', strtotime ($_POST['supply_date'])),
            'item_expiry_date' => $expiry_date,
            'location_id' => $_POST['location_id'],
            'supply_remarks' => $_POST['supply_remarks'],
            'supply_total' => ($_POST['item_qty_supplied'])* $item_unit_cost,
        );
        
        //Update on Database
        $this->load->model('Suppliesmodel');
        $supply_data=$this->Suppliesmodel->add_supply($supply);
        
        //print_r($supply_data);
        //echo '<br/>';
        //print_r($supply_data['supply_status']);
        //die();
        
        if($supply_data['supply_status'] == '1'){
            
            //Update Inventory Database
            
            foreach($inventory as $invent){
                //location id == 0 && item_quantity == 0
                if($invent->location_id == 0 && $invent->item_quantity == 0){

                    //$inventory_worth = ($item_unit_cost * ($_POST['item_qty_supplied']));
                    $inventory_id = $invent->inventory_id;
                    
                    $inventorydb = array(

                        'item_id' => $_POST['item_id'],
                        'item_quantity' => $_POST['item_qty_supplied'],
                        'location_id' => $_POST['location_id'],
                        'item_total_worth' => ($item_unit_cost * ($_POST['item_qty_supplied'])),
                    );

                    //Update on Database
                    $this->load->model('Suppliesmodel');
                    $this->Suppliesmodel->update_inventory($inventorydb, $inventory_id);
                }
                //location id == Given in POST (Quantity could be any)
                elseif($invent->location_id == $_POST['location_id']){
                    
                    $inventory_id = $invent->inventory_id;
                    
                    $inventorydb = array(

                        'item_id' => $_POST['item_id'],
                        'item_quantity' => ($invent->item_quantity)+($_POST['item_qty_supplied']),
                        'item_total_worth' => ($invent->item_total_worth) + ($item_unit_cost * ($_POST['item_qty_supplied'])),
                    );
                    
                    //Update on Database
                    $this->load->model('Suppliesmodel');
                    $this->Suppliesmodel->update_inventory($inventorydb, $inventory_id);
                }
                //location id !== Given in POST (Quantity could be any). Insert as new field
                else{
                        
                    $inventorydb = array(

                    'item_id' => $_POST['item_id'],
                    'item_quantity' => ($_POST['item_qty_supplied']),
                    'location_id' => $_POST['location_id'],
                    'item_total_worth' => ($item_unit_cost * ($_POST['item_qty_supplied'])),
                    );
                        
                    //Update on Database
                    $this->load->model('Suppliesmodel');
                    $this->Suppliesmodel->add_inventory($inventorydb);
                }
                
                
            }
            /********************************Order To Notification Table Start****************************************/
        
            $notification=array(

                'description' => 'New Supplies Has been Added By '.$_SESSION['user_fullname'],
                'path' => 'supplies/view_supply',
                'post_id' => $supply_data['supply_id'],
                'created_on' => date('Y-m-d H:i:s'),
                'status' => '0',
            );

            //Update on Database
            $this->load->model('Notificationsmodel');
            $this->Notificationsmodel->add_notification($notification);
        
        
            /********************************Order To Notification Table End****************************************/
            
            $this->session->set_flashdata('success-supplies-add', 'Successfully added the Supplies');
            redirect('supplies/index');
        }
        else{
            
            $this->session->set_flashdata('error-supplies-add', 'Error occurred while adding new Supplies. Please redo !');
            redirect('supplies/index');
        }
    }
    
    public function view_supply($supply_id){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='View Supply';
        
        //Get Supply Details
        $this->load->model('Suppliesmodel');
        $data['supply']=$this->Suppliesmodel->get_supply($supply_id);
        
        //Get Supplier Details
        $supplier_id = $data['supply']->supplier_id;
        $this->load->model('Suppliesmodel');
        $data['supplier']=$this->Suppliesmodel->get_supplier_details($supplier_id);
        
        //Get Item Details
        $item_id = $data['supply']->item_id;
        $this->load->model('Suppliesmodel');
        $data['item']=$this->Suppliesmodel->get_item_details($item_id);
        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('view_supply', $data);
            $this->load->view('components/footer'); 
        }
        else{
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('view_supply', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function delete_supply(){
        
        $supply_id = $_POST['supplies_id'];
        
        //Get Supply Details
        $this->load->model('Suppliesmodel');
        $supply=$this->Suppliesmodel->get_supply($supply_id);
        
        $item_id = $supply->item_id;
        $location = $supply->location_id;
        $supply_qty = $supply->item_qty_supplied;
        
        $this->load->model('Suppliesmodel');
        $inventory=$this->Suppliesmodel->get_inventorydb($item_id, $location);
        
        $inventory_id = $inventory->inventory_id;
        $qty_check=($inventory->item_quantity)-$supply_qty;
        
        if($qty_check<0){
            
            $inventorydb=array(
                
                'item_quantity' => '0',
                'item_total_worth' => '0',
                'location_id' => '0'
            );
            
            $this->load->model('Suppliesmodel');
            $status=$this->Suppliesmodel->delete_supply($supply_id);
            
            if($status == TRUE){
                
                $this->load->model('Suppliesmodel');
                $this->Suppliesmodel->update_inventorydb($inventorydb, $inventory_id);
                
                $this->session->set_flashdata('success-supplies-edit', 'Successfully Deleted the Supply & Updated the Inventory');
                redirect('supplies/index');
                
            }
            else{
                
                $this->session->set_flashdata('error-supplies-edit', 'Error occurred while Deleting the Supplies. Please redo');
                redirect('supplies/index');
            }
        }
        else{
            
            $inventorydb = array(
                
                'item_quantity' => ($inventory->item_quantity)-($supply_qty),
                'item_total_worth' => ($inventory->item_total_worth)-($supply->supply_total),
            );
            
            $this->load->model('Suppliesmodel');
            $status=$this->Suppliesmodel->delete_supply($supply_id);
            
            if($status == TRUE){
                
                $this->load->model('Suppliesmodel');
                $this->Suppliesmodel->update_inventorydb($inventorydb, $inventory_id);
                
                $this->session->set_flashdata('success-supplies-edit', 'Successfully Deleted the Supply & Updated the Inventory');
                redirect('supplies/index');
                
            }
            else{
                
                $this->session->set_flashdata('error-supplies-edit', 'Error occurred while Deleting the Supplies. Please redo');
                redirect('supplies/index');
            }
        }
        
        
    }
}

