<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Manageorders extends CI_Controller{
    
    public function __construct(){
        
        parent::__construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Manage Orders';
        
        $user_id=$_SESSION['user_id'];
        
        //Get Order Details from Database
        $this->load->model('Manageordermodel');
        $data['orders']=$this->Manageordermodel->get_all_orders();
        

        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('manage_orders', $data);
            $this->load->view('components/footer'); 
        }
        else{
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('manage_orders', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function edit_order($order_id){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Manage Orders';
        
        $user_id=$_SESSION['user_id'];
        
        
        //Get Order Details from Database
        $this->load->model('Manageordermodel');
        $data['order']=$this->Manageordermodel->get_order_details($order_id);
        
        
        //Get Buffet Details
        $buffet_name = $data['order']->order_buffet;
        $this->load->model('Manageordermodel');
        $data['buffet']=$this->Manageordermodel->get_buffet($buffet_name);
        
        //Get Order Courses
        $this->load->model('Manageordermodel');
        $data['order_courses']=$this->Manageordermodel->get_order_courses($order_id);
        
        //Get Course Items and append in array
        $a=0;
        foreach($data['order_courses'] as $odcos){
            
            $order_courses_id = $odcos->order_courses_id;
            
            //Get Items From Database
            $this->load->model('Manageordermodel');
            $order_items=$this->Manageordermodel->get_order_items($order_courses_id);
            
            $data['order_courses'][$a]->order_items=$order_items;
            $a++;
        }
        
        //Get Addon Items Details
        $this->load->model('Manageordermodel');
        $data['addons']=$this->Manageordermodel->get_addons($order_id);
        
        //Total number of Items
        $data['nos_addons']=count($data['addons']);
        
        //Total number of Nulls
        $i=0;
        $data['total_null']=0;
        foreach($data['addons'] as $addon){
            
            if($addon->order_add_items_pax == 0){
                
                $i++;
                $data['total_null']=$i;
            }
        }
        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('edit_order', $data);
            $this->load->view('components/footer'); 
        }
        else{
                
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('edit_order', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function update_discount(){
        
        

        $order_id = $_POST['order_id'];
        $order_discount = $_POST['order_disc'];
        
        $this->form_validation->set_rules('order_disc', 'Discount Value', 'required|numeric');
        
        if($this->form_validation->run()==TRUE){
            
            //Get Basic Settings Details
            $this->load->model('Settingsmodel');
            $return_settings=$this->Settingsmodel->get_settings();
            $data['settings']=$return_settings->row();

            //Get Order Details
            $this->load->model('Manageordermodel');
            $order=$this->Manageordermodel->get_order_details($order_id);

            $order_discount_amount = $order->order_stotal * (($order_discount)/100);

            if($data['settings']->entity_gst == 0){

                $order_gst = 0;
            }
            else{

                $order_gst = (($order->order_stotal - $order_discount_amount) + $order->order_delivery )* (7/100);

            }

            $order_gtotal = (($order->order_stotal - $order_discount_amount) + $order->order_delivery )+ ($order_gst);

            //echo $order->order_stotal.'<br/>'.$order_discount_amount.'<br/>'.$order_gst.'<br/>'.$order_gtotal;


            $update_discount = array(

                'order_id' => $order_id,
                'order_discount' => $order_discount,
                'order_gst' => $order_gst,
                'order_gtotal' => $order_gtotal,
            );

            //Update Database
            $this->load->model('Manageordermodel');
            $discount=$this->Manageordermodel->update_discount($update_discount);

            if($discount == TRUE){

                $this->session->set_flashdata('order-success', 'Successfully updated Discount');
                redirect('manageorders/edit_order/'.$order_id);
            }
            else{

                $this->session->set_flashdata('order-error', 'Error occurred while Updating Discount. Please redo !');
                redirect('manageorders/edit_order/'.$order_id);
            }
        }
        
        else{
            
            $error = validation_errors();
            $this->session->set_flashdata('order-error', $error);
            redirect('manageorders/edit_order/'.$order_id);
        }
        
    }
    
    public function update_delivery(){
        
        $order_id = $_POST['order_id'];
        $order_delivery = $_POST['order_delivery'];
        
        $this->form_validation->set_rules('order_delivery', 'Delivery Charges', 'required|numeric');
        
        if($this->form_validation->run() == TRUE){
            
            //Get Basic Settings Details
            $this->load->model('Settingsmodel');
            $return_settings=$this->Settingsmodel->get_settings();
            $data['settings']=$return_settings->row();

            //Get Order Details
            $this->load->model('Manageordermodel');
            $order=$this->Manageordermodel->get_order_details($order_id);
            
            //Calculate Gtotal
            if($data['settings']->entity_gst == 0){

                $order_gst = 0;
            }
            else{

                $order_gst = (($order->order_stotal - ($order->order_stotal * $order->order_discount/100))+ $order_delivery) * (7/100);

            }

            $order_gtotal = (($order->order_stotal - ($order->order_stotal * $order->order_discount/100))+ $order_delivery) + ($order_gst);
            
            $update_delivery = array(
                
                'order_id' => $order_id,
                'order_delivery' => $order_delivery,
                'order_gst' => $order_gst,
                'order_gtotal' => $order_gtotal,
            );
            
            //Update on Database
            $this->load->model('Manageordermodel');
            $delivery = $this->Manageordermodel->update_delivery($update_delivery);
            
            if($delivery == TRUE){

                $this->session->set_flashdata('order-success', 'Successfully updated Delivery Caharges');
                redirect('manageorders/edit_order/'.$order_id);
            }
            else{

                $this->session->set_flashdata('order-error', 'Error occurred while Updating Delivery Charges. Please redo !');
                redirect('manageorders/edit_order/'.$order_id);
            }
            
        }
        
        else{
            
            $error = validation_errors();
            $this->session->set_flashdata('order-error', $error);
            redirect('manageorders/edit_order/'.$order_id);
        }
    }
    
    public function update_order(){
        
        $order_id=$_POST['order_id'];

        $this->form_validation->set_rules('customer_name', 'Customer Name', 'required');
        $this->form_validation->set_rules('customer_contact', 'Customer Contact', 'required');
        $this->form_validation->set_rules('customer_email', 'Customer Email', 'required');
        $this->form_validation->set_rules('address_block', 'Block/Building Name', 'required');
        $this->form_validation->set_rules('address_postal', 'Postal Code', 'required');
        $this->form_validation->set_rules('address_street', 'Street Address', 'required');
        $this->form_validation->set_rules('dining_date', 'Dining Date', 'required');
        $this->form_validation->set_rules('dining_time', 'Dining Time', 'required');
        
        if($this->form_validation->run()==TRUE){
        
            $order_form = array(

                'customer_name' => $_POST['customer_name'],
                'customer_contact' => $_POST['customer_contact'],
                'customer_email' => $_POST['customer_email'],
                'address_block' => $_POST['address_block'],
                'address_postal' => $_POST['address_postal'],
                'address_street' => $_POST['address_street'],
                'dining_date' => $_POST['dining_date'],
                'dining_time' => $_POST['dining_time']
            );
            
            //Update Database
            $this->load->model('Manageordermodel');
            $status=$this->Manageordermodel->update_order($order_form, $order_id);

            if($status==TRUE){

                $this->session->set_flashdata('order-success', 'Successfully update order details');
                redirect('manageorders/edit_order/'.$order_id);
            }
            else{

                $this->session->set_flashdata('order-error', 'Error occurred while updating the Details. Please redo !');
                redirect('manageorders/edit_order/'.$order_id);
            }
        
        }
        
        else{
            
            $errors = validation_errors();
            $this->session->set_flashdata('order-error', $errors);
            redirect('manageorders/edit_order/'.$order_id);
        }
    }
    
    public function update_order_pax($order_id){
        
        $buffet_min_pax = $_POST['buffet_min_pax'];
        $new_min_pax = $_POST['new_min_pax'];
        
        if($buffet_min_pax > $new_min_pax){
            
            $this->session->set_flashdata('order-error', 'Pax updated is lower than minimum allowed pax ( ie: '.$buffet_min_pax .' ) for this Buffet');
            redirect('manageorders/edit_order/'.$order_id);
        }
        else{
            
            $old_min_pax = $_POST['old_min_pax'];
            $order_stotal = $_POST['order_stotal'];
            $buffet_price = $_POST['order_buffet_price'];
            $order_gst = $_POST['order_gst'];
            $order_discount = $_POST['order_discount'];
            
            //Get Order Details
            $this->load->model('Manageordermodel');
            $order=$this->Manageordermodel->get_order_details($order_id);

            $addons_total = ($order_stotal) - ($old_min_pax * $buffet_price);
            $new_stotal = ($new_min_pax * $buffet_price) + $addons_total;

            $new_discount_total = $new_stotal * ($order_discount/100);

            if($order_gst == 0){

                $new_order_gst = 0;
            }
            else{

                $new_order_gst = (($new_stotal-$new_discount_total) + $order->order_delivery )* (7/100);
            }

            $new_order_gtotal = ($new_stotal-$new_discount_total) + $order->order_delivery + $new_order_gst;

            //Append date into new array
            $update_order_pax = array(

                'order_pax' => $new_min_pax,
                'order_stotal' => $new_stotal,
                'order_gst' => $new_order_gst,
                'order_discount' => $order_discount,
                'order_gtotal' => $new_order_gtotal,
            );

            //Update on Database
            $this->load->model('Manageordermodel');
            $update_status=$this->Manageordermodel->update_order_pax($update_order_pax, $order_id);

            if($update_status == TRUE){

                $this->session->set_flashdata('order-success', 'Successfully updated Pax');
                redirect('manageorders/edit_order/'.$order_id);
            }
            else{

                $this->session->set_flashdata('order-error', 'Error occurred while updating number of pax');
                redirect('manageorders/edit_order/'.$order_id);
            }
            
        }
        
    }
    
    public function update_addon_pax(){
        
        $new_addon_pax = $_POST['new_addon_pax'];
        $addon_item_id = $_POST['addon_item_id'];
        $addon_item_price = $_POST['addon_item_price'];
        $addon_order_id=$_POST['addon_order_id'];
        
        $addon_pax = array(
            
            'order_add_items_pax' => $new_addon_pax,
        );
        
        //Update Addon Pax on Database Table
        $this->load->model('Manageordermodel');
        $this->Manageordermodel->update_addon_pax($addon_pax, $addon_item_id);
        
        
        //Get Addon items from Database Table
        $order_id = $addon_order_id;
        $this->load->model('Manageordermodel');
        $addons=$this->Manageordermodel->get_addons($order_id);
        
        $addon_total=0;
        foreach($addons as $addon){
            
            $addon_pax = $addon->order_add_items_pax;
            $addon_price = $addon->order_add_items_price;
            
            $addon_total = $addon_total + ($addon_pax * $addon_price);
        }
        
        //Get Order Details
        $this->load->model('Manageordermodel');
        $order=$this->Manageordermodel->get_order_details($order_id);
        
        $order_stotal = ($order->order_pax * $order->order_buffet_price) + $addon_total;
        
        $discount_total = $order_stotal * ($order->order_discount/100);
        
        if($order->order_gst == 0){
            
            $order_gst = 0;
        }
        else{
            
            $order_gst = (($order_stotal - $discount_total) + $order->order_delivery )* (7/100);
        }
        
        $order_gtotal = ($order_stotal - $discount_total) + $order->order_delivery + $order_gst;
        
        //Append on Array
        $order_form = array(
            
            'order_stotal' => $order_stotal,
            'order_gst' => $order_gst,
            'order_gtotal' => $order_gtotal,
        );
        
        //Update on Database Table
        $this->load->model('Manageordermodel');
        $status = $this->Manageordermodel->update_order($order_form, $order_id);
        
        if($status == TRUE){
            
            $this->session->set_flashdata('order-success', 'Successfully updated Addon Pax');
            redirect('manageorders/edit_order/'.$order_id);
        }
        else{
            
            $this->session->set_flashdata('order-error', 'Error occurred while updating Addon Pax. Please redo !');
            redirect('manageorders/edit_order/'.$order_id);
        }
    }
    
    
    public function update_status(){
        
        $order_id = $_POST['order_id'];
        $order_status = array(
            
            'order_status' => $_POST['order_status'],
            );
        
        //Update Database Table
        $this->load->model('Manageordermodel');
        $status=$this->Manageordermodel->update_order_status($order_id, $order_status);
        
        if($status == TRUE){
            
            $this->session->set_flashdata('order-success', 'Successfully Updated order Status');
            redirect('manageorders/edit_order/'.$order_id);
        }
        else{
            
            $this->session->set_flashdata('order-error', 'Error occurred while updating Order Status. Please redo !');
            redirect('manageorders/edit_order/'.$order_id);
        }
    }
    
    public function delete_order($order_id){
        
        //Get Order Details
        $this->load->model('Manageordermodel');
        $order=$this->Manageordermodel->get_order_details($order_id);
             
         //Get Order Courses Ids
         $this->load->model('Manageordermodel');
         $courses_ids=$this->Manageordermodel->get_courses_ids($order_id);


         //Delete order (Order Table, Order Addons Table, Order Courses Table)
         $this->load->model('Manageordermodel');
         $delete_order=$this->Manageordermodel->delete_order($order_id);

         //Delete Order Items (Order items Table)
         foreach($courses_ids as $ids){

             $id=$ids->order_courses_id;

             //Delete Course Items
             $this->load->model('Manageordermodel');
             $this->Manageordermodel->delete_course_items($id);

         }

         if($delete_order == TRUE){

             $this->session->set_flashdata('order-success', 'Successfully deleted the Order');
             redirect('manageorders/index');
         }
         else{

             $this->session->set_flashdata('order-error', 'Error occured while deleting the Order. Please redo !');
             redirect('manageorders/index');
         }
         
    }
    
    public function delete_order_list(){
        
        $order_id = $_POST['order_id'];
        
        //Get Order Details
        $this->load->model('Manageordermodel');
        $order=$this->Manageordermodel->get_order_details($order_id);
        
        
         if($order->order_status==0){
             
             //Get Order Courses Ids
             $this->load->model('Manageordermodel');
             $courses_ids=$this->Manageordermodel->get_courses_ids($order_id);
             
             
             //Delete order (Order Table, Order Addons Table, Order Courses Table)
             $this->load->model('Manageordermodel');
             $delete_order=$this->Manageordermodel->delete_order($order_id);
             
             //Delete Order Items (Order items Table)
             foreach($courses_ids as $ids){
                 
                 $id=$ids->order_courses_id;
                 
                 //Delete Course Items
                 $this->load->model('Manageordermodel');
                 $this->Manageordermodel->delete_course_items($id);
                 
             }
             
             if($delete_order == TRUE){
                 
                 $this->session->set_flashdata('order-success', 'Successfully deleted the Order');
                 redirect('manageorders/index');
             }
             else{
                 
                $this->session->set_flashdata('order-error', 'Error occured while deleting the Order. Please redo !');
                redirect('manageorders/index');
             }
         }
         
         elseif($order->order_status==1){
             
             $this->session->set_flashdata('order-error', 'Cant Delete this Order as it has already "Approved"');
             redirect('manageorders/index');
         }
         
         elseif($order->order_status==2){
            
            $this->session->set_flashdata('order-error', 'Cant Delete this Order as it has already "Cancelled"');
            redirect('manageorders/index');
         }
         
    }
    
    public function view_order($order_id){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Manage Orders';
        
        $user_id=$_SESSION['user_id'];
        
        
        //Get Order Details from Database
        $this->load->model('Manageordermodel');
        $data['order']=$this->Manageordermodel->get_order_details($order_id);
        
        //Get Buffet Details
        $buffet_name = $data['order']->order_buffet;
        $this->load->model('Manageordermodel');
        $data['buffet']=$this->Manageordermodel->get_buffet($buffet_name);
        
        //Get Order Courses
        $this->load->model('Manageordermodel');
        $data['order_courses']=$this->Manageordermodel->get_order_courses($order_id);
        
        //Get Course Items and append in array
        $a=0;
        foreach($data['order_courses'] as $odcos){
            
            $order_courses_id = $odcos->order_courses_id;
            
            //Get Items From Database
            $this->load->model('Manageordermodel');
            $order_items=$this->Manageordermodel->get_order_items($order_courses_id);
            
            $data['order_courses'][$a]->order_items=$order_items;
            $a++;
        }
        
        //Get Addon Items Details
        $this->load->model('Manageordermodel');
        $data['addons']=$this->Manageordermodel->get_addons($order_id);
        
        //Total number of Items
        $data['nos_addons']=count($data['addons']);
        
        //Total number of Nulls
        $i=0;
        $data['total_null']=0;
        foreach($data['addons'] as $addon){
            
            if($addon->order_add_items_pax == 0){
                
                $i++;
                $data['total_null']=$i;
            }
        }
        
        if($_SESSION['user_type']==1){
            
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('view_order', $data);
            $this->load->view('components/footer'); 
        }
        else{
                
            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('view_order', $data);
            $this->load->view('components/footer');
        }
    }
   
}
