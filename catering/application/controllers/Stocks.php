<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Stocks extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        if(!isset($_SESSION['active'])){
            session_destroy();
            redirect(base_url());
        }
    }
    
    public function index(){
        
        //Get Basic Settings Details
        $this->load->model('Settingsmodel');
        $return_settings=$this->Settingsmodel->get_settings();
        $data['settings']=$return_settings->row();
        $data['settings']->pagetitle='Stocks Management';
        
        //Get Data from Inventory Table
        $this->load->model('Stocksmodel');
        $data['stocks']=$this->Stocksmodel->get_stocks();
        
        //Get Locations
        $this->load->model('Stocksmodel');
        $data['locations']=$this->Stocksmodel->get_locations();
        
        //Populate Data On Datatable
        $x=0;
        foreach($data['stocks'] as $stocks){
            
            $item_id = $stocks->item_id;
            $location_id = $stocks->location_id;
            
            //Get Item Details from Database
            $this->load->model('Stocksmodel');
            $item=$this->Stocksmodel->get_item($item_id);
            
            //Get Location Details from Database
            $this->load->model('Stocksmodel');
            $location=$this->Stocksmodel->get_location($location_id);
            
            $data['stocks'][$x]->item = $item;
            $data['stocks'][$x]->location = $location;
            $x++;
        }
        
        //Summay of Stock
        $this->load->model('Stocksmodel');
        $data['items']=$this->Stocksmodel->get_inventory_items();
        
        //Calculation of Item Worth & Stock Percentage Based on Last Supply
        $i=0;
        $total_stock_worth = 0;
        foreach($data['items'] as $item){
            
            $item_id = $item->item_id;
            $item_unit_price = $item->item_cost_price;
            
            $this->load->model('Stocksmodel');
            $itms=$this->Stocksmodel->get_items($item_id);
            
            $item_worth = '0';
            $item_quantity = '0';
            
            foreach($itms as $itm){
                
                $item_worth = ($item_worth) + ($item_unit_price * ($itm->item_quantity));
                $item_quantity = ($item_quantity) + ($itm->item_quantity);
            }
            
            $data['items'][$i]->item_worth = $item_worth;
            $data['items'][$i]->item_quantity = $item_quantity;
            $i++;
            
            $total_stock_worth = $total_stock_worth + $item_worth;
        }
        
        $data['total_stock_worth'] = $total_stock_worth;
        
        if($_SESSION['user_type']==1){

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/admin/main_menu');
            $this->load->view('manage_stocks', $data);
            $this->load->view('components/footer');
        }
        else{

            $this->load->view('components/header', $data);
            $this->load->view('components/navigation/employee/main_menu');
            $this->load->view('manage_stocks', $data);
            $this->load->view('components/footer');
        }
    }
    
    public function adjust_quantity(){
        
        $inventory_id = $_POST['inventory_id'];
        $item_cost_price = $_POST['item_cost'];
        
        $inventory=array(
            
            'item_quantity' => $_POST['item_quantity'],
            'item_total_worth' => $_POST['item_quantity'] * $item_cost_price,
        );
        
        //Update on Database
        $this->load->model('Stocksmodel');
        $status=$this->Stocksmodel->adjust_quantity($inventory_id, $inventory);
        
        if($status == TRUE){
            
            $this->session->set_flashdata('success-stock', 'Successfully Updated the Stock Quantity');
            redirect('stocks/index');
        }
        
        else{
            
            $this->session->set_flashdata('error-stock', 'Error occurred while updatinght Stock Quantity. Please redo !');
            redirect('stocks/index');
        }
    }
    
    public function update_location(){
        
        $inventory_id = $_POST['inventory_id'];
        $item_id = $_POST['item_id'];
        $item_location = $_POST['location_id'];
        $item_quantity = $_POST['item_quantity'];
        $item_total_worth = $_POST['item_total_worth'];
        
        
        //Get Item Details From Database
        $this->load->model('Stocksmodel');
        $inventory=$this->Stocksmodel->get_inventory($inventory_id);
        
        if($item_quantity <= 0){
            
            $this->session->set_flashdata('error-stock', 'Entry cant be 0 or less than 0.' );
            redirect('stocks/index');
        }
        
        elseif($item_quantity > $inventory->item_quantity){
            
            $this->session->set_flashdata('error-stock', 'Quantity entered is more than the available Quantity at the Location' );
            redirect('stocks/index');
        }
        
        elseif($item_location == $inventory->location_id){
            
            $this->session->set_flashdata('error-stock', 'You cant select the Same location to move the Item' );
            redirect('stocks/index');
        }
        
        elseif($item_quantity == $inventory->item_quantity){
            
            //Check Item Recurrance on Database
            $this->load->model('Stocksmodel');
            $inventory_item=$this->Stocksmodel->get_inventory_item($item_id);

            foreach($inventory_item as $item){
                
                if($item_location == $item->location_id){
                    
                    $inventory_item = array(
                        
                        'item_quantity' => $item->item_quantity + $item_quantity,
                        'item_total_worth' => $item->item_total_worth + $item_total_worth,
                    );
                    
                    $new_inventory_id = $item->inventory_id.'<br/>';
                    
                    //Update Database
                    $this->load->model('Stocksmodel');
                    $status = $this->Stocksmodel->update_location($inventory_item, $new_inventory_id);
                    
                    if($status == TRUE){
                        
                        $this->load->model('Stocksmodel');
                        $this->Stocksmodel->delete_inventory($inventory_id);
                        
                        $this->session->set_flashdata('success-stock', 'Successfully moved Stock into new Location');
                        redirect('stocks/index');
                    }
                    else{
                        
                        $this->session->set_flashdata('error-stock', 'Error occurred while updating the Stock to new Location. Please try again !');
                        redirect('stocks/index');
                    }
                    
                }
                
            }
                    
            $inventory_item = array(

                'item_id' => $item_id,
                'item_quantity' => $item_quantity,
                'location_id' => $item_location,
                'item_total_worth' => $item_total_worth,

            );
            
            //Insert into Database
            $this->load->model('Stocksmodel');
            $status = $this->Stocksmodel->insert_location($inventory_item);
            
            if($status == TRUE){
                        
                $this->load->model('Stocksmodel');
                $this->Stocksmodel->delete_inventory($inventory_id);

                $this->session->set_flashdata('success-stock', 'Successfully moved Stock into new Location');
                redirect('stocks/index');
            }
            else{

                $this->session->set_flashdata('error-stock', 'Error occurred while updating the Stock to new Location. Please try again !');
                redirect('stocks/index');
            }
            
        }
        
        elseif($item_quantity < $inventory->item_quantity){
            
            //Check Item Recurrance on Database
            $this->load->model('Stocksmodel');
            $inventory_item=$this->Stocksmodel->get_inventory_item($item_id);
            
            
            //Get item Unit Price
            $this->load->model('Stocksmodel');
            $item_price=$this->Stocksmodel->get_item($item_id);

            foreach($inventory_item as $item){
                
                
                if($item_location == $item->location_id){
                    
                    $inventory_new_item = array(
                        
                        'item_quantity' => $item->item_quantity + $item_quantity,
                        'item_total_worth' => $item->item_total_worth + ($item_price->item_cost_price * $item_quantity),
                    );
                    
                    $inventory_old_item = array(
                        
                        'item_quantity' => $inventory->item_quantity - $item_quantity,
                        'item_total_worth' => $inventory->item_total_worth - ($item_price->item_cost_price * $item_quantity),
                    );
                    
                    $new_inventory_id = $item->inventory_id.'<br/>';
                    
                    //Update Database
                    $this->load->model('Stocksmodel');
                    $status=$this->Stocksmodel->update_new_location($inventory_new_item, $new_inventory_id);
                    
                    if($status == TRUE){
                        
                        $this->load->model('Stocksmodel');
                        $this->Stocksmodel->update_old_location($inventory_old_item, $inventory_id);
                        
                        $this->session->set_flashdata('success-stock', 'Successfully moved Stock into new Location');
                        redirect('stocks/index');
                    }
                    else{
                        
                        $this->session->set_flashdata('error-stock', 'Error occurred while updating the Stock to new Location. Please try again !');
                        redirect('stocks/index');
                    }
                    
                }
                
            }
            
            $inventory_item = array(

                'item_id' => $item_id,
                'item_quantity' => $item_quantity,
                'location_id' => $item_location,
                'item_total_worth' => ($item_price->item_cost_price * $item_quantity),

            );
            
            $inventory_old_item = array(

                'item_quantity' => $inventory->item_quantity - $item_quantity,
                'item_total_worth' => $inventory->item_total_worth - ($item_price->item_cost_price * $item_quantity),
            );
            
            //Insert into Database
            $this->load->model('Stocksmodel');
            $status = $this->Stocksmodel->insert_location($inventory_item);
            
            if($status == TRUE){
                
                $this->load->model('Stocksmodel');
                $this->Stocksmodel->update_old_location($inventory_old_item, $inventory_id);

                $this->session->set_flashdata('success-stock', 'Successfully moved Stock into new Location');
                redirect('stocks/index');
            }
            else{

                $this->session->set_flashdata('error-stock', 'Error occurred while updating the Stock to new Location. Please try again !');
                redirect('stocks/index');
            }
        }
        
    }
}
