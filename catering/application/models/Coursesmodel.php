<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Coursesmodel extends CI_Model{

    public function get_categories(){

        $this->db->SELECT('category.category_id, category.category_name, category.category_description, items.item_id, items.item_eng');
        //$this->db->SELECT('*');
        $this->db->JOIN('items', 'items.category_id = category.category_id');
        return $this->db->GET('category');
    }

    public function get_items($category_id){

        $this->db->SELECT('*');
        $this->db->WHERE('category_id', $category_id);
        return $this->db->GET('items');
    }

    public function get_category(){

        $this->db->SELECT('category.category_id, category.category_name, category.category_description');
        //$this->db->SELECT('*');
        //$this->db->JOIN('items', 'items.category_id = category.category_id');
        return $this->db->GET('category');
    }
    
    public function get_sort_id($buffet_id){
        
        $this->db->SELECT('buffet.buffet_course_nos');
        $this->db->WHERE('buffet_id', $buffet_id);
        return $this->db->GET('buffet');
    }
    
    public function add_course($course){
        
        $this->db->INSERT('courses', $course);
        return TRUE;
    }
    
    public function get_course_id($buffet_id, $course_name){
        
        $this->db->SELECT('courses.course_id');
        $this->db->WHERE('buffet_id', $buffet_id);
        $this->db->WHERE('course_name', $course_name);
        return $this->db->GET('courses');
    }
    
    public function add_course_items($items){
        
        $this->db->INSERT('courses_item', $items);
        return TRUE;
    }
    
    public function validate_sort_id($buffet_id, $sort_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('buffet_id', $buffet_id);
        $this->db->WHERE('sort_id', $sort_id);
        return $this->db->GET('courses');
    }
}
