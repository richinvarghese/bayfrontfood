<?php
defined('BASEPATH')OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Addonmodel extends CI_Model{
    
    public function add_addon_item($addon_item){
        
        $this->db->INSERT('addons', $addon_item);
        return TRUE;
    }
    
    public function get_addon_items(){
        
        $this->db->SELECT('*');
        return $this->db->GET('addons');
    }
    
    public function update_addon_item($addon_id, $addon_item){
        
        $this->db->WHERE('addon_id', $addon_id);
        $this->db->UPDATE('addons', $addon_item);
        return TRUE;
    }
    
    public function delete_addon_item($addon_id){
        
        $this->db->WHERE('addon_id', $addon_id);
        $this->db->DELETE('addons');
        return TRUE;
    }
}
