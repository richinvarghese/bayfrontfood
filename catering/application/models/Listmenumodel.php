<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Listmenumodel extends CI_Model{
    
    public function get_buffets($master_id){
        
        //$this->db->SELECT('*');
        $this->db->WHERE('master_cat_id', $master_id);
        return $this->db->GET('buffet')->result();
    }
    
    public function get_buffet($buffet_id){
        
        $this->db->WHERE('buffet_id', $buffet_id);
        return $this->db->GET('buffet')->row();
    }
    
    public function get_addons(){
        
        $this->db->SELECT('*');
        return $this->db->GET('addons')->result();
    }
    
    public function get_courses($buffet_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('buffet_id', $buffet_id);
        return $this->db->GET('courses')->result();
    }
    
    public function get_courses_item($course_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('course_id', $course_id);
        return $this->db->GET('courses_item')->result();
    }
    
    public function get_item($item_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_id', $item_id);
        return $this->db->GET('items')->result();
    }
}
