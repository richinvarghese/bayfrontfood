<?php
defined('BASEPATH')OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Ordermodel extends CI_Model{
    
    public function get_buffet($buffet_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('buffet_id', $buffet_id);
        return $this->db->GET('buffet')->row();
    }
    
    public function get_courses($buffet_id){
        
       $this->db->SELECT('*');
       $this->db->WHERE('buffet_id', $buffet_id);
       return $this->db->GET('courses')->result();
    }
    
    public function get_course_items($course_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('course_id', $course_id);
        return $this->db->GET('courses_item')->result();
    }
    
    public function get_item($id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_id', $id);
        return $this->db->GET('items')->row();
    }
    
    public function get_addon_item($addon_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('addon_id', $addon_id);
        return $this->db->GET('addons')->row();
    }
    
    public function add_order($order_data){
        
        $this->db->INSERT('orders', $order_data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }
    
    public function add_order_course($order_course){
        
        $this->db->INSERT('order_courses', $order_course);
        $insert_course_id = $this->db->insert_id();
        return $insert_course_id;
    }
    
    public function add_order_items($order_items){
        
        $this->db->INSERT('order_items', $order_items);
        return TRUE;
    }
    
    public function add_order_addons($order_addon_items){
        
        $this->db->INSERT('order_add_items', $order_addon_items);
        return TRUE;
    }
    
    public function get_orders(){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_status', '1');
        return $this->db->GET('orders');
    }
}

