<?php
defined('BASEPATH')OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Itemsmodel extends CI_Model{
    
    public function add_item($item){
        
        $this->db->INSERT('items', $item);
        return TRUE;
    }
    
    public function get_items(){
        
        $this->db->SELECT('*');
        $this->db->JOIN('category', 'category.category_id = items.category_id');
        return $this->db->GET('items');
    }
    
    public function item_edit($item, $item_id){
        
        $this->db->WHERE('item_id', $item_id);
        $this->db->UPDATE('items', $item);
        return TRUE;
    }
    
    public function get_item($item_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_id', $item_id);
        return $this->db->GET('items');
    }
    
    public function update_image_name($image, $item_id){
        
        $this->db->WHERE('item_id', $item_id);
        $this->db->UPDATE('items', $image);
        return TRUE;
    }
    
    public function delete_item($item_id){
        
        $this->db->WHERE('item_id', $item_id);
        $this->db->DELETE('items');
        return TRUE;
    }
    
}

