<?php
defined('BASEPATH') OR exit('No direct script allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Inventorymodel extends CI_Model{
    
    
    public function add_unit($unit){
        
        $this->db->INSERT('units', $unit);
        return TRUE;
    }
    
    public function get_units(){
        
        $this->db->SELECT('*');
        return $this->db->GET('units')->result();
    }
    
    public function edit_unit($unit, $unit_id){
        
        $this->db->WHERE('unit_id', $unit_id);
        $this->db->UPDATE('units',$unit);
        return TRUE;
    }
    
    public function delete_unit($unit_id){
        
        $this->db->WHERE('unit_id', $unit_id);
        $this->db->DELETE('units');
        return TRUE;
    }
    
    public function add_location($location){
        
        $this->db->INSERT('inventory_location', $location);
        return TRUE;
    }
    
    public function get_locations(){
        
        $this->db->SELECT('*');
        return $this->db->GET('inventory_location')->result();
    }
    
    public function edit_location($location, $location_id){
        
        $this->db->WHERE('location_id', $location_id);
        $this->db->UPDATE('inventory_location', $location);
        return TRUE;
    }
    
    public function delete_location($location_id){
        
        $this->db->WHERE('location_id', $location_id);
        $this->db->DELETE('inventory_location');
        return TRUE;
    }
    
    public function add_item($item){
        
        $this->db->INSERT('inventory_items', $item);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }
    
    public function get_items(){
        
        $this->db->SELECT('*');
        return $this->db->GET('inventory_items')->result();
    }
    
    public function edit_item($item_id, $item){
        
        $this->db->WHERE('item_id', $item_id);
        $this->db->UPDATE('inventory_items', $item);
        return TRUE;
    }
    
    public function delete_item($item_id){
        
        $this->db->WHERE('item_id', $item_id);
        $this->db->DELETE('inventory_items');
        return TRUE;
    }
    
    public function add_inventory($inventory){
        
        $this->db->INSERT('inventory', $inventory);
        return TRUE;
    }
    
    public function check_location($location_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('location_id', $location_id);
        return $this->db->GET('inventory');
    }
    
    public function get_sku($item_id){
        
        $this->db->SELECT('inventory_items.item_sku, inventory_items.item_name');
        $this->db->WHERE('item_id', $item_id);
        return $this->db->GET('inventory_items')->row();
    }
    
    public function delete_inventory_item($item_id){
        
        $this->db->WHERE('item_id', $item_id);
        $this->db->DELETE('inventory');
        return TRUE;
    }
    
    public function get_inventory(){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_status', '1');
        return $this->db->GET('inventory_items');
    }
    
    public function inventory_locations($item_id){
        
        $this->db->SELECT('inventory_location.location_name, inventory_location.location_stack, inventory.item_quantity, inventory.location_id');
        $this->db->WHERE('item_id', $item_id);
        $this->db->JOIN('inventory_location','inventory_location.location_id = inventory.location_id');
        $this->db->WHERE('location_status', '1');
        return $this->db->GET('inventory');
    }
}
