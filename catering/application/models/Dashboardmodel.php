<?php
defined('BASEPATH')OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Dashboardmodel extends CI_Model{
    
    public function get_orders($month, $day){
        
        $this->db->SELECT('*');
        $this->db->WHERE("DATE_FORMAT(order_date,'%m-%Y')", $month);
        $this->db->WHERE("DATE_FORMAT(order_date,'%d')", $day);
        return $this->db->GET('orders');
    }
    
    public function get_supplies($month, $day){
        
        $this->db->SELECT('*');
        $this->db->WHERE("DATE_FORMAT(supply_date,'%m-%Y')", $month);
        $this->db->WHERE("DATE_FORMAT(supply_date,'%d')", $day);
        return $this->db->GET('supplies');
    }
    
    public function get_nos_orders($month){
        
        $this->db->SELECT('*');
        $this->db->WHERE("DATE_FORMAT(order_date,'%m-%Y')", $month);
        return $this->db->GET('orders');
    }
    
    
    public function get_orders_curr($month){
        
        $this->db->SELECT('*');
        $this->db->WHERE("DATE_FORMAT(order_date,'%m-%Y')", $month);
        return $this->db->GET('orders')->result();
    }
    
    public function get_orders_last($last_month){
        
        $this->db->SELECT('*');
        $this->db->WHERE("DATE_FORMAT(order_date,'%m-%Y')", $last_month);
        return $this->db->GET('orders')->result();
    }
    
    public function get_supplies_curr($month){
        
        $this->db->SELECT('*');
        $this->db->WHERE("DATE_FORMAT(supply_date,'%m-%Y')", $month);
        return $this->db->GET('supplies')->result();
    }
    
    public function get_supplies_last($last_month){
        
        $this->db->SELECT('*');
        $this->db->WHERE("DATE_FORMAT(supply_date,'%m-%Y')", $last_month);
        return $this->db->GET('supplies')->result();
    }
    
    public function get_customers(){
        
        $this->db->SELECT('customer_name, address_street, order_status');
        $this->db->ORDER_BY('order_date', 'DESC');
        $this->db->LIMIT('3');
        return $this->db->GET('orders')->result();
    }
    
    public function get_all_orders(){
        
        $this->db->SELECT('order_gtotal');
        return $this->db->GET('orders')->result();
    }
    
    public function get_all_supplies(){
        
        $this->db->SELECT('supply_total');
        return $this->db->GET('supplies')->result();
    }
    
    public function get_buffet_orders(){
        
        $this->db->SELECT('buffet_name');
        return $this->db->GET('buffet')->result();
    }
    
    public function get_nos_buffets($order_buffet){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_buffet', $order_buffet);
        return $this->db->GET('orders');
    }
    
    public function getall_orders(){
        
        $this->db->SELECT('orders.order_buffet, orders.dining_date, orders.dining_time');
        $this->db->WHERE('order_status', '1');
        return $this->db->GET('orders')->result();
    }
}
