<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Displaymodel extends CI_Model{
    
    public function get_orders(){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_status', '1');
        return $this->db->GET('orders')->result();
    }
    
    public function get_courses($order_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_id', $order_id);
        return $this->db->GET('order_courses')->result();
    }
    
    public function get_course_items($order_course_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_courses_id', $order_course_id);
        return $this->db->GET('order_items')->result();
    }
    
    public function get_addons($order_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_id', $order_id);
        return $this->db->GET('order_add_items')->result();
    }
}