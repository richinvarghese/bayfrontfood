<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Coursemanage_model extends CI_Model{
    
    public function get_courses($buffet_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('buffet_id', $buffet_id);
        return $this->db->GET('courses');
    }
    
    public function get_item_ids($course_id){
        
        $this->db->SELECT('courses_item.item_id');
        $this->db->WHERE('course_id', $course_id);
        return $this->db->GET('courses_item');
    }
    
    public function get_course_items($course_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('course_id', $course_id);
        return $this->db->GET('courses_item');
    }
    
    public function get_item_data($item_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_id', $item_id);
        return $this->db->GET('items');
    }
    
    public function delete_course_item($item_id, $course_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('course_id', $course_id);
        $this->db->WHERE('item_id', $item_id);
        $this->db->DELETE('courses_item');
        return TRUE;
    }
    
    public function delete_course($course_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('courses.course_id', $course_id);
        $this->db->DELETE('courses');
        $this->db->WHERE('courses_item.course_id', $course_id);
        $this->db->DELETE('courses_item');
        return TRUE;
    }
    
    public function get_categories(){
        
        $this->db->SELECT('*');
        return $this->db->GET('category')->result();
    }
    
    public function get_items($category_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('category_id', $category_id);
        return $this->db->GET('items')->result();
    }
    
    public function check_items($course_id, $item_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('course_id', $course_id);
        $this->db->WHERE('item_id', $item_id);
        return $this->db->GET('courses_item');
    }
    
    public function add_course_item($course_item){
        
        $this->db->INSERT('courses_item', $course_item);
        return TRUE;
    }
    
    public function check_course_exist($buffet_id){
        
        $this->db->SELECT('course_id');
        $this->db->WHERE('buffet_id', $buffet_id);
        return $this->db->GET('courses');
    }
    
}
