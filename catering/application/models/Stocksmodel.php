<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Stocksmodel extends CI_Model{
    
    public function get_stocks(){
        
        $this->db->SELECT('*');
        return $this->db->GET('inventory')->result();
    }
    
    public function get_item($item_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_id', $item_id);
        return $this->db->GET('inventory_items')->row();
    }
    
    public function get_location($location_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('location_id', $location_id);
        return $this->db->GET('inventory_location')->row();
    }
    
    public function get_inventory_items(){
        
        $this->db->SELECT('*');
        return $this->db->GET('inventory_items')->result();
    }
    
    public function get_items($item_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_id', $item_id);
        return $this->db->GET('inventory')->result();
    }
    
    public function adjust_quantity($inventory_id, $inventory){
        
        $this->db->WHERE('inventory_id', $inventory_id);
        $this->db->UPDATE('inventory', $inventory);
        return TRUE;
    }
    
    public function get_locations(){
        
        $this->db->SELECT('*');
        $this->db->WHERE('location_status', '1');
        return $this->db->GET('inventory_location')->result();
    }
    
    public function get_inventory($inventory_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('inventory_id', $inventory_id);
        return $this->db->GET('inventory')->row();
    }
    
    public function get_inventory_item($item_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_id', $item_id);
        return $this->db->GET('inventory')->result();
    }
    
    
    public function update_location($inventory_item, $new_inventory_id){
        
        $this->db->WHERE('inventory_id', $new_inventory_id);
        $this->db->UPDATE('inventory', $inventory_item);
        return TRUE;
    }
    
    public function delete_inventory($inventory_id){
        
        $this->db->WHERE('inventory_id', $inventory_id);
        $this->db->DELETE('inventory');
        return TRUE;
    }
    
    public function insert_location($inventory_item){
        
        $this->db->INSERT('inventory', $inventory_item);
        return TRUE;
    }
    
    public function update_new_location($inventory_new_item, $new_inventory_id){
        
        $this->db->WHERE('inventory_id', $new_inventory_id);
        $this->db->UPDATE('inventory', $inventory_new_item);
        return TRUE;
    }
    
    public function update_old_location($inventory_old_item, $inventory_id){
        
        $this->db->WHERE('inventory_id', $inventory_id);
        $this->db->UPDATE('inventory', $inventory_old_item);
        return TRUE;
    }
    
    public function get_orders(){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_status', '1');
        return $this->db->GET('orders')->result();
    }
    
    public function get_users(){
        
        $this->db->SELECT('*');
        $this->db->WHERE('user_status', '1');
        return $this->db->GET('users')->result();
    }
    
    public function get_inventorydb($item_id, $location_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_id', $item_id);
        $this->db->WHERE('location_id', $location_id);
        return $this->db->GET('inventory')->row();
    }
    
    public function add_draw_inventory($drawn){
        
        $this->db->INSERT('inventory_draws', $drawn);
        return TRUE;
    }
    
    public function get_drawn_inventory(){
        
        $this->db->SELECT('*');
        return $this->db->GET('inventory_draws')->result();
    }
    
    public function get_order($order_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_id', $order_id);
        return $this->db->GET('orders')->row();
    }
    
    public function get_user($user_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('user_id', $user_id);
        return $this->db->GET('users')->row();
    }
    
    public function amend_order($drawn_id, $order_id){
        
        $this->db->WHERE('drawn_id', $drawn_id);
        $this->db->SET('order_id', $order_id);
        $this->db->UPDATE('inventory_draws');
        return TRUE;
    }
    
    public function get_item_id($item_sku){
        
        $this->db->SELECT('item_id, item_unit, item_name');
        $this->db->WHERE('item_sku', $item_sku);
        return $this->db->GET('inventory_items');
    }
    
    public function get_draw_orders(){
        
        $this->db->SELECT('orders.order_id, orders.order_buffet, orders.order_pax, orders.dining_date');
        $this->db->WHERE('order_status', '1');
        return $this->db->GET('orders');
    }
}
