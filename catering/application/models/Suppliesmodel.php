<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Suppliesmodel extends CI_Model{
    
    public function get_suppliers(){
        
        $this->db->SELECT('*');
        $this->db->WHERE('supplier_status', '1');
        return $this->db->GET('suppliers')->result();
    }
    
    public function get_items(){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_status', '1');
        return $this->db->GET('inventory_items')->result();
    }
    
    public function get_supplier($supplier_id){
        
        $this->db->SELECT('supplier_name');
        $this->db->WHERE('supplier_id', $supplier_id);
        return $this->db->GET('suppliers')->row();
    }
    
    public function get_item($item_id){
        
        $this->db->SELECT('item_name, item_cost_price');
        $this->db->WHERE('item_id', $item_id);
        return $this->db->GET('inventory_items')->row();
    }
    
    public function add_supply($supply){
        
        $this->db->INSERT('supplies', $supply);
        $supply_data = array(
            'supply_id'=> $this->db->insert_id(),
            'supply_status' => TRUE,
        );
        return ($supply_data);
    }
    
    public function get_supplies(){
        
        $this->db->SELECT('*');
        return $this->db->GET('supplies')->result();
    }
    
    public function get_this_month(){
        
        $date = new DateTime();
        //$date->modify("last day of previous month");
        $last_month=$date->format("Y-m");
        
        $this->db->select('*');
        $this->db->from('supplies');
        $this->db->like('supply_date', $last_month);
        return $this->db->get();
    }
    
    public function get_full_supplies(){
        
        $this->db->SELECT('*');
        return $this->db->GET('supplies');
    }
    
    public function get_locations(){
        
        $this->db->SELECT('*');
        $this->db->WHERE('location_status', '1');
        return $this->db->GET('inventory_location')->result();
    }
    
    public function get_inventory($item_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_id', $item_id);
        return $this->db->GET('inventory')->result();
    }
    
    public function add_inventory($inventorydb){
        
        $this->db->INSERT('inventory', $inventorydb);
        return TRUE;
    }
    
    public function update_inventory($inventorydb, $inventory_id){
        
        $this->db->WHERE('inventory_id', $inventory_id);
        $this->db->UPDATE('inventory', $inventorydb);
        return TRUE;
    }
    
    public function get_supply($supply_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('supplies_id', $supply_id);
        return $this->db->GET('supplies')->row();
    }
    
    public function get_supplier_details($supplier_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('supplier_id', $supplier_id);
        return $this->db->GET('suppliers')->row();
    }
    
    public function get_item_details($item_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_id', $item_id);
        return $this->db->GET('inventory_items')->row();
    }
    
    public function get_inventorydb($item_id, $location){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_id', $item_id);
        $this->db->WHERE('location_id', $location);
        return $this->db->GET('inventory')->row();
    }
    
    public function delete_supply($supply_id){
        
        $this->db->WHERE('supplies_id', $supply_id);
        $this->db->DELETE('supplies');
        return TRUE;
    }
    
    public function update_inventorydb($inventorydb, $inventory_id){
        
        $this->db->WHERE('inventory_id', $inventory_id);
        $this->db->UPDATE('inventory', $inventorydb);
        return TRUE;
    }
}

