<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Buffetsmodel extends CI_Model{
    
    public function add_buffet($buffet){
        
        $this->db->INSERT('buffet', $buffet);
        return TRUE;
    }
    
    public function get_buffets(){
        
        $this->db->SELECT('*');
        $this->db->JOIN('master_category', 'master_category.master_cat_id = buffet.master_cat_id', 'left');
        return $this->db->GET('buffet');
    }
    
    public function edit_buffet($buffet, $buffet_id){
        
        $this->db->WHERE('buffet_id', $buffet_id);
        $this->db->UPDATE('buffet', $buffet);
        return TRUE;
    }
    
    public function get_buffet($buffet_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('buffet_id', $buffet_id);
        return $this->db->GET('buffet');
    }
    
    public function update_banner($banner, $buffet_id){
        
        $this->db->WHERE('buffet_id', $buffet_id);
        $this->db->UPDATE('buffet', $banner);
        return TRUE;
    }
    
    public function delete_buffet($buffet_id){
        
        $this->db->WHERE('buffet_id', $buffet_id);
        $this->db->DELETE('buffet');
        return TRUE;
    }
    
    public function add_master_category($master_cat){
        
        $this->db->INSERT('master_category', $master_cat);
        return TRUE;
    }
    
    public function get_master_categories(){
        
        $this->db->SELECT('*');
        return $this->db->GET('master_category');
        
    }
    
    public function update_master_category($master_cat){
        
        $this->db->WHERE('master_cat_id', $master_cat['master_cat_id']);
        $this->db->UPDATE('master_category', $master_cat);
        return TRUE;
    }
    
    public function check_master_cat($master_cat_id){
        
        $this->db->SELECT('master_cat_id');
        $this->db->WHERE('master_cat_id', $master_cat_id);
        return $this->db->GET('buffet');
    }
    
    public function delete_master_category($master_cat_id){
        
        $this->db->WHERE('master_cat_id', $master_cat_id);
        $this->db->DELETE('master_category');
        return TRUE;
    }
    
    public function update_master_cat_image($master_cat){
        
        $this->db->WHERE('master_cat_id', $master_cat['master_cat_id']);
        $this->db->UPDATE('master_category', $master_cat);
        return TRUE;
    }
}
