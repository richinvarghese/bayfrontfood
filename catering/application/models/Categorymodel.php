<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Categorymodel extends CI_Model{
    
    public function add_category($category){
        
        $this->db->INSERT('category', $category);
        return TRUE;
    }
    
    public function get_categories(){
        
        $this->db->SELECT('*');
        return $this->db->GET('category');
    }
    
    public function update_category($category_id, $category){
        
        $this->db->WHERE('category_id', $category_id);
        $this->db->UPDATE('category', $category);
        return TRUE;
    }
    
    public function delete_category($category_id){
        
        $this->db->WHERE('category_id', $category_id);
        $this->db->DELETE('category');
        return TRUE;
    }
    
}
