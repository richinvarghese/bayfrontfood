<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Notificationsmodel extends CI_Model{
    
    public function add_notification($notification){
        
        $this->db->INSERT('notifications', $notification);
        return TRUE;
    }
    
    public function get_notifications(){
        
        $this->db->SELECT('*');
        $this->db->ORDER_BY('created_on', 'DESC');
        $this->db->WHERE('status', '0');
        //$this->db->LIMIT('5');
        return $this->db->get('notifications');
    }
    
    public function process_notif($notif_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('notification_id', $notif_id);
        return $this->db->GET('notifications')->row();
    }
    
    public function update_notif($notif_id){
        
        $this->db->WHERE('notification_id', $notif_id);
        $this->db->SET('status', '1');
        $this->db->UPDATE('notifications');
        return TRUE;
    }
    
    public function get_expiry_items(){
        
        $this->db->SELECT('*');
        $this->db->WHERE('item_expiry_date !=', '0000-00-00');
        return $this->db->GET('supplies')->result();
    }
}
