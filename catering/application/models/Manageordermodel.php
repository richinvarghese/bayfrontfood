<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Manageordermodel extends CI_Model{
    
    
    public function get_all_orders(){
        
        $this->db->SELECT('*');
        return $this->db->GET('orders')->result();
    }
    
    public function get_order_details($order_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_id', $order_id);
        return $this->db->GET('orders')->row();
    }
    
    public function get_order_courses($order_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_id', $order_id);
        return $this->db->GET('order_courses')->result();
    }
    
    public function get_order_items($order_courses_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_courses_id', $order_courses_id);
        return $this->db->GET('order_items')->result();
    }
    
    public function get_addons($order_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_id', $order_id);
        return $this->db->GET('order_add_items')->result();
    }
    
    public function update_discount($update_discount){
        
        $this->db->WHERE('order_id', $update_discount['order_id']);
        $this->db->UPDATE('orders', $update_discount);
        return TRUE;
    }
    
    public function update_delivery($update_delivery){
        
        $this->db->WHERE('order_id', $update_delivery['order_id']);
        $this->db->UPDATE('orders', $update_delivery);
        return TRUE;
    }
    
    public function update_order($order_form, $order_id){
        
        $this->db->WHERE('order_id', $order_id);
        $this->db->UPDATE('orders', $order_form);
        return TRUE;
    }
    
    public function get_buffet($buffet_name){
        
        $this->db->SELECT('*');
        $this->db->WHERE('buffet_name', $buffet_name);
        return $this->db->GET('buffet')->row();
    }
    
    public function update_order_pax($update_order_pax, $order_id){
        
        $this->db->WHERE('order_id', $order_id);
        $this->db->UPDATE('orders', $update_order_pax);
        return TRUE;
    }
    
    public function update_addon_pax($addon_pax, $addon_item_id){
        
        $this->db->WHERE('order_add_items_id', $addon_item_id);
        $this->db->UPDATE('order_add_items', $addon_pax);
        return TRUE;
    }
    
    public function update_order_status($order_id, $order_status){
        
        $this->db->WHERE('order_id', $order_id);
        $this->db->UPDATE('orders', $order_status);
        return TRUE;
    }
    
    public function delete_order($order_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_id', $order_id);
        $this->db->DELETE('orders');
        $this->db->WHERE('order_id', $order_id);
        $this->db->DELETE('order_add_items');
        $this->db->WHERE('order_id', $order_id);
        $this->db->DELETE('order_courses');
        return TRUE;
    }
    
    public function get_courses_ids($order_id){
        
        $this->db->SELECT('order_courses_id');
        $this->db->WHERE('order_id', $order_id);
        return $this->db->GET('order_courses')->result();
        
    }
    
    public function delete_course_items($id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('order_courses_id', $id);
        $this->db->DELETE('order_items');
        return TRUE;
    }
}
