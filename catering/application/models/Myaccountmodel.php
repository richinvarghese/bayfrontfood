<?php
defined('BASEPATH')OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Myaccountmodel extends CI_Model{
    
    public function get_user($user_id){
        
        $this->db->SELECT('*');
        $this->db->WHERE('user_id', $user_id);
        return $this->db->GET('users');
    }
    
    public function update_display($display, $user_id){
        
        $this->db->WHERE('user_id', $user_id);
        $this->db->UPDATE('users', $display);
        return TRUE;
    }
    
    public function verify_password($password){
        
        $this->db->SELECT('*');
        $this->db->WHERE('password', $password['current_password']);
        $this->db->WHERE('user_id', $_SESSION['user_id']);
        return $this->db->GET('users');
    }
    
    public function update_password($user){
        
        $this->db->WHERE('user_id', $_SESSION['user_id']);
        $this->db->UPDATE('users', $user);
        return TRUE;
    }
    
    public function update_profile($profile, $user_id){
        
        
        $this->db->WHERE('user_id', $user_id);
        $this->db->UPDATE('users', $profile);
        return TRUE;
    }
}

