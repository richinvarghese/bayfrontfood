<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Usersmodel extends CI_Model{
    
    
    public function get_users(){
        
        $this->db->SELECT('*');
        return $this->db->GET('users')->result();
    }
    
    public function add_user($user){
        
        $this->db->INSERT('users', $user);
        return TRUE;
    }
    
    public function edit_user($user, $user_id){
        
        $this->db->WHERE('user_id', $user_id);
        $this->db->UPDATE('users', $user);
        return TRUE;
    }
    
    public function reset_password($user_id, $password){
        
        $this->db->WHERE('user_id', $user_id);
        $this->db->SET('password', $password);
        $this->db->UPDATE('users');
        return TRUE;
    }
    
    public function delete_user($user_id){
        
        $this->db->WHERE('user_id', $user_id);
        $this->db->DELETE('users');
        return TRUE;
    }
    
    public function check_login($user){
        
        $this->db->SELECT('*');
        $this->db->WHERE($user);
        return $this->db->GET('users');
    }
    
    public function check_password($current_password, $user_id){
        
        $this->db->SELECT('user_id');
        $this->db->WHERE('password', $current_password);
        $this->db->WHERE('user_id', $user_id);
        return $this->db->GET('users');
    }
    
    public function new_password($user_id, $new_password){
        
        $this->db->SELECT('*');
        $this->db->WHERE('user_id', $user_id);
        $this->db->SET('password', $new_password);
        $this->db->UPDATE('users');
        return TRUE;
    }
}
