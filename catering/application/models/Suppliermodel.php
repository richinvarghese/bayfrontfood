<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Suppliermodel extends CI_Model{
    
    public function add_supplier($supplier){
        
        $this->db->INSERT('suppliers', $supplier);
        return TRUE;
    }
    
    public function get_suppliers(){
        
        $this->db->SELECT('*');
        return $this->db->GET('suppliers')->result();
    }
    
    public function edit_supplier($supplier_id, $supplier){
        
        $this->db->WHERE('supplier_id', $supplier_id);
        $this->db->UPDATE('suppliers', $supplier);
        return TRUE;
    }
    
    public function delete_supplier($supplier_id){
        
        $this->db->WHERE('supplier_id', $supplier_id);
        $this->db->DELETE('suppliers');
        return TRUE;
    }
}
