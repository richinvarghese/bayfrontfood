<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Settingsmodel extends CI_Model{
    
    public function get_settings(){
        
        $this->db->SELECT('*');
        return $this->db->GET('settings');
    }
    
    public function upload_logo($filename){
        
        $this->db->UPDATE('settings', $filename);
        return TRUE;
    }
    
    public function update_general($general, $settings_id){
        
        $this->db->WHERE('settings_id', $settings_id);
        $this->db->UPDATE('settings', $general);
        return TRUE;
    }
    
    public function update_application($app_settings){
        
        $this->db->UPDATE('settings', $app_settings);
        return TRUE;
    }
    
    public function update_invoice_terms($invoice_terms){
        
        $this->db->UPDATE('settings', $invoice_terms);
        return TRUE;
    }
}