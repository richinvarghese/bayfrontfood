<!--Main Content-->
    <h1 class="margin-bottom hidden-print">View Order</h1>
    <ol class="breadcrumb 2 hidden-print">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li>
          <a href="<?php echo base_url()?>supplies/index">
            <i class="fa-home"></i>Supplies
          </a>
        </li>
        <li class="active">
          <strong>View Supply</strong>
        </li>
    </ol>

    <div class="row">
        <div class="col-sm-12">
            <div class="invoice">
                <div class="row" style="padding:0px 15px 0px 15px;">
                    <div class="col-sm-6 invoice-left">
                        <a href="#">
                            <img src="<?php echo base_url();?>/assets/images/app_settings/entity/logo.png" width="185" alt="">
                        </a>
                    </div>
                    <div class="col-sm-6 invoice-right">
                        <h3>SUPPLIES ID. <?php echo $settings->entity_invoice_prefix.' #'.$supply->supplies_id;?></h3>
                        <span><?php echo 'Supplied On : <i class="glyphicon glyphicon-calendar" style="color:#bea26a"></i> '.date('d-M-Y', strtotime($supply->supply_date));?></span>
                    </div>
                </div>
                <hr class="margin hidden-print">
                <div class="row" style="padding:0px 15px 0px 15px;">
                    <div class="col-sm-3 invoice-left">
                        <h4>Customer</h4>
                        <?php echo $supplier->supplier_name;?>
                        <br>
                        <?php echo $supplier->supplier_block.', #'.$supplier->supplier_unit;?>
                        <br>
                        <?php echo $supplier->supplier_street;?>
                        <br>
                        <?php echo 'Singapore ('.$supplier->supplier_postal.') '?>
                    </div>
                    <div class="col-sm-3 invoice-left">
                        <h4>Contact Details</h4>
                        <?php echo 'Contact Person: '.$supplier->supplier_person;?>
                        <br>
                        <?php echo 'Phone: '.$supplier->supplier_contact;?>
                        <br>
                        <?php echo 'Email: '.$supplier->supplier_email;?>
                        <br>
                    </div>
                    <div class="col-md-6 invoice-right">
                        <h4><u>Supply Details</u></h4>
                            <?php if($supply->item_expiry_date == '0000-00-00'){
                                
                                echo '<strong>Expiry Date: </strong>'.'<div class="label label-secondary">No Expiry for this Item</div>';
                            }
                            else{
                                
                                echo '<strong>Expiry Date: </strong><i class="glyphicon glyphicon-calendar" style="color:#bea26a"></i> '.date('d-M-Y', strtotime($supply->item_expiry_date));
                            };?>
                        <br>
                    </div>
                </div>
                <div class="margin hidden-print"></div>
                <div class="margin hidden-print"></div>
                <div class="jumbotron" style="padding-left: 0px; padding-right: 0px; background-color: #fff">
                    <br>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                                <div class="panel-title">Supply Details</div>
                                <div class="panel-options">
                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                </div>
                        </div>

                        <div class="panel-body with-table">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <th>Item Name</th>
                                        <th>Ordered Quantity</th>
                                        <th>Supplied Quantity</th>
                                        <th>Price Per Unit</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <tr>
                                        <!--Item Details-->
                                        <td>
                                            <h4><?php echo $item->item_name ?></h4>
                                        </td>

                                        <!--Ordered Quantity-->
                                        <td>
                                            <p><?php echo $supply->item_qty_order.$item->item_unit;?></p>
                                        </td>
                                        <!--Ordered Supplied-->
                                        <td>
                                            <p><?php echo $supply->item_qty_supplied.$item->item_unit;?></p>
                                        </td>
                                        <!--Item Unit Price-->
                                        <td>
                                            <p><?php echo $settings->entity_currency_symbol.' '.$item->item_cost_price;?></p>
                                        </td>

                                        <!--Item Total-->
                                        <td>
                                            <p><?php echo $settings->entity_currency_symbol.' '.$supply->supply_total;?></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-9">
                            <div class="panel panel-primary" data-collapsed="0" style="border-color: #fff;">
                                <!-- panel head -->
                                <div class="panel-heading" style="border-color: #fff">
                                    <div class="panel-title"><h4>Remarks: </h4></div>
                                </div>
                                <!-- panel body -->
                                <div class="panel-body">
                                    <p style="font-size: small;"><?php echo $supply->supply_remarks;?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <table class="table responsive">
                                    <thead>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style='border-top: 0px solid #fff;font-size: x-large;'><strong>SUPPLY TOTAL</strong></td>
                                            <td style='border-top: 0px solid #fff;font-size: x-large;'><strong><?php echo $settings->entity_currency_symbol." ". number_format(round($supply->supply_total, 1), 2);?></strong></td>
                                        </tr>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <!-- Order Print and Email starts here-->
                    <div class="row">
                        <div class="col-md-10"></div>
                        <div class="col-md-1 invoice-right">
                            <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">Print Invoice<i class="entypo-doc-text"></i></a>
                        </div>
                        <div class="col-md-1 invoice-right">
                            <a href="#" class="btn btn-success btn-icon icon-left hidden-print">Send Invoice<i class="entypo-mail"></i></a>
                        </div>
                    </div>
                    <!--Order Print and Email Ends Here-->
                    
                </div>
            </div>
        </div>  
    </div>
               
<!--End of Main Content-->

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>