    <!--Main Content-->
    
    <script type="text/javascript">
        
      
      jQuery(document).ready(function($) 
      
                             {
        // Sample Toastr Notification
       /* setTimeout(function()
                   {
          var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
            "toastClass": "black",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          };
          //toastr.success("You have been awarded with 1 year free subscription. Enjoy it!", "Account Subcription Updated", opts);
        }
                   , 3000);*/
        // Sparkline Charts
        $(".top-apps").sparkline('html', {
          type: 'line',
          width: '50px',
          height: '15px',
          lineColor: '#ff4e50',
          fillColor: '',
          lineWidth: 2,
          spotColor: '#a9282a',
          minSpotColor: '#a9282a',
          maxSpotColor: '#a9282a',
          highlightSpotColor: '#a9282a',
          highlightLineColor: '#f4c3c4',
          spotRadius: 2,
          drawNormalOnTop: true
        }
        );
                        
                        
        //Total Orders (This Month)
        //$(".monthly-sales").sparkline([1,5,6,7,10,12,16,11,9,8.9,8.7,7,8,7,6,5.6,5,7,5,4,5,6,7,8,6,7,6,3,2], {
        $(".monthly-sales").sparkline([<?php foreach($order_entries as $entry){echo $entry.',';}?>], {
          type: 'bar',
          barColor: '#ff4e50',
          height: '55px',
          width: '100%',
          barWidth: 8,
          barSpacing: 1
        }
        );   
                             
                             
        $(".pie-chart").sparkline([<?php echo $income_curr.','.$expense_curr;?>], {
          type: 'pie',
          width: '95',
          height: '95',
          sliceColors: ['#ff4e50','#db3739','#a9282a']
        }
        );
                         
        // % more(Compared to Last month) -> Order Statistics
        //$(".daily-visitors").sparkline([1,5,5.5,5.4,5.8,6,8,9,13,12,10,11.5,9,8,5,8,9], {
        $(".daily-visitors").sparkline([<?php foreach($order_entries as $entry){echo $entry.',';}?>], {
          type: 'line',
          width: '100%',
          height: '55',
          lineColor: '#ff4e50',
          fillColor: '#ffd2d3',
          lineWidth: 2,
          spotColor: '#a9282a',
          minSpotColor: '#a9282a',
          maxSpotColor: '#a9282a',
          highlightSpotColor: '#a9282a',
          highlightLineColor: '#f4c3c4',
          spotRadius: 2,
          drawNormalOnTop: true
        }
        );
                              
        // % more(Compared to Last month) -> Supplies Statistics                      
        //$(".stock-market").sparkline([1,8,6,7,10,12,16,11,9,8.9,8.7,7,8,7,6,5.6,5,7,5], {
        $(".stock-market").sparkline([<?php foreach($supply_entries as $entry){echo $entry.',';}?>], { 
          type: 'line',
          width: '100%',
          height: '55',
          lineColor: '#ff4e50',
          fillColor: '',
          lineWidth: 2,
          spotColor: '#a9282a',
          minSpotColor: '#a9282a',
          maxSpotColor: '#a9282a',
          highlightSpotColor: '#a9282a',
          highlightLineColor: '#f4c3c4',
          spotRadius: 2,
          drawNormalOnTop: true
        }
        );
                            
                            
        //Events Calendar
        var events =  <?php echo '[';foreach($orders_events as $order){ echo '{ title:"'. date("h : s  A",strtotime($order->dining_time)).'",'.'start:"'.$order->dining_date.'",},' ;?><?php } echo']'?>
        
        $("#calendar").fullCalendar({
            
            eventSources: [
                    {
                        color: '#306EFE',   
                        textColor: '#ffffff',
                        events:events,
                    }
            ],
            
            
          header: {
            left: '',
            right: '',
          }
          ,
          firstDay: 1,
          height: 200,
        }
        );
      }
    );
                    
                    
                    
      function getRandomInt(min, max) 
      {
        return Math.floor(Math.random() * (max - min + 1)) + min;
      }
    </script>
    <div class="row">
        
        <!--Orders-->
        <div class="col-md-3 col-sm-6">
            <div class="tile-stats tile-white stat-tile">
                <h3><?php echo (number_format(round($eval,0))).'% '.$eval_status;?> revenue (to Last month)</h3>
                <p>Online Orders statistics</p>
                <span class="daily-visitors"><canvas width="373" height="55" style="display: inline-block; width: 373px; height: 55px; vertical-align: top;"></canvas></span>
            </div>		
        </div>
        
        <!--Daily Order Statistics-->
        <div class="col-md-3 col-sm-6">
            <div class="tile-stats tile-white stat-tile">
                <h3><?php echo $nos_orders;?> Orders (This Month)</h3>
                <p>Avg Orders per day</p>
                <span class="monthly-sales"><canvas width="260" height="55" style="display: inline-block; width: 260px; height: 55px; vertical-align: top;"></canvas></span>
            </div>		
        </div>
        
        <!--Supplies-->
        <div class="col-md-3 col-sm-6">
            <div class="tile-stats tile-white stat-tile">
                <h3><?php echo (number_format(round($eval2,0))).'% '.$eval_status2;?> expenditure (to Last month)</h3>
                <p>Supplies</p>
                <span class="stock-market"><canvas width="373" height="55" style="display: inline-block; width: 373px; height: 55px; vertical-align: top;"></canvas></span>
            </div>		
        </div>
        
        <!--Orders and Supplies Together-->
        <div class="col-md-3 col-sm-6">
            <div class="tile-stats tile-white stat-tile">
                <h3>Income vs Expense (This Month)</h3>
                <p>Based on Orders and Supplies</p>
                <span class="pie-chart"><canvas width="95" height="95" style="display: inline-block; width: 95px; height: 95px; vertical-align: top;"></canvas></span>
            </div>		
        </div>
    </div>
    <br />
    
    
    
    <div class="row">
      <div class="col-md-9">
        <script type="text/javascript">
          jQuery(document).ready(function($)
                                 {
            var map = $("#map-2");
            map.vectorMap({
              map: 'europe_merc_en',
              zoomMin: '3',
              backgroundColor: '#f4f4f4',
              focusOn: {
                x: 0.5, y: 0.7, scale: 3 }
              ,
              markers: [
                {
                  latLng: [50.942, 6.972], name: 'Cologne'}
                ,
                {
                  latLng: [42.6683, 21.164], name: 'Prishtina'}
                ,
                {
                  latLng: [41.3861, 2.173], name: 'Barcelona'}
                ,
              ],
              markerStyle: {
                initial: {
                  fill: '#ff4e50',
                  stroke: '#ff4e50',
                  "stroke-width": 6,
                  "stroke-opacity": 0.3,
                }
              }
              ,	
              regionStyle: 
              {
                initial: {
                  fill: '#e9e9e9',
                  "fill-opacity": 1,
                  stroke: 'none',
                  "stroke-width": 0,
                  "stroke-opacity": 1
                }
                ,
                hover: {
                  "fill-opacity": 0.8
                }
                ,
                selected: {
                  fill: 'yellow'
                }
                ,
                selectedHover: {
                }
              }
            }
                         );
          }
                                );
        </script>
        
        
        <div class="tile-group tile-group-2">
          <div class="tile-left tile-white">
            <div class="tile-entry">
              <h3>Last 3 Customers
              </h3>
              <span>Where they made Orders from
              </span>
            </div>
            <ul class="country-list">
            <?php foreach($customers as $cust):?>
              <li>
                  <?php if($cust->order_status == 0):?>
                  <span class="badge badge-warning">Pending</span>
                  <?php elseif($cust->order_status == 1):?>
                  <span class="badge badge-success">Approved</span>
                  <?php elseif($cust->order_status == 2):?>
                  <span class="badge badge-danger">Cancelled</span>
                  <?php elseif($cust->order_status == 3):?>
                  <span class="badge badge-primary">Completed</span>
                  <?php endif;?>
                <?php echo ' '.'<strong>'.$cust->customer_name.'</strong>, '.$cust->address_street;?>
              </li>
            <?php endforeach;?>
            </ul>
          </div>
          <div class="tile-right">
            <div id="map-2" class="map">
            </div>
          </div>
        </div>
      </div>
        
        
        
      <div class="col-md-3">
        <div class="tile-stats tile-neon-red">
          <div class="icon">
            <i class="entypo-chat">
            </i>
          </div>
          <div class="num" data-start="0" data-end="<?php echo $income;?>" data-prefix="$ " data-duration="1400" data-delay="0">0
          </div>
          <h3>Total Income So Far</h3>
          <p>From Orders Only</p>
        </div>
        <br />
        <div class="tile-stats tile-primary">
          <div class="icon">
            <i class="entypo-users">
            </i>
          </div>
          <div class="num" data-start="0" data-end="<?php echo $expense;?>" data-prefix="$ " data-duration="1400" data-delay="0">0
          </div>
          <h3>Total Expenditure</h3>
          <p>Based on Supplies</p>
        </div>
      </div>
    </div>
    <br />
    
    
    
    <div class="row">
      <div class="col-sm-8">
        <div class="panel panel-primary panel-table">
          <div class="panel-heading">
            <div class="panel-title">
              <h3>Top Pick Buffets</h3>
              <span>For Last 12 months</span>
            </div>
            <div class="panel-options">
              <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg">
                <i class="entypo-cog">
                </i>
              </a>
              <a href="#" data-rel="collapse">
                <i class="entypo-down-open">
                </i>
              </a>
              <a href="#" data-rel="reload">
                <i class="entypo-arrows-ccw">
                </i>
              </a>
              <a href="#" data-rel="close">
                <i class="entypo-cancel">
                </i>
              </a>
            </div>
          </div>
          <div class="panel-body">
            <table class="table table-responsive no-margin">
              <thead>
                <tr>
                  <th>Buffet Name</th>
                  <th>Revenue Generated</th>
                  <th>Total Number of Orders</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($buffets as $buffet):?>
                <tr>
                  <td><?php echo $buffet->buffet_name;?></td>
                  <td><?php echo '$ '.number_format(round($buffet->order_gtotal, 1),2);?></td>
                  <td><?php echo $buffet->rows;?></td>
                </tr>
                <?php endforeach;?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
        
        
      <div class="col-sm-4">
        <div class="panel panel-primary panel-table">
          <div class="panel-heading">
            <div class="panel-title">
              <h3>Delivery Date & Time</h3>
              <span>Only Approved Orders</span>
            </div>
            <div class="panel-options">
              <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg">
                <i class="entypo-cog">
                </i>
              </a>
              <a href="#" data-rel="collapse">
                <i class="entypo-down-open">
                </i>
              </a>
              <a href="#" data-rel="reload">
                <i class="entypo-arrows-ccw">
                </i>
              </a>
              <a href="#" data-rel="close">
                <i class="entypo-cancel">
                </i>
              </a>
            </div>
          </div>
          <div class="panel-body">
              <div id="calendar" class="calendar-widget" style="text-align:center;">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--<script type="text/javascript">
      function showAjaxModal()
      {
        jQuery('#modal-7').modal('show', {
          backdrop: 'static'}
                                );
        jQuery.ajax({
          url: "data/ajax-content.txt",
          success: function(response)
          {
            jQuery('#modal-7 .modal-body').html(response);
          }
        }
                   );
      }
    </script>-->
    
    
    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/rickshaw/rickshaw.min.css">
    <!-- Bottom scripts (common) -->
    <script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url()?>assets/js/joinable.js"></script>
    <script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
    <script src="<?php echo base_url()?>assets/js/neon-api.js"></script>
    <script src="<?php echo base_url()?>assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <!-- Imported scripts on this page -->
    <script src="<?php echo base_url()?>assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/rickshaw/vendor/d3.v3.js"></script>
    <script src="<?php echo base_url()?>assets/js/rickshaw/rickshaw.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/raphael-min.js"></script>
    <script src="<?php echo base_url()?>assets/js/morris.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/toastr.js"></script>
    <script src="<?php echo base_url()?>assets/js/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/neon-chat.js"></script>
    <!-- JavaScripts initializations and stuff -->
    <script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>
    <!-- Demo Settings -->
    <script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>