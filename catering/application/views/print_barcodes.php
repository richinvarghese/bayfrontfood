<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Akstech Development Team" />
	<meta name="author" content="" />

	<link rel="icon" href="assets/images/favicon.ico">
        
	<title><?php echo $settings->pagetitle;?></title>
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css">


</head>
<body>
    <ol class="breadcrumb bc-2 hidden-print" >
        <li>
            <a href="<?php echo base_url()?>dashboard/index"><i class="fa-home"></i>Home</a>
        </li>
        <li>
            <a href="<?php echo base_url()?>inventoryitems/index"><i class="fa-home"></i>Inventory Items</a>
        </li>
        <li class="active">
            <strong>Print Barcodes</strong>
        </li>
    </ol>
    <br class="hidden-print" />
<div class="row">
    <div class="col-md-12">
        <?php for($x=1; $x<= $nos_copies;$x++):?>
            <div class="col-md-2">
                <img src="<?php echo base_url().'assets/images/barcodes/'.$item_sku.'.png'?>" alt="" style="width: 250px;">&nbsp;&nbsp;
            </div>
        <?php endfor; ?>
    </div>
</div>
    
  <br class="hidden-print" />
  <br class="hidden-print" />
  
<div class="row">
    <div class="col-md-5">
        <center><div class="col-md-1 invoice-right">
            <a href="javascript:window.print();" class="btn btn-primary btn-icon hidden-print" style="text-align: center">Print</a>
        </div></center>
        <center><div class="col-md-1 invoice-right">
            <a href="<?php echo base_url()?>inventoryitems/index" class="btn btn-default btn-icon hidden-print" style="text-align: center">Back</a>
        </div></center>
    </div>
</div>
    
<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/bootstrap-switch.min.js"></script>

</body>
</html>