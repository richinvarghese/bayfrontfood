<h1 class="margin-bottom">Manage Stocks</h1>
<ol class="breadcrumb 2">
    <li>
        <a href="index.html">
            <i class="fa-home"></i>Home
        </a>
    </li>
    <li class="active">
        <strong>Manage Stocks</strong>
    </li>
</ol>
<br>
        
        <div class="row">
            <div class="col-sm-9">
        
        <!--<script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
                var $table1 = jQuery( '#item-table' );

                // Initialize DataTable
                $table1.DataTable( {
                        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "bStateSave": true
                });

                // Initalize Select Dropdown after DataTables is created
                $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                        minimumResultsForSearch: -1
                });
        } );
        </script>-->
        
        
        <?php if(isset($_SESSION['success-stock'])){?>
            <center>
                <div class="form-session-success">
                <?php echo $_SESSION['success-stock'];?>
                </div>
            </center>
          <br>
        <?php }?>

        <?php if(isset($_SESSION['error-stock'])){?>
            <center>
                <div class="form-session-errors">
                <?php echo ($_SESSION['error-stock']);?>
                </div>
            </center>
          <br>
        <?php }?>
      
        <table class="table table-bordered datatable" id="item-table">
            <thead>
                <tr>
                    <th>#Item No:</th>
                    <th>Name</th>
                    <th>Stock Left</th>
                    <th>Stock Location</th>
                    <th>Unit Cost</th>
                    <th>Total Cost</th>
                    <th>Manage</th>
                </tr>
            </thead>
            <tbody>
                <?php $x=1;?>
                <?php $i=0?>
                <?php foreach($stocks as $stock):?>
                <tr class="odd gradeX">
                    <td class="center"><?php echo $x;?></td>
                    <td class="center"><?php echo $stock->item->item_name;?></td>
                    <td class="center"><?php echo $stock->item_quantity.' '.$stock->item->item_unit;?></td>
                    <td class="center"><?php if($stock->location_id == 0){echo ' - ';}else{echo $stock->location->location_name.' - '.$stock->location->location_stack;}?></td>
                    <td class="center"><?php echo $settings->entity_currency_symbol.number_format(round($stock->item->item_cost_price, 1), 2);?></td>
                    <td class="center"><?php echo $settings->entity_currency_symbol.' '.number_format(round($stock->item_total_worth,1), 2)?></td>
                    <td class="center">
                        
                       
                        <a href="#" class="btn btn-info btn-sm btn-icon icon-left adjust-quantity" data-target="#adjust-quantity" data-toggle="modal" 
                           data-item_cost1="<?php echo $stock->item->item_cost_price;?>" 
                           data-inventory_id1="<?php echo $stock->inventory_id;?>"
                           data-item_quantity1="<?php echo $stock->item_quantity;?>" 
                           data-location_id1="<?php echo $stock->location_id;?>" >
                            <i class="entypo-archive"></i>
                            Adjust Quantity
                        </a>
                        
                        <a href="#" class="btn btn-warning btn-sm btn-icon icon-left update-location" data-target="#update-location" data-toggle="modal" 
                           data-inventory_id2="<?php echo $stock->inventory_id;?>" 
                           data-item_id2="<?php echo $stock->item_id;?>"
                           data-item_quantity2="<?php echo $stock->item_quantity;?>"
                           data-item_total_worth="<?php echo $stock->item_total_worth;?>"
                           data-location_id2="<?php echo $stock->location_id;?>" >
                            <i class="entypo-location"></i>
                            Update Location
                        </a>
                        
                        <!-- Modal for Adjust Stock Quantity Starts Here-->
                            <div class="modal fade" id="adjust-quantity">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>stocks/adjust_quantity" novalidate="novalidate">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Adjust Quantity</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                 <label for="field-1" class="col-sm-3 control-label">Current Stock</label>
                                                 <div class="col-sm-6">
                                                   <input type="hidden" name="item_cost" id="item_cost1">
                                                   <input type="hidden" name="inventory_id" id="inventory_id1">
                                                   <input type="hidden" name="location_id" id="location_id1">
                                                   <input type="text" name="item_quantity" placeholder="New Stock"  class="form-control" id="item_quantity1" data-validate="required">
                                                 </div>
                                           </div>
                                        </div>
                                        <br>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Close It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Update Quantity</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                        <!-- Modal for Adjust Stock Quantity Ends Here-->
                        
                        <!-- Modal for Update Location Starts Here-->
                            <div class="modal fade" id="update-location">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>stocks/update_location" novalidate="novalidate">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Update Stock Location</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                <input type="hidden" name="inventory_id" id="inventory_id2">
                                                <input type="hidden" name="location_id" id="location_id2">
                                                <input type="hidden" name="item_id" id="item_id2">
                                                <input type="hidden" name="item_total_worth" id="item_total_worth">
                                                 <label for="field-1" class="col-sm-4 control-label">Choose a Location to Transfer the Stocks</label>
                                                 <div class="col-sm-6">
                                                   <select name="location_id" class="selectboxit">
                                                        <!-- Pending Default Selection-->
                                                        <optgroup label="Choose Location">
                                                            <?php foreach($locations as $locs):?>
                                                            <option value="<?php echo $locs->location_id;?>"><?php echo $locs->location_name.'-'.$locs->location_stack;?></option>
                                                            <?php endforeach;?>
                                                        </optgroup>
                                                    </select>
                                                 </div>
                                           </div>
                                        </div>
                                        <br>
                                        <div class="row"> 
                                            <div class="form-group">
                                                 <label for="field-1" class="col-sm-4 control-label">Quantity to be moved</label>
                                                 <div class="col-sm-6">
                                                    <input type="text" name="item_quantity" placeholder="Quantity to be Moved" id='item_quantity2' class="form-control">
                                                 </div>
                                           </div>
                                        </div>
                                        <br>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Close It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Update Location</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                        <!-- Modal for Update Location Ends Here-->
                        
                    </td>
                </tr>
                <?php $i++;?>
                <?php $x++;?>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                    <th>#Item No:</th>
                    <th>Name</th>
                    <th>Stock Left</th>
                    <th>Stock Location</th>
                    <th>Unit Cost</th>
                    <th>Total Cost</th>
                    <th>Manage</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="col-sm-3">
        <pre>
        <h3 style="text-align: center;">Stock Summary</h3>
            <table class="table table-responsive no-margin">
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Worth</th>
                            <th class="text-center">In-Stock</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach($items as $item):?>
                        <tr>
                            <td><?php echo $item->item_name?></td>
                            <td><?php echo $settings->entity_currency_symbol.number_format(round($item->item_worth,1),2);?></td>
                            <td class="text-center"><span class="badge badge-secondary"><?php echo $item->item_quantity.' '.$item->item_unit;?></span></td>
                        </tr>
                        <?php endforeach;?>
                        <tr>
                            <td><?php echo '<h3>Total Stock Worth</h3>'?></td>
                            <td><?php echo '<h3>'.$settings->entity_currency_symbol.number_format(round($total_stock_worth, 1), 2).'</h3>';?></td>
                            <td></td>
                        </tr>
                    </tbody>
            </table>
        </pre>
    </div>
  </div>

<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.css">

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url();?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/js/joinable.js"></script>
<script src="<?php echo base_url();?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url();?>assets/js/neon-api.js"></script>

<!-- Imported scripts on this page -->
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<!--<script src="<?php echo base_url()?>assets/js/datatables/datatables.js"></script>-->
<script src="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url();?>assets/js/neon-custom.js"></script>

<!-- Demo Settings -->
<script src="<?php echo base_url();?>assets/js/neon-demo.js"></script>

<!--Modal Data-->
<script type="text/javascript">

$(document).on("click", ".adjust-quantity", function(event){
    
    $('#item_cost1').val($(this).data('item_cost1'))
    $('#inventory_id1').val($(this).data('inventory_id1'))
    $('#location_id1').val($(this).data('location_id1'))
    $('#item_quantity1').val($(this).data('item_quantity1'))
})

$(document).on("click", ".update-location", function(event){

    $('#inventory_id2').val($(this).data('inventory_id2'))
    $('#location_id2').val($(this).data('location_id2'))
    $('#item_id2').val($(this).data('item_id2'))
    $('#item_total_worth').val($(this).data('item_total_worth'))
    $('#item_quantity2').val($(this).data('item_quantity2'))
})

</script>