
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Akstech Catering Management" />
	<meta name="author" content="Akstech" />

	<link rel="icon" href="assets/images/favicon.ico">

	<title><?php echo $settings->pagetitle;?></title>

	<link rel="stylesheet" href="<?php echo base_url()?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/neon-core.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/neon-theme.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">

	<script src="<?php echo base_url()?>assets/js/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body style='background: #303641; padding: 25px;'>
    
    <h2 style='color: #fff;'>Orders - Kitchen Display</h2>
    
    <div class="row">
        <?php foreach($orders as $order):?>
        <div class="col-sm-3">
            <div class="tile-block tile-red">
                <div class="tile-header">
                    <i class="glyphicon"><?php echo date('d-M-Y',strtotime($order->dining_date))?><span><?php echo 'DINING TIME: '.date('h:i:A',strtotime($order->dining_time));?></span></i>
                    <a href="#">
                        <?php echo $order->order_buffet;?>
                        <span><?php echo "<strong>For ".$order->order_pax." Pax</strong>";?></span>
                    </a>
                </div>
                <div class="tile-content">
                    <?php foreach($order->order_courses as $course):?>
                    <u><h4 style='color:#fff;'><?php echo $course->order_course_name;?></h4></u>
                        <?php foreach($course->course_items as $course_item):?>
                        <p style='color:#fff;'><?php echo $course_item->order_items_eng;?></p>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </div>
                <div class="tile-footer">
                    <u><h4 style='color:#fff;'>Add-on Items</h4></u>
                    <?php foreach($order->addon_items as $addon_item):?>
                        <?php if($addon_item->order_add_items_pax > 0):?>
                        <p style='color:#fff;'><?php echo $addon_item->order_add_items_eng.' X '.$addon_item->order_add_items_pax.' pax';?></p>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
    
</body>