<!--================Banner Area =================-->
<style>
input:disabled+label {
  color: #ccc;
}

</style>
<section class="banner_area" style='background: url(<?php echo base_url();?>/frontend/assets/img/banner/banner-bg-1.jpg) no-repeat scroll center center;'>
    <div class="container">
        <div class="banner_content">
            <h4>BUFFET MENU</h4>
            <a href="http://bayfrontfood.sg">Home</a>
            <a href="<?php echo base_url()?>frontend_controller/listmenu/index">Buffet List</a>
            <a class="active">Order Menu</a>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->

<!--================End Our feature Area =================-->
<section class="most_popular_item_area menu_list_page">
    <div class="container">

        <div class="container">
                <div class="s_black_title">
                    <h3>Make Your Booking for </h3>
                    <br>
                    <h2>Buffet </h2>
                    <hr>
                </div>
                <!--<form class="form_area" style='border: brown; border-style: double; padding: 10px;'>-->
                <form class="form_area" method="post" id="regForm" action='<?php echo base_url()."frontend_controller/orders/confirm_order/".$buffet->buffet_id;?>'>
                    <div class="row">
                        <!--Tab 1 Starts Here-->
                        <div class="tab">
                            <br>
                            <article class="blog_list_item row m0">
                                <div class="col-md-4">
                                        <img src="<?php echo base_url()."assets/images/menu/banners/".$buffet->buffet_banner;?>" alt="" style='max-width: 100%;'>
                                        <div class="blog_social" style='background: #bea26a;'>
                                            <div class="pull-left">
                                                <h4>Buffet Name: <a href="#"><?php echo $buffet->buffet_name;?></a></h4>
                                            </div>
                                            <div class="pull-right">
                                                <h5>Price: </h5>
                                                <ul class="social_icon">
                                                    <li><a href="#" style='font-size: 10px; color: #bea26a; font-weight: bolder;'><?php echo $settings->entity_currency_symbol.''.$buffet->buffet_price;?></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                </div>

                                <div class="col-md-8" id="main">

                                    <div class="blog_list_content">
                                        <?php
                                          $count = 1;
                                          $total_selection = 0;
                                        foreach($courses as $cos):?>

                                        <?php
                                          $total_selection += $cos->course_allowed_selection;
                                        ?>
                                            <div class="sidebar_title">
                                                <h3><?php echo $cos->course_name;?>
                                                    <span style='font-size: small; color: brown;'><?php echo '( Maximum Allowed Sections : '.$cos->course_allowed_selection.' )'?></span>
                                                </h3>
                                            </div>
                                            <div class="row" id="<?php echo $cos->course_id ?>">

                                                    <?php foreach ($cos->items as $item):?>
                                                <div class="col-md-6">

                                                    <input type="hidden"  value="<?php echo $cos->course_id;?>">
                                                    <input type="checkbox" id="item_<?php echo $count ?>" onclick="check(<?php echo $cos->course_id ?>, <?php echo $cos->course_allowed_selection ?>)"  name="item_id[]" value="<?php echo $item[0]->item_id;?>" style='width: 10%'>
                                                    <label for ="item_<?php echo $count++ ?>" ><?php echo $item[0]->item_eng.' ( '.$item[0]->item_chn.' )';?></label>
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <br/>
                                        <?php endforeach;?>
                                    </div>
                                    <input type="hidden" id="totalselection" value="<?php echo $total_selection?>">
                                </div>
                            </article>
                        </div>
                        <!--Tab 1 Ends Here-->

                        <!--Tab 2 Starts Here-->
                        <div class="tab">
                            <br>
                            <article class="blog_list_item row m0">
                                <div class="col-md-4">
                                        <img src="<?php echo base_url()."assets/images/menu/banners/".$buffet->buffet_banner;?>" alt="" style='max-width: 100%;'>
                                        <div class="blog_social" style='background: #bea26a;'>
                                            <div class="pull-left">
                                                <h4>Buffet Name: <a href="#"><?php echo $buffet->buffet_name;?></a></h4>
                                            </div>
                                            <div class="pull-right">
                                                <h5>Price: </h5>
                                                <ul class="social_icon">
                                                    <li><a href="#" style='font-size: 10px; color: #bea26a; font-weight: bolder;'><?php echo $settings->entity_currency_symbol.''.$buffet->buffet_price;?></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="blog_list_content">
                                        <div class="sidebar_title">
                                            <h3>Additional Items
                                                <span style='font-size: small; color: brown;'><?php echo '(Optional)'?></span>
                                            </h3>
                                        </div>
                                        <?php foreach($addons as $addon):?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6 form-group">
                                                        <label><?php echo $addon->addon_eng.' '.$addon->addon_chn;?><span style='font-size: 12px; color: brown;'><?php echo " (Price per Pax $".$addon->addon_price." ) ";?></span></label>
                                                    </div>
                                                    <div class="col-md-6 form-group">
                                                        <input type="text" class="form-control" name='addon_pax[]' value='' placeholder="Indicate no: of Pax" style='width: 100%' novalidate>
                                                        <input type="hidden"  name="addon_id[]" value="<?php echo $addon->addon_id;?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <br/>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <!--Tab 2 Endss Here-->


                        <!--Tab 3 Starts Here-->
                        <div class="tab">
                          
                            <div class="form-group col-md-6">
                                <input type="datetime-local" class="form-control" id="date"  name='order_datetime' placeholder="Date Time">
                            </div>
 <div class="form-group col-md-6">
                              <input type="hidden" readonly class="form-control" id="partypax"  name='partypax' value="<?php echo $buffet->buffet_min_pax;?>" placeholder="Date Time">

                                      <input type="number"  onblur="if(this.value<document.getElementById('partypax').value){this.value=''}" min="<?php echo $buffet->buffet_min_pax;?>" required class="form-control" id="nopax"  name='order_pax'  placeholder="Minimum Pax is <?php echo $buffet->buffet_min_pax;?>">

                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="name"  name='customer_name' placeholder="Name">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="email" class="form-control" id="email" name='customer_email' placeholder="Email">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="phone" name='customer_contact' placeholder="Phone">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="block"  name='address_block' placeholder="Unit No, Building/Block">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="email" class="form-control" id="street" name='address_street' placeholder="Street Name">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" id="postal" name='address_postal' placeholder="Postal">
                            </div>
                            <div class="form-group col-md-12">
                                <textarea class="form-control" rows="3" name='order_additional_note' placeholder="Additional notes.."></textarea>
                            </div>
                        </div>
                        <!--Tab 3 Endss Here-->

                        <div class="col-md-12 form-group">
                            <div style="overflow:auto;">
                                <div style="float:right;">
                                  <button type="button" class="btn btn-default" id="prevBtn" onclick="nextPrev(-1)" style='background:#bea26a; border-color: none; color:#fff; '>Previous</button>
                                  <button type="button" class="btn btn-default"  id="nextBtn" onclick="nextPrev(1)" style='background:#bea26a; border-color: none; color: #fff;'>Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <!-- Circles which indicates the steps of the form: -->
            <div style="text-align:center; margin-top:40px;">
              <span class="step"></span>
              <span class="step"></span>
              <span class="step"></span>
            </div>

        </form>


    </div>
</section>
<!--================End Our Menu Listing Area =================-->

<script type="text/javascript">

    $(document).ready(function() {
        var itemid =[];

        /*$("#itemid").change(function() {
            $('input[type=checkbox]:checked').each(function() {
                itemid.push($(this).val());
                //alert(itemid);
              });
              alert(itemid);
              //return itemid;
        });*/

      //  var courseid =  $("#courseid").val();
    });
    $("#nextBtn").hide(true);
    function check(cid, max){
      var selected = [];
      $('#'+cid+' input:checked').each(function() {
          selected.push($(this).attr('id'));
      });

      var unselected = [];
      $('#'+cid+' input:checkbox:not(:checked)').each(function() {
          unselected.push($(this).attr('id'));
      });
      var mainselected = [];
      $('#main input:checked').each(function() {
          mainselected.push($(this).attr('id'));
      });
      //alert(unselected);
      var total = $("#totalselection").val();
      var rem = total - mainselected.length
      if (selected.length == max){
        $.each(unselected, function(index, value){
          $("#"+value).attr("disabled", true);
          //$("#label_"+value).empty();
        //  alert(value);

        });


        //alert(mainselected.length);
        if(mainselected.length == total){
            $("#nextBtn").show();
            $("#emsg").text("");
        }
        else {

          $("#emsg").text("Please choose remaining "+rem+" items");
        }

      }

      else if (selected.length < max){
        $.each(unselected, function(index, value){
          $("#"+value).attr("disabled", false);


        });
        $("#emsg").text("Please choose remaining "+rem+" items");

         $("#nextBtn").hide(true);
      }
    }


</script>
