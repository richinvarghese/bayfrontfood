<!--Main Content-->
    <h1 class="margin-bottom">Menu Items</h1>
    <div class="row">
        <div class="form-group default-padding" style="text-align:right">
            <a href="#"><button type="button" class="btn btn-green btn-icon item-edit" style="background: #bea26a; border: 1px solid #bea26a;" data-target="#add-category" data-toggle="modal">
                Add a Category
                <i class="glyphicon glyphicon-folder-open" style="background: #31271e; border: 1px solid #31271e;"></i>
                </button>
            </a>
            <!--<a href="<?php echo base_url()?>courses/index"><button type="button" class="btn btn-green btn-icon item-edit" style="background: #bea26a; border: 1px solid #bea26a;">
                Add Courses
                <i class="glyphicon glyphicon-list-alt" style="background: #31271e; border: 1px solid #31271e;"></i>
                </button>
            </a>-->
        </div>
    </div>
    
    <!-- Modal Add Category Starts Here-->

    <div class="modal fade" id="add-category">
       <div class="modal-dialog">
          <div class="modal-content">
            <form role="form" method="POST" action="<?php echo base_url()?>category/add_category" novalidate="novalidate" enctype="multipart/form-data">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add a Category</h4>
             </div>

             <div class="modal-body">
                <div class="row"> 
                    <div class="form-group">
                         <label for="field-1" class="col-sm-3 control-label">Category Name</label>
                         <div class="col-sm-6">
                           <input type="text" name="cat_name" placeholder="Category name"  class="form-control" id="item_eng" data-validate="required">
                         </div>
                   </div>
                </div>
                <br>
                <div class="row"> 
                   <div class="form-group">
                         <label for="field-2" class="col-sm-3 control-label">Description</label>
                         <div class="col-sm-6">
                             <textarea name="cat_descrip" placeholder="A Short description on Category" class="form-control" id="item_chn" data-validate="required"></textarea>
                         </div>
                   </div>
                <br>
                </div>
             </div>     
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
             </div>
            </form>
          </div>
       </div>
    </div>

    <!-- Modal Add Category Ends Here-->
    
    <ol class="breadcrumb 2">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li class="active">
          <strong>Menu Items</strong>
        </li>
    </ol>

    <div class="row">
     <div class="col-md-4">
      <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>items/index" enctype="multipart/form-data">
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
              <div class="panel-heading"  style="background: #31271e;">
                <div class="panel-title" style="color: #fff">
                  Add an Item
                </div>
              </div>
              <div class="panel-body" style="background: #31271e;">

                  <?php if(isset($_SESSION['item-success'])){?>
                    <center>
                        <div class="form-session-success">
                        <?php echo $_SESSION['item-success'];?>
                        </div>
                    </center>
                  <br>
                <?php }?>

                <?php if(isset($_SESSION['item-error'])){?>
                    <center>
                        <div class="form-session-errors">
                        <?php echo $_SESSION['item-error'];?>
                        </div>
                    </center>
                  <br>
                <?php }?>

                <div class="form-group">
                  <label for="field-1" class="col-sm-3 control-label">Item Name</label>
                    <span class="description">(In English Characters)</span>
                  <div class="col-sm-6">
                    <input type="text" name="item_eng" value="" placeholder="Item name"  class="form-control" id="field-1" data-validate="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="field-2" class="col-sm-3 control-label">Item</label>
                    <span class="description">(In Chinese Characters)</span>
                  <div class="col-sm-6">
                      <input type="" name="item_chn" value="" placeholder="项目名称" class="form-control" id="field-2" data-validate="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="field-3" class="col-sm-3 control-label">Price</label>
                    <span class="description">(Ala-Carte)</span>
                  <div class="col-sm-6">
                    <input type="text" name="item_price" value="" placeholder="25"  class="form-control" id="field-3" data-validate="required">
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="field-3" class="col-sm-3 control-label">Category</label>
                  <div class="col-sm-6">
                    <select class="form-control" name="category_id">
                        <?php foreach($categories as $cats):?>
                        <option value="<?php echo $cats->category_id;?>"><?php echo $cats->category_name;?></option>
                        <?php endforeach;?>
                    </select>
                  </div>
                </div>
                  
                <div class="form-group">
                  <label for="field-4" class="col-sm-3 control-label">Image</label>
                  <div class="col-sm-6">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-info btn-file" style="background: #bea26a; border: 1px solid #bea26a;">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="item_image">
                        </span>
                        <span class="fileinput-filename">(Max Height & Width: 300px)</span>
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                    </div>
                  </div>
                </div>
                <div class="form-group default-padding" style="text-align:center">
                    <button type="submit" name='item_submit' class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                    <button type="reset" class="btn">Reset</button>
                </div>
                </div>
              </div>
            </div>
          </div>
        </form>
        
        <div class="row">    
           <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0" style="border-color: #31271e">
              <div class="panel-heading"  style="background: #31271e;">
                <div class="panel-title" style="color: #fff">
                  Categories
                </div>
              </div>
              <div class="panel-body">

                  <?php if(isset($_SESSION['category-success'])){?>
                    <center>
                        <div class="form-session-success">
                        <?php echo $_SESSION['category-success'];?>
                        </div>
                    </center>
                  <br>
                <?php }?>

                <?php if(isset($_SESSION['category-error'])){?>
                    <center>
                        <div class="form-session-errors">
                        <?php print_r($_SESSION['category-error']);?>
                        </div>
                    </center>
                  <br>
                <?php }?>
                  
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No#</th>
                            <th>Category Name</th>
                            <th>Manage</th>
                        </tr>
                    </thead>
                    <?php $x=1;?>
                    <tbody>
                        <?php while($x<=$cats_rows):?>
                        <?php foreach($categories as $cats):?>
                        <tr>
                            <td><?php echo $x;?></td>
                            <td><?php echo $cats->category_name?></td>
                            <td>
                                <div class="icon-el col-md-2 col-sm-5"><a href="#"><i class="glyphicon glyphicon-edit cat-edit" data-target="#cat-edit" data-toggle="modal" 
                                        data-cat_id="<?php echo $cats->category_id?>" 
                                        data-cat_name="<?php echo $cats->category_name?>" 
                                        data-cat_descrip="<?php echo $cats->category_description?>" >                                      
                                        </i></a></div>
                                
                                <!-- Modal Category Edit Starts Here-->

                                    <div class="modal fade" id="cat-edit">
                                       <div class="modal-dialog">
                                          <div class="modal-content">
                                            <form role="form" method="POST" action="<?php echo base_url()?>category/edit_category" novalidate="novalidate">
                                             <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Edit Category Details</h4>
                                             </div>
                                             <div class="modal-body">
                                                <div class="row"> 
                                                    <div class="form-group">
                                                         <label for="field-1" class="col-sm-3 control-label">Category Name</label>
                                                         <div class="col-sm-6">
                                                           <input type="hidden" name="cat_id" id="cat_id">
                                                           <input type="text" name="cat_name" placeholder="Category name"  class="form-control" id="cat_name" data-validate="required">
                                                         </div>
                                                   </div>
                                                </div>
                                                <br>
                                                <div class="row"> 
                                                   <div class="form-group">
                                                         <label for="field-2" class="col-sm-3 control-label">Description</label>
                                                         <div class="col-sm-6">
                                                             <textarea name="cat_descrip" placeholder="A Short description on Category" class="form-control" id="cat_descrip" data-validate="required"></textarea>
                                                         </div>
                                                   </div>
                                                <br>
                                                </div>
                                             </div>
                                             <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                                             </div>
                                            </form>
                                          </div>
                                       </div>
                                    </div>

                                <!-- Modal Category Edit Ends Here-->
                                
                                
                                <div class="icon-el col-md-2 col-sm-5"><a href="#"><i class="glyphicon glyphicon-remove-sign cat-delete" data-target="#cat-delete" data-toggle="modal" 
                                            data-cat_delete_id="<?php echo $cats->category_id ?>" >
                                        </i></a></div>
                                
                                <!-- Modal Item Delete Starts Here-->

                                    <div class="modal fade" id="cat-delete">
                                       <div class="modal-dialog">
                                          <div class="modal-content">
                                            <form role="form" method="POST" action="<?php echo base_url()?>category/delete_category" novalidate="novalidate">
                                             <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Delete Item</h4>
                                             </div>
                                             <div class="modal-body">
                                                <div class="row"> 
                                                    <div class="col-md-12">
                                                        <input type="hidden" id="cat_delete_id" name="cat_id">
                                                        <h4>Are you sure you wanna delete this Category ?</h4>
                                                        <span class="xx-large" style="color: #bea26a;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                            Warning : The Items associated to this category also will be deleted..!!
                                                        </span>
                                                    </div>
                                                </div>
                                             </div>     
                                             <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                                <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
                                             </div>
                                            </form>
                                          </div>
                                       </div>
                                    </div>

                                <!-- Modal Category Delete Ends Here-->
                                
                                
                            </td>
                        </tr>
                        <?php $x++;?>
                        <?php endforeach;?>
                        <?php endwhile;?>
                    </tbody>
                   </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        
    <div class="col-sm-8">
        
        <!--<script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
                var $table1 = jQuery( '#item-table' );

                // Initialize DataTable
                $table1.DataTable( {
                        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "bStateSave": true
                });

                // Initalize Select Dropdown after DataTables is created
                $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                        minimumResultsForSearch: -1
                });
        } );
        </script>-->
        
        
        <?php if(isset($_SESSION['item-edit-success'])){?>
            <center>
                <div class="form-session-success">
                <?php echo $_SESSION['item-edit-success'];?>
                </div>
            </center>
          <br>
        <?php }?>

        <?php if(isset($_SESSION['item-edit-error'])){?>
            <center>
                <div class="form-session-errors">
                <?php print_r($_SESSION['item-edit-error']);?>
                </div>
            </center>
          <br>
        <?php }?>
      
        <table class="table table-bordered datatable" id="item-table">
            <thead>
                <tr>
                    <th>Thumbnail</th>
                    <th>Item Name<i><span class="description"> (In English) </span></i></th>
                    <th>Item Name<i><span class="description"> (In Chinese) </span></i></th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Manage Items</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($item_list as $item):?>
                <tr class="odd gradeX">
                    <td class="center"><?php echo '<img src="'.base_url().'assets/images/menu/items/'.$item->item_image.'" alt="" class="img-circle" style= "width:60px; border:2px solid #f5f5f5; border-radius: 50%;">'?></td>
                    <td class="center"><?php echo $item->item_eng;?></td>
                    <td class="center"><?php echo $item->item_chn;?></td>
                    <td class="center"><?php echo $settings->entity_currency_symbol.' '.$item->item_price;?></td>
                    <td class="center"><?php echo $item->category_name?></td>
                    <td class="center">
                        
                        <a href="#" class="btn btn-warning btn-sm btn-icon icon-left item-edit" data-target="#item-edit" data-toggle="modal" 
                           data-item_id = "<?php echo $item->item_id ?>" 
                           data-item_add_eng = "<?php echo $item->item_eng ?>" 
                           data-item_add_chn = "<?php echo $item->item_chn ?>" 
                           data-item_price = "<?php echo $item->item_price ?>" >
                           <i class="entypo-pencil"></i>
                            Edit
                        </a>
                        
                        <!-- Modal Item Edit Starts Here-->

                            <div class="modal fade" id="item-edit">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>items/item_edit" novalidate="novalidate" enctype="multipart/form-data">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Edit Item Details</h4>
                                     </div>

                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                 <label for="field-1" class="col-sm-3 control-label">Item Name</label>
                                                   <span class="description">(In English Characters)</span>
                                                 <div class="col-sm-6">
                                                   <input type="text" name="item_eng" placeholder="Item name"  class="form-control" id="item_add_eng">
                                                   <input type="hidden" name="item_id" class="form-control" id="item_id">
                                                 </div>
                                           </div>
                                        <br>
                                           <div class="form-group">
                                                 <label for="field-2" class="col-sm-3 control-label">Item</label>
                                                   <span class="description">(In Chinese Characters)</span>
                                                 <div class="col-sm-6">
                                                     <input type="" name="item_chn" placeholder="项目名称" class="form-control" id="item_add_chn">
                                                 </div>
                                           </div>
                                        <br>
                                           <div class="form-group">
                                                <label for="field-3" class="col-sm-3 control-label">Price</label>
                                                  <span class="description">(Ala-Carte)</span>
                                                <div class="col-sm-6">
                                                  <input type="text" name="item_price" placeholder="25"  class="form-control" id="item_price" data-validate="required">
                                                </div>
                                           </div>
                                        <br>
                                        
                                        <div class="form-group">
                                            <label for="field-3" class="col-sm-3 control-label">Category</label>
                                            <div class="col-sm-6">
                                              <select name="category_id" class="selectboxit" data-first-option="false">
                                                <option value="0">Select Category</option>
                                                <?php foreach($categories as $cats):?>
                                                <option value="<?php echo $cats->category_id?>"><?php echo $cats->category_name;?></option>
                                                <?php endforeach;?>
                                              </select>
                                            </div>
                                        </div>
                                        <br>
                                        
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" name='item_edit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>

                            <!-- Modal Item Edit Ends Here-->
                        
                        <a href="#" class="btn btn-primary btn-sm btn-icon icon-left item-image-edit" data-target="#item-image-edit" data-toggle="modal" 
                           data-item_image_id="<?php echo $item->item_id;?>" 
                           data-item_image="<?php echo $item->item_image;?>" >
                            <i class="entypo-camera"></i>
                            Update Item Picture
                        </a>
                            
                        <!-- Modal to Update Image Starts Here-->

                        <div class="modal fade" id="item-image-edit">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                <form role="form" method="POST" action="<?php echo base_url()?>items/item_edit_image" novalidate="novalidate" enctype="multipart/form-data">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Change Display Picture</h4>
                                 </div>

                                 <div class="modal-body">
                                    <div class="row"> 
                                        <div class="col-md-12">
                                            <div class="form-group">
                                               <label for="field-8" class="col-sm-5 control-label">Update Item Image</label>
                                                  <div class="col-sm-10">
                                                      <div class="fileinput fileinput-new" data-provides="fileinput">
                                                         <input type="hidden">
                                                         <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                                             <img src="" alt=""  id="item_image">
                                                             <input type="hidden" id="item_image_id" name="item_id">
                                                         </div>
                                                         <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 10px;"></div>
                                                         <div>
                                                             <br>
                                                             <span class="btn btn-white btn-file">
                                                                 <span class="fileinput-new">Select image</span>
                                                                 <span class="fileinput-exists">Change</span>
                                                                 <input type="file" name="item_image" size="20">
                                                             </span>
                                                             <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                         </div>
                                                     </div>
                                                      <span class="line small"><i>(Max Height & Width: 300px)</i></span>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>     
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" name='item_edit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                                 </div>
                                </form>
                              </div>
                           </div>
                        </div>

                        <!-- Modal to Update Image Ends Here-->
                        
                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left item-delete" data-target="#item-delete" data-toggle="modal" 
                           data-item_delete_id="<?php echo $item->item_id?>" >
                            <i class="entypo-cancel"></i>
                            Delete
                        </a>
                        
                        
                        <!-- Modal Item Delete Starts Here-->

                            <div class="modal fade" id="item-delete">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>items/delete_item" novalidate="novalidate" enctype="multipart/form-data">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Delete Item</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="col-md-12">
                                                <input type="hidden" id="item_delete_id" name="item_delete">
                                                <span class="line large">Are you sure you wanna delete this Item ?</span>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                        
                        <!-- Modal Item Delete Ends Here-->
                        
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Thumbnail</th>
                    <th>Item Name<i><span class="description"> (In English) </span></i></th>
                    <th>Item Name<i><span class="description"> (In Chinese) </span></i></th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Manage Items</th>
                </tr>
            </tfoot>
        </table>
    </div>  
   </div>
               
<!--End of Main Content-->


<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/selectboxit/jquery.selectBoxIt.css">

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>

<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script src="<?php echo base_url()?>assets/js/fileinput.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<!--<script src="<?php echo base_url()?>assets/js/datatables/datatables.js"></script>-->
<script src="<?php echo base_url()?>assets/js/select2/select2.min.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>

<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

<script>
    
//Item Edit Script    
$(document).on("click", ".item-edit", function (event) {
    $("#item_id").val($(this).data('item_id'))
    $("#item_add_eng").val($(this).data('item_add_eng'))
    $("#item_add_chn").val($(this).data('item_add_chn'))
    $("#item_price").val($(this).data('item_price'))
});

//Item Edit Image Script    
$(document).on("click", ".item-image-edit", function (event) {
    $("#item_image_id").val($(this).data('item_image_id'))
    var item_image = (baseurl + 'assets/images/menu/items/'+ $(this).data('item_image'))
    $('#item_image').attr("src", item_image)
});

//Item Delete Script    
$(document).on("click", ".item-delete", function (event) {
    $("#item_delete_id").val($(this).data('item_delete_id'))
});

//Category Edit Script    
$(document).on("click", ".cat-edit", function (event) {
    $("#cat_id").val($(this).data('cat_id'))
    $("#cat_name").val($(this).data('cat_name'))
    $("#cat_descrip").val($(this).data('cat_descrip'))
});

//Item Delete Script    
$(document).on("click", ".cat-delete", function (event) {
    $("#cat_delete_id").val($(this).data('cat_delete_id'))
});
</script>