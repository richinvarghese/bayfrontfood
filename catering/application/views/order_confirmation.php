<!--================Banner Area =================-->
<section class="banner_area" style='background: url(<?php echo base_url();?>/frontend/assets/img/banner/banner-bg-1.jpg) no-repeat scroll center center;'>
    <div class="container">
        <div class="banner_content">
            <h4>BUFFET MENU</h4>
            <a href="http://bayfrontfood.sg">Home</a>
            <a href="<?php echo base_url()?>frontend_controller/listmenu/index">Buffet List</a>
            <a class="active">Buffet List</a>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->

<!--================End Our feature Area =================-->
        <section class="most_popular_item_area menu_list_page">
            <div class="container">
                <div class="s_black_title">
                    <h3>Booking Confirmation</h3><br/>
                    <h4><bold>For Buffet <?php echo $buffet->buffet_name;?></bold></h4>
                    <br/>
                    <hr>
                    <p>Thank you for opted Bayfront Catering Services. We will get back you shortly.</p>
                    <p>Have a Great Day</p>
                </div>
            </div>
        </section>
        <!--================End Our Menu Listing Area =================-->