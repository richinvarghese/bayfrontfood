<!--Main Content-->
    <h1 class="margin-bottom">Manage Buffet Addons</h1>

    <ol class="breadcrumb 2">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li class="active">
          <strong>Manage Buffet Addons</strong>
        </li>
    </ol>

    <div class="row">
     <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>addons/add_addon_item" enctype="multipart/form-data">
      <div class="col-md-4">
        <div class="panel panel-primary" data-collapsed="0">
          <div class="panel-heading"  style="background: #31271e;">
            <div class="panel-title" style="color: #fff">
              Create New Add-on Menu
            </div>
          </div>
          <div class="panel-body" style="background: #31271e;">
              
              <?php if(isset($_SESSION['addon-success'])){?>
                <center>
                    <div class="form-session-success">
                    <?php echo $_SESSION['addon-success'];?>
                    </div>
                </center>
              <br>
            <?php }?>

            <?php if(isset($_SESSION['addon-error'])){?>
                <center>
                    <div class="form-session-errors">
                    <?php echo $_SESSION['addon-error'];?>
                    </div>
                </center>
              <br>
            <?php }?>
            
            <div class="form-group">
              <label for="field-1" class="col-sm-3 control-label">Add-on Item Name</label>
                <span class="description">(Eg: Lobster)</span>
              <div class="col-sm-6">
                <input type="text" name="addon_eng" value="" placeholder="Item Name (In English)"  class="form-control" id="field-1" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label for="field-1" class="col-sm-3 control-label">Chinese Name</label>
                <span class="description">(In Chinese Characters)</span>
              <div class="col-sm-6">
                <input type="text" name="addon_chn" value="" placeholder="Item Name (In Chinese Characters)"  class="form-control" id="field-1" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label for="field-3" class="col-sm-3 control-label">Price</label>
              <span class="description">(Per Pax)</span>
              <div class="col-sm-6">
                <input type="text" name="addon_price" value="" placeholder="Eg: 10.80"  class="form-control" id="field-3" data-validate="required">
              </div>
            </div>
            <div class="form-group default-padding" style="text-align:center">
                <button type="submit" name='submit' class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                <button type="reset" class="btn">Reset</button>
            </div>
            </div>
          </div>
        </div>
     </form>     
    <div class="col-sm-8">
        
        <!--<script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
                var $table1 = jQuery( '#item-table' );

                // Initialize DataTable
                $table1.DataTable( {
                        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "bStateSave": true
                });

                // Initalize Select Dropdown after DataTables is created
                $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                        minimumResultsForSearch: -1
                });
        } );
        </script>-->
        
        
        <?php if(isset($_SESSION['addon-edit-success'])){?>
            <center>
                <div class="form-session-success">
                <?php echo $_SESSION['addon-edit-success'];?>
                </div>
            </center>
          <br>
        <?php }?>

        <?php if(isset($_SESSION['addon-edit-error'])){?>
            <center>
                <div class="form-session-errors">
                <?php print_r($_SESSION['addon-edit-error']);?>
                </div>
            </center>
          <br>
        <?php }?>
      
        <table class="table table-bordered datatable" id="item-table">
            <thead>
                <tr>
                    <th>Addon Item</th>
                    <th>In Chinese</th>
                    <th>Price/Pax</th>
                    <th>Manage</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($addon_items as $addon):?>
                <tr class="odd gradeX">
                    <td class="center"><?php echo $addon->addon_eng?></td>
                    <td class="center"><?php echo $addon->addon_chn?></td>
                    <td class="center"><?php echo $settings->entity_currency_symbol.' '.$addon->addon_price;?></td>
                    <td class="center">
                        
                        <a href="#" class="btn btn-warning btn-sm btn-icon icon-left addon-edit" data-target="#addon-edit" data-toggle="modal" 
                           data-addon_id = "<?php echo $addon->addon_id;?>"
                           data-addon_eng = "<?php echo $addon->addon_eng;?>"
                           data-addon_chn = "<?php echo $addon->addon_chn;?>" 
                           data-addon_price = "<?php echo $addon->addon_price;?>" >
                           <i class="entypo-pencil"></i>
                            Edit
                        </a
                        
                        <!-- Modal Item Delete Starts Here-->
                        <div class="modal fade" id="addon-edit">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                <form role="form" method="POST" action="<?php echo base_url()?>addons/update_addon_item" novalidate="novalidate">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Delete Item</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="row"> 
                                      <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Add-on Item Name</label>
                                        <div class="col-sm-6">
                                          <input type="text" name="addon_eng" value="" placeholder="Item Name (In English)"  class="form-control" id="addon_eng" data-validate="required">
                                          <input type="hidden" name="addon_id" value=""  id="addon_id" >
                                        </div>
                                      </div>
                                    </div>
                                     <br>
                                    <div class="row"> 
                                      <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Add-on Item Name</label>
                                        <div class="col-sm-6">
                                          <input type="text" name="addon_chn" value="" placeholder="Item Name (In Chinese Character2"  class="form-control" id="addon_chn" data-validate="required">
                                        </div>
                                      </div>
                                    </div>
                                     <br>
                                    <div class="row"> 
                                      <div class="form-group">
                                        <label for="field-3" class="col-sm-3 control-label">Price</label>
                                        <div class="col-sm-6">
                                          <input type="text" name="addon_price" value="" placeholder="Eg: 10.80"  class="form-control" id="addon_price" data-validate="required">
                                        </div>
                                      </div><br>
                                    </div>
                                    <br>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                    <button type="submit" name="submit" class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Yes, Update It</button>
                                 </div>
                                </form>
                              </div>
                           </div>
                        </div>
                    <!-- Modal Item Delete Ends Here-->
                        
                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left addon-delete" data-target="#addon-delete" data-toggle="modal" 
                           data-addon_delete_id="<?php echo $addon->addon_id?>" >
                            <i class="entypo-cancel"></i>
                            Delete
                        </a>
                    
                    <!-- Modal Item Delete Starts Here-->
                        <div class="modal fade" id="addon-delete">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                <form role="form" method="POST" action="<?php echo base_url()?>addons/delete_addon_item" novalidate="novalidate">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Delete Item</h4>
                                 </div>
                                 <div class="modal-body">
                                        <div class="row"> 
                                            <div class="col-md-12">
                                                <input type="hidden" id="addon_delete_id" name="addon_id">
                                                <span class="line xlarge">Are you sure you wanna delete this Buffet ?</span><br>
                                                <span class="xx-large" style="color: #bea26a;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                    Warning : Once Deleted, this item won't be available in Add-ons Item List..!!
                                                </span>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
                                     </div>
                                </form>
                              </div>
                           </div>
                        </div>
                    <!-- Modal Item Delete Ends Here-->
                        
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Addon Item</th>
                    <th>In Chinese</th>
                    <th>Price/Pax</th>
                    <th>Manage</th>
                </tr>
            </tfoot>
        </table>
    </div>  
   </div>
               
<!--End of Main Content-->


<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2.css">

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>

<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/fileinput.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<!--<script src="<?php echo base_url()?>assets/js/datatables/datatables.js"></script>-->
<script src="<?php echo base_url()?>assets/js/select2/select2.min.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>

<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

<script>
    
//Item Edit Script    
$(document).on("click", ".addon-edit", function (event) {
    $("#addon_id").val($(this).data('addon_id'))
    $("#addon_eng").val($(this).data('addon_eng'))
    $("#addon_chn").val($(this).data('addon_chn'))
    $("#addon_price").val($(this).data('addon_price'))
});

//Item Delete Script    
$(document).on("click", ".addon-delete", function (event) {
    $("#addon_delete_id").val($(this).data('addon_delete_id'))
});

</script>