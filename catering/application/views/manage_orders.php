<!--Main Content-->
    <h1 class="margin-bottom">Manage Orders</h1>
    <ol class="breadcrumb 2">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li class="active">
          <strong>Manage Orders</strong>
        </li>
    </ol>

    <div class="row">
    <div class="col-sm-12">
        
        <script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
                var $table1 = jQuery( '#item-table' );

                // Initialize DataTable
                $table1.DataTable( {
                        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "bStateSave": true
                });

                // Initalize Select Dropdown after DataTables is created
                $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                        minimumResultsForSearch: -1
                });
        } );
        </script>
        
        
        <?php if(isset($_SESSION['order-success'])){?>
            <center>
                <div class="form-session-success">
                <?php echo $_SESSION['order-success'];?>
                </div>
            </center>
          <br>
        <?php }?>

        <?php if(isset($_SESSION['order-error'])){?>
            <center>
                <div class="form-session-errors">
                <?php echo ($_SESSION['order-error']);?>
                </div>
            </center>
          <br>
        <?php }?>
      
        <table class="table table-bordered datatable" id="item-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Order</th>
                    <th>Total Pax</th>
                    <th>Customer</th>
                    <th>Contact</th>
                    <th>Grand Total</th>
                    <th>Dining Date & Time</th>
                    <th>Location</th>
                    <th>Order Status</th>
                    <th>Manage</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=1;?>
                <?php foreach($orders as $order):?>
                <tr class="odd gradeX">
                    <td class="center"><?php echo $i;?></td>
                    <td class="center"><?php echo '<div class="label label-secondary"><strong>'.$order->order_buffet.'</strong></div>';?></td>
                    <td class="center"><?php echo '<i class="entypo-users" style="color:#bea26a"></i> '.$order->order_pax;?></td>
                    <td class="center"><?php echo $order->customer_name;?></td>
                    <td class="center"><?php echo $order->customer_contact;?></td>
                    <td class="center"><?php echo $settings->entity_currency_symbol.' '. number_format(round($order->order_gtotal, 1), 2);?></td>
                    <td class="center"><?php echo '<i class="glyphicon glyphicon-calendar" style="color:#bea26a"></i> '.date('d-M-Y', strtotime($order->dining_date)).'  <i class="glyphicon glyphicon-time" style="color:#bea26a"></i> '.date('h:i:A', strtotime($order->dining_time));?></td>
                    <td class="center"><?php echo '<i class="glyphicon glyphicon-map-marker" style="color:#bea26a"></i> '.$order->address_block.', '.$order->address_street.', '.$order->address_postal;?></td>
                    <td class="center"><?php if($order->order_status == 0){ 
                        echo "<div class='label label-secondary'><strong>Pending</strong></div>";  
                        }
                        elseif($order->order_status == 1){
                            echo "<div class='label label-success'><strong>Approved</strong></div>";
                        }
                        elseif($order->order_status == 2){
                            echo "<div class='label label-danger'><strong>Cancelled</strong></div>";
                        }
                        elseif($order->order_status == 3){
                            echo "<div class='label label-info'><strong>Completed</strong></div>";
                        }
                        ?>
                    </td>
                    <td class="center">
                        
                       
                        <a href="<?php echo base_url().'manageorders/edit_order/'.$order->order_id?>" class="btn btn-info btn-sm btn-icon icon-left" >
                            <i class="entypo-docs"></i>
                            View / Manage Order
                        </a>
                        
                        
                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left delete-order" data-target="#delete-order" data-toggle="modal" data-order_id="<?php echo $order->order_id;?>">
                            <i class="entypo-cancel"></i>
                            Delete
                        </a>
                        
                        <!-- Modal for Delete Order Starts Here-->
                            <div class="modal fade" id="delete-order">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>manageorders/delete_order_list" novalidate="novalidate">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Delete Order</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                <input type="hidden" name="order_id" id="order_id">
                                                <span class="line xlarge" style="text-align: -webkit-left;">Are you sure you wanna Delete this Order ?</span><br>
                                                <span class="xx-large" style="color: #bea26a; text-align: -webkit-left;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                    Warning : Once Deleted, all information entered will be deleted from Database !!
                                                </span>
                                            </div>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Close It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete this Order</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                            <!-- Modal for Delete Order Ends Here-->
                        
                    </td>
                    <?php $i++;?>
                </tr>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Order</th>
                    <th>Total Pax</th>
                    <th>Customer</th>
                    <th>Contact</th>
                    <th>Grand Total</th>
                    <th>Dining Date & Time</th>
                    <th>Location</th>
                    <th>Order Status</th>
                    <th>Manage</th>
                </tr>
            </tfoot>
        </table>
    </div>  
   </div>
               
<!--End of Main Content-->


<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2.css">

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>

<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/fileinput.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url()?>assets/js/datatables/datatables.js"></script>
<script src="<?php echo base_url()?>assets/js/select2/select2.min.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>

<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

<script>
    
//Item Edit Script    
$(document).on("click", ".buffet-edit", function (event) {
    $("#buffet_id").val($(this).data('buffet_id'))
    $("#buffet_name").val($(this).data('buffet_name'))
    $("#buffet_min_pax").val($(this).data('buffet_min_pax'))
    $("#buffet_price").val($(this).data('buffet_price'))
    $("#buffet_course_nos").val($(this).data('buffet_course_nos'))
});
$(document).on("click", ".delete-order", function(event){
    
    $('#order_id').val($(this).data('order_id'));
})

</script>