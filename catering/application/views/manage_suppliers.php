<h1 class="margin-bottom">Inventory Settings</h1>
<ol class="breadcrumb 2">
    <li>
        <a href="index.html">
            <i class="fa-home"></i>Home
        </a>
    </li>
    <li class="active">
        <strong>Suppliers</strong>
    </li>
</ol>
<br>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary" data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Add Supplier
                        </div>
                    </div>
                <form role="form" method="post" class="form-horizontal form-groups-bordered validate" style="margin: 10px;" action="<?php echo base_url();?>supplier/add_supplier" novalidate="novalidate">    
                    <div class="panel-body">
                        <?php if(isset($_SESSION['success-supplier-add'])){?>
                            <center>
                                <div class="badge badge-success">
                                <?php echo $_SESSION['success-supplier-add'];?>
                                </div>
                            </center>
                        <?php }?>

                        <?php if(isset($_SESSION['error-supplier-add'])){?>
                            <center>
                                <div class="badge badge-danger">
                                <?php echo $_SESSION['error-supplier-add'];?>
                                </div>
                            </center>
                        <?php }?>
                        <br>
                        <div class="row">		
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Supplier</label>
                                    <input class="form-control" name="supplier_name" data-validate="required" placeholder="Eg: Akstech Pte Ltd" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Building/Block</label>
                                    <input class="form-control" name="supplier_block" data-validate="required" placeholder="Building Name or Block" />
                                </div>
                            </div>			
			</div>
					
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Unit</label>
                                    <input class="form-control" name="supplier_unit" data-validate="required" placeholder="Unit Number" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Street Address</label>
                                    <input class="form-control" name="supplier_street" placeholder="Street Address" data-validate="required"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Postal</label>
                                    <input class="form-control" name="supplier_postal" data-validate="required,number" placeholder="Postal Code" />
                                </div>
                            </div>
                        </div>		
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Contact Person</label>
                                    <input class="form-control" name="supplier_person" data-validate="required" placeholder="Contact Person" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Contact No:</label>
                                    <input class="form-control" name="supplier_contact" data-validate="required" placeholder="Contact No:" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input class="form-control" name="supplier_email" data-validate="required,email" placeholder="Supplier Contact Email" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group default-padding" style="text-align:center">
                            <button type="submit" name="submit" class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Add Supplier</button>
                            <button type="reset" class="btn">Reset</button>
                        </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    
        <div class="row">

            <div class="col-sm-12">
        
        <!--<script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
                var $table1 = jQuery( '#item-table' );

                // Initialize DataTable
                $table1.DataTable( {
                        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "bStateSave": true
                });

                // Initalize Select Dropdown after DataTables is created
                $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                        minimumResultsForSearch: -1
                });
        } );
        </script>-->
        
        
        <?php if(isset($_SESSION['success-supplier-edit'])){?>
            <center>
                <div class="form-session-success">
                <?php echo $_SESSION['success-supplier-edit'];?>
                </div>
            </center>
          <br>
        <?php }?>

        <?php if(isset($_SESSION['error-supplier-edit'])){?>
            <center>
                <div class="form-session-errors">
                <?php echo ($_SESSION['error-supplier-edit']);?>
                </div>
            </center>
          <br>
        <?php }?>
      
        <table class="table table-bordered datatable" id="item-table">
            <thead>
                <tr>
                    <th>Supplier Name</th>
                    <th>Address</th>
                    <th>Contact</th>
                    <th>Email</th>
                    <th>Total Supplies</th>
                    <th>Status</th>
                    <th>Manage</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($suppliers as $supplier):?>
                <tr class="odd gradeX">
                    <td class="center"><?php echo '<div class="label label-secondary"><strong>'.$supplier->supplier_name.'</strong></div>';?></td>
                    <td class="center"><?php echo '<i class="glyphicon glyphicon-map-marker" style="color:#bea26a"></i> '.$supplier->supplier_block.', #'.$supplier->supplier_unit.', '.$supplier->supplier_street.', Singapore '.$supplier->supplier_postal;?></td>
                    <td class="center"><?php echo $supplier->supplier_contact;?></td>
                    <td class="center"><?php echo $supplier->supplier_email;?></td>
                    <td class="center"><?php echo $settings->entity_currency_symbol.' '. number_format(round($supplier->supplier_total_supplies, 1), 2);?></td>
                    <td class="center"><?php if($supplier->supplier_status == 1){ 
                        echo "<div class='label label-success'><strong>Active</strong></div>";  
                        }
                        else{
                            echo "<div class='label label-danger'><strong>Not Avtive</strong></div>";
                        }
                        ?>
                    </td>
                    <td class="center">
                        
                       
                        <a href="" class="btn btn-info btn-sm btn-icon icon-left edit-supplier" data-target="#edit-supplier" data-toggle="modal" 
                           data-supplier_id="<?php echo $supplier->supplier_id;?>" 
                           data-supplier_name="<?php echo $supplier->supplier_name;?>" 
                           data-supplier_block="<?php echo $supplier->supplier_block;?>" 
                           data-supplier_unit="<?php echo $supplier->supplier_unit;?>" 
                           data-supplier_street="<?php echo $supplier->supplier_street;?>" 
                           data-supplier_postal="<?php echo $supplier->supplier_postal;?>" 
                           data-supplier_person="<?php echo $supplier->supplier_person;?>" 
                           data-supplier_contact="<?php echo $supplier->supplier_contact;?>" 
                           data-supplier_email="<?php echo $supplier->supplier_email;?>"
                           ><i class="entypo-docs"></i>
                            Edit Supplier Info
                        </a>
                        
                        
                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left delete-supplier" data-target="#delete-supplier" data-toggle="modal" data-supplier_id2="<?php echo $supplier->supplier_id;?>">
                            <i class="entypo-cancel"></i>
                            Delete Supplier
                        </a>
                        
                        <!-- Modal for Delete Order Starts Here-->
                        <div class="modal fade" id="edit-supplier">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                <form role="form" method="POST" action="<?php echo base_url()?>supplier/edit_supplier" novalidate="novalidate">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" style="text-align: -webkit-left;">Supplier Details</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Supplier Name</label>
                                                <input type="text" name="supplier_name" id="supplier_name" class="form-control" disabled>
                                                <input type="hidden" name="supplier_id" id="supplier_id" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Block/Building</label>
                                                <input type="text" name="supplier_block" id="supplier_block" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Unit</label>
                                                <input type="text" name="supplier_unit" id="supplier_unit" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Street Address</label>
                                                <input type="text" name="supplier_street" id="supplier_street" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Postal</label>
                                                <input type="text" name="supplier_postal" id="supplier_postal" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Contact Person</label>
                                                <input type="text" name="supplier_person" id="supplier_person" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Phone</label>
                                                <input type="text" name="supplier_contact" id="supplier_contact" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Email</label>
                                                <input type="text" name="supplier_email" id="supplier_email" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Status</label>
                                                <select name="supplier_status" class="selectboxit">
                                                    <!-- Pending Default Selection-->
                                                    <optgroup label="Choose Status">
                                                        <option value="1">Active</option>
                                                        <option value="2">Deactive</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                     <br/>
                                 </div>     
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Close It</button>
                                    <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Update</button>
                                 </div>
                                </form>
                              </div>
                           </div>
                        </div>
                        <!-- Modal for Delete Order Ends Here-->
                        
                        <!-- Modal for Delete Order Starts Here-->
                            <div class="modal fade" id="delete-supplier">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>supplier/delete_supplier" novalidate="novalidate">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Delete Supplier</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                <input type="hidden" name="supplier_id" id="supplier_id2">
                                                <span class="line xlarge" style="text-align: -webkit-left;">Are you sure you wanna Delete this Supplier ?</span><br>
                                                <span class="xx-large" style="color: #bea26a; text-align: -webkit-left;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                    Warning : Once Deleted, all information supplies associated with this Supplier also will be affected !!
                                                </span>
                                            </div>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Close It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete this Order</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                            <!-- Modal for Delete Order Ends Here-->
                        
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Supplier Name</th>
                    <th>Address</th>
                    <th>Contact</th>
                    <th>Email</th>
                    <th>Total Supplies</th>
                    <th>Status</th>
                    <th>Manage</th>
                </tr>
            </tfoot>
        </table>
    </div>
        </div>
        



<style>
    .form-horizontal .form-group {
        margin-left: auto;
        margin-right:auto;
}
</style>
<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.css">


<!-- Bottom scripts (common) -->
<script src="<?php echo base_url();?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/js/joinable.js"></script>
<script src="<?php echo base_url();?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url();?>assets/js/neon-api.js"></script>


<!-- Imported scripts on this page -->
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<!--<script src="<?php echo base_url()?>assets/js/datatables/datatables.js"></script>-->
<script src="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url();?>assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="<?php echo base_url();?>assets/js/neon-demo.js"></script>

<script type="text/javascript">
$(document).on("click", ".edit-supplier", function(event){
    $('#supplier_id').val($(this).data('supplier_id'))
    $('#supplier_name').val($(this).data('supplier_name'))
    $('#supplier_block').val($(this).data('supplier_block'))
    $('#supplier_unit').val($(this).data('supplier_unit'))
    $('#supplier_street').val($(this).data('supplier_street'))
    $('#supplier_postal').val($(this).data('supplier_postal'))
    $('#supplier_person').val($(this).data('supplier_person'))
    $('#supplier_contact').val($(this).data('supplier_contact'))
    $('#supplier_email').val($(this).data('supplier_email'))
})

$(document).on("click", ".delete-supplier", function(event){
    $('#supplier_id2').val($(this).data('supplier_id2'))
})

</script>