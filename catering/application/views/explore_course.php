
<!--Main Content-->
    <h1 class="margin-bottom">Manage Buffets</h1>
    <ol class="breadcrumb 2">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li>
          <a href="<?php echo base_url()?>buffets/index">
            <i class="fa-home"></i>Buffets
          </a>
        </li>
        <li class="active">
          <strong>Add Courses</strong>
        </li>
    </ol>
    
        

    
    <h1>Manage Courses</h1>
		
    <div class="row">
              
        <div class="col-md-3">
            <div id="toc"></div>
        </div>

            <div class="col-md-9 tocify-content">
                
                <!--Validation Message Starts Here-->
                <?php if(isset($_SESSION['course-success'])){?>
                    <center>
                        <div class="form-session-success">
                        <?php echo $_SESSION['course-success'];?>
                        </div>
                    </center>
                  <br>
                <?php }?>

                <?php if(isset($_SESSION['course-error'])){?>
                    <center>
                        <div class="form-session-errors">
                        <?php echo $_SESSION['course-error'];?>
                        </div>
                    </center>
                  <br>
                <?php }?>
                <!--Validation Message Ends Here-->
                
                <?php foreach($course as $cos):?>
                <h2><?php  echo $cos->course_name;?>
                    <a href="#">
                        <i class="entypo-trash delete-course" data-target="#delete-course" data-toggle="modal" 
                           data-course_id="<?php echo $cos->course_id;?>" 
                           data-buffet_id="<?php echo $settings->buffet_id;?>">
                        </i>
                    </a>
                    <a href="#">
                        <i class="entypo-plus-circled add-items" data-target="#add-items" data-toggle="modal" data-backdrop="static"
                           data-course_id_3="<?php echo $cos->course_id;?>"
                           data-buffet_id_3="<?php echo $cos->buffet_id;?>" >
                        </i>
                    </a>
                </h2>
                    <i style="color: red;"><?php echo "( Maximum selections allowed for this course is ".$cos->course_allowed_selection ." )<br><br>";?></i>
                        <div class="row showcase-icon-list">
                            <?php foreach($cos->items as $item):?> 
                            <div class="icon-el col-md-3 col-sm-4">
                                <a href="#"><i class="entypo-trash delete-item" data-target="#delete-item" data-toggle="modal" 
                                               data-item_id="<?php echo $item[0]->item_id;?>" 
                                               data-buffet_id_2="<?php echo $settings->buffet_id;?>"
                                               data-course_id_2 = "<?php echo $cos->course_id;?>">
                                    </i><?php echo $item[0]->item_eng. " (". $item[0]->item_chn.") ";?>
                                </a>
                            </div>
                            <?php endforeach;?>
                        </div>
                    
                <?php endforeach;?>
            </div>
        
            <!-- Modal Delete Course Starts Here-->
            <div class="modal fade" id="delete-course">
               <div class="modal-dialog">
                  <div class="modal-content">
                    <form role="form" method="POST" action="<?php echo base_url()?>manage_courses/delete_course" novalidate="novalidate">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Course</h4>
                     </div>
                     <div class="modal-body">
                        <div class="row"> 
                            <div class="col-md-12">
                                <input type="hidden" id="course_id" name="course_id">
                                <input type="hidden" id="buffet_id" name="buffet_id">
                                <h4>Are you sure you wanna delete this Course ?</h4>
                                <span class="xx-large" style="color: #bea26a;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                    Warning : The course items associated to this course will also be deleted..!!
                                </span>
                            </div>
                        </div>
                        <br>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                        <button type="submit" name="submit" class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
                     </div>
                    </form>
                  </div>
               </div>
            </div>
        <!-- Modal Delete Course Ends Here-->
        
        <!-- Modal Item Delete Starts Here-->
            <div class="modal fade" id="delete-item">
               <div class="modal-dialog">
                  <div class="modal-content">
                    <form role="form" method="POST" action="<?php echo base_url()?>manage_courses/delete_item" novalidate="novalidate">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Item</h4>
                     </div>
                     <div class="modal-body">
                        <div class="row"> 
                            <div class="col-md-12">
                                <input type="hidden" id="item_id" name="item_id" value="">
                                <input type="hidden" id="course_id_2" name="course_id" value="">
                                <input type="hidden" id="buffet_id_2" name="buffet_id" value="">
                                <h4>Are you sure you wanna delete this Item ?</h4>
                                <span class="xx-large" style="color: #bea26a;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                    Warning : The item will be deleted from this Course Item List..!!
                                </span>
                            </div>
                        </div>
                        <br>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                        <button type="submit" name="submit" class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
                     </div>
                    </form>
                  </div>
               </div>
            </div>
        <!-- Modal Item Delete Ends Here-->
        
        <!-- Modal Item Add Starts Here-->
            <div class="modal fade" id="add-items">
               <div class="modal-dialog">
                  <div class="modal-content">
                    <form role="form" method="POST" action="<?php echo base_url()?>manage_courses/add_course_item" novalidate="novalidate">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Course Items</h4>
                     </div>
                     <div class="modal-body">
                        <div class="row"> 
                            <div class="col-md-12">
                                <input type="hidden" id="course_id_3" name="course_id" value="">
                                <input type="hidden" id="buffet_id_3" name="buffet_id" value="">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Select an Item</label>
                                    <div class="col-sm-5">
                                        <select name="item_id" class="select2" data-allow-clear="true" data-placeholder="Select Item">
                                            <option></option>
                                            <?php foreach($categories as $cats):?>
                                            <optgroup label="<?php echo $cats->category_name;?>">
                                                <?php foreach($cats->items as $items):?>
                                                <option value="<?php echo $items->item_id;?>"><?php echo $items->item_eng;?></option>
                                                <?php endforeach;?>
                                            </optgroup>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                        <button type="submit" name="submit" class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Add this Item</button>
                     </div>
                    </form>
                  </div>
               </div>
            </div>
        <!-- Modal Item Add Ends Here-->

    </div>
    
<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2.css">

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>

<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/tocify/jquery.tocify.min.js"></script>
<script src="<?php echo base_url()?>assets/js/select2/select2.min.js"></script>

<!-- Addon Styles on this page -->
<style>
.select2-drop{z-index: 999999;}
</style>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>

<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

<script>
//Course Delete Script    
$(document).on("click", ".delete-course", function (event) {
    $("#course_id").val($(this).data('course_id'))
    $("#buffet_id").val($(this).data('buffet_id'))
});

//Item Delete Script    
$(document).on("click", ".delete-item", function (event) {
    $("#item_id").val($(this).data('item_id'))
    $("#course_id_2").val($(this).data('course_id_2'))
    $("#buffet_id_2").val($(this).data('buffet_id_2'))
});

//Item Add Script    
$(document).on("click", ".add-items", function (event) {
    $("#course_id_3").val($(this).data('course_id_3'))
    $("#buffet_id_3").val($(this).data('buffet_id_3'))
});

</script>