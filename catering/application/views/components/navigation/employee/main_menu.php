<!--Navigations-->
<div class="page-container horizontal-menu">
  <header class="navbar navbar-fixed-top hidden-print">
    <!-- set fixed position by adding class "navbar-fixed-top" -->
    <div class="navbar-inner">
      <!-- logo -->
      <div class="navbar-brand">
        <a href="<?php echo base_url()?>dashboard/index">
          <img src="<?php echo base_url().'assets/images/app_settings/entity/'.$settings->entity_logo;?>" width="88" alt="" />
        </a>
      </div>
      <!-- main menu -->
      <ul class="navbar-nav">
        <li class="has-sub">
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="entypo-gauge">
            </i>
            <span class="title">Dashboard
            </span>
          </a>
        </li>
        <li class="has-sub">
          <a href="#">
            <i class="glyphicon glyphicon-cutlery">
            </i>
            <span class="title">Menu Manager
            </span>
          </a>
          <ul class="visible">
            <li>
              <a href="<?php echo base_url()?>items/index">
                <span class="title">Items
                </span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url()?>buffets/index">
                <span class="title">Buffets
                </span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url()?>addons/index">
                <span class="title">Add-ons
                </span>
              </a>
            </li>
          </ul>
        </li>
        <li class="has-sub">
          <a href="#">
            <i class="glyphicon glyphicon-list-alt">
            </i>
            <span class="title">Orders
            </span>
          </a>
          <ul>
            <li>
              <a href="<?php echo base_url()?>manageorders/index">
                <span class="title">Manage Orders
                </span>
              </a>
            </li>
          </ul>
        </li>
        <li class="has-sub">
          <a href="#">
            <i class="glyphicon glyphicon-shopping-cart">
            </i>
            <span class="title">Inventory
            </span>
          </a>
          <ul>
            <li>
              <a href="<?php echo base_url()?>supplies/index">
                <span class="title">Manage Supplies
                </span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url()?>inventoryitems/index">
                <span class="title">Inventory Items
                </span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url()?>stocks/index">
                <span class="title">Manage Stocks
                </span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url()?>manage_stocks/index">
                <span class="title">Draw Stocks
                </span>
              </a>
            </li>
          </ul>
        </li>
      </ul>
      <!-- notifications and other links -->
      <ul class="nav navbar-right pull-right">
        <!-- raw links -->
        <li class="notifications dropdown">

                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                        <i class="glyphicon glyphicon-bell"></i>
                        <span class="badge badge-info" id="nos_notif"></span>
                </a>

                <ul class="dropdown-menu">
                    <li class="top">
                        <p class="small">
                            <a href="#" class="pull-right">Mark all Read</a>
                            You have <strong id="nos_notif2"></strong> new notifications.
                        </p>
                    </li>

                    <li>
                        <ul class="dropdown-menu-list scroller" tabindex="5001" style="overflow: hidden; outline: none;">
                            <li class="unread notification-success" id="notifs">
                            </li>
                        </ul>
                    </li>
                    <!--<li class="external">
                        <a href="#">View all notifications</a>
                    </li>-->
                <div id="ascrail2001" class="nicescroll-rails" style="padding-right: 3px; width: 10px; z-index: 1000; cursor: default; position: absolute; top: 41px; left: 359px; height: 290px; display: block; opacity: 0;"><div style="position: relative; top: 0px; float: right; width: 5px; height: 242px; background-color: rgb(212, 212, 212); border: 1px solid rgb(204, 204, 204); background-clip: padding-box; border-radius: 1px;"></div></div><div id="ascrail2001-hr" class="nicescroll-rails" style="height: 7px; z-index: 1000; top: 324px; left: 1px; position: absolute; cursor: default; display: none; width: 358px; opacity: 0;"><div style="position: relative; top: 0px; height: 5px; width: 368px; background-color: rgb(212, 212, 212); border: 1px solid rgb(204, 204, 204); background-clip: padding-box; border-radius: 1px;"></div></div></ul>

        </li>

        <li class="dropdown">

               <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true" aria-expanded="true">
                   <i class="entypo-cog"></i>
               </a>

               <ul class="dropdown-menu pull-left">
                  <li>
                     <a href="<?php echo base_url()?>myaccount/index">
                     <span>My Account</span>
                     </a>
                  </li>
               </ul>
            </li>

      <li class="sep">
      </li>
      <li>
        <a href="<?php echo base_url()?>auth/logout">
          Log Out
          <i class="entypo-logout left">
          </i>
        </a>
      </li>
      <!-- mobile only -->
      <li class="visible-xs">
        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="horizontal-mobile-menu visible-xs">
          <a href="#" class="with-animation">
            <!-- add class "with-animation" to support animation -->
            <i class="entypo-menu">
            </i>
          </a>
        </div>
      </li>
      </ul>
    </div>
  </header>

<!--Submenu-->
<div class="main-content">
  <div class="container" style="width: 100% !important">
    <div class="row hidden-print">
      <!-- Profile Info and Notifications -->
      <div class="col-md-6 col-sm-8 clearfix">
        <ul class="user-info pull-left pull-none-xsm">
          <!-- Profile Info -->
          <li class="profile-info dropdown">
            <!-- add class "pull-right" if you want to place this from right -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url().'assets/images/users/'.$_SESSION['user_display']?>" alt="" class="img-circle" width="44" />
              <?php echo $_SESSION['user_fullname']?>
            </a>
            <ul class="dropdown-menu">
              <!-- Reverse Caret -->
              <li class="caret">
              </li>
              <!-- Profile sub-links -->
              <li>
                <a href="<?php echo base_url()?>myaccount/index">
                  <i class="entypo-user">
                  </i>
                  Edit Profile
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- Raw Links -->
      <div class="col-md-6 col-sm-4 clearfix hidden-xs">
        <ul class="list-inline links-list pull-right">
          <!-- Language Selector -->
          <li class="dropdown language-selector">
            Language: &nbsp;
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
              <img src="<?php echo base_url()?>assets/images/flags/flag-uk.png" width="16" height="16" />
            </a>
            <ul class="dropdown-menu pull-right">
              <li>
                <a href="#">
                  <img src="<?php echo base_url()?>assets/images/flags/flag-de.png" width="16" height="16" />
                  <span>Chinese
                  </span>
                </a>
              </li>
              <li class="active">
                <a href="#">
                  <img src="<?php echo base_url()?>assets/images/flags/flag-uk.png" width="16" height="16" />
                  <span>English
                  </span>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <hr class="hidden-print"/>

    <script type="text/javascript">

        //Get Number of Notifications Starts Here
        $('#nos_notif').ready(function(){
            var nos_notif = $(this).val();

            $.ajax({
                url: "<?php echo base_url()?>notifications/nos_notif",
                type: "POST",
                dataType:'json',
                data: {nos_notif : nos_notif},
                success: function(data){
                //  alert(data)
                    $("#nos_notif").html(data);
                    $("#nos_notif2").html(data);
                }
            });
        })
        //Get Number of Notifications Ends Here

        //Get Notifications Starts Here
        $('#notifs').ready(function(){
            var notifs = $(this).val();

            $.ajax({
                url: "<?php echo base_url()?>notifications/ajax_list_notif",
                type: "POST",
                dataType:'json',
                data: {notifs : notifs},
                success: function(data){
                    var not = "";
                    // alert(data[3]);

                    for(x=0; x<data.length; x++){
                      not += data[x];
                    }
                    //alert(not);
                    $("#notifs").html(not);
                }
            });
        })

        //Get Notifications Ends Here

    </script>
