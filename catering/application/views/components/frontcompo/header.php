
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<?php echo base_url()?>frontend/assets/img/express-favicon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo 'Bayfrontfood | '.$settings->pagetitle?></title>

        <!-- Icon css link -->
        <link href="<?php echo base_url()?>frontend/assets/vendors/material-icon/css/materialdesignicons.min.css" rel="stylesheet">
        <link href="<?php echo base_url()?>frontend/assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url()?>frontend/assets/vendors/linears-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo base_url()?>frontend/assets/css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="<?php echo base_url()?>frontend/assets/vendors/bootstrap-selector/bootstrap-select.css" rel="stylesheet">
        <link href="<?php echo base_url()?>frontend/assets/vendors/bootatrap-date-time/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="<?php echo base_url()?>frontend/assets/vendors/owl-carousel/assets/owl.carousel.css" rel="stylesheet">
        
        <link href="<?php echo base_url()?>frontend/assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url()?>frontend/assets/css/responsive.css" rel="stylesheet">
        
        <!-- Form Wizard css -->
        <link href="<?php echo base_url()?>frontend/form_wizard/css/style.css" rel="stylesheet">
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url()?>frontend/assets/js/jquery-2.1.4.min.js"></script>
        
    </head>
    <body>
       
       <div id="preloader">
            <div class="loader absolute-center">
                <div class="loader__box"><b class="top"></b></div>
                <div class="loader__box"><b class="top"></b></div>
                <div class="loader__box"><b class="top"></b></div>
            </div>
        </div>
       
        <!--================ Frist hader Area =================-->
        <div class="first_header">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="header_contact_details">
                            <a href="#"><i class="fa fa-phone"></i><?php echo ' '.$settings->entity_contact?></a>
                            <a href="#"><i class="fa fa-envelope-o"></i><?php echo ' '.$settings->entity_email?></a>
                        </div>
                    </div>
                    <!--<div class="col-md-4">
                        <div class="event_btn_inner">
                            <a class="event_btn" href="#"><i class="fa fa-table" aria-hidden="true"></i>Book a Table</a>
                            <a class="event_btn" href="#"><i class="fa fa-calendar" aria-hidden="true"></i>Book an Event</a>
                        </div>
                    </div>-->
                    <div class="col-md-4">
                        <div class="header_social">
                            <ul>
                                <li><a href="https://www.facebook.com/marinabaybbqsteamboatbuffet/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.instagram.com/marinabaybbqsteamboat/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--================End Footer Area =================-->
        
        <!--================End Footer Area =================-->
        <header class="main_menu_area">
            <nav class="navbar navbar-default" style='height: 80px; text-align: center;'>
                <div class="container">
                    <a href="#"><img src="<?php echo base_url().'/assets/images/app_settings/entity/'.$settings->entity_logo;?>" alt=""  style='max-width: 180px;'></a>
                </div><!-- /.container-fluid -->
            </nav>
        </header>
        <!--================End Footer Area =================-->