<!--================Start of Footer Area =================-->
        <footer class="footer_area">
            <div class="footer_widget_area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <aside class="f_widget about_widget">
                                <div class="f_w_title">
                                    <h4>ABOUT BayFrontFood</h4>
                                </div>
                                <p>We know man’s thirst for uniqueness. Marina Bay BBQ Steamboat is for the people who are in search of uniqueness; especially in sea food.</p>
                                <ul>
                                    <li><a href="https://www.facebook.com/marinabaybbqsteamboatbuffet/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://www.instagram.com/marinabaybbqsteamboat/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-md-3">
                            <aside class="f_widget contact_widget">
                                <div class="f_w_title">
                                    <h4>CONTACT US</h4>
                                </div>
                                <p>Have questions, comments or just want to say hello:</p>
                                <ul>
                                    <li><a href="#"><i class="fa fa-envelope"></i><?php echo ' '.$settings->entity_email?></a></li>
                                    <li><a href="#"><i class="fa fa-phone"></i><?php echo ' '.$settings->entity_contact?></a></li>
                                    <li><a href="#"><i class="fa fa-map-marker"></i><?php echo ' '.$settings->entity_address;?><br /> <?php echo "Singapore ,". $settings->entity_postal;?></a></li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-md-3">
                            <aside class="f_widget twitter_widget">
                                <div class="f_w_title">
                                    <h4>TESTIMONIALS</h4>
                                </div>
                                <ul>
                                    <li>
                                        <u><a href="#">Mardi Mad : </a></u> Nice place also.. The food was great...we just choose n take. Staffs are very friendly
                                    </li>
                                    <li>
                                        <u><a href="#">NorKhalida Syafira : </a></u> Wide selection of meat and seafood. Table is abit small and update the facebook page
                                    </li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-md-3">
                            <aside class="f_widget gallery_widget">
                                <div class="f_w_title">
                                    <a href='http://bayfrontfood.sg/gallery/' target='_blank'><h4>Our Gallery</h4></a>
                                </div>
                                <ul>
                                    <li><a href="#"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02435-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                    <li><a href="#"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02389-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                    <li><a href="#"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02409-1-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                    <li><a href="#"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02433-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                    <li><a href="#"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02402-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                    <li><a href="#"><img src="http://bayfrontfood.sg/wp-content/uploads/2016/05/DSC02425-150x150.jpg" alt=""><i class="fa fa-search"></i></a></li>
                                </ul>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copy_right_area">
                <div class="container">
                    <div class="pull-left">
                        <h5><p>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="http//akstech.com.sg" target="_blank">AKS TECH PTE. LTD.</a>
</p></h5>
                    </div>
                </div>
            </div>
        </footer>
        
        <!--================End of Footer Area =================-->        
        
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url()?>frontend/assets/js/bootstrap.min.js"></script>
        <!-- Extra plugin js -->
        <script src="<?php echo base_url()?>frontend/assets/vendors/bootstrap-selector/bootstrap-select.js"></script>
        <script src="<?php echo base_url()?>frontend/assets/vendors/bootatrap-date-time/bootstrap-datetimepicker.min.js"></script>
        <script src="<?php echo base_url()?>frontend/assets/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url()?>frontend/assets/vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="<?php echo base_url()?>frontend/assets/vendors/isotope/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url()?>frontend/assets/vendors/countdown/jquery.countdown.js"></script>
        <script src="<?php echo base_url()?>frontend/assets/vendors/js-calender/zabuto_calendar.min.js"></script>
        <script src="<?php echo base_url()?>frontend/form_wizard/js/main.js"></script>
        
        <script src="<?php echo base_url()?>frontend/assets/js/theme.js"></script>
    </body>
</html>