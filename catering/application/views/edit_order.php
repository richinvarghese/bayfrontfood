<!--Main Content-->
    <h1 class="margin-bottom">Manage Order</h1>
    <ol class="breadcrumb 2">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li>
          <a href="<?php echo base_url()?>manageorders/index">
            <i class="fa-home"></i>Orders
          </a>
        </li>
        <li class="active">
          <strong>Manage Order</strong>
        </li>
    </ol>

    <div class="row">
        <div class="col-sm-12">
            <div class="invoice">
                <div class="row">
                    <div class="row" style="padding:0px 15px 0px 15px;">
                        <div class="col-md-6">
                        <?php if($order->order_status == 0):?>
                            <blockquote class="blockquote-gold">
                        <?php elseif($order->order_status == 1):?>
                            <blockquote class="blockquote-green">
                        <?php elseif($order->order_status == 2):?>
                            <blockquote class="blockquote-red">
                        <?php elseif($order->order_status == 3):?>
                            <blockquote class="blockquote-blue">
                        <?php endif;?>
                                <?php if($order->order_status == 0):?>
                                <p><strong>Order Status : Pending Approval</strong></p>
                                <?php elseif($order->order_status == 1):?>
                                <p><strong>Order Status : Approved</strong></p>
                                <?php elseif($order->order_status == 2):?>
                                <p><strong>Order Status : Cancelled</strong></p>
                                <?php elseif($order->order_status == 3):?>
                                <p><strong>Order Status : Completed</strong></p>
                                <?php endif;?>
                                <p>Buffet: <?php echo $order->order_buffet?></p>
                                <p>Party for: <?php echo $order->order_pax?></p>
                                <p>Date Ordered: <?php echo '<i class="glyphicon glyphicon-calendar" style="color:#31271e"></i> '.date('d-M-Y', strtotime($order->order_date));?></p>
                                <p>Dining Date & Time: <?php echo '<i class="glyphicon glyphicon-calendar" style="color:#31271e"></i> '.date('d-M-Y', strtotime($order->dining_date)).'  <i class="glyphicon glyphicon-time" style="color:#31271e"></i> '.date('h:i:A', strtotime($order->dining_time))?></p>
                                <p>Customer: <?php echo $order->customer_name?></p>
                                <p>Location: <?php echo $order->address_block.', '.$order->address_street.', '.$order->address_postal?></p>                      
                            </blockquote>
                        </div>
                        <div class="col-md-6" style="text-align: right">
                            
                            <?php if($order->order_status == 0):?>
                            <button type="button" class="btn btn-green btn-icon order_discount" data-target="#order_discount" data-toggle="modal" 
                                    data-order_disc="<?php echo $order->order_discount;?>" 
                                    data-order_id = "<?php echo $order->order_id;?>" >
                                Apply Discount<i class="glyphicon glyphicon-usd"></i>
                            </button>
                            <?php endif;?>
                            
                            <!-- Modal for Discount Starts Here-->
                            <div class="modal fade" id="order_discount">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>manageorders/update_discount" novalidate="novalidate" enctype="multipart/form-data">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Apply Discount for this Order</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                <label for="field-1" class="col-sm-4 control-label" style="top: 5px;">Discount in Percentage</label>
                                                <div class="col-sm-5">
                                                  <input type="text" name="order_disc" id="order_disc" placeholder="Numbers Only (Eg: 10)" class="form-control">
                                                  <input type="hidden" name="order_id" id="order_id">
                                                </div>
                                                <div class="col-sm-3" style="text-align: -webkit-left;top: 5px;">
                                                <span class="xx-large" style="color: #bea26a; font-size: 10px;">
                                                Numbers Only (Eg: 10)
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Update</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                            <!-- Modal for Discount Ends Here-->
                            
                            
                            
                            <?php if($order->order_status == 0):?>
                            <button type="button" class="btn btn-default btn-icon order_delivery" data-target="#order_delivery" data-toggle="modal" 
                                    data-order_deli="<?php echo $order->order_delivery;?>" 
                                    data-order_id_3 = "<?php echo $order->order_id;?>" >
                                Add Delivery<i class="glyphicon glyphicon-road"></i>
                            </button>
                            <?php endif;?>
                            
                            
                            <!-- Modal for Delivery Starts Here-->
                            <div class="modal fade" id="order_delivery">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>manageorders/update_delivery" novalidate="novalidate" enctype="multipart/form-data">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Apply Delivery for this Order</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                <label for="field-1" class="col-sm-4 control-label" style="top: 5px;">Add Delivery Charges</label>
                                                <div class="col-sm-5">
                                                  <input type="text" name="order_delivery" id="order_deli" placeholder="Numbers Only (Eg: 50)" class="form-control">
                                                  <input type="hidden" name="order_id" id="order_id_3">
                                                </div>
                                                <div class="col-sm-3" style="text-align: -webkit-left;top: 5px;">
                                                <span class="xx-large" style="color: #bea26a; font-size: 10px;">
                                                Numbers Only (Eg: 50)
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Update</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                            <!-- Modal for Discount Ends Here-->
                            
                            
                            
                            <?php if($order->order_status == 0):?>
                            <button type="button" class="btn btn-gold btn-icon edit-order" data-target="#edit-order" data-toggle="modal" 
                                    data-order_id_2="<?php echo $order->order_id;?>"
                                    data-customer_name="<?php echo $order->customer_name;?>" 
                                    data-customer_contact="<?php echo $order->customer_contact;?>" 
                                    data-customer_email="<?php echo $order->customer_email;?>" 
                                    data-address_block="<?php echo $order->address_block;?>" 
                                    data-address_postal="<?php echo $order->address_postal;?>" 
                                    data-address_street="<?php echo $order->address_street;?>" 
                                    data-dining_date="<?php echo $order->dining_date;?>" 
                                    data-dining_time="<?php echo $order->dining_time;?>" >
                                Edit Order<i class="glyphicon glyphicon-pencil"></i>
                            </button>
                            <?php endif;?>
                            
                            <!-- Modal for Edit Order Starts Here-->
                            <div class="modal fade" id="edit-order">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>manageorders/update_order" novalidate="novalidate" enctype="multipart/form-data">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Edit Order Details</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                <label for="field-1" class="col-sm-4 control-label" style="top: 5px;">Customer Name</label>
                                                <div class="col-sm-7">
                                                  <input type="text" name="customer_name" id="customer_name" placeholder="Customer Name" class="form-control">
                                                  <input type="hidden" name="order_id" id="order_id_2">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row"> 
                                            <div class="form-group">
                                                <label for="field-1" class="col-sm-4 control-label" style="top: 5px;">Customer Contact</label>
                                                <div class="col-sm-7">
                                                  <input type="text" name="customer_contact" id="customer_contact" placeholder="Contact No:" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row"> 
                                            <div class="form-group">
                                                <label for="field-1" class="col-sm-4 control-label" style="top: 5px;">Customer Email</label>
                                                <div class="col-sm-7">
                                                  <input type="text" name="customer_email" id="customer_email" placeholder="Customer Email" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row"> 
                                            <div class="form-group">
                                                <label for="field-1" class="col-sm-4 control-label" style="top: 5px;">Block</label>
                                                <div class="col-sm-3">
                                                  <input type="text" name="address_block" id="address_block" placeholder="Building/Block" class="form-control">
                                                </div>
                                                <label for="field-1" class="col-sm-1 control-label" style="top: 5px;">Postal</label>
                                                <div class="col-sm-3">
                                                  <input type="text" name="address_postal" id="address_postal" placeholder="Postal Code" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row"> 
                                            <div class="form-group">
                                                <label for="field-1" class="col-sm-4 control-label" style="top: 5px;">Street Address</label>
                                                <div class="col-sm-7">
                                                  <input type="text" name="address_street" id="address_street" placeholder="Street Address" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row"> 
                                            <div class="form-group">
                                                <label for="field-1" class="col-sm-4 control-label" style="top: 5px;">Dining Date</label>
                                                <div class="col-sm-7">
                                                  <input type="date" class="form-control" name="dining_date" id="dining_date" data-format="D, dd MM yyyy">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row"> 
                                            <div class="form-group">
                                                <label for="field-1" class="col-sm-4 control-label" style="top: 5px;">Dining Time</label>
                                                <div class="col-sm-7">
                                                    <input type="time" name="dining_time" id="dining_time" placeholder="Dining Time" class="form-control" data-second-step="30">
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Update</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                            <!-- Modal for Edit Order Ends Here-->
                            
                            <?php if($order->order_status == 0):?>
                            <div class="btn-group open">
                                <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Update Status <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-blue" role="menu" style="min-width: 115px;">
                                        <li><a href="#" data-target="#order-status_approve" data-toggle="modal" data-order_approve="<?php echo $order->order_id;?>" >Approve</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#" data-target="#order-status_cancel" data-toggle="modal" data-order_cancel="<?php echo $order->order_id;?>" >Cancel</a></li>
                                </ul>
                            </div>
                            
                            <?php elseif($order->order_status == 1):?>
                                <div class="btn-group open">
                                <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Mark this Order as <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-blue" role="menu" style="min-width: 115px;">
                                        <li><a href="#" data-target="#order-status_completed" data-toggle="modal" data-order_cancel="<?php echo $order->order_id;?>" >Completed</a></li>
                                </ul>
                            </div>
                            <?php endif;?>
                            
                            <!-- Modal for Update Status (Approve) Starts Here-->
                            <div class="modal fade" id="order-status_approve">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>manageorders/update_status" novalidate="novalidate" enctype="multipart/form-data">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Approve Order</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                <input type="hidden" value="1" name="order_status">
                                                <input type="hidden" value="<?php echo $order->order_id;?>" name="order_id">
                                                <span class="line xlarge" style="text-align: -webkit-left;">Are you sure you wanna Approve this Order ?</span><br>
                                                <span class="xx-large" style="color: #bea26a; text-align: -webkit-left;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                    Warning : You cant edit anything once the order is approved!!
                                                </span>
                                            </div>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Approve it</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                            <!-- Modal for Update Status (Approve) Ends Here-->
                            
                            <!-- Modal for Update Status (Cancel) Starts Here-->
                            <div class="modal fade" id="order-status_cancel">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>manageorders/update_status" novalidate="novalidate" enctype="multipart/form-data">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Cancel Order</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                <input type="hidden" value="2" name="order_status">
                                                <input type="hidden" value="<?php echo $order->order_id;?>" name="order_id">
                                                <span class="line xlarge" style="text-align: -webkit-left;">Are you sure you wanna Cancel this Order ?</span><br>
                                                <span class="xx-large" style="color: #bea26a; text-align: -webkit-left;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                    Warning : You cant edit anything once the order is been Cancelled !!
                                                </span>
                                            </div>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Close It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Cancel this Order</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                            <!-- Modal for Update Status (Cancel) Ends Here-->
                            
                            <!-- Modal for Update Status (Completed) Starts Here-->
                            <div class="modal fade" id="order-status_completed">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>manageorders/update_status" novalidate="novalidate" enctype="multipart/form-data">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Mark as Completed</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                <input type="hidden" value="3" name="order_status">
                                                <input type="hidden" value="<?php echo $order->order_id;?>" name="order_id">
                                                <span class="line xlarge" style="text-align: -webkit-left;">Are you sure you wanna mark this Order as Completed ?</span><br>
                                                <span class="xx-large" style="color: #bea26a; text-align: -webkit-left;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                    Warning : You can't edit anything once the order is been mark as Completed!!
                                                </span>
                                            </div>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Close It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Mark as Completed</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                            <!-- Modal for Update Status (Completed) Ends Here-->
                            
                            <?php if($order->order_status == 0):?>
                            <button type="button" class="btn btn-red btn-icon" data-target="#delete-order"  data-toggle="modal" >
                                Delete Order <i class="glyphicon glyphicon-trash"></i>
                            </button>
                            <?php endif;?>
                            
                            <!-- Modal for Delete Order Starts Here-->
                            <div class="modal fade" id="delete-order">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url().'manageorders/delete_order/'.$order->order_id;?>" novalidate="novalidate">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Delete Order</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                <span class="line xlarge" style="text-align: -webkit-left;">Are you sure you wanna Delete this Order ?</span><br>
                                                <span class="xx-large" style="color: #bea26a; text-align: -webkit-left;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                    Warning : Once Deleted, all information entered will be deleted from Database !!
                                                </span>
                                            </div>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Close It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete this Order</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                            <!-- Modal for Delete Order Ends Here-->

                            <a href="<?php echo base_url().'manageorders/view_order/'.$order->order_id;?>" target="_blank">
                                <button type="button" class="btn btn-blue btn-icon" >View Order<i class="glyphicon glyphicon-open-file"></i></button>
                            </a>
                        </div>
                    </div>
                </div>
                
                <?php if(isset($_SESSION['order-success'])){?>
                    <center>
                        <div class="form-session-success">
                        <?php echo $_SESSION['order-success'];?>
                        </div>
                    </center>
                  <br>
                <?php }?>

                <?php if(isset($_SESSION['order-error'])){?>
                    <center>
                        <div class="form-session-errors">
                        <?php print_r($_SESSION['order-error']);?>
                        </div>
                    </center>
                  <br>
                <?php }?>
                  
                <div class="margin"></div>
                <div class="jumbotron">
                    <br>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                                <div class="panel-title">Courses and Add-on Items</div>
                                <div class="panel-options">
                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                </div>
                        </div>

                        <div class="panel-body with-table">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <th>Buffet Courses & Items Opted</th>
                                        <th>Pax</th>
                                        <th>Price Per Pax</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <tr>
                                        <!--Order Details-->
                                        <td>
                                            <h4><?php echo 'Buffet :'.$order->order_buffet;?></h4>
                                            <br/>
                                            <?php $a=1;?>
                                            <?php foreach($order_courses as $courses):?>
                                            <p style="font-size: medium"><u><?php echo $a.': '.$courses->order_course_name;?></u></p>
                                                
                                                <?php foreach($courses->order_items as $item):?>
                                                    <p style="font-size: small"><i class="glyphicon glyphicon-cutlery" style="color:#bea26a"></i><?php echo ' '.$item->order_items_eng;?></p>
                                                <?php endforeach;?>
                                                    
                                                <?php $a++;?>
                                            <?php endforeach;?>
                                        </td>

                                        <!--Party Pax-->
                                        <td>
                                            <p><?php echo $order->order_pax.' ';?>
                                                <?php if($order->order_status == 0):?>
                                                <span class="badge badge-info order-pax" <a href="#" data-target="#order-pax" data-toggle="modal" data-min_pax="<?php echo $order->order_pax;?>">
                                                        <i class="entypo-pencil" style="color:#fff"></i></a>
                                                </span>
                                                <?php endif;?>
                                            </p>
                                        </td>
                                        
                                        
                                        <!-- Modal for Edit Order Starts Here-->
                                        <div class="modal fade" id="order-pax">
                                           <div class="modal-dialog">
                                              <div class="modal-content">
                                                <form role="form" method="POST" action="<?php echo base_url().'manageorders/update_order_pax/'.$order->order_id;?>" >
                                                 <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" style="text-align: -webkit-left;">Edit Party Pax</h4>
                                                 </div>
                                                 <div class="modal-body">
                                                    <div class="row"> 
                                                        <div class="form-group">
                                                            <label for="field-1" class="col-sm-4 control-label" style="top: 5px;">No: of Pax</label>
                                                            <div class="col-sm-7">
                                                              <input type="text" name="new_min_pax" id="min_pax" placeholder="<?php echo "Minimum number of pax should be ".$buffet->buffet_min_pax;?>" class="form-control">
                                                              <input type="hidden" name="order_buffet_price" value="<?php echo $order->order_buffet_price;?>" class="form-control">
                                                              <input type="hidden" name="old_min_pax" value="<?php echo $order->order_pax;?>" class="form-control">
                                                              <input type="hidden" name="order_stotal" value="<?php echo $order->order_stotal;?>" class="form-control">
                                                              <input type="hidden" name="order_discount" value="<?php echo $order->order_discount;?>" class="form-control">
                                                              <input type="hidden" name="order_gst" value="<?php echo $order->order_gst;?>" class="form-control">
                                                              <input type="hidden" name="buffet_min_pax" value="<?php echo $buffet->buffet_min_pax;?>" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                 </div>     
                                                 <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                                    <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Update</button>
                                                 </div>
                                                </form>
                                              </div>
                                           </div>
                                        </div>
                                        <!-- Modal for Edit Order Ends Here-->
                                        
                                        


                                        <td><p><?php echo $settings->entity_currency_symbol.' '.$order->order_buffet_price;?></p></td>

                                        <!--Buffet Total-->
                                        <td><p><?php echo $settings->entity_currency_symbol.' '.(($order->order_pax) *($order->order_buffet_price));?></p></td>
                                    </tr>
                                    
                                    <!--Addons Section-->
                                    
                                    <?php if($nos_addons !== $total_null):?>
                                    <tr>
                                        <!--Addon Items-->
                                        <td>
                                            <p style="font-size: medium"><u><?php echo 'Addon-ons'?></u></p>
                                            <?php foreach($addons as $adds):?>
                                                <?php if($adds->order_add_items_pax==0):?>
                                                <?php continue;?>
                                                <?php else:?>
                                                <p style="font-size: small"><i class="glyphicon glyphicon-cutlery" style="color:#bea26a"></i><?php echo ' '.$adds->order_add_items_eng;?></p>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </td>

                                        <!--Addon Pax-->
                                        <td>
                                            <?php foreach($addons as $adds):?>
                                                <?php if($adds->order_add_items_pax==0):?>
                                                <?php continue;?>
                                                <?php else:?>
                                                    <p></p><p><?php echo $adds->order_add_items_pax.' ';?>
                                                        <?php if($order->order_status == 0):?>
                                                        <span class="badge badge-info addon-pax" <a href="#" data-target="#addon-pax" data-toggle="modal" 
                                                                                                    data-addon_pax="<?php echo $adds->order_add_items_pax;?>" 
                                                                                                    data-addon_item_id="<?php echo $adds->order_add_items_id;?>" 
                                                                                                    data-addon_item_price="<?php echo $adds->order_add_items_price;?>" 
                                                                                                    data-addon_order_id="<?php echo $adds->order_id;?>">
                                                            <i class="entypo-pencil" style="color:#fff"></i></a>
                                                        </span>
                                                        <?php endif;?>
                                                    </p>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </td>
                                        
                                        <!-- Modal for Update Addon Pax Starts Here-->
                                        <div class="modal fade" id="addon-pax">
                                           <div class="modal-dialog">
                                              <div class="modal-content">
                                                <form role="form" method="POST" action="<?php echo base_url().'manageorders/update_addon_pax/'?>" >
                                                 <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" style="text-align: -webkit-left;">Edit Party (Addons) Pax</h4>
                                                 </div>
                                                 <div class="modal-body">
                                                    <div class="row"> 
                                                        <div class="form-group">
                                                            <label for="field-1" class="col-sm-4 control-label" style="top: 5px;">No: of Pax</label>
                                                            <div class="col-sm-7">
                                                              <input type="text" name="new_addon_pax" id="addon_pax" placeholder="<?php echo "Update number of Pax";?>" class="form-control">
                                                              <input type="hidden" name="addon_item_id" id="addon_item_id" class="form-control">
                                                              <input type="hidden" name="addon_item_price" id="addon_item_price" class="form-control">
                                                              <input type="hidden" name="addon_order_id" id="addon_order_id" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                 </div>     
                                                 <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                                    <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Update</button>
                                                 </div>
                                                </form>
                                              </div>
                                           </div>
                                        </div>
                                        <!-- Modal for Update Adddon Pax Ends Here-->

                                        <!--Addon Price/Pax-->
                                        <td>
                                            <?php foreach($addons as $adds):?>
                                            <?php if($adds->order_add_items_pax==0):?>
                                            <?php continue;?>
                                            <?php else:?>
                                                <p></p><p><?php echo $settings->entity_currency_symbol.' '.$adds->order_add_items_price.' ';?></p>
                                            <?php endif;?>
                                            <?php endforeach;?>
                                        </td>

                                        <!--Addons Total-->
                                        <td>
                                            <?php foreach($addons as $adds):?>
                                            <?php if($adds->order_add_items_pax==0):?>
                                            <?php continue;?>
                                            <?php else:?>
                                                <p></p><p><?php echo $settings->entity_currency_symbol.' '.(($adds->order_add_items_pax) * ($adds->order_add_items_price));?></p>
                                            <?php endif;?>
                                            <?php endforeach;?>
                                        </td>
                                    </tr>
                                    <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-9">
                            <div class="panel panel-primary" data-collapsed="0" style="border-color: #fff;">
                                <!-- panel head -->
                                <div class="panel-heading" style="border-color: #fff">
                                    <div class="panel-title"><h4>Additional Notes: </h4></div>
                                </div>
                                <!-- panel body -->
                                <div class="panel-body">
                                    <p style="font-size: small;"><?php echo $order->order_notes;?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <table class="table responsive">
                                    <thead>
                                    </thead>
                                    <tbody>
                                            <tr>
                                                <td><strong>Subtotal</strong></td>
                                                <td><?php echo $settings->entity_currency_symbol." ". number_format(round($order->order_stotal, 1), 2);?></td>
                                            </tr>

                                            <tr>
                                                <td><strong>Discount </strong><?php echo "(".$order->order_discount."%)"?></td>
                                                    <td><?php echo $settings->entity_currency_symbol." ".number_format(round($order->order_discount * ($order->order_stotal/100), 1), 2);?></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Delivery </strong></td>
                                                    <td><?php echo $settings->entity_currency_symbol." ".number_format(round($order->order_delivery, 1), 2);?></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <?php if($settings->entity_gst == 0):?>
                                                <strong>GST </strong><?php echo "(0%)";?>
                                                <?php else:?>
                                                <strong>GST </strong><?php echo "(7%)";?>
                                                <?php endif;?>
                                                </td>
                                                <td>
                                                 <?php echo $settings->entity_currency_symbol." ".number_format(round($order->order_gst, 1), 2);?>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td><strong>GRAND TOTAL</strong></td>
                                                <td><strong><?php echo $settings->entity_currency_symbol." ". number_format(round($order->order_gtotal, 1), 2);?></strong></td>
                                            </tr>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
               
<!--End of Main Content-->


<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>


<script type="text/javascript">

$(document).on("click",".order_discount", function(event){
   
   $('#order_id').val($(this).data('order_id'))
   $('#order_disc').val($(this).data('order_disc'))
    
});

$(document).on("click",".order_delivery", function(event){
   
   $('#order_id_3').val($(this).data('order_id_3'))
   $('#order_deli').val($(this).data('order_deli'))
    
});

$(document).on("click", ".edit-order", function(event){
   
   $('#order_id_2').val($(this).data('order_id_2'))
   $('#customer_name').val($(this).data('customer_name'))
   $('#customer_contact').val($(this).data('customer_contact'))
   $('#customer_email').val($(this).data('customer_email'))
   $('#address_block').val($(this).data('address_block'))
   $('#address_postal').val($(this).data('address_postal'))
   $('#address_street').val($(this).data('address_street'))
   $('#dining_date').val($(this).data('dining_date'))
   $('#dining_time').val($(this).data('dining_time'))
});

$(document).on("click", ".order-pax", function(event){
    
    $('#min_pax').val($(this).data('min_pax'))
})

$(document).on("click", ".addon-pax", function(event){
    
    $('#addon_pax').val($(this).data('addon_pax'))
    $('#addon_item_id').val($(this).data('addon_item_id'))
    $('#addon_item_price').val($(this).data('addon_item_price'))
    $('#addon_order_id').val($(this).data('addon_order_id'))
})


</script>