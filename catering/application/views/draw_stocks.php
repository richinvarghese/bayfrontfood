<!--Main Content-->
    <h1 class="margin-bottom">Draw Stocks</h1>
    <ol class="breadcrumb 2">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li class="active">
          <strong>Draw Stocks</strong>
        </li>
    </ol>

    <div class="row">
     <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>manage_stocks/draw_item" enctype="multipart/form-data">
      <div class="col-md-4">
        <div class="panel panel-primary" data-collapsed="0">
          <div class="panel-heading"  style="background: #31271e;">
            <div class="panel-title" style="color: #fff">
              Draw and Item from Stocks
            </div>
          </div>
          <div class="panel-body" style="background: #31271e;">

              <?php if(isset($_SESSION['draw-success'])){?>
                <center>
                    <div class="form-session-success">
                    <?php echo $_SESSION['draw-success'];?>
                    </div>
                </center>
              <br>
            <?php }?>

            <?php if(isset($_SESSION['draw-error'])){?>
                <center>
                    <div class="form-session-errors">
                    <?php echo $_SESSION['draw-error'];?>
                    </div>
                </center>
              <br>
            <?php }?>

            <div class="form-group">
              <label class="col-sm-3 control-label">Select Item</label>
                <span class="description">(Tap to Scan Barcode)</span>
              <div class="col-sm-4">
                <select name="item_id" id='item_id' class="form-control">
                    <!-- Pending Default Selection-->
                    <option value="" selected>Select Item</option>
                        <?php foreach($inventory_items as $item):?>
                        <option value="<?php echo $item->item_id;?>"><?php echo $item->item_name;?></option>
                        <?php endforeach;?>
                </select>
              </div>
                 <div class="col-sm-2">
                    <button type="button" class="btn btn-block">
                            <i class="glyphicon glyphicon-barcode"></i>
                    </button>
              </div>
            </div>
            <div class="form-group">
              <label for="field-4" class="col-sm-3 control-label">Drawn From</label>
              <span class="description">(Location)</span>
              <div class="col-sm-6">
                  <select name="location_id" id='location_id' class="form-control" disabled="">
                    <option value="" selected>Select Location</option>
                    <!-- Pending Default Selection-->
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Quantity</label>
              <div class="col-sm-6">
                  <input type="text" name="item_quantity" value="" placeholder="Eg: 20" class="form-control" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Select an Order</label>
              <span class="description">(Item withdrawal for Order)</span>
              <div class="col-sm-6">
                <select name="order_id" class="selectboxit">
                    <!-- Pending Default Selection-->
                    <optgroup label="Select Order">
                        <?php foreach($orders as $order):?>
                        <option value="<?php echo $order->order_id;?>"><?php echo $order->order_buffet.' For '.$order->order_pax.'Pax on '.date('d-m-y', strtotime($order->dining_date));?></option>
                        <?php endforeach;?>
                    </optgroup>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="field-4" class="col-sm-3 control-label">Drawn By</label>
              <span class="description">(Select User)</span>
              <div class="col-sm-6">
                <select name="user_id" class="selectboxit">
                    <!-- Pending Default Selection-->
                    <optgroup label="Select User">
                        <?php foreach($users as $user):?>
                        <option value="<?php echo $user->user_id;?>"><?php echo $user->user_fullname;?></option>
                        <?php endforeach;?>
                    </optgroup>
                </select>
              </div>
            </div>
            <div class="form-group default-padding" style="text-align:center">
                <button type="submit" name='submit' class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Draw Item</button>
                <button type="reset" class="btn">Reset</button>
            </div>
            </div>
          </div>
        </div>
     </form>
    <div class="col-sm-8">

        <!--<script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
                var $table1 = jQuery( '#item-table' );

                // Initialize DataTable
                $table1.DataTable( {
                        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "bStateSave": true
                });

                // Initalize Select Dropdown after DataTables is created
                $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                        minimumResultsForSearch: -1
                });
        } );
        </script>-->


        <?php if(isset($_SESSION['drawn-edit-success'])){?>
            <center>
                <div class="form-session-success">
                <?php echo $_SESSION['drawn-edit-success'];?>
                </div>
            </center>
          <br>
        <?php }?>

        <?php if(isset($_SESSION['drawn-edit-error'])){?>
            <center>
                <div class="form-session-errors">
                <?php print_r($_SESSION['drawn-edit-error']);?>
                </div>
            </center>
          <br>
        <?php }?>

        <table class="table table-bordered datatable" id="item-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Item Name</th>
                    <th>User</th>
                    <th>Order</th>
                    <th>Min Quantity Drawn</th>
                    <th>Date</th>
                    <th>Estimated Cost</th>
                    <th>Manage</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=1;?>
                <?php foreach($draws as $draw):?>
                <tr class="odd gradeX">
                    <td class="center"><?php echo $i;?></td>
                    <td class="center"><?php echo $draw->item->item_name;?></td>
                    <td class="center"><?php echo $draw->user_fullname;?></td>
                    <td class="center"><?php echo $draw->order->order_buffet;?></td>
                    <td class="center"><?php echo $draw->item_quantity.' '.$draw->item->item_unit;?></td>
                    <td class="center"><?php echo date('D, d-M-Y', strtotime($draw->drawn_date));?>
                    <td class="center"><?php echo $settings->entity_currency_symbol.''.number_format(round($draw->item_quantity * $draw->item->item_cost_price, 1), 2);?></td>
                    </td>
                    <td class="center">

                        <button type="button" class="btn btn-primary amend-order" data-target="#amend-order" data-toggle="modal" 
                                data-drawn_id="<?php echo $draw->drawn_id;?>" 
                                data-order_id="<?php echo $draw->order->order_id;?>" >
                            Amend Order
                        </button>
                        
                         <!-- Modal Set Password Starts Here-->
                        <div class="modal fade" id="amend-order">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>manage_stocks/amend_order" novalidate="novalidate">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Amend Order</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group"> 
                                                   <input type="hidden" name="drawn_id" id="drawn_id" class="form-control">
                                                   <label for="field-1" class="control-label">Select Order</label>
                                                    <select name="order_id" id="order_id" class="form-control">
                                                        <?php foreach($orders as $order):?>
                                                        <option value="<?php echo $order->order_id?>"><?php echo $order->order_buffet;?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="modal-footer">
                                           <button type="button" class="btn btn-default" data-dismiss="modal">Cancel It</button>
                                           <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Set Amend Order</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Modal Set Password Ends Here-->
                        
                    </td>
                </tr>
                <?php $i++;?>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>Item Name</th>
                    <th>User</th>
                    <th>Order</th>
                    <th>Min Quantity Drawn</th>
                    <th>Date</th>
                    <th>Estimated Cost</th>
                    <th>Manage</th>
                </tr>
            </tfoot>
        </table>
    </div>
   </div>

<!--End of Main Content-->


<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.css">
<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>

<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/fileinput.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<!--<script src="<?php echo base_url()?>assets/js/datatables/datatables.js"></script>-->
<script src="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>

<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

<script type='text/javascript'>
$(document).ready(function(){   
    $('#item_id').on('change', function(){
        
        var item_id = $('#item_id').val();
            
        $('#location_id').prop('disabled', false);
        $.ajax({
                url: "<?php echo base_url()?>manage_stocks/ajax_location",
                type: "POST",
                dataType:'json',
                data: {item_id : item_id},
                success: function(data){
                   $('#location_id').html(data); 
                    }
            });
        
    });
});

$(document).on("click", '.amend-order', function(event){

    $('#drawn_id').val($(this).data('drawn_id'));
    $('#order_id').val($(this).data('order_id'));
    
})
    
</script>
