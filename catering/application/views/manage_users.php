<!--Main Content-->
    <h1 class="margin-bottom">Manage Users</h1>

    <ol class="breadcrumb 2">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li class="active">
          <strong>Manage Users</strong>
        </li>
    </ol>

    <div class="row">
     <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>users/add_user" enctype="multipart/form-data">
      <div class="col-md-4">
        <div class="panel panel-primary" data-collapsed="0">
          <div class="panel-heading"  style="background: #31271e;">
            <div class="panel-title" style="color: #fff">
              Add New User
            </div>
          </div>
          <div class="panel-body" style="background: #31271e;">
              
              <?php if(isset($_SESSION['user-success'])){?>
                <center>
                    <div class="form-session-success">
                    <?php echo $_SESSION['user-success'];?>
                    </div>
                </center>
              <br>
            <?php }?>

            <?php if(isset($_SESSION['user-error'])){?>
                <center>
                    <div class="form-session-errors">
                    <?php echo $_SESSION['user-error'];?>
                    </div>
                </center>
              <br>
            <?php }?>
            
            <div class="form-group">
              <label for="field-1" class="col-sm-3 control-label">Full Name</label>
                <span class="description">(Eg: Mok Nicholas)</span>
              <div class="col-sm-6">
                <input type="text" name="user_fullname" placeholder="User's Fullname"  class="form-control" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label for="field-1" class="col-sm-3 control-label">Username</label>
                <span class="description">(Eg: moknick)</span>
              <div class="col-sm-6">
                <input type="text" name="username" placeholder="Username"  class="form-control" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label for="field-3" class="col-sm-3 control-label">Email</label>
              <div class="col-sm-6">
                <input type="text" name="user_email" placeholder="Email ID"  class="form-control" data-validate="required,email">
              </div>
            </div>
            <div class="form-group">
              <label for="field-3" class="col-sm-3 control-label">Mobile</label>
              <div class="col-sm-6">
                <input type="text" name="user_contact" placeholder="Mobile no:"  class="form-control" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label for="field-3" class="col-sm-3 control-label">User Type</label>
              <div class="col-sm-6">
                <select name="user_type" class="selectboxit">
                    <!-- Pending Default Selection-->
                    <optgroup label="Select User">
                        <option value="1">Administrator</option>
                        <option value="2">Employee</option>
                    </optgroup>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="field-3" class="col-sm-3 control-label">User Display</label>
                <div class="col-sm-8">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="input-group">
                        <div class="form-control uneditable-input" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new" style="color:#000">Select file</span>
                                <span class="fileinput-exists" style="color:#000">Change</span>
                                <input type="file" name="user_display">
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput" style="color:#000">Remove</a>
                    </div>
                </div>
                </div>
              </div>
            <div class="form-group default-padding" style="text-align:center">
                <button type="submit" name='submit' class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                <button type="reset" class="btn">Reset</button>
            </div>
            </div>
          </div>
        </div>
     </form>     
    <div class="col-sm-8">
        
        <!--<script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
                var $table1 = jQuery( '#item-table' );

                // Initialize DataTable
                $table1.DataTable( {
                        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "bStateSave": true
                });

                // Initalize Select Dropdown after DataTables is created
                $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                        minimumResultsForSearch: -1
                });
        } );
        </script>-->
        
        
        <?php if(isset($_SESSION['user-edit-success'])){?>
            <center>
                <div class="form-session-success">
                <?php echo $_SESSION['user-edit-success'];?>
                </div>
            </center>
          <br>
        <?php }?>

        <?php if(isset($_SESSION['user-edit-error'])){?>
            <center>
                <div class="form-session-errors">
                <?php print_r($_SESSION['user-edit-error']);?>
                </div>
            </center>
          <br>
        <?php }?>
      
        <table class="table table-bordered datatable" id="item-table">
            <thead>
                <tr>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Manage</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($users as $user):?>
                <tr class="odd gradeX">
                    <td class="center"><?php echo $user->user_fullname?></td>
                    <td class="center"><?php echo $user->user_email?></td>
                    <td class="center"><?php echo $user->user_contact;?></td>
                    <td class="center"><?php if($user->user_type == 1){ echo '<span class="badge badge-roundless">Administrator</span>';}else{echo '<span class="badge badge-roundless">Employee</span>';}?></td>
                    <td class="center"><?php if($user->user_status == 1){ echo '<span class="badge badge-success">Active</span>';}else{echo '<span class="badge badge-danger">Deactive</span>';}?></td>
                    <td class="center">
                        
                        <a href="#" class="btn btn-warning btn-sm btn-icon icon-left user-edit" data-target="#user-edit" data-toggle="modal"
                           data-user_id1="<?php echo $user->user_id?>" 
                           data-username="<?php echo $user->username?>" 
                           data-user_fullname="<?php echo $user->user_fullname?>" 
                           data-user_email="<?php echo $user->user_email?>" 
                           data-user_contact="<?php echo $user->user_contact?>" 
                           data-user_type="<?php echo $user->user_type?>" 
                           data-user_status="<?php echo $user->user_status?>" >
                           <i class="entypo-pencil"></i>
                            Edit
                        </a>
                        
                        <!-- Modal Item Edit Starts Here-->
                        <div class="modal fade" id="user-edit">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                <form role="form" method="POST" action="<?php echo base_url()?>users/edit_user" novalidate="novalidate">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Edit User Information</h4>
                                 </div>
                                 <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                               <label for="field-1" class="control-label">Full Name</label>
                                               <input type="text" name="user_fullname" id="user_fullname" class="form-control">
                                               <input type="hidden" name="user_id" id="user_id1" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                               <label for="field-1" class="control-label">User Name</label>
                                               <input type="text" name="username" id="username" class="form-control" disabled="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                               <label for="field-1" class="control-label">Email</label>
                                               <input type="text" name="user_email" id="user_email" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                               <label for="field-1" class="control-label">Contact</label>
                                               <input type="text" name="user_contact" id="user_contact" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                               <label for="field-1" class="control-label">Role</label>
                                               <select name="user_type" id="user_type" class="form-control">
                                                        <option value="1">Administrator</option>
                                                        <option value="2">Employee</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                               <label for="field-1" class="control-label">Status</label>
                                               <select name="user_status" id="user_status" class="form-control">
                                                        <option value="1">Active</option>
                                                        <option value="2">Deactivate</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>     
                                <div class="modal-footer">
                                   <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                   <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Update</button>
                                </div>
                                </form>
                              </div>
                           </div>
                        </div>
                        <!-- Modal Item Edit Ends Here-->
                        
                        
                        
                        <a href="#" class="btn btn-info btn-sm btn-icon icon-left update-password" data-target="#update-password" data-toggle="modal" data-user_id2="<?php echo $user->user_id;?>">
                           <i class="entypo-key"></i>
                            Reset Password
                        </a>
                        
                    <!-- Modal Set Password Starts Here-->
                    <div class="modal fade" id="update-password">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form role="form" method="POST" action="<?php echo base_url()?>users/reset_password" novalidate="novalidate">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Set/Reset Password</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                               <label for="field-1" class="control-label">Password</label>
                                               <input type="password" name="user_password" id="" class="form-control">
                                               <input type="hidden" name="user_id" id="user_id2" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                               <label for="field-1" class="control-label">Confirm Password</label>
                                               <input type="password" name="confirm_password" class="form-control">
                                            </div>
                                        </div>
                                    </div>     
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                       <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Set Password</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Set Password Ends Here-->
                        
                        <a href="#" class="btn btn-primary btn-sm btn-icon icon-left edit-display" data-target="#edit-display" data-toggle="modal" 
                           data-user_id3="<?php echo $user->user_id;?>" 
                           data-user_display="<?php echo $user->user_display;?>"
                           data-username2="<?php echo $user->username;?>"
                           >
                           <i class="entypo-camera"></i>
                           Change Display
                        </a>
                    <!-- Modal Change Display Starts Here-->
                        <div class="modal fade" id="edit-display">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                <form role="form" method="POST" action="<?php echo base_url()?>users/edit_user_display" enctype="multipart/form-data">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Delete Item</h4>
                                 </div>
                                 <div class="modal-body">
                                        <div class="row"> 
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="field-8" class="col-sm-5 control-label">Update Display Picture</label>
                                                       <div class="col-sm-10">
                                                           <div class="fileinput fileinput-new" data-provides="fileinput">
                                                              <input type="hidden">
                                                              <div class="fileinput-new thumbnail" style="width: 80px; height: 80px;" data-trigger="fileinput">
                                                                  <img src="" alt=""  id="user_display">
                                                                  <input type="hidden" id="user_id3" name="user_id">
                                                                  <input type="hidden" id="username2" name="username">
                                                              </div>
                                                              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 150px; line-height: 10px;"></div>
                                                              <div>
                                                                  <br>
                                                                  <span class="btn btn-white btn-file">
                                                                      <span class="fileinput-new">Select image</span>
                                                                      <span class="fileinput-exists">Change</span>
                                                                      <input type="file" name="user_display" size="20">
                                                                  </span>
                                                                  <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                              </div>
                                                          </div>
                                                           <span class="line small"><i>(Height: 150px & Width: 150px)</i></span>
                                                       </div>
                                                 </div>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
                                     </div>
                                </form>
                              </div>
                           </div>
                        </div>
                    <!-- Modal Change Display Ends Here-->
                        
                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left user-delete" data-target="#user-delete" data-toggle="modal" 
                           data-user_id4="<?php echo $user->user_id?>">
                            <i class="entypo-cancel"></i>
                            Delete
                        </a>
                    
                    <!-- Modal Item Delete Starts Here-->
                        <div class="modal fade" id="user-delete">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                <form role="form" method="POST" action="<?php echo base_url()?>users/delete_user" novalidate="novalidate">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Delete User</h4>
                                 </div>
                                 <div class="modal-body">
                                        <div class="row"> 
                                            <div class="col-md-12">
                                                <input type="hidden" id="user_id4" name="user_id">
                                                <span class="line xlarge">Are you sure you wanna delete this User ?</span><br>
                                                <span class="xx-large" style="color: #bea26a;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                    Warning : Once Deleted, this transactions associated with this User wont be available anymore..!!
                                                </span>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
                                     </div>
                                </form>
                              </div>
                           </div>
                        </div>
                    <!-- Modal Item Delete Ends Here-->
                        
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Manage</th>
                </tr>
            </tfoot>
        </table>
    </div>  
   </div>
               
<!--End of Main Content-->


<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.css">

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>

<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/fileinput.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<!--<script src="<?php echo base_url()?>assets/js/datatables/datatables.js"></script>-->
<script src="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>

<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

<script type="text/javascript">
    $(document).on("click", ".user-edit", function(event){
        
        $('#user_id1').val($(this).data('user_id1'));
        $('#user_fullname').val($(this).data('user_fullname'));
        $('#username').val($(this).data('username'));
        $('#user_email').val($(this).data('user_email'));
        $('#user_contact').val($(this).data('user_contact'));
        $('#user_type').val($(this).data('user_type'));
        $('#user_status').val($(this).data('user_status'));
    })
    
    $(document).on("click", ".update-password", function(event){
        $('#user_id2').val($(this).data('user_id2'));
    })
    
    //User Edit Image Script    
    $(document).on("click", ".edit-display", function (event) {
        $("#user_id3").val($(this).data('user_id3'))
        $("#username2").val($(this).data('username2'))
        var user_display = (baseurl + '/assets/images/users/'+ $(this).data('user_display'))
        $('#user_display').attr("src", user_display)
    });
    
    //User Edit Image Script
    $(document).on("click", ".user-delete", function(event){
        $('#user_id4').val($(this).data('user_id4'));
    })
</script>