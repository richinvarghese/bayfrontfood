<!--Main Content-->
    <h1 class="margin-bottom">Manage Buffets</h1>
    <ol class="breadcrumb 2">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li>
          <a href="<?php echo base_url()?>buffets/index">
            <i class="fa-home"></i>Buffets
          </a>
        </li>
        <li class="active">
          <strong>Add Courses</strong>
        </li>
    </ol>

    <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>courses/add_course" enctype="multipart/form-data">
    <div class="row">
       <div class="col-md-4">
        <div class="panel panel-primary" data-collapsed="0">
          <div class="panel-heading"  style="background: #31271e;">
            <div class="panel-title" style="color: #fff">
              Add a Course
            </div>
          </div>
          <div class="panel-body" style="background: #31271e;">

              <?php if(isset($_SESSION['course-success'])){?>
                <center>
                    <div class="form-session-success">
                    <?php echo $_SESSION['course-success'];?>
                    </div>
                </center>
              <br>
            <?php }?>

            <?php if(isset($_SESSION['course-error'])){?>
                <center>
                    <div class="form-session-errors">
                    <?php print_r($_SESSION['course-error']);?>
                    </div>
                </center>
              <br>
            <?php }?>

            <div class="form-group">
              <label for="field-1" class="col-sm-3 control-label">Course Name</label>
                <span class="description">(Eg: 8+1 Courses)</span>
              <div class="col-sm-6">
                <input type="text" name="course_name" placeholder="Course Name"  class="form-control" id="field-1" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label for="field-2" class="col-sm-3 control-label">Buffet</label>
              <div class="col-sm-6">
                <select name="buffet_id" class="selectboxit" id="buffet_id" data-first-option="false">
                  <option value="0">Select Buffet</option>
                  <?php foreach($buffets as $buff):?>
                  <option value="<?php echo $buff->buffet_id?>"><?php echo $buff->buffet_name;?></option>
                  <?php endforeach;?>
                </select>
                <span class="error_form" id="buffet_error_message"></span>
              </div>
            </div>
            <div class="form-group">
              <label for="field-3" class="col-sm-3 control-label">Sort ID</label>
              <span class="description">Eg: No.1 appears on top</span>
              <div class="col-sm-6">
                <select name="sort_id" class="form-control" id="sortid" data-first-option="false">
                    <option value="0" style='color:#999999;'>Select Sort ID</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="field-4" class="col-sm-3 control-label">Max Selections</label>
              <span class="description">(Eg: 4)</span>
              <div class="col-sm-6">
                <input type="text" name="course_allowed_selection" value="" placeholder="Numbers only"  class="form-control" id="field-4" data-validate="required,number,maxlength[2]">
              </div>
            </div>
            <div class="form-group default-padding" style="text-align:center">
                <button type="submit" name='submit' id="submitbutton" class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                <button type="reset" class="btn">Reset</button>
            </div>
            </div>
          </div>
        </div>
        
    <div class="col-sm-8">
        <?php foreach($categories as $cats):?>
        <div class="panel panel-dark" data-collapsed="0">
            <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title"><?php echo $cats['category_name'].'<i> ('.$cats['category_description'].')</i>';?></div>
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                </div>
            </div>

            <!-- panel body -->
            <div class="panel-body">
              <?php  foreach ($cats['details'] as $item) {?>
                <div class="col-sm-4">
                    <div class="checkbox checkbox-replace color-primary">
                        <input type="checkbox" name='item_id[]' value="<?php echo $item->item_id;?>">
                        <label><?php echo $item->item_eng;?></label>
                    </div>
                </div>
              <?php } ?>
            </div>
        </div>
        <?php endforeach;?>
   </div>
</div>
</form>
<!--End of Main Content-->


<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/selectboxit/jquery.selectBoxIt.css">

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>

<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/fileinput.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<!--<script src="<?php echo base_url()?>assets/js/datatables/datatables.js"></script>-->
<script src="<?php echo base_url()?>assets/js/select2/select2.min.js"></script>
<script src="<?php echo base_url()?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>

<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

<script>

//Item Edit Script
$(document).on("click", ".buffet-edit", function (event) {
    $("#buffet_id").val($(this).data('buffet_id'))
    $("#buffet_name").val($(this).data('buffet_name'))
    $("#buffet_min_pax").val($(this).data('buffet_min_pax'))
    $("#buffet_price").val($(this).data('buffet_price'))
    $("#buffet_course_nos").val($(this).data('buffet_course_nos'))
});

//Item Edit Image Script
$(document).on("click", ".buffet-banner-edit", function (event) {
    $("#buffet_banner_id").val($(this).data('buffet_banner_id'))
    var buffet_banner = (baseurl + '/assets/images/menu/banners/'+ $(this).data('buffet_banner_image'))
    $('#buffet_banner_edit').attr("src", buffet_banner)
});

//Item Delete Script
$(document).on("click", ".buffet-delete", function (event) {
    $("#buffet_delete_id").val($(this).data('buffet_delete_id'))
});


$(document).ready(function(){

    $('#buffet_id').change(function(){
        //alert('Get Course No:')
        $("#sortid").html("");
        var buffet_id = $(this).val()
        //alert(buffet_id);

        if(buffet_id == ''){

            var cansubmit = false;
            data = '<span style="color: #cc2424; display: inline-block; margin-top: 5px;">Buffet ID Error.</span>';
            //$msg = $(data).html();
            $('#buffet_error_message').html(data);
            document.getElementById('submitbutton').disabled = !cansubmit;
            return false;
        }
        else{

            $.ajax({

                url: "<?php echo base_url()?>courses/ajax_sort_id",
                type: "POST",
                dataType: "json",
                data: {buffet_id : buffet_id},
                success: function(data){

                    var selOpts = "";
                    for(i=1; i<=data; i++){

                        selOpts+= "<option value='"+i+"'>"+i+"</option>";
                    }

                    $('#sortid').append(selOpts);
                    //continue;
                }

            });
        }
    });
});

</script>
