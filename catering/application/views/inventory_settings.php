<h1 class="margin-bottom">Inventory Settings</h1>
<ol class="breadcrumb 2">
    <li>
        <a href="index.html">
            <i class="fa-home"></i>Home
        </a>
    </li>
    <li class="active">
        <strong>Inventory Settings</strong>
    </li>
</ol>
<br>
        <div class="row">
            
            <!-- Units Adding Starts Here-->
            <div class="col-md-6">
                <div class="panel panel-primary" data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Add Inventory Units
                        </div>
                    </div>
                <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url();?>inventory/add_unit" novalidate="novalidate">    
                    <div class="panel-body">
                        <?php if(isset($_SESSION['success-inventory-add'])){?>
                            <center>
                                <div class="badge badge-success">
                                <?php echo $_SESSION['success-inventory-add'];?>
                                </div>
                            </center>
                        <?php }?>

                        <?php if(isset($_SESSION['error-inventory-add'])){?>
                            <center>
                                <div class="badge badge-danger">
                                <?php echo $_SESSION['error-inventory-add'];?>
                                </div>
                            </center>
                        <?php }?>
                        <br>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">Unit</label>
                            <div class="col-sm-5">
                                <input type="text" placeholder="Eg: KiloGrams" name="unit_name" class="form-control" data-validate="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">Short Format</label>
                            <div class="col-sm-5">
                                <input type="text" name="unit_short" id="field-ta" placeholder="Eg: Kg" class="form-control" data-validate="required">
                            </div>
                        </div>
                        
                        <div class="form-group default-padding" style="text-align:center">
                            <button type="submit" name="submit" class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Save Unit</button>
                            <button type="reset" class="btn">Reset</button>
                        </div>
                    </div>
                  </form>
                </div>
            </div>
            <!-- Units Adding Ends Here-->
            
                        
            <!-- Units Listing Starts Here-->
            <div class="col-md-6">
                <div class="panel panel-primary" data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Inventory Units
                        </div>
                    </div>
                        <div class="panel-body"> 
                        <?php if(isset($_SESSION['success-inventory-edit'])){?>
                            <center>
                                <div class="badge badge-success">
                                <?php echo $_SESSION['success-inventory-edit'];?>
                                </div>
                            </center>
                        <?php }?>

                        <?php if(isset($_SESSION['error-inventory-edit'])){?>
                            <center>
                                <div class="badge badge-danger">
                                <?php print_r ($_SESSION['error-inventory-edit']);?>
                                </div>
                            </center>
                        <?php }?>
                        <br>
                        <div class="form-group">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Unit</th>
                                        <th>Short Format</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $x=1;?>
                                    <?php foreach($units as $unit):?>
                                    <tr>
                                        <td><?php echo $x;?></td>
                                        <td><?php echo $unit->unit_name;?></td>
                                        <td><?php echo $unit->unit_short;?></td>
                                        <td>
                                            <?php
                                            if($unit->unit_status == 1){
                                                echo '<span class="badge badge-success">Active</span>';
                                            }
                                            else{
                                                echo '<span class="badge badge-danger">Deactive</span>';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a href="#"><i class="entypo-pencil edit-unit" style="color: #bea26a;" data-target="#edit-unit" data-toggle="modal" 
                                                           data-unit_name="<?php echo $unit->unit_name;?>" 
                                                           data-unit_short="<?php echo $unit->unit_short;?>" 
                                                           data-unit_status="<?php echo $unit->unit_status;?>"
                                                           data-unit_id="<?php echo $unit->unit_id;?>" ></i></a>
                                            &nbsp;
                                            <a href="#"><i class="entypo-trash delete-unit" style="color: #bea26a;" data-target="#delete-unit" data-toggle="modal" 
                                                           data-unit_id2="<?php echo $unit->unit_id;?>"></i></a>
                                        </td>
                                        
                                            <!-- Modal for Edit Unit Starts Here-->
                                            <div class="modal fade" id="edit-unit">
                                               <div class="modal-dialog">
                                                  <div class="modal-content">
                                                    <form role="form" method="POST" action="<?php echo base_url()?>inventory/edit_unit" novalidate="novalidate">
                                                     <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">Edit Units</h4>
                                                        <input type="hidden" name="unit_id" id="unit_id">
                                                     </div>
                                                     <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                               <div class="form-group">
                                                                  <label for="field-1" class="control-label">Unit</label>
                                                                  <input type="text" name="unit_name" id="unit_name" class="form-control" data-validate="required">
                                                               </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                               <div class="form-group">
                                                                  <label for="field-1" class="control-label">Short Format</label>
                                                                  <input type="text" name="unit_short" id="unit_short" class="form-control" data-validate="required">
                                                               </div>
                                                          </div>
                                                        </div>
                                                         <div class="row">
                                                            <div class="col-md-12">
                                                               <div class="form-group">
                                                                  <label for="field-1" class="control-label">Status</label>
                                                                    <select name="unit_status" id="unit_status" class="form-control">
                                                                        <!-- Pending Default Selection-->
                                                                        <optgroup label="Choose Status">
                                                                            <option value="1">Active</option>
                                                                            <option value="2">Deactive</option>
                                                                        </optgroup>
                                                                    </select>
                                                               </div>
                                                          </div>
                                                        </div>
                                                     </div>
                                                     <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" name="submit" class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Save Unit</button>
                                                     </div>
                                                   </form>
                                                  </div>
                                               </div>
                                            </div>
                                            <!-- Modal for Edit Ends Here-->
                                            
                                            <!-- Modal Unit Delete Starts Here-->
                                            <div class="modal fade" id="delete-unit">
                                               <div class="modal-dialog">
                                                  <div class="modal-content">
                                                    <form role="form" method="POST" action="<?php echo base_url()?>inventory/delete_unit" novalidate="novalidate" enctype="multipart/form-data">
                                                     <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">Delete Unit</h4>
                                                     </div>
                                                     <div class="modal-body">
                                                        <div class="row"> 
                                                            <div class="col-md-12">
                                                                <input type="hidden" id="unit_id2" name="unit_id">
                                                                    <h4>Are you sure you wanna delete this Unit ?</h4>
                                                                    <span class="xx-large" style="color: #bea26a;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                                        Warning : The Items associated to this Unit also will be affected..!!
                                                                    </span>
                                                            </div>
                                                        </div>
                                                     </div>     
                                                     <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
                                                     </div>
                                                    </form>
                                                  </div>
                                               </div>
                                            </div>
                                            <!-- Modal Unit Delete Ends Here-->
                                            
                                        </tr>
                                        <?php $x++;?>
                                    </tbody>
                                    <?php endforeach;?>
				</table>
                            <!-- Units Listing Ends Here-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    <hr class="margin"><br/>
        
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary" data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Add Location and Shelf Details
                        </div>
                    </div>
                <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url();?>inventory/add_location" novalidate="novalidate">    
                    <div class="panel-body">
                        <?php if(isset($_SESSION['success-inventory-location'])){?>
                            <center>
                                <div class="badge badge-success">
                                <?php echo $_SESSION['success-inventory-location'];?>
                                </div>
                            </center>
                        <?php }?>

                        <?php if(isset($_SESSION['error-inventory-location'])){?>
                            <center>
                                <div class="badge badge-danger">
                                <?php echo $_SESSION['error-inventory-location'];?>
                                </div>
                            </center>
                        <?php }?>
                        <br>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">Location</label>
                            <div class="col-sm-5">
                                <input type="text" placeholder="Eg: Refrigerator" name="location_name" class="form-control" data-validate="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">Stack/Shelf</label>
                            <div class="col-sm-5">
                                <input type="text" placeholder="Eg: Shelf A" name="location_stack" class="form-control" data-validate="required">
                            </div>
                        </div>
                        
                        <div class="form-group default-padding" style="text-align:center">
                            <button type="submit" name="submit" class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Save Location</button>
                            <button type="reset" class="btn">Reset</button>
                        </div>
                    </div>
                  </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary" data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Locations
                        </div>
                    </div>
                        <div class="panel-body"> 
                        <?php if(isset($_SESSION['success-location-edit'])){?>
                            <center>
                                <div class="badge badge-success">
                                <?php echo $_SESSION['success-location-edit'];?>
                                </div>
                            </center>
                        <?php }?>

                        <?php if(isset($_SESSION['error-location-edit'])){?>
                            <center>
                                <div class="badge badge-danger">
                                <?php print_r ($_SESSION['error-location-edit']);?>
                                </div>
                            </center>
                        <?php }?>
                        <br>
                        
                        <div class="form-group">
                            <!-- Payment Methods Listing Starts Here-->
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Stack</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                        
					<tbody>
                                        <?php $x=1;?>
                                        <?php foreach($locations as $locs):?>
						<tr>
                                                    <td><?php echo $x;?></td>
                                                    <td><?php echo $locs->location_name;?></td>
                                                    <td><?php echo $locs->location_stack;?></td>
                                                    <td>
                                                    <?php
                                                    if($locs->location_status == 1){
                                                       echo '<span class="badge badge-success">Active</span>';
                                                    }
                                                    else{
                                                        echo '<span class="badge badge-danger">Deactive</span>';
                                                    }
                                                    ?></td>
                                                    
                                                    <td>
                                                        <a href="#"><i class="entypo-pencil edit-location" style="color: #bea26a;" data-target="#edit-location" data-toggle="modal"
                                                                       data-location_name="<?php echo $locs->location_name;?>" 
                                                                       data-location_stack="<?php echo $locs->location_stack;?>" 
                                                                       data-location_status="<?php echo $locs->location_status;?>" 
                                                                       data-location_id="<?php echo $locs->location_id;?>"
                                                                       ></i></a>
                                                        &nbsp;
                                                        <a href="#"><i class="entypo-trash delete-location" style="color: #bea26a;" data-target="#delete-location" data-toggle="modal" 
                                                                       data-location_id2="<?php echo $locs->location_id;?>"
                                                                       ></i></a>
                                                    </td>
                                                    
                                                    
                                                    <!-- Modal for Edit Location Starts Here-->
                                                    <div class="modal fade" id="edit-location">
                                                       <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <form role="form" method="POST" action="<?php echo base_url()?>inventory/edit_location" novalidate="novalidate">
                                                             <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">Edit Location</h4>
                                                                <input type="hidden" name="location_id" id="location_id">
                                                             </div>
                                                             <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                       <div class="form-group">
                                                                          <label for="field-1" class="control-label">Unit</label>
                                                                          <input type="text" name="location_name" id="location_name" class="form-control" data-validate="required">
                                                                       </div>
                                                                  </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                       <div class="form-group">
                                                                          <label for="field-1" class="control-label">Short Format</label>
                                                                          <input type="text" name="location_stack" id="location_stack" class="form-control" data-validate="required">
                                                                       </div>
                                                                  </div>
                                                                </div>
                                                                 <div class="row">
                                                                    <div class="col-md-12">
                                                                       <div class="form-group">
                                                                          <label for="field-1" class="control-label">Location Status</label>
                                                                            <select name="location_status" id="location_status" class="form-control">
                                                                                <!-- Pending Default Selection-->
                                                                                <optgroup label="Choose Status">
                                                                                    <option value="1">Active</option>
                                                                                    <option value="2">Deactive</option>
                                                                                </optgroup>
                                                                            </select>
                                                                       </div>
                                                                  </div>
                                                                </div>
                                                             </div>
                                                             <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="submit" name="submit" class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Save Unit</button>
                                                             </div>
                                                           </form>
                                                          </div>
                                                       </div>
                                                    </div>
                                                    <!-- Modal for Edit Ends Here-->

                                                    <!-- Modal Unit Delete Starts Here-->
                                                    <div class="modal fade" id="delete-location">
                                                       <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <form role="form" method="POST" action="<?php echo base_url()?>inventory/delete_location" novalidate="novalidate" enctype="multipart/form-data">
                                                             <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">Delete Location</h4>
                                                             </div>
                                                             <div class="modal-body">
                                                                <div class="row"> 
                                                                    <div class="col-md-12">
                                                                        <input type="hidden" id="location_id2" name="location_id">
                                                                            <h4>Are you sure you wanna delete this Location ?</h4>
                                                                            <span class="xx-large" style="color: #bea26a;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                                                Warning : The Items associated to this Location also will be affected..!!
                                                                            </span>
                                                                    </div>
                                                                </div>
                                                             </div>     
                                                             <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                                                <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
                                                             </div>
                                                            </form>
                                                          </div>
                                                       </div>
                                                    </div>
                                                    <!-- Modal Unit Delete Ends Here-->
                                                    
                                                    
                                                    
						</tr>
                                            <?php $x++;?>
					</tbody>
                                        <?php endforeach;?>
				</table>
                            
                            <!-- Payment Methods Listing Ends Here-->
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>



<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/select2/select2.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.css">


<!-- Bottom scripts (common) -->
<script src="<?php echo base_url();?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/js/joinable.js"></script>
<script src="<?php echo base_url();?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url();?>assets/js/neon-api.js"></script>


<!-- Imported scripts on this page -->
<script src="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script src="<?php echo base_url();?>assets/js/select2/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>


<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url();?>assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="<?php echo base_url();?>assets/js/neon-demo.js"></script>

<script type="text/javascript">
$(document).on("click", ".edit-unit", function(event){
    $('#unit_name').val($(this).data('unit_name'))
    $('#unit_short').val($(this).data('unit_short'))
    $('#unit_status').val($(this).data('unit_status'))
    $('#unit_id').val($(this).data('unit_id'))
})
$(document).on("click", '.delete-unit', function(event){
    $('#unit_id2').val($(this).data('unit_id2'))
})

$(document).on("click", ".edit-location", function(event){
    
    $('#location_name').val($(this).data('location_name'))
    $('#location_stack').val($(this).data('location_stack'))
    $('#location_id').val($(this).data('location_id'))
    $('#location_status').val($(this).data('location_status'))
})

$(document).on("click", ".delete-location", function(event){
    
    $('#location_id2').val($(this).data('location_id2'))
})
</script>