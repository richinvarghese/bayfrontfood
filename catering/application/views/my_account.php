<!--Main Content-->
    <h1 class="margin-bottom">Settings</h1>
    <ol class="breadcrumb 2">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li class="active">
          <strong>My Account</strong>
        </li>
    </ol>

    <div class="row">
     <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>myaccount/update_profile">
      <div class="col-md-6">
        <div class="panel panel-primary" data-collapsed="0">
          <div class="panel-heading">
            <div class="panel-title">
              Profile Details
            </div>
            <div class="panel-options">
              <a href="#" data-rel="collapse">
                <i class="entypo-down-open">
                </i>
              </a>
              <a href="#" data-rel="reload">
                <i class="entypo-arrows-ccw">
                </i>
              </a>
            </div>
          </div>
          <div class="panel-body">
              
              <?php if(isset($_SESSION['profile-success'])){?>
                <center>
                    <div class="form-session-success">
                    <?php echo $_SESSION['profile-success'];?>
                    </div>
                </center>
              <br>
            <?php }?>

            <?php if(isset($_SESSION['profile-error'])){?>
                <center>
                    <div class="form-session-errors">
                    <?php echo $_SESSION['profile-error'];?>
                    </div>
                </center>
              <br>
            <?php }?>

            <div class="form-group">
              <label for="field-1" class="col-sm-3 control-label">Display Picture</label>
              <div class="col-sm-6">
                <div class="img-circle" style="" data-trigger="fileinput">
                    <img src="<?php echo base_url().'assets/images/users/'.$user->user_display?>" alt="" style="width:70px; border:2px solid #f5f5f5; border-radius: 50%;">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="field-1" class="col-sm-3 control-label">Full Name</label>
              <div class="col-sm-6">
                <input type="text" name="user_fullname" value="<?php echo $user->user_fullname?>" placeholder="Eg: Jesus Christ"  class="form-control" id="field-1" data-validate="required">
                <input type="hidden" name="user_id" value="<?php echo $user->user_id?>">
              </div>
            </div>
            <div class="form-group">
              <label for="field-2" class="col-sm-3 control-label">Role</label>
              <div class="col-sm-6">
                  <input type="" name="user_type" value="<?php if($user->user_type == 1){echo "Administrator";}else{ echo "Employee";}?>" class="form-control" id="field-2" data-validate="required" disabled="">
              </div>
            </div>
            <div class="form-group">
              <label for="field-3" class="col-sm-3 control-label">Username</label>
              <div class="col-sm-6">
                <input type="text" name="username" value="<?php echo $user->username?>" placeholder="Eg: admin"  class="form-control" id="field-3" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label for="field-4" class="col-sm-3 control-label">Email address</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" value="<?php echo $user->user_email?>" placeholder="Eg: admin@bayfrontfood.sg" name="user_email" id="field-4" data-validate="required,email">
                <span class="description">Forgot Password Link witll be send over to this Email
                </span>
              </div>
            </div>
            <div class="form-group">
              <label for="field-5" class="col-sm-3 control-label">Contact</label>
              <div class="col-sm-6">
                <input type="text" name="user_contact" value="<?php echo $user->user_contact?>" placeholder="(65)-XXXX XXXX"  class="form-control" id="field-5" data-validate="required">
              </div>
            </div>
            <div class="form-group default-padding" style="text-align:center">
                <button type="submit" name='submit' class="btn btn-success">Save Changes</button>
                <button type="reset" class="btn">Reset</button>
            </div>
            </div>
          </div>
        </div>
     </form>
        
    <div class="col-md-6">
     <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>myaccount/update_display" novalidate="novalidate" enctype="multipart/form-data">
      <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
          <div class="panel-heading">
            <div class="panel-title">
              Display Picture
            </div>
            <div class="panel-options">
              <a href="#" data-rel="collapse">
                <i class="entypo-down-open">
                </i>
              </a>
              <a href="#" data-rel="reload">
                <i class="entypo-arrows-ccw">
                </i>
              </a>
            </div>
          </div>
            <br>
          <div class="panel-body">

          <?php if(isset($_SESSION['display-success'])){?>
                <center>
                    <div class="form-session-success">
                    <?php echo $_SESSION['display-success'];?>
                    </div>
                </center>
              <br>
            <?php }?>

            <?php if(isset($_SESSION['display-error'])){?>
                <center>
                    <div class="form-session-errors">
                    <?php print_r ($_SESSION['display-error']['errors']);?>
                    </div>
                </center>
              <br>
            <?php }?>

            <label for="field-3" class="col-sm-5 control-label">Display Picture</label>
            <div class="col-sm-5">
                <div class="fileinput fileinput-new" data-provides="fileinput"><input type="hidden">
                    <div class="input-group">
                        <div class="form-control uneditable-input" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new" style="color:#000">Select file</span>
                                <span class="fileinput-exists" style="color:#000">Change</span>
                                <input type="file" name="user_display">
                                <input type="hidden" name="user_id" value="<?php echo $user->user_id?>">
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput" style="color:#000">Remove</a>
                    </div>
                </div>
               <div class="form-group default-padding col-sm-12">
                  <button type="submit" name='submit' class="btn btn-success">Save Changes</button>
                  <button type="reset" class="btn">Reset</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
        
    <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>myaccount/update_password" novalidate="novalidate" enctype="multipart/form-data">
      <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
          <div class="panel-heading">
            <div class="panel-title">
              Update Password
            </div>
            <div class="panel-options">
              <a href="#" data-rel="collapse">
                <i class="entypo-down-open">
                </i>
              </a>
              <a href="#" data-rel="reload">
                <i class="entypo-arrows-ccw">
                </i>
              </a>
            </div>
          </div>
          <div class="panel-body">
              
            <?php if(isset($_SESSION['password-success'])){?>
                <center>
                    <div class="form-session-success">
                    <?php echo $_SESSION['password-success'];?>
                    </div>
                </center>
              <br>
            <?php }?>

            <?php if(isset($_SESSION['password-error'])){?>
                <center>
                    <div class="form-session-errors">
                    <?php print_r ($_SESSION['password-error']);?>
                    </div>
                </center>
              <br>
            <?php }?>

            <div class="form-group">
              <label for="field-1" class="col-sm-3 control-label">Current Password</label>
              <div class="col-sm-6">
                <input type="password" name="current_password" value="" placeholder="Current Password"  class="form-control" id="field-1" data-validate="required">
                <input type="hidden" name="user_id" value="<?php echo $user->user_id?>">
              </div>
            </div>
            <div class="form-group">
              <label for="field-2" class="col-sm-3 control-label">New Password</label>
              <div class="col-sm-6">
                <input type="password" name="new_password" value="" placeholder="New Password"  class="form-control" id="field-2" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label for="field-3" class="col-sm-3 control-label">Repeat New Password</label>
              <div class="col-sm-6">
                <input type="password" name="repeat_password" value="" placeholder="Repeat Password" class="form-control" id="field-3" data-validate="required">
              </div>
            </div>
            <div class="form-group default-padding" style="text-align:center">
                <button type="submit" name='submit' class="btn btn-success">Save Changes</button>
                <button type="reset" class="btn">Reset</button>
            </div>
          </div>
        </div>
      </div>
     </form>
    </div>    
   </div>
               
<!--End of Main Content-->

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>


<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/fileinput.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>


<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>