<!--Main Content-->
    <h1 class="margin-bottom">Manage Buffets</h1>
    <div class="row">
        <div class="form-group default-padding" style="text-align:right">
            
            <button type="button" class="btn btn-green btn-icon master-cat" style="background: #bea26a; border: 1px solid #bea26a;" data-target="#master-cat" data-toggle="modal">
                Add Master Category
                <i class="entypo-book" style="background: #31271e; border: 1px solid #31271e;"></i>
            </button>
            
            <!-- Modal Master Category Add Starts Here-->

            <div class="modal fade custom-width in" id="master-cat">
               <div class="modal-dialog" style="width: 60%;">
                  <div class="modal-content">
                    <form role="form" method="POST" action="<?php echo base_url()?>buffets/add_master_category" novalidate="novalidate" enctype="multipart/form-data">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" style="text-align: left;">Enter Master Category Details</h4>
                     </div>
                     <div class="modal-body">
                        <div class="row"> 
                            <div class="form-group">
                                 <label for="field-1" class="col-sm-3 control-label">Master Category Name</label>
                                 <div class="col-sm-9">
                                   <input type="text" name="master_cat_name" placeholder="Master Category name"  class="form-control" id="cat_name">
                                 </div>
                           </div>
                        </div>
                        <br>
                        <div class="row"> 
                           <div class="form-group">
                                 <label for="field-2" class="col-sm-3 control-label">Description</label>
                                 <div class="col-sm-9">
                                     <textarea name="master_cat_descrip" id="field-1" placeholder="A Short description on Master Category" class="form-control wysihtml5"></textarea>
                                 </div>
                           </div>
                        <br/>
                        </div>
                        <br/>
                        <div class="row"> 
                           <div class="form-group">
                                <label for="field-4" class="col-sm-3 control-label">Select a Display Picture</label>
                                <div class="col-sm-4">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                      <span class="btn btn-info btn-file" style="background: #bea26a; border: 1px solid #bea26a;">
                                          <span class="fileinput-new">Browse</span>
                                          <span class="fileinput-exists">Change</span>
                                          <input type="file" name="master_cat_image">
                                      </span>
                                      <span class="fileinput-filename">(Max Height & Width: 300px)</span>
                                      <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                  </div>
                                </div>
                              </div>
                            <br>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                     </div>
                    </form>
                  </div>
               </div>
            </div>

        <!-- Modal Master Category Add Ends Here-->
            
            <a href="<?php echo base_url()?>courses/index"><button type="button" class="btn btn-green btn-icon" style="background: #bea26a; border: 1px solid #bea26a;">
                Add a Course
                <i class="entypo-book" style="background: #31271e; border: 1px solid #31271e;"></i>
                </button>
            </a>
            
        </div>
    </div>
    <ol class="breadcrumb 2">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li class="active">
          <strong>Manage Buffets</strong>
        </li>
    </ol>

    <div class="row">
     
      <div class="col-md-4">
          
        <!-- Nested Divs Start Here-->
        <div class="row">
          <div class="col-md-12">
            <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>buffets/add_buffet" enctype="multipart/form-data">
            <div class="panel panel-primary" data-collapsed="0">
              <div class="panel-heading"  style="background: #31271e;">
                <div class="panel-title" style="color: #fff">
                  Add a Buffet Menu
                </div>
              </div>
              <div class="panel-body" style="background: #31271e;">

                  <?php if(isset($_SESSION['buffet-success'])){?>
                    <center>
                        <div class="form-session-success">
                        <?php echo $_SESSION['buffet-success'];?>
                        </div>
                    </center>
                  <br>
                <?php }?>

                <?php if(isset($_SESSION['buffet-error'])){?>
                    <center>
                        <div class="form-session-errors">
                        <?php echo $_SESSION['buffet-error'];?>
                        </div>
                    </center>
                  <br>
                <?php }?>

                <div class="form-group">
                  <label for="field-1" class="col-sm-3 control-label">Buffet Name</label>
                    <span class="description">(Eg: 8+1 Courses)</span>
                  <div class="col-sm-6">
                    <input type="text" name="buffet_name" value="" placeholder="Course Name"  class="form-control" id="field-1" data-validate="required">
                  </div>
                </div>
                  <div class="form-group">
                  <label for="field-1" class="col-sm-3 control-label">Master Category</label>
                    <span class="description">Eg: Vegetarian Buffet</span>
                  <div class="col-sm-6">
                    <select class="form-control" name="master_cat_id">
                        <?php foreach($master_categories as $cats):?>
                        <option value="<?php echo $cats->master_cat_id;?>"><?php echo $cats->master_cat_name;?></option>
                        <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="field-2" class="col-sm-3 control-label">Minimum Pax</label>
                  <div class="col-sm-6">
                      <input type="text" name="buffet_min_pax" value="" placeholder="Eg: 30" class="form-control" id="field-2" data-validate="required,number">
                  </div>
                </div>
                <div class="form-group">
                  <label for="field-3" class="col-sm-3 control-label">Price</label>
                  <span class="description">(Per Pax)</span>
                  <div class="col-sm-6">
                    <input type="text" name="buffet_price" value="" placeholder="Eg: 10.80"  class="form-control" id="field-3" data-validate="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="field-4" class="col-sm-3 control-label">No: of Courses</label>
                  <span class="description">(Eg: 4)</span>
                  <div class="col-sm-6">
                    <input type="text" name="buffet_course_nos" value="" placeholder="Eg: 4"  class="form-control" id="field-4" data-validate="required,number,maxlength[2]">
                  </div>
                </div>
                <div class="form-group">
                  <label for="field-5" class="col-sm-3 control-label">Banner Image</label>
                  <div class="col-sm-6">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-info btn-file" style="background: #bea26a; border: 1px solid #bea26a;">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="buffet_banner">
                        </span>
                        <span class="fileinput-filename">(Height: 150px & Width: 450px)</span>
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                    </div>
                  </div>
                </div>
                <div class="form-group default-padding" style="text-align:center">
                    <button type="submit" name='submit' class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                    <button type="reset" class="btn">Reset</button>
                </div>
                </div>
              </div>
             </form>
            </div>
          <!-- Nested Divs End Here-->
          
          <!--Master Category Starts Here-->    
                <div class="col-md-12">
                 <div class="panel panel-primary" data-collapsed="0" style="border-color: #31271e">
                   <div class="panel-heading"  style="background: #31271e;">
                     <div class="panel-title" style="color: #fff">
                       Categories
                     </div>
                   </div>
                   <div class="panel-body">

                       <?php if(isset($_SESSION['master-category-success'])){?>
                         <center>
                             <div class="form-session-success">
                             <?php echo $_SESSION['master-category-success'];?>
                             </div>
                         </center>
                       <br>
                     <?php }?>

                     <?php if(isset($_SESSION['master-category-error'])){?>
                         <center>
                             <div class="form-session-errors">
                             <?php print_r($_SESSION['master-category-error']);?>
                             </div>
                         </center>
                       <br>
                     <?php }?>

                     <table class="table table-striped">
                         <thead>
                             <tr>
                                 <th>No#</th>
                                 <th>Category Name</th>
                                 <th>Manage</th>
                             </tr>
                         </thead>
                         <?php $x=1;?>
                         <tbody>
                             <?php foreach($master_categories as $cats):?>
                             <tr>
                                 <td><?php echo $x;?></td>
                                 <td><?php echo $cats->master_cat_name?></td>
                                 <td>
                                     <div class="icon-el col-md-2 col-sm-5"><a href="#"><i class="glyphicon glyphicon-edit master_cat_edit" data-target="#master-cat-edit" data-toggle="modal" 
                                             data-master_cat_id="<?php echo $cats->master_cat_id?>" 
                                             data-master_cat_name="<?php echo $cats->master_cat_name?>" 
                                             data-master_cat_descrip="<?php echo $cats->master_cat_descrip?>" >                                      
                                             </i></a></div>

                                     <!-- Modal Category Edit Starts Here-->

                                         <div class="modal fade custom-width in" id="master-cat-edit">
                                            <div class="modal-dialog" style="width: 60%">
                                               <div class="modal-content">
                                                   <form role="form" method="POST" action="<?php echo base_url()?>buffets/edit_master_category" novalidate="novalidate">
                                                   <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                     <h4 class="modal-title">Edit Master Category Details</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row"> 
                                                         <div class="form-group">
                                                              <label for="field-1" class="col-sm-3 control-label">Category Name</label>
                                                              <div class="col-sm-9">
                                                                <input type="hidden" name="master_cat_id" id="master_cat_id">
                                                                <input type="text" name="master_cat_name" placeholder="Category name"  class="form-control" id="master_cat_name" data-validate="required">
                                                              </div>
                                                        </div>
                                                     </div>
                                                     <br>
                                                     <div class="row"> 
                                                        <div class="form-group">
                                                              <label for="field-2" class="col-sm-3 control-label">Description</label>
                                                              <div class="col-sm-9">
                                                                  <textarea name="master_cat_descrip" placeholder="A Short description on Category" class="form-control" id="master_cat_descrip" data-validate="required"></textarea>
                                                                  <!--<textarea class="form-control" placeholder="A Short description on Category" name="master_cat_descrip" id="master_cat_descrip" data-uk-markdownarea></textarea>-->
                                                                  <!--<textarea name="master_cat_descrip" id="master_cat_descrip" placeholder="A Short description on Master Category" class="form-control wysihtml5"></textarea>-->
                                                              </div>
                                                        </div>
                                                     <br>
                                                     </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                     <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                                                  </div>
                                                  </form>
                                               </div>
                                            </div>
                                         </div>

                                     <!-- Modal Category Edit Ends Here-->


                                     <div class="icon-el col-md-2 col-sm-5"><a href="#"><i class="glyphicon glyphicon-remove-sign master_cat_delete" data-target="#master-cat-delete" data-toggle="modal" 
                                                 data-master_cat_delete_id="<?php echo $cats->master_cat_id ?>" >
                                             </i></a></div>

                                     <!-- Modal Master category Delete Starts Here-->

                                         <div class="modal fade" id="master-cat-delete">
                                            <div class="modal-dialog">
                                               <div class="modal-content">
                                                 <form role="form" method="POST" action="<?php echo base_url()?>buffets/delete_master_category" novalidate="novalidate">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                     <h4 class="modal-title">Delete Item</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row"> 
                                                         <div class="col-md-12">
                                                             <input type="hidden" id="master_cat_delete_id" name="master_cat_delete_id">
                                                             <h4>Are you sure you wanna delete this Category ?</h4>
                                                             <span class="xx-large" style="color: #bea26a;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                                 Warning : The Items associated to this category also will be deleted..!!
                                                             </span>
                                                         </div>
                                                     </div>
                                                  </div>     
                                                  <div class="modal-footer">
                                                     <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                                     <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
                                                  </div>
                                                 </form>
                                               </div>
                                            </div>
                                         </div>

                                     <!-- Modal Master Category Delete Ends Here-->
                                     
                                     
                                     <div class="icon-el col-md-2 col-sm-5"><a href="#"><i class="glyphicon glyphicon-picture master_cat_edit_image" data-target="#master_cat_edit_images" data-toggle="modal" 
                                                 data-master_cat_edit_image_name="<?php echo $cats->master_cat_image ?>" 
                                                 data-master_cat_id3="<?php echo $cats->master_cat_id ?>" 
                                                 >
                                             </i></a></div>

                                     <!-- Modal Master Category Edit Image Starts Here-->

                                         <div class="modal fade" id="master_cat_edit_images">
                                            <div class="modal-dialog">
                                               <div class="modal-content">
                                                 <form role="form" method="POST" action="<?php echo base_url()?>buffets/edit_master_cat_image" novalidate="novalidate" enctype="multipart/form-data">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                     <h4 class="modal-title">Change Master Display</h4>
                                                  </div>

                                                  <div class="modal-body">
                                                     <div class="row"> 
                                                         <div class="col-md-12">
                                                             <div class="form-group">
                                                                <label for="field-8" class="col-sm-5 control-label">Update Master Display</label>
                                                                   <div class="col-sm-10">
                                                                       <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                          <input type="hidden">
                                                                          <div class="fileinput-new thumbnail" style="width: 150px; height: 150px;" data-trigger="fileinput">
                                                                              <img src="" alt=""  id="master_cat_edit_src">
                                                                              <input type="hidden" id="master_cat_id3" name="master_cat_id">
                                                                              <input type="hidden" id="master_cat_image2" name="master_cat_image">
                                                                          </div>
                                                                          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px; line-height: 10px;"></div>
                                                                          <div>
                                                                              <br>
                                                                              <span class="btn btn-white btn-file">
                                                                                  <span class="fileinput-new">Select image</span>
                                                                                  <span class="fileinput-exists">Change</span>
                                                                                  <input type="file" name="master_cat_image_file" size="20">
                                                                              </span>
                                                                              <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                                          </div>
                                                                      </div>
                                                                       <span class="line small"><i>(Height: 300px & Width: 300px)</i></span>
                                                                   </div>
                                                             </div>
                                                         </div>
                                                     </div>
                                                  </div>     
                                                  <div class="modal-footer">
                                                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                     <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                                                  </div>
                                                 </form>
                                               </div>
                                            </div>
                                         </div>

                                     <!-- Modal Master Category Edit Image Ends Here-->



                                 </td>
                             </tr>
                             <?php $x++;?>
                             <?php endforeach;?>
                         </tbody>
                        </table>
                     </div>
                   </div>
                </div>
          
          <!--Master Category Starts Here-->
          
            </div>
        </div>
     </form>     
    <div class="col-sm-8">
        
        <!--<script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
                var $table1 = jQuery( '#item-table' );

                // Initialize DataTable
                $table1.DataTable( {
                        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "bStateSave": true
                });

                // Initalize Select Dropdown after DataTables is created
                $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                        minimumResultsForSearch: -1
                });
        } );
        </script>-->
        
        
        <?php if(isset($_SESSION['buffet-edit-success'])){?>
            <center>
                <div class="form-session-success">
                <?php echo $_SESSION['buffet-edit-success'];?>
                </div>
            </center>
          <br>
        <?php }?>

        <?php if(isset($_SESSION['buffet-edit-error'])){?>
            <center>
                <div class="form-session-errors">
                <?php print_r($_SESSION['buffet-edit-error']);?>
                </div>
            </center>
          <br>
        <?php }?>
      
        <table class="table table-bordered datatable" id="item-table">
            <thead>
                <tr>
                    <th>Buffet Title</th>
                    <th>Master Category</th>
                    <th>Min Pax</th>
                    <th>Price/Pax</th>
                    <th>No: of Courses</th>
                    <th>Manage</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($buffets as $buffet):?>
                <tr class="odd gradeX">
                    <td class="center"><?php echo $buffet->buffet_name?></td>
                    <td class="center"><?php echo $buffet->master_cat_name?></td>
                    <td class="center"><?php echo $buffet->buffet_min_pax?></td>
                    <td class="center"><?php echo $settings->entity_currency_symbol.' '.$buffet->buffet_price;?></td>
                    <td class="center"><?php echo $buffet->buffet_course_nos;?></td>
                    <td class="center">
                        
                        <a href="#" class="btn btn-warning btn-sm btn-icon icon-left buffet-edit" data-target="#buffet-edit" data-toggle="modal" 
                           data-buffet_id = "<?php echo $buffet->buffet_id ?>" 
                           data-master_cat_id = "<?php echo $buffet->master_cat_id?>"
                           data-buffet_name = "<?php echo $buffet->buffet_name ?>" 
                           data-buffet_min_pax = "<?php echo $buffet->buffet_min_pax ?>" 
                           data-buffet_price = "<?php echo $buffet->buffet_price ?>" 
                           data-buffet_course_nos = "<?php echo $buffet->buffet_course_nos ?>" >
                           <i class="entypo-pencil"></i>
                            Edit
                        </a>
                        
                        <!-- Modal Item Edit Starts Here-->

                        <div class="modal fade" id="buffet-edit">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                <form role="form" method="POST" action="<?php echo base_url()?>buffets/edit_buffet" novalidate="novalidate" enctype="multipart/form-data">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Edit Buffet Details</h4>
                                 </div>

                                 <div class="modal-body">
                                    <div class="row"> 
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Buffet Name</label>
                                            <div class="col-sm-6">
                                              <input type="text" name="buffet_name" value="" placeholder="Buffet Name"  class="form-control" id="buffet_name" data-validate="required">
                                              <input type="hidden" name="buffet_id" id="buffet_id">
                                            </div>
                                        </div>
                                    </div><br>
                                    <div class="row"> 
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Master Category</label>
                                            <div class="col-sm-6">
                                                <select class="form-control" name="master_cat_id" id="master_cat_id">
                                                    <!-- Pending Default Selection-->
                                                    <optgroup label="Choose Master Category">
                                                        <?php foreach($master_categories as $cat):?>
                                                            <option value="<?php echo $cat->master_cat_id;?>"><?php echo $cat->master_cat_name;?></option>
                                                        <?php endforeach;?>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div><br>
                                    <div class="row"> 
                                        <div class="form-group">
                                            <label for="field-2" class="col-sm-3 control-label">Minimum Pax</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="buffet_min_pax" value="" placeholder="Eg: 30" class="form-control" id="buffet_min_pax" data-validate="required,number">
                                            </div>
                                        </div>
                                    </div><br>
                                    <div class="row"> 
                                        <div class="form-group">
                                            <label for="field-3" class="col-sm-3 control-label">Price</label>
                                            <div class="col-sm-6">
                                              <input type="text" name="buffet_price" value="" placeholder="Eg: 10.80"  class="form-control" id="buffet_price" data-validate="required">
                                            </div>
                                        </div>
                                    </div><br>
                                    <div class="row"> 
                                        <div class="form-group">
                                            <label for="field-4" class="col-sm-3 control-label">No: of Courses</label>
                                            <div class="col-sm-6">
                                              <input type="text" name="buffet_course_nos" value="" placeholder="Eg: 4"  class="form-control" id="buffet_course_nos" data-validate="required,number,maxlength[1]">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>     
                                <div class="modal-footer">
                                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                   <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                                </div>
                               </form> 
                            </div>
                          </div>
                        </div>

                        <!-- Modal Item Edit Ends Here-->
                        
                        <a href="#" class="btn btn-primary btn-sm btn-icon icon-left buffet-banner-edit" data-target="#buffet-banner-edit" data-toggle="modal" 
                           data-buffet_banner_id="<?php echo $buffet->buffet_id;?>" 
                           data-buffet_banner_image="<?php echo $buffet->buffet_banner;?>" >
                            <i class="entypo-camera"></i>
                            Update Buffet Banner
                        </a>
                            
                        <!-- Modal to Update Image Starts Here-->

                        <div class="modal fade" id="buffet-banner-edit">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                <form role="form" method="POST" action="<?php echo base_url()?>buffets/edit_banner" novalidate="novalidate" enctype="multipart/form-data">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Update Banner</h4>
                                 </div>

                                 <div class="modal-body">
                                    <div class="row"> 
                                        <div class="col-md-12">
                                            <div class="form-group">
                                               <label for="field-8" class="col-sm-5 control-label">Update Buffet Banner</label>
                                                  <div class="col-sm-10">
                                                      <div class="fileinput fileinput-new" data-provides="fileinput">
                                                         <input type="hidden">
                                                         <div class="fileinput-new thumbnail" style="width: 450px; height: 150px;" data-trigger="fileinput">
                                                             <img src="" alt=""  id="buffet_banner_edit">
                                                             <input type="hidden" id="buffet_banner_id" name="buffet_id">
                                                         </div>
                                                         <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 450px; max-height: 150px; line-height: 10px;"></div>
                                                         <div>
                                                             <br>
                                                             <span class="btn btn-white btn-file">
                                                                 <span class="fileinput-new">Select image</span>
                                                                 <span class="fileinput-exists">Change</span>
                                                                 <input type="file" name="buffet_banner" size="20">
                                                             </span>
                                                             <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                         </div>
                                                     </div>
                                                      <span class="line small"><i>(Height: 150px & Width: 450px)</i></span>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                 </div>     
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" name='item_edit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Save Changes</button>
                                 </div>
                                </form>
                              </div>
                           </div>
                        </div>

                        <!-- Modal to Update Image Ends Here-->
                        
                        <a href="<?php echo base_url().'manage_courses/course/'.$buffet->buffet_id?>" class="btn btn-info btn-sm btn-icon icon-left" >
                            <i class="entypo-docs"></i>
                            Manage Courses
                        </a>
                        
                        
                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left buffet-delete" data-target="#buffet-delete" data-toggle="modal" 
                           data-buffet_delete_id="<?php echo $buffet->buffet_id?>" >
                            <i class="entypo-cancel"></i>
                            Delete
                        </a>
                        
                        
                        <!-- Modal Item Delete Starts Here-->

                            <div class="modal fade" id="buffet-delete">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>buffets/delete_buffet" novalidate="novalidate" enctype="multipart/form-data">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Delete Item</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="col-md-12">
                                                <input type="hidden" id="buffet_delete_id" name="buffet_id">
                                                <span class="line xlarge">Are you sure you wanna delete this Buffet ?</span><br>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                        
                        <!-- Modal Item Delete Ends Here-->
                        
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Buffet Title</th>
                    <th>Master Category</th>
                    <th>Min Pax</th>
                    <th>Price/Pax</th>
                    <th>No: of Courses</th>
                    <th>Manage</th>
                </tr>
            </tfoot>
        </table>
    </div>  
   </div>
               
<!--End of Main Content-->


<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/wysihtml5/bootstrap-wysihtml5.css">
<!--<link rel="stylesheet" href="<?php echo base_url()?>assets/js/codemirror/lib/codemirror.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/uikit/css/uikit.min.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/uikit/addons/css/markdownarea.css">-->

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>
<script src="<?php echo base_url()?>assets/js/wysihtml5/wysihtml5-0.4.0pre.min.js"></script>

<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/fileinput.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-switch.min.js"></script>
<!--<script src="<?php echo base_url()?>assets/js/datatables/datatables.js"></script>-->
<script src="<?php echo base_url()?>assets/js/select2/select2.min.js"></script>
<script src="<?php echo base_url()?>assets/js/wysihtml5/bootstrap-wysihtml5.js"></script>
<!--<script src="<?php echo base_url()?>assets/js/uikit/js/uikit.min.js"></script>
<script src="<?php echo base_url()?>assets/js/codemirror/lib/codemirror.js"></script>
<script src="<?php echo base_url()?>assets/js/marked.js"></script>
<script src="<?php echo base_url()?>assets/js/uikit/addons/js/markdownarea.min.js"></script>-->

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>

<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

<script>
    
//Item Edit Script    
$(document).on("click", ".buffet-edit", function (event) {
    $("#buffet_id").val($(this).data('buffet_id'))
    $("#master_cat_id").val($(this).data('master_cat_id'))
    $("#buffet_name").val($(this).data('buffet_name'))
    $("#buffet_min_pax").val($(this).data('buffet_min_pax'))
    $("#buffet_price").val($(this).data('buffet_price'))
    $("#buffet_course_nos").val($(this).data('buffet_course_nos'))
});

//Item Edit Image Script    
$(document).on("click", ".buffet-banner-edit", function (event) {
    $("#buffet_banner_id").val($(this).data('buffet_banner_id'))
    var buffet_banner = (baseurl + '/assets/images/menu/banners/'+ $(this).data('buffet_banner_image'))
    $('#buffet_banner_edit').attr("src", buffet_banner)
});

//Item Delete Script    
$(document).on("click", ".buffet-delete", function (event) {
    $("#buffet_delete_id").val($(this).data('buffet_delete_id'))
});

//Master Category Edit Script
$(document).on('click', '.master_cat_edit', function(event){
    
    $('#master_cat_id').val($(this).data('master_cat_id'))
    $('#master_cat_name').val($(this).data('master_cat_name'))
    $('#master_cat_descrip').val($(this).data('master_cat_descrip'))
});

//Master Category Delete
$(document).on('click', '.master_cat_delete', function(event){
    
    $('#master_cat_delete_id').val($(this).data('master_cat_delete_id'))
});

//Item Edit Master Category Image Script    
$(document).on("click", ".master_cat_edit_image", function (event) {
    $("#master_cat_id3").val($(this).data('master_cat_id3'))
    $("#master_cat_image2").val($(this).data('master_cat_edit_image_name'))
    var master_cat_img = (baseurl + '/assets/images/master_cat/'+ $(this).data('master_cat_edit_image_name'))
    $('#master_cat_edit_src').attr("src", master_cat_img)
});

</script>