<!--================Banner Area =================-->
<section class="banner_area" style='background: url(<?php echo base_url();?>/frontend/assets/img/banner/banner-bg-1.jpg) no-repeat scroll center center;'>
    <div class="container">
        <div class="banner_content">
            <h4>BUFFET MENU</h4>
            <a href="http://bayfrontfood.sg">Home</a>
            <a class="active">Buffet List</a>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->

<!--================End Our feature Area =================-->
        <section class="most_popular_item_area menu_list_page">
            <div class="container">
                <div class="popular_filter">
                    <ul>
                        <li class="active" data-filter="*" style='margin:5px;'><a href="">All</a></li>
                        <?php foreach($buffets as $buff):?>
                        <li data-filter="<?php echo ".".$buff->buffet_id?>" style='margin:5px;'><a href="" ><?php echo $buff->buffet_name;?></a></li>
                        <?php endforeach;?>
                    </ul>
                </div>
                
                <div class="p_recype_item_main">
                    <div class="row p_recype_item_active">
                        <?php foreach($buffets as $buf):?>
                        <div class="col-md-4 col-sm-6 <?php echo $buf->buffet_id?>">
                            <div class="feature_item">
                                <div class="feature_item_inner">
                                    <img src="<?php echo base_url().'/assets/images/menu/banners/'.$buf->buffet_banner?>" alt="">
                                    <div class="icon_hover">
                                        <a href="<?php echo base_url().'frontend_controller/listmenu/view_menu/'.$buf->buffet_id;?>"><i class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class="title_text" style='background: #bea26a;'>
                                    <div class="feature_left"><a href="#"><span><?php echo $buf->buffet_name.' ('.$buf->buffet_min_pax.' Pax)';?></span></a></div>
                                    <div class="restaurant_feature_dots"></div>
                                    <div class="feature_right"><?php echo $settings->entity_currency_symbol.' '.$buf->buffet_price;?></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Our Menu Listing Area =================-->