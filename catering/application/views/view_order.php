<!--Main Content-->
    <h1 class="margin-bottom hidden-print">View Order</h1>
    <ol class="breadcrumb 2 hidden-print">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li>
          <a href="<?php echo base_url()?>manageorders/index">
            <i class="fa-home"></i>Orders
          </a>
        </li>
        <li class="active">
          <strong>Manage Order</strong>
        </li>
    </ol>

    <div class="row">
        <div class="col-sm-12">
            <div class="invoice">
                <div class="row" style="padding:0px 15px 0px 15px;">
                    <div class="col-sm-6 invoice-left">
                        <a href="#">
                            <img src="<?php echo base_url();?>/assets/images/app_settings/entity/logo.png" width="185" alt="">
                        </a>
                    </div>
                    <div class="col-sm-6 invoice-right">
                        <h3>ORDER NO. <?php echo $settings->entity_invoice_prefix.' #'.$order->order_id;?></h3>
                        <span><?php echo 'Ordered On : <i class="glyphicon glyphicon-calendar" style="color:#bea26a"></i> '.date('d-M-Y', strtotime($order->order_date));?></span></br>
                        <span><?php echo 'Order Status : ';?><?php if($order->order_status == 0){ 
                        echo "<div class='label label-secondary'><strong>Pending Approval</strong></div>";  
                        }
                        elseif($order->order_status == 1){
                            echo "<div class='label label-success'><strong>Approved</strong></div>";
                        }
                        elseif($order->order_status == 2){
                            echo "<div class='label label-danger'><strong>Cancelled</strong></div>";
                        }
                        elseif($order->order_status == 3){
                            echo "<div class='label label-info'><strong>Completed</strong></div>";
                        }
                        ?></span>
                    </div>
                </div>
                <hr class="margin hidden-print">
                <div class="row" style="padding:0px 15px 0px 15px;">
                    <div class="col-sm-3 invoice-left">
                        <h4>Customer</h4>
                        <?php echo $order->customer_name;?>
                        <br>
                        <?php echo $order->address_block.', '.$order->address_street;?>
                        <br>
                        <?php echo 'Singapore ('.$order->address_postal.') '?>
                    </div>
                    <div class="col-sm-3 invoice-left">
                        <h4>Contact Details</h4>
                        <?php echo 'Phone: '.$order->customer_contact;?>
                        <br>
                        <?php echo 'Email: '.$order->customer_email;?>
                        <br>
                    </div>
                    <div class="col-md-6 invoice-right">
                        <h4><u>Party Details</u></h4>
                        <strong>Party Pax:</strong> <?php echo $order->order_pax;?>
                        <br>
                        <strong>Date:</strong> <?php echo '<i class="glyphicon glyphicon-calendar" style="color:#bea26a"></i> '.date('d-M-Y', strtotime($order->dining_date));?>
                        <br>
                        <strong>Time:</strong> <?php echo '<i class="glyphicon glyphicon-time" style="color:#bea26a"></i> '.date('h:i:A', strtotime($order->dining_time));?>
                    </div>
                </div>
                <div class="margin hidden-print"></div>
                <?php if(isset($_SESSION['order-success'])){?>
                    <center>
                        <div class="form-session-success">
                        <?php echo $_SESSION['order-success'];?>
                        </div>
                    </center>
                  <br>
                <?php }?>

                <?php if(isset($_SESSION['order-error'])){?>
                    <center>
                        <div class="form-session-errors">
                        <?php print_r($_SESSION['order-error']);?>
                        </div>
                    </center>
                  <br>
                <?php }?>
                  
                <div class="margin hidden-print"></div>
                <div class="jumbotron" style="padding-left: 0px; padding-right: 0px; background-color: #fff">
                    <br>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                                <div class="panel-title">Courses and Add-on Items</div>
                                <div class="panel-options">
                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                </div>
                        </div>

                        <div class="panel-body with-table">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <th>Buffet Courses & Items Opted</th>
                                        <th>Pax</th>
                                        <th>Price Per Pax</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <tr>
                                        <!--Order Details-->
                                        <td>
                                            <h4><?php echo 'Buffet :'.$order->order_buffet;?></h4>
                                            <br/>
                                            <?php $a=1;?>
                                            <?php foreach($order_courses as $courses):?>
                                            <p style="font-size: medium"><u><?php echo $a.': '.$courses->order_course_name;?></u></p>
                                                
                                                <?php foreach($courses->order_items as $item):?>
                                                    <p style="font-size: small"><i class="glyphicon glyphicon-cutlery" style="color:#bea26a"></i><?php echo ' '.$item->order_items_eng;?></p>
                                                <?php endforeach;?>
                                                    
                                                <?php $a++;?>
                                            <?php endforeach;?>
                                        </td>

                                        <!--Party Pax-->
                                        <td>
                                            <p><?php echo $order->order_pax.' ';?>
                                            </p>
                                        </td>

                                        <td><p><?php echo $settings->entity_currency_symbol.' '.$order->order_buffet_price;?></p></td>

                                        <!--Buffet Total-->
                                        <td><p><?php echo $settings->entity_currency_symbol.' '.(($order->order_pax) *($order->order_buffet_price));?></p></td>
                                    </tr>
                                    
                                    <!--Addons Section-->
                                    
                                    <?php if($nos_addons !== $total_null):?>
                                    <tr>
                                        <!--Addon Items-->
                                        <td>
                                            <p style="font-size: medium"><u><?php echo 'Addon-ons'?></u></p>
                                            <?php foreach($addons as $adds):?>
                                                <?php if($adds->order_add_items_pax==0):?>
                                                <?php continue;?>
                                                <?php else:?>
                                                <p style="font-size: small"><i class="glyphicon glyphicon-cutlery" style="color:#bea26a"></i><?php echo ' '.$adds->order_add_items_eng;?></p>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </td>

                                        <!--Addon Pax-->
                                        <td>
                                            <?php foreach($addons as $adds):?>
                                                <?php if($adds->order_add_items_pax==0):?>
                                                <?php continue;?>
                                                <?php else:?>
                                                    <p></p><p><?php echo $adds->order_add_items_pax.' ';?>
                                                    </p>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </td>

                                        <!--Addon Price/Pax-->
                                        <td>
                                            <?php foreach($addons as $adds):?>
                                            <?php if($adds->order_add_items_pax==0):?>
                                            <?php continue;?>
                                            <?php else:?>
                                                <p></p><p><?php echo $settings->entity_currency_symbol.' '.$adds->order_add_items_price.' ';?></p>
                                            <?php endif;?>
                                            <?php endforeach;?>
                                        </td>

                                        <!--Addons Total-->
                                        <td>
                                            <?php foreach($addons as $adds):?>
                                            <?php if($adds->order_add_items_pax==0):?>
                                            <?php continue;?>
                                            <?php else:?>
                                                <p></p><p><?php echo $settings->entity_currency_symbol.' '.(($adds->order_add_items_pax) * ($adds->order_add_items_price));?></p>
                                            <?php endif;?>
                                            <?php endforeach;?>
                                        </td>
                                    </tr>
                                    <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-9">
                            <div class="panel panel-primary" data-collapsed="0" style="border-color: #fff;">
                                <!-- panel head -->
                                <div class="panel-heading" style="border-color: #fff">
                                    <div class="panel-title"><h4>Additional Notes: </h4></div>
                                </div>
                                <!-- panel body -->
                                <div class="panel-body">
                                    <p style="font-size: small;"><?php echo $order->order_notes;?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <table class="table responsive">
                                    <thead>
                                    </thead>
                                    <tbody>
                                            <tr>
                                                <td style='border-top: 0px solid #fff;'><strong>Subtotal</strong></td>
                                                <td style='border-top: 0px solid #fff;'><?php echo $settings->entity_currency_symbol." ". number_format(round($order->order_stotal, 1), 2);?></td>
                                            </tr>

                                            <tr>
                                                <td style='border-top: 0px solid #fff;'><strong>Discount </strong><?php echo "(".$order->order_discount."%)"?></td>
                                                <td style='border-top: 0px solid #fff;'><?php echo $settings->entity_currency_symbol." ".number_format(round($order->order_discount * ($order->order_stotal/100), 1), 2);?></td>
                                            </tr>
                                            <tr>
                                                <td style='border-top: 0px solid #fff;'><strong>Delivery </strong></td>
                                                    <td style='border-top: 0px solid #fff;'><?php echo $settings->entity_currency_symbol." ".number_format(round($order->order_delivery, 1), 2);?></td>
                                            </tr>
                                            <tr>
                                                <td style='border-top: 0px solid #fff;'>
                                                <?php if($settings->entity_gst == 0):?>
                                                <strong>GST </strong><?php echo "(0%)";?>
                                                <?php else:?>
                                                <strong>GST </strong><?php echo "(7%)";?>
                                                <?php endif;?>
                                                </td>
                                                <td style='border-top: 0px solid #fff;'>
                                                 <?php echo $settings->entity_currency_symbol." ".number_format(round($order->order_gst, 1), 2);?>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td style='border-top: 0px solid #fff;'><strong>GRAND TOTAL</strong></td>
                                                <td style='border-top: 0px solid #fff;'><strong><?php echo $settings->entity_currency_symbol." ". number_format(round($order->order_gtotal, 1), 2);?></strong></td>
                                            </tr>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Order Terms starts here-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default" data-collapsed="0">
                                <!-- panel head -->
                                <div class="panel-heading">
                                    <div class="panel-title"><h3>Order Terms & Conditions</h3></div>
                                </div>
                                <!-- panel body -->
                                <div class="panel-body">
                                    <p><?php echo $settings->entity_invoice_terms?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Order Terms Ends Here-->
                    
                    <!-- Order Print and Email starts here-->
                    <div class="row">
                        <div class="col-md-10"></div>
                        <div class="col-md-1 invoice-right">
                            <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">Print Order<i class="entypo-doc-text"></i></a>
                        </div>
                        <div class="col-md-1 invoice-right">
                            <a href="#" class="btn btn-success btn-icon icon-left hidden-print">Send Order<i class="entypo-mail"></i></a>
                        </div>
                    </div>
                    <!--Order Print and Email Ends Here-->
                    
                </div>
            </div>
        </div>  
    </div>
               
<!--End of Main Content-->

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>