<!--Main Content-->
    <h1 class="margin-bottom">Manage Inventory Items</h1>
    <ol class="breadcrumb 2">
        <li>
          <a href="<?php echo base_url()?>dashboard/index">
            <i class="fa-home"></i>Dashboard
          </a>
        </li>
        <li class="active">
          <strong>Inventory Items</strong>
        </li>
    </ol>

    <div class="row">
     <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>inventoryitems/add_item" enctype="multipart/form-data">
      <div class="col-md-4">
        <div class="panel panel-primary" data-collapsed="0">
          <div class="panel-heading"  style="background: #31271e;">
            <div class="panel-title" style="color: #fff">
              Add an Inventory Item
            </div>
          </div>
          <div class="panel-body" style="background: #31271e;">

              <?php if(isset($_SESSION['item-success'])){?>
                <center>
                    <div class="form-session-success">
                    <?php echo $_SESSION['item-success'];?>
                    </div>
                </center>
              <br>
            <?php }?>

            <?php if(isset($_SESSION['item-error'])){?>
                <center>
                    <div class="form-session-errors">
                    <?php echo $_SESSION['item-error'];?>
                    </div>
                </center>
              <br>
            <?php }?>

            <div class="form-group">
              <label class="col-sm-3 control-label">Item Name</label>
                <span class="description">(Eg: Australian Beef)</span>
              <div class="col-sm-6">
                <input type="text" name="item_name" value="" placeholder="Item Name"  class="form-control" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Item SKU</label>
              <span class="description">(Item Code)</span>
              <div class="col-sm-6">
                  <input type="text" name="item_sku" value="" placeholder="Eg: IT012019" class="form-control" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Cost Price</label>
              <span class="description">(Supplier Quote)</span>
              <div class="col-sm-6">
                <input type="text" name="item_cost_price" value="" placeholder="Eg: 10.80"  class="form-control" data-validate="required">
              </div>
            </div>
            <div class="form-group">
              <label for="field-4" class="col-sm-3 control-label">Metric Unit</label>
              <span class="description">(Eg: Kg)</span>
              <div class="col-sm-6">
                <select name="item_unit" class="selectboxit">
                    <!-- Pending Default Selection-->
                    <optgroup label="Select a Matric Unit">
                        <?php foreach($units as $unit):?>
                        <option value="<?php echo $unit->unit_short;?>"><?php echo $unit->unit_name;?></option>
                        <?php endforeach;?>
                    </optgroup>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="field-5" class="col-sm-3 control-label">Min Quatity Alert</label>
                <div class="col-sm-6">
                  <input type="text" name="item_min_qty" value="" placeholder="Eg: 10"  class="form-control" data-validate="required,number">
                </div>
            </div>
            <div class="form-group default-padding" style="text-align:center">
                <button type="submit" name='submit' class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Add Item</button>
                <button type="reset" class="btn">Reset</button>
            </div>
            </div>
          </div>
        </div>
     </form>
    <div class="col-sm-8">

        <!--<script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
                var $table1 = jQuery( '#item-table' );

                // Initialize DataTable
                $table1.DataTable( {
                        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "bStateSave": true
                });

                // Initalize Select Dropdown after DataTables is created
                $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                        minimumResultsForSearch: -1
                });
        } );
        </script>-->


        <?php if(isset($_SESSION['item-edit-success'])){?>
            <center>
                <div class="form-session-success">
                <?php echo $_SESSION['item-edit-success'];?>
                </div>
            </center>
          <br>
        <?php }?>

        <?php if(isset($_SESSION['item-edit-error'])){?>
            <center>
                <div class="form-session-errors">
                <?php print_r($_SESSION['item-edit-error']);?>
                </div>
            </center>
          <br>
        <?php }?>

        <table class="table table-bordered datatable" id="item-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Item Name</th>
                    <th>Sku</th>
                    <th>Cost Price</th>
                    <th>Min Quantity Alert</th>
                    <th>Status</th>
                    <th>Manage</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=1;?>
                <?php foreach($items as $item):?>
                <tr class="odd gradeX">
                    <td class="center"><?php echo $i;?></td>
                    <td class="center"><?php echo $item->item_name?></td>
                    <td class="center"><?php echo $item->item_sku?></td>
                    <td class="center"><?php echo $settings->entity_currency_symbol.' '.$item->item_cost_price.' / '.$item->item_unit;?></td>
                    <td class="center"><?php echo $item->item_min_qty.' '.$item->item_unit;?></td>
                    <td class="center"><?php if($item->item_status == 1){
                                                       echo '<span class="badge badge-success">Active</span>';
                                                    }
                                                    else{
                                                        echo '<span class="badge badge-danger">Deactive</span>';
                                                    }?>
                    </td>
                    <td class="center">

                        <a href="#" class="btn btn-warning btn-sm btn-icon icon-left edit-item" data-target="#edit-item" data-toggle="modal"
                           data-item_id="<?php echo $item->item_id;?>"
                           data-item_name="<?php echo $item->item_name;?>"
                           data-item_sku="<?php echo $item->item_sku;?>"
                           data-item_cost_price="<?php echo $item->item_cost_price;?>"
                           data-item_unit="<?php echo $item->item_unit;?>"
                           data-item_min_qty="<?php echo $item->item_min_qty;?>"
                           data-item_status="<?php echo $item->item_status;?>"
                           ></i>
                            Edit
                        </a>

                        <a href="#" class="btn btn-primary btn-sm btn-icon icon-left print-barcode" data-target="#print-barcode" data-toggle="modal" 
                           data-item_sku2="<?php echo $item->item_sku;?>">
                            <i class="entypo-camera"></i>
                            Label
                        </a>

                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left item-delete" data-target="#item-delete" data-toggle="modal"
                           data-item_id2="<?php echo $item->item_id;?>"
                           ><i class="entypo-cancel"></i>
                            Delete
                        </a>

                    </td>
                </tr>
                <?php $i++;?>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>Item Name</th>
                    <th>Sku</th>
                    <th>Cost Price</th>
                    <th>Min Quantity Alert</th>
                    <th>Status</th>
                    <th>Manage</th>
                </tr>
            </tfoot>
        </table>
    </div>
   </div>

<!-- Modal for Edit Location Starts Here-->
<div class="modal fade" id="edit-item">
   <div class="modal-dialog">
      <div class="modal-content">
        <form role="form" method="POST" action="<?php echo base_url()?>inventoryitems/edit_item" novalidate="novalidate">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit Item</h4>
            <input type="hidden" name="item_id" id="item_id">
         </div>
         <div class="modal-body" id="test">
            <div class="row">
                <div class="col-md-12">
                   <div class="form-group">
                      <label for="field-1" class="control-label">Item Name</label>
                      <input type="text" name="item_name" id="item_name" class="form-control">
                   </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                   <div class="form-group">
                      <label for="field-1" class="control-label">Item Sku</label>
                      <input type="text" name="item_sku" id="item_sku" class="form-control" readonly>
                   </div>
              </div>
            </div>
             <div class="row">
                <div class="col-md-12">
                   <div class="form-group">
                      <label for="field-1" class="control-label">Item Cost Price</label>
                      <input type="text" name="item_cost_price" id="item_cost_price" class="form-control">
                   </div>
              </div>
            </div>
             <div class="row">
                <div class="col-md-12">
                   <div class="form-group">
                      <label for="field-1" class="control-label">Metric Unit</label>

                        <select name="item_unit" id="item_unit" class="form-control" >
                            <!-- Pending Default Selection-->
                            <optgroup label="Choose Status">
                                <?php foreach($units as $unit):?>
                                <option value="<?php echo $unit->unit_short;?>"><?php echo $unit->unit_name;?></option>
                                <?php endforeach;?>
                            </optgroup>
                        </select>
                   </div>
              </div>
            </div>

             <div class="row">
                <div class="col-md-12">
                   <div class="form-group">
                      <label for="field-1" class="control-label">Item Min Quantity</label>
                      <input type="text" name="item_min_qty" id="item_min_qty" class="form-control">
                   </div>
              </div>
            </div>
             <div class="row">
                <div class="col-md-12">
                   <div class="form-group">
                      <label for="field-1" class="control-label">Item Status</label>
                        <select name="item_status" id="item_status" class="form-control">
                            <!-- Pending Default Selection-->
                            <optgroup label="Choose Status">
                                <option value="1">Active</option>
                                <option value="2">Deactive</option>
                            </optgroup>
                        </select>
                   </div>
              </div>
            </div>
         </div>
         <script>
         //alert(document.getElementById('temp_unit').value)
         </script>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" name="submit" class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Save Unit</button>
         </div>
       </form>
      </div>
   </div>
</div>
<!-- Modal for Edit Ends Here-->


<!-- Modal Item Delete Starts Here-->
    <div class="modal fade" id="item-delete">
       <div class="modal-dialog">
          <div class="modal-content">
            <form role="form" method="POST" action="<?php echo base_url()?>inventoryitems/delete_item" novalidate="novalidate" enctype="multipart/form-data">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Item</h4>
             </div>
             <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" id="item_id2" name="item_id">
                        <h4>Are you sure you wanna delete this Item ?</h4>
                        <span class="xx-large" style="color: #bea26a;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                            Warning : The transactions associated to this Item also will be affected..!!
                        </span>
                    </div>
                </div>
             </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Cancel It</button>
                <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete It</button>
             </div>
            </form>
          </div>
       </div>
    </div>
<!-- Modal Item Delete Ends Here-->

<!-- Modal to Update Image Starts Here-->

<div class="modal fade" id="print-barcode">
   <div class="modal-dialog">
      <div class="modal-content">
        <form role="form" method="POST" action="<?php echo base_url().'inventoryitems/print_barcodes/'.$item->item_sku;?>" novalidate="novalidate" enctype="multipart/form-data">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Item Barcode</h4>
         </div>

         <div class="modal-body">
            <div class="row"> 
                <div class="col-md-12">
                    <div class="form-group">
                       <div class="col-sm-6">
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                 <div class="fileinput-new thumbnail" style="width: 150px; height: 100px;" data-trigger="fileinput">
                                     <img src="" alt=""  id="print_barcode">
                                 </div> 
                             </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="col-sm-12">
                                <label class="col-sm-12 control-label">Select Number of Copies</label>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-spinner">
                                    <button type="button" class="btn btn-primary btn-lg">-</button>
                                        <input type="text" name="nos_copies" class="form-control size-1 input-lg" value="1">
                                    <button type="button" class="btn btn-primary btn-lg">+</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>     
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" name='item_edit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Submit</button>
            <!--<a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">Print Barcode<i class="entypo-doc-text"></i></a>-->
         </div>
        </form>
      </div>
   </div>
</div>

<!-- Modal to Update Image Ends Here-->


<!--End of Main Content-->


<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.css">
<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>

<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/fileinput.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-switch.min.js"></script>
<!--<script src="<?php echo base_url()?>assets/js/datatables/datatables.js"></script>-->
<script src="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>

<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

<script>
    $(document).on("click", ".edit-item", function(event){
        $('#item_id').val($(this).data('item_id'));
        $('#item_name').val($(this).data('item_name'));
        $('#item_sku').val($(this).data('item_sku'));
        $('#item_cost_price').val($(this).data('item_cost_price'));
        $('#item_unit').val($(this).data('item_unit'));
        $('#item_min_qty').val($(this).data('item_min_qty'));
        $('#item_status').val($(this).data('item_status'));
    })
    
    $(document).on("click", ".item-delete", function(event){
        
        $('#item_id2').val($(this).data('item_id2'));
    })
    
    $(document).on("click", ".print-barcode", function(event){
        
        //var sku = $('#item_sku2').val($(this).data('item_sku2'))
        var baseurl = '<?php echo base_url()?>'
        var print_barcode = (baseurl + 'assets/images/barcodes/'+ $(this).data('item_sku2')+'.png');
        $('#print_barcode').attr("src", print_barcode);
        
    })
    
</script>
