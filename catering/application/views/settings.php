        <!--Main Content-->
        <h1 class="margin-bottom">Settings</h1>
        <ol class="breadcrumb 2">
            <li>
              <a href="<?php echo base_url()?>dashboard/index">
                <i class="fa-home"></i>Dashboard
              </a>
            </li>
            <li class="active">
              <strong>Settings</strong>
            </li>
        </ol>
        
        <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>settings/update_general">
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
              <div class="panel-heading">
                <div class="panel-title">
                  Basic Information
                </div>
                <div class="panel-options">
                  <a href="#" data-rel="collapse">
                    <i class="entypo-down-open">
                    </i>
                  </a>
                  <a href="#" data-rel="reload">
                    <i class="entypo-arrows-ccw">
                    </i>
                  </a>
                </div>
              </div>
              <div class="panel-body">
                  
                <?php if(isset($_SESSION['general-success'])){?>
                    <center>
                        <div class="form-session-success">
                        <?php echo $_SESSION['general-success'];?>
                        </div>
                    </center>
                    <br>  
                <?php }?>

                <?php if(isset($_SESSION['genral-error'])){?>
                    <center>
                        <!--<div class="badge badge-danger">-->
                        <div class="form-session-errors">
                        <?php echo ($_SESSION['general-error']);?>
                        </div>
                    </center>
                    <br>  
                <?php }?>
                
                  
                <div class="form-group">
                  <label for="field-1" class="col-sm-3 control-label">Entity Name</label>
                  <div class="col-sm-6">
                    <input type="text" name="entity" value="<?php echo $settings->entity;?>" placeholder="Bayfront Pte. Ltd."  class="form-control" id="field-1" data-validate="required">
                    <input type="hidden" name="settings_id" value="<?php echo $settings->settings_id;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="field-2" class="col-sm-3 control-label">Address</label>
                  <div class="col-sm-6">
                    <input type="text" name="entity_address" value="<?php echo $settings->entity_address;?>" placeholder="Satay By the Bay, Singapore"  class="form-control" id="field-2" data-validate="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="field-3" class="col-sm-3 control-label">Postal</label>
                  <div class="col-sm-6">
                    <input type="text" name="entity_postal" value="<?php echo $settings->entity_postal;?>" placeholder="S(XXXXXX)"  class="form-control" id="field-3" data-validate="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="field-4" class="col-sm-3 control-label">Email address</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" value="<?php echo $settings->entity_email;?>" placeholder="admin@example.com" name="entity_email" id="field-4" data-validate="required,email">
                    <span class="description">Invoice will be send out with this Email Address Header.
                    </span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="field-5" class="col-sm-3 control-label">Contact</label>
                  <div class="col-sm-6">
                    <input type="text" name="entity_contact" value="<?php echo $settings->entity_contact;?>" placeholder="(65)-XXXX XXXX"  class="form-control" id="field-5" data-validate="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="field-6" class="col-sm-3 control-label">Website</label>
                  <div class="col-sm-6">
                    <input type="text" name="entity_web" value="<?php echo $settings->entity_web;?>" placeholder="http://example.com"  class="form-control" id="field-6" data-validate="required,url">
                  </div>
                </div>
                <div class="form-group default-padding" style="text-align: -webkit-center;">
                    <button type="submit" name="settings-general" class="btn btn-success">Save Changes</button>
                    <button type="reset" class="btn">Reset Previous</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        </form>
        
        <div class="row">
         <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>settings/update_application">
          <div class="col-md-6">
            <div class="panel panel-primary" data-collapsed="0">
              <div class="panel-heading">
                <div class="panel-title">
                  Application Settings
                </div>
                <div class="panel-options">
                  <a href="#" data-rel="collapse">
                    <i class="entypo-down-open">
                    </i>
                  </a>
                  <a href="#" data-rel="reload">
                    <i class="entypo-arrows-ccw">
                    </i>
                  </a>
                </div>
              </div>
              <div class="panel-body">
                  
                <?php if(isset($_SESSION['application-success'])){?>
                    <center>
                        <div class="form-session-success">
                        <?php echo $_SESSION['application-success'];?>
                        </div>
                    </center>
                <?php }?>

                <?php if(isset($_SESSION['application-error'])){?>
                    <center>
                        <!--<div class="badge badge-danger">-->
                        <div class="form-session-errors">
                        <?php echo ($_SESSION['application-error']);?>
                        </div>
                    </center>
                <?php }?>
                <br>
                  
                <div class="form-group">
                    <label class="col-sm-5 control-label">Timezone</label>
                    <div class="col-sm-5">
                        <select name="entity_timezone" class="select2" data-allow-clear="true" data-placeholder="Select your timezone">
                            <option></option>
                            <optgroup label="Asia">
                                <option value="Asia/Singapore" <?php if($settings->entity_timezone == 'Asia/Singapore'){echo "selected='selected'";}?>>Asia/Singapore</option>
                                <option value="Asia/Beijing" <?php if($settings->entity_timezone == 'Asia/Beijing'){echo "selected='selected'";}?>>Asia/Beijing</option>
                                <option value="Asia/Karachi" <?php if($settings->entity_timezone == 'Asia/Karachi'){echo "selected='selected'";}?>>Asia/Karachi</option>
                            </optgroup>
                        </select>
                        <input type="hidden" name="settings_id" value="<?php echo $settings->settings_id;?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Currency</label>
                    <div class="col-sm-5">
                        <select name="entity_currency" class="select2" data-allow-clear="true" data-placeholder="Select Currency">
                            <option></option>
                            <optgroup label="Currency">
                                <option value="INR" <?php if($settings->entity_currency == 'INR'){echo "selected='selected'";}?>>INR</option>
                                <option value="AED" <?php if($settings->entity_currency == 'AED'){echo "selected='selected'";}?>>AED</option>
                                <option value="SGD" <?php if($settings->entity_currency == 'SGD'){echo "selected='selected'";}?>>SGD</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-5" class="col-sm-5 control-label">Currency Symbol</label>
                    <div class="col-sm-5">
                        <input type="text" name="entity_currency_symbol" value="<?php echo $settings->entity_currency_symbol?>" id="field-5" class="form-control" data-validate="required">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">GST</label>
                    <div class="col-sm-5">
                        <select class="select2" data-allow-clear="true" name='entity_gst'>
                            <option value='0' <?php if($settings->entity_gst == 0){echo "selected= 'selected'";}?>>No</option>
                            <option value='1' <?php if($settings->entity_gst == 1){echo "selected= 'selected'";}?>>Yes</option>
                        </select>
                    </div>
                </div>
                <div class="form-group default-padding" style="text-align:center">
                    <button type="submit" name='settings-application' class="btn btn-success">Save Changes</button>
                    <button type="reset" class="btn">Reset</button>
                </div>
                </div>
              </div>
            </div>
         </form>
            
         <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>settings/upload_logo" novalidate="novalidate" enctype="multipart/form-data">
          <div class="col-md-6">
            <div class="panel panel-primary" data-collapsed="0">
              <div class="panel-heading">
                <div class="panel-title">
                  Company Logo
                </div>
                <div class="panel-options">
                  <a href="#" data-rel="collapse">
                    <i class="entypo-down-open">
                    </i>
                  </a>
                  <a href="#" data-rel="reload">
                    <i class="entypo-arrows-ccw">
                    </i>
                  </a>
                </div>
              </div>
              <div class="panel-body">
                  
                <?php if(isset($_SESSION['logo-success'])){?>
                    <center>
                        <div class="form-session-success">
                        <?php echo $_SESSION['logo-success'];?>
                        </div>
                    </center>
                <?php }?>

                <?php if(isset($_SESSION['logo-error'])){?>
                    <center>
                        <!--<div class="badge badge-danger">-->
                        <div class="form-session-errors">
                        <?php print_r ($_SESSION['logo-error']['errors']);?>
                        </div>
                    </center>
                <?php }?>
                <br>  
                  
                <label for="field-3" class="col-sm-5 control-label">Upload Logo</label>
                <div class="col-sm-5">
                   <div class="fileinput fileinput-new" data-provides="fileinput">
                       <input type="hidden">
                       <div class="fileinput-new thumbnail" style="width: 150px; height: 125px;" data-trigger="fileinput">
                           <img src="<?php echo base_url().'assets/images/app_settings/entity/'.$settings->entity_logo;?>" alt="">
                       </div>
                       <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                       <div><br>
                           <span class="btn btn-white btn-file">
                               <span class="fileinput-new">Select image</span>
                               <span class="fileinput-exists">Change</span>
                               <input type="file" name="logo" size="20">
                           </span>
                           <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                       </div>
                   </div>   
                   <div class="form-group default-padding col-sm-8" style="text-align:center">
                      <button type="submit" name='submit' class="btn btn-success">Save Changes</button>
                      <button type="reset" class="btn">Reset</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
         </form>
        </div>
        
        <form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="<?php echo base_url()?>settings/update_invoice_terms">
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
              <div class="panel-heading">
                <div class="panel-title">
                  Invoice Settings
                </div>
                <div class="panel-options">
                  <a href="#" data-rel="collapse">
                    <i class="entypo-down-open">
                    </i>
                  </a>
                  <a href="#" data-rel="reload">
                    <i class="entypo-arrows-ccw">
                    </i>
                  </a>
                </div>
              </div>
              <div class="panel-body">
                  
                  <?php if(isset($_SESSION['invoice_terms-success'])){?>
                    <center>
                        <div class="form-session-success">
                        <?php echo $_SESSION['invoice_terms-success'];?>
                        </div>
                    </center>
                <?php }?>

                <?php if(isset($_SESSION['invoice_terms-error'])){?>
                    <center>
                        <!--<div class="badge badge-danger">-->
                        <div class="form-session-errors">
                        <?php echo ($_SESSION['invoice_terms-error']);?>
                        </div>
                    </center>
                <?php }?>
                <br>
                
                <div class="form-group">
                      <label for="field-1" class="col-sm-3 control-label">Invoice Prefix</label>
                      <div class="col-sm-5">
                          <input type="text" class="form-control" name="entity_invoice_prefix" id="field-1" value="<?php echo $settings->entity_invoice_prefix;?>" data-validate="required">
                          <input type="hidden" name="settings_id" value="<?php echo $settings->settings_id;?>">
                      </div>
                </div>
                <div class="form-group">
                  <label for="field-1" class="col-sm-3 control-label">Invoice Terms & Conditions</label>
                  <div class="col-sm-5">
                      <textarea class="form-control ckeditor" name="entity_invoice_terms" id="field-1" data-validate="required"><?php echo $settings->entity_invoice_terms;?></textarea>
                  </div>
                </div>  
                <div class="form-group default-padding" style="text-align:center">
                    <button type="submit" name='settings-invoice' class="btn btn-success">Save Changes</button>
                    <button type="reset" class="btn">Reset</button>
                </div>
              </div>
            </div>
          </div>
        </div>
       </form>
        
        
<!--End of Main Content-->    

    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/wysihtml5/bootstrap-wysihtml5.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/select2/select2-bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/select2/select2.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.css">

    <!-- Imported styles on this page -->
    <!--<link rel="stylesheet" href="<?php echo base_url();?>assets/js/codemirror/lib/codemirror.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/uikit/css/uikit.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/uikit/addons/css/markdownarea.css">-->

    <!-- Bottom scripts (common) -->
    <script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url()?>assets/js/joinable.js"></script>
    <script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
    <script src="<?php echo base_url()?>assets/js/neon-api.js"></script>
    
    <!-- Imported scripts on this page -->
    <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/select2/select2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/fileinput.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>assets/js/dropzone/dropzone.js"></script>
    
    <!-- Imported scripts on this page -->
    <script src="<?php echo base_url();?>assets/js/wysihtml5/bootstrap-wysihtml5.js"></script>
    <script src="<?php echo base_url();?>assets/js/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url();?>assets/js/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/js/uikit/js/uikit.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/codemirror/lib/codemirror.js"></script>
    <script src="<?php echo base_url();?>assets/js/marked.js"></script>
    <script src="<?php echo base_url();?>assets/js/uikit/addons/js/markdownarea.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/codemirror/mode/markdown/markdown.js"></script>
    <script src="<?php echo base_url();?>assets/js/codemirror/addon/mode/overlay.js"></script>
    <script src="<?php echo base_url();?>assets/js/codemirror/mode/xml/xml.js"></script>
    <script src="<?php echo base_url();?>assets/js/codemirror/mode/gfm/gfm.js"></script>
    <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>-->
    


    <!-- JavaScripts initializations and stuff -->
    <script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>


    <!-- Demo Settings -->
    <script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>