<h1 class="margin-bottom">Manage Supplies</h1>
<ol class="breadcrumb 2">
    <li>
        <a href="index.html">
            <i class="fa-home"></i>Home
        </a>
    </li>
    <li class="active">
        <strong>Manage Supplies</strong>
    </li>
</ol>
<br>

        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-primary" data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Add new Supply
                        </div>
                    </div>
                <form role="form" method="post" class="form-horizontal form-groups-bordered validate" style="margin: 10px;" action="<?php echo base_url();?>supplies/add_supply" novalidate="novalidate">    
                    <div class="panel-body">
                        <?php if(isset($_SESSION['success-supplies-add'])){?>
                            <center>
                                <div class="badge badge-success">
                                <?php echo $_SESSION['success-supplies-add'];?>
                                </div>
                            </center>
                        <?php }?>

                        <?php if(isset($_SESSION['error-supplies-add'])){?>
                            <center>
                                <div class="badge badge-danger">
                                <?php echo $_SESSION['error-supplies-add'];?>
                                </div>
                            </center>
                        <?php }?>
                        <br>
                        <div class="row">		
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Select Supplier</label>
                                    <select name="supplier_id" class="selectboxit">
                                        <!-- Pending Default Selection-->
                                        <optgroup label="Choose Supplier">
                                            <?php foreach($suppliers as $supplier):?>
                                            <option value="<?php echo $supplier->supplier_id;?>"><?php echo $supplier->supplier_name;?></option>
                                            <?php endforeach;?>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Select Item</label>
                                    <select name="item_id" class="selectboxit">
                                        <!-- Pending Default Selection-->
                                        <optgroup label="Choose Item">
                                            <?php foreach($items as $item):?>
                                            <option value="<?php echo $item->item_id;?>"><?php echo $item->item_name;?></option>
                                            <?php endforeach;?>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Date</label>
                                    <div class="input-group">
                                        <input type="text" name="supply_date" class="form-control datepicker" placeholder="Pick Supply Date" data-format="D, dd MM yyyy">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="entypo-calendar"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
			</div>
					
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Quantity Ordered</label>
                                    <input class="form-control" name="item_qty_order" data-validate="required" placeholder="Eg: 10" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Quantity Supplied</label>
                                    <input class="form-control" name="item_qty_supplied" placeholder="Eg: 10" data-validate="required"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Storage Location</label>
                                    <select name="location_id" class="selectboxit">
                                        <!-- Pending Default Selection-->
                                        <optgroup label="Choose Item">
                                            <?php foreach($locations as $locs):?>
                                            <option value="<?php echo $locs->location_id;?>"><?php echo $locs->location_name.'-'.$locs->location_stack;?></option>
                                            <?php endforeach;?>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Expiry Date (if require)</label>
                                    <div class="input-group">
                                        <input type="text" name="item_expiry_date" class="form-control datepicker" placeholder="Item's Expiry Date (optional)" data-format="D, dd MM yyyy">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="entypo-calendar"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>		
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label">Remarks</label>
                                <textarea class="form-control" name="supply_remarks" placeholder="Remarks"></textarea>
                            </div>
                        </div><br/>
                        <div class="form-group default-padding" style="text-align:center">
                            <button type="submit" name="submit" class="btn btn-success" style="background: #bea26a; border: 1px solid #bea26a;">Add Supplies</button>
                            <button type="reset" class="btn">Reset</button>
                        </div>
                    </div>
                  </form>
                </div>
            </div>
            <div class="row">		
                <div class="col-md-4">
                    <div class="tile-block tile-green" style="background: #31271e;">
                        <div class="tile-header">
                            <a href="#">
                                Supplies
                                <span>Overview of Item Supplies</span>
                            </a>
                        </div>
                        <div class="tile-content">
                            <ul class="todo-list">
                                <li>
                                    <div class="checkbox checkbox-replace color-white neon-cb-replacement">
                                        <div class="tile-stats tile-gray">
                                            <div class="icon"><i class="entypo-calendar"></i></div>
                                            <div class="num"><?php echo 'Expenditure: '.$settings->entity_currency_symbol.''.number_format(round($lmonth_cost,1),2);?></div>
                                            <h3>This Month</h3>
                                            <p>Number of Supplies : <?php echo $lmonth_supplies;?></p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-replace color-white neon-cb-replacement">
                                        <div class="tile-stats tile-gray">
                                            <div class="icon"><i class="entypo-chart-area"></i></div>
                                            <div class="num"><?php echo 'Expenditure: '.$settings->entity_currency_symbol.''.number_format(round($full_cost,1),2);?></div>
                                            <h3>So Far</h3>
                                            <p>Number of Supplies : <?php echo $full_supplies;?></p>
                                        </div>
                                    </div>
                                </li>
                            </ul>		
                        </div>
                    </div>
                </div>			
            </div>
        </div>
    
        <div class="row">

            <div class="col-sm-12">
        
        <!--<script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
                var $table1 = jQuery( '#item-table' );

                // Initialize DataTable
                $table1.DataTable( {
                        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "bStateSave": true
                });

                // Initalize Select Dropdown after DataTables is created
                $table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
                        minimumResultsForSearch: -1
                });
        } );
        </script>-->
        
        
        <?php if(isset($_SESSION['success-supplies-edit'])){?>
            <center>
                <div class="form-session-success">
                <?php echo $_SESSION['success-supplies-edit'];?>
                </div>
            </center>
          <br>
        <?php }?>

        <?php if(isset($_SESSION['error-supplies-edit'])){?>
            <center>
                <div class="form-session-errors">
                <?php echo ($_SESSION['error-supplies-edit']);?>
                </div>
            </center>
          <br>
        <?php }?>
      
        <table class="table table-bordered datatable" id="item-table">
            <thead>
                <tr>
                    <th>Supplier Name</th>
                    <th>Item</th>
                    <th>Quantity Ordered</th>
                    <th>Quantity Supplied</th>
                    <th>Supply Date</th>
                    <th>Total Cost</th>
                    <th>Manage</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($supplies as $supply):?>
                <tr class="odd gradeX">
                    <td class="center"><?php echo '<div class="label label-secondary"><strong>'.$supply->supplier_name.'</strong></div>';?></td>
                    <td class="center"><?php echo $supply->item_name;?></td>
                    <td class="center"><?php echo $supply->item_qty_order;?></td>
                    <td class="center"><?php echo $supply->item_qty_supplied;?></td>
                    <td class="center"><?php echo date('D, d-M-Y', strtotime ($supply->supply_date));?></td>
                    <td class="center"><?php echo $settings->entity_currency_symbol.' '.number_format(round($supply->supply_total,1), 2)?></td>
                    <td class="center">
                        
                       
                        <a href="<?php echo base_url().'supplies/view_supply/'.$supply->supplies_id;?>" class="btn btn-info btn-sm btn-icon icon-left edit-supplier"><i class="entypo-docs"></i>
                            View Supply Details
                        </a>
                        
                        
                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left delete-supply" data-target="#delete-supply" data-toggle="modal" data-supplies_id='<?php echo $supply->supplies_id;?>'>
                            <i class="entypo-cancel"></i>
                            Delete Supply
                        </a>
                        
                        <!-- Modal for Delete Order Starts Here-->
                            <div class="modal fade" id="delete-supply">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                    <form role="form" method="POST" action="<?php echo base_url()?>supplies/delete_supply" novalidate="novalidate">
                                     <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" style="text-align: -webkit-left;">Delete Supplier</h4>
                                     </div>
                                     <div class="modal-body">
                                        <div class="row"> 
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                <input type="hidden" name="supplies_id" id="supplies_id">
                                                <span class="line xlarge" style="text-align: -webkit-left;">Are you sure you wanna Delete this Supply ?</span><br>
                                                <span class="xx-large" style="color: #bea26a; text-align: -webkit-left;"><i class="glyphicon glyphicon-alert" style="color: red;"></i>
                                                    Warning : Once Deleted, the items associated in Inventory listing also will be affected !!
                                                </span>
                                            </div>
                                            </div>
                                        </div>
                                     </div>     
                                     <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Sorry, Close It</button>
                                        <button type="submit" name='submit' class="btn btn-info" style="background: #bea26a; border: 1px solid #bea26a;">Sure, Just Delete this Order</button>
                                     </div>
                                    </form>
                                  </div>
                               </div>
                            </div>
                            <!-- Modal for Delete Order Ends Here-->
                        
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                   <th>Supplier Name</th>
                    <th>Item</th>
                    <th>Quatity Ordered</th>
                    <th>Quantity Supplied</th>
                    <th>Supply Date</th>
                    <th>Total Cost</th>
                    <th>Manage</th>
                </tr>
            </tfoot>
        </table>
    </div>
        </div>
        



<style>
    .form-horizontal .form-group {
        margin-left: auto;
        margin-right:auto;
}
</style>
<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/datatables.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.css">


<!-- Bottom scripts (common) -->
<script src="<?php echo base_url();?>assets/js/gsap/TweenMax.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/js/joinable.js"></script>
<script src="<?php echo base_url();?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url();?>assets/js/neon-api.js"></script>


<!-- Imported scripts on this page -->
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<!--<script src="<?php echo base_url()?>assets/js/datatables/datatables.js"></script>-->
<script src="<?php echo base_url();?>assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url();?>assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="<?php echo base_url();?>assets/js/neon-demo.js"></script>

<script type="text/javascript">
$(document).on("click", ".delete-supply", function(event){
    $('#supplies_id').val($(this).data('supplies_id'))
})

</script>