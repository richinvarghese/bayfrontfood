
    <div class="login-container">
        <div class="login-header login-caret">	
          <div class="login-content">
            <a href="index.html" class="logo">
              <img src="<?php echo base_url().'assets/images/app_settings/entity/'.$settings->entity_logo;?>" width="120" alt="" />
            </a>
            <p class="description">Dear user, log in to access the admin area!
            </p>
            <!-- progress bar indicator -->
            <div class="login-progressbar-indicator">
              <h3>43%
              </h3>
              <span>logging in...
              </span>
            </div>
          </div>
        </div>
        <div class="login-progressbar">
          <div>
          </div>
        </div>
        <div class="login-form">
          <div class="login-content">
            <div class="form-login-error">
              <h3>Invalid login</h3>
              <p>Either <strong>Username </strong>or <strong> Password</strong> is incorrect. Please try again.</p>
            </div>
            <form method="post" role="form" id="form_login">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="entypo-user">
                    </i>
                  </div>
                  <input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" value="admin"/>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="entypo-key">
                    </i>
                  </div>
                  <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" value="123456789"/>
                </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-login">
                  <i class="entypo-login">
                  </i>
                  Login In
                </button>
              </div>
            </form>
            <div class="login-bottom-links">
              <a href="extra-forgot-password.html" class="link">Forgot your password?
              </a>
            </div>
          </div>
        </div>
      </div>

    <!-- Bottom scripts (common) -->
    <script src="<?php echo base_url()?>assets/js/gsap/TweenMax.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url()?>assets/js/joinable.js"></script>
    <script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
    <script src="<?php echo base_url()?>assets/js/neon-api.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/neon-login.js"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>


    <!-- Demo Settings -->
    <script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

</body>
</html>