-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2019 at 05:15 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bayfront`
--

-- --------------------------------------------------------

--
-- Table structure for table `addons`
--

CREATE TABLE `addons` (
  `addon_id` int(11) NOT NULL,
  `addon_eng` varchar(50) NOT NULL,
  `addon_chn` varchar(50) CHARACTER SET utf16 NOT NULL,
  `addon_price` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addons`
--

INSERT INTO `addons` (`addon_id`, `addon_eng`, `addon_chn`, `addon_price`) VALUES
(1, 'Jumbo Prawns', '项目名称', '9.20'),
(4, 'Lobster', '项目名', '20.50');

-- --------------------------------------------------------

--
-- Table structure for table `buffet`
--

CREATE TABLE `buffet` (
  `buffet_id` int(11) NOT NULL,
  `master_cat_id` int(11) NOT NULL,
  `buffet_name` varchar(55) NOT NULL,
  `buffet_min_pax` int(11) NOT NULL,
  `buffet_price` varchar(15) NOT NULL,
  `buffet_course_nos` int(11) NOT NULL,
  `buffet_banner` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buffet`
--

INSERT INTO `buffet` (`buffet_id`, `master_cat_id`, `buffet_name`, `buffet_min_pax`, `buffet_price`, `buffet_course_nos`, `buffet_banner`) VALUES
(1, 10, '8+1 Courses', 33, '10.80', 4, '8+1_Courses.jpg'),
(2, 10, '9+1 Courses', 35, '11.80', 5, '9+1_Courses.jpg'),
(3, 9, '10+1 Courses', 40, '12.80', 6, '10+1_Courses.jpg'),
(4, 9, '11+1 Courses', 55, '15.50', 3, '11+1_Courses.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(55) NOT NULL,
  `category_description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `category_description`) VALUES
(4, 'Rice', 'List of Rices'),
(5, 'Noodles', 'All noodles Types'),
(7, 'Desserts', 'All Desserts'),
(9, 'Drinks', 'Hot and Cold Drinks'),
(10, 'Main Course', 'Main Course Dishes only'),
(11, 'Starters', 'Starter Items');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `course_id` int(11) NOT NULL,
  `course_name` varchar(55) NOT NULL,
  `buffet_id` int(11) NOT NULL,
  `sort_id` int(11) NOT NULL,
  `course_allowed_selection` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`course_id`, `course_name`, `buffet_id`, `sort_id`, `course_allowed_selection`) VALUES
(73, 'Starters', 1, 1, 1),
(74, 'Main Course', 1, 2, 2),
(79, 'Drinks', 1, 3, 1),
(80, 'Starters', 2, 1, 1),
(81, 'Main Course', 2, 2, 3),
(82, 'Drinks', 2, 4, 2),
(83, 'Starters', 4, 1, 2),
(84, 'Main Course', 4, 2, 3),
(85, 'Drinks', 4, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `courses_item`
--

CREATE TABLE `courses_item` (
  `courses_item_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses_item`
--

INSERT INTO `courses_item` (`courses_item_id`, `course_id`, `item_id`) VALUES
(100, 73, 17),
(101, 73, 18),
(102, 74, 2),
(103, 74, 4),
(119, 79, 13),
(120, 79, 16),
(123, 81, 2),
(124, 81, 3),
(126, 81, 4),
(128, 82, 13),
(129, 82, 16),
(130, 80, 18),
(131, 80, 17),
(133, 74, 3),
(134, 83, 2),
(135, 83, 3),
(136, 83, 14),
(137, 84, 4),
(138, 84, 15),
(139, 84, 5),
(140, 84, 6),
(141, 85, 13),
(142, 85, 16);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `inventory_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_quantity` varchar(10) NOT NULL,
  `location_id` int(11) NOT NULL,
  `item_total_worth` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`inventory_id`, `item_id`, `item_quantity`, `location_id`, `item_total_worth`) VALUES
(36, 30, '149', 4, '178.8'),
(37, 31, '15', 5, '82.5'),
(38, 32, '15', 1, '52.5'),
(39, 33, '0', 0, '0'),
(40, 34, '1', 1, '2.8'),
(41, 29, '230', 3, '460'),
(42, 32, '10', 5, '35'),
(43, 30, '200', 3, '240');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_draws`
--

CREATE TABLE `inventory_draws` (
  `drawn_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `item_quantity` varchar(15) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_fullname` varchar(55) NOT NULL,
  `drawn_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory_draws`
--

INSERT INTO `inventory_draws` (`drawn_id`, `item_id`, `location_id`, `item_quantity`, `order_id`, `user_id`, `user_fullname`, `drawn_date`) VALUES
(18, 32, 1, '3', 51, 1, 'Mok Nicholas', '2019-08-26'),
(19, 32, 5, '2', 51, 1, 'Mok Nicholas', '2019-08-26'),
(20, 34, 1, '24', 51, 2, 'Employee', '2019-08-31');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_items`
--

CREATE TABLE `inventory_items` (
  `item_id` int(11) NOT NULL,
  `item_sku` varchar(15) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `item_cost_price` varchar(10) NOT NULL,
  `item_unit` varchar(10) NOT NULL,
  `item_min_qty` int(11) NOT NULL,
  `item_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory_items`
--

INSERT INTO `inventory_items` (`item_id`, `item_sku`, `item_name`, `item_cost_price`, `item_unit`, `item_min_qty`, `item_status`) VALUES
(29, 'IT002019', 'Disposable Spoons', '2.00', 'Pax', 20, 1),
(30, 'IT012019', 'Napkins', '1.20', 'Pax', 20, 1),
(31, 'IT032019', 'Prawn', '5.50', 'Kg', 3, 1),
(32, 'IT042019', 'Kampong Chicken', '3.50', 'Kg', 5, 1),
(33, 'IT062019', 'Beef (Quality 1st)', '4.00', 'Kg', 3, 1),
(34, 'IT072019', 'Beef (Quality 2nd)', '2.80', 'Kg', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_location`
--

CREATE TABLE `inventory_location` (
  `location_id` int(11) NOT NULL,
  `location_name` varchar(50) NOT NULL,
  `location_stack` varchar(50) NOT NULL,
  `location_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory_location`
--

INSERT INTO `inventory_location` (`location_id`, `location_name`, `location_stack`, `location_status`) VALUES
(1, 'Refrigerator', 'Fridge A', 1),
(3, 'Shelf A', 'Stack A', 1),
(4, 'Wardrobe', 'Wardrobe Section A', 1),
(5, 'Refrigerator', 'Fridge B', 1);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `item_eng` varchar(40) NOT NULL,
  `item_chn` varchar(55) CHARACTER SET utf8 NOT NULL,
  `item_price` varchar(10) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_image` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item_eng`, `item_chn`, `item_price`, `category_id`, `item_image`) VALUES
(2, 'Yang Chow Fried Rice', '项目名称', '4.50', 4, 'Yang_Chao_Fried_Rice.png'),
(3, 'Chicken Fried Rice', '项目名称', '5.50', 4, 'Chicken_Fried_Rice.png'),
(4, 'Mee Goreng', '项目名称', '3.50', 5, 'Mee_goreng.png'),
(5, 'Sauteed Fish Fillet', '炒鱼片', '8.90', 10, 'Sauteed_Fish_Fillet.png'),
(6, 'Sweet & Sour Chicken', ' 酸甜鸡', '6.90', 10, 'Sweet_Sour_Chicken.png'),
(13, 'Lime Cordial', '青柠汁', '2.50', 9, 'Lime_Cordial.jpg'),
(14, 'Steamed Fragrant White Rice', '蒸香白米饭', '1.50', 4, 'Steamed_Fragrant_White_Rice.png'),
(15, 'Xing Zhao Bee Hoon', '蒸香白米饭', '3.50', 5, 'Xing_Zhao_Bee_Hoon.png'),
(16, 'Orange Juice', '橙汁', '2.0', 9, 'Orange_Juice.jpg'),
(17, 'Crispy Chicken Wings', ' 脆皮鸡翅', '4.50', 11, 'Crispy_Chicken_Wings.jpg'),
(18, 'Cheesy Sausage Balls', '俗气的香肠球', '4.50', 11, 'Cheesy_Sausage_Balls.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `master_category`
--

CREATE TABLE `master_category` (
  `master_cat_id` int(11) NOT NULL,
  `master_cat_name` varchar(50) NOT NULL,
  `master_cat_descrip` varchar(700) NOT NULL,
  `master_cat_image` varchar(25) NOT NULL,
  `master_cat_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_category`
--

INSERT INTO `master_category` (`master_cat_id`, `master_cat_name`, `master_cat_descrip`, `master_cat_image`, `master_cat_status`) VALUES
(9, 'Mini Buffet', 'Mini Buffet<br>', 'Mini_Buffet.png', 1),
(10, 'Standard Buffet', 'Standard Buffet<br>', 'Standard_Buffet.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notification_id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `path` varchar(150) NOT NULL,
  `created_on` datetime NOT NULL,
  `post_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notification_id`, `description`, `path`, `created_on`, `post_id`, `status`) VALUES
(70, 'New Supplies Has been Added By Mok Nicholas', 'supplies/view_supply', '2019-07-09 01:05:47', 77, 1),
(71, 'New Supplies Has been Added By Mok Nicholas', 'supplies/view_supply', '2019-07-09 01:07:41', 78, 1),
(72, '20 Items supplied on Sat, 06 Jul is about to expire in 1 days', 'stocks/index', '2019-07-09 01:07:47', 0, 1),
(73, 'New Supplies Has been Added By Mok Nicholas', 'supplies/view_supply', '2019-07-09 13:56:10', 79, 1),
(74, 'New Supplies Has been Added By Vimal Karthik', 'supplies/view_supply', '2019-07-09 13:59:20', 80, 1),
(75, 'Stock Drawn Request has been Placed', 'manage_stocks/index', '2019-07-09 15:26:56', 0, 1),
(76, 'New Supplies Has been Added By Mok Nicholas', 'supplies/view_supply', '2019-07-11 16:14:05', 81, 1),
(77, 'New Supplies Has been Added By Mok Nicholas', 'supplies/view_supply', '2019-07-11 21:10:58', 82, 1),
(78, 'A new New Order Has been Placed by Mayuri Mohandas', 'manageorders/edit_order', '2019-08-23 21:03:20', 63, 1),
(79, 'Stock Drawn Request has been Placed', 'manage_stocks/index', '2019-08-26 09:53:28', 0, 1),
(80, 'Stock Drawn Request has been Placed', 'manage_stocks/index', '2019-08-26 09:58:33', 0, 1),
(81, 'New Supplies Has been Added By Mok Nicholas', 'supplies/view_supply', '2019-08-26 16:24:42', 83, 1),
(82, 'New Supplies Has been Added By Mok Nicholas', 'supplies/view_supply', '2019-08-26 16:30:44', 84, 1),
(83, '15 Items supplied on Tue, 27 Aug is about to expire in 2 days', 'stocks/index', '2019-08-28 18:36:20', 0, 1),
(84, '15 Items supplied on Tue, 27 Aug is about to expire in 2 days', 'stocks/index', '2019-08-28 18:37:11', 0, 1),
(85, '15 Items supplied on Tue, 27 Aug is about to expire in 1 days', 'stocks/index', '2019-08-29 16:44:22', 0, 1),
(86, '15 Items supplied on Tue, 27 Aug is about to expire in 1 days', 'stocks/index', '2019-08-29 22:41:15', 0, 1),
(87, '15 Items supplied on Tue, 27 Aug is about to expire in 1 days', 'stocks/index', '2019-08-31 10:39:49', 0, 1),
(88, 'Stock Drawn Request has been Placed', 'manage_stocks/index', '2019-08-31 10:42:07', 0, 1),
(89, '15 Items supplied on Tue, 27 Aug is about to expire in 1 days', 'stocks/index', '2019-08-31 17:32:12', 0, 1),
(90, 'Napkins', 'stocks/index', '2019-08-31 11:41:59', 0, 1),
(91, 'Prawn', 'stocks/index', '2019-08-31 11:41:59', 0, 1),
(92, 'Kampong Chicken', 'stocks/index', '2019-08-31 11:41:59', 0, 1),
(93, 'Beef (Quality 1st)', 'stocks/index', '2019-08-31 11:41:59', 0, 1),
(94, 'Beef (Quality 2nd)', 'stocks/index', '2019-08-31 11:41:59', 0, 1),
(95, 'Tuna', 'stocks/index', '2019-08-31 11:41:59', 0, 1),
(96, 'Napkins is running Low Stock', 'stocks/index', '2019-08-31 11:42:45', 0, 1),
(97, 'Prawn is running Low Stock', 'stocks/index', '2019-08-31 11:42:45', 0, 1),
(98, 'Kampong Chicken is running Low Stock', 'stocks/index', '2019-08-31 11:42:45', 0, 1),
(99, 'Beef (Quality 1st) is running Low Stock', 'stocks/index', '2019-08-31 11:42:45', 0, 1),
(100, 'Beef (Quality 2nd) is running Low Stock', 'stocks/index', '2019-08-31 11:42:45', 0, 1),
(101, 'Tuna is running Low Stock', 'stocks/index', '2019-08-31 11:42:46', 0, 1),
(102, 'Napkins is running Low Stock', 'stocks/index', '2019-08-31 11:42:50', 0, 1),
(103, 'Prawn is running Low Stock', 'stocks/index', '2019-08-31 11:42:50', 0, 1),
(104, 'Kampong Chicken is running Low Stock', 'stocks/index', '2019-08-31 11:42:50', 0, 1),
(105, 'Beef (Quality 1st) is running Low Stock', 'stocks/index', '2019-08-31 11:42:50', 0, 1),
(106, 'Beef (Quality 2nd) is running Low Stock', 'stocks/index', '2019-08-31 11:42:50', 0, 1),
(107, 'Tuna is running Low Stock', 'stocks/index', '2019-08-31 11:42:50', 0, 1),
(108, 'Napkins is running Low Stock', 'stocks/index', '2019-08-31 11:42:53', 0, 1),
(109, 'Prawn is running Low Stock', 'stocks/index', '2019-08-31 11:42:53', 0, 1),
(110, 'Kampong Chicken is running Low Stock', 'stocks/index', '2019-08-31 11:42:53', 0, 1),
(111, 'Beef (Quality 1st) is running Low Stock', 'stocks/index', '2019-08-31 11:42:53', 0, 1),
(112, 'Beef (Quality 2nd) is running Low Stock', 'stocks/index', '2019-08-31 11:42:53', 0, 1),
(113, 'Tuna is running Low Stock', 'stocks/index', '2019-08-31 11:42:53', 0, 1),
(114, '15 Items supplied on Tue, 27 Aug is about to expire in 2 days', 'stocks/index', '2019-09-01 00:02:28', 0, 1),
(115, '15 Items supplied on Tue, 27 Aug is about to expire in 2 days', 'stocks/index', '2019-09-01 00:03:53', 0, 1),
(116, 'A new New Order Has been Placed by Vishnu Karthik', 'manageorders/edit_order', '2019-11-13 13:21:15', 64, 1),
(117, 'A new New Order Has been Placed by Krishnan', 'manageorders/edit_order', '2019-11-15 00:03:32', 65, 1),
(118, 'A new New Order Has been Placed by Chitra', 'manageorders/edit_order', '2019-11-27 16:33:04', 66, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `order_buffet` varchar(55) NOT NULL,
  `order_date` date NOT NULL,
  `order_buffet_price` varchar(20) NOT NULL,
  `dining_date` date NOT NULL,
  `dining_time` time NOT NULL,
  `order_pax` int(11) NOT NULL,
  `address_block` varchar(25) NOT NULL,
  `address_street` varchar(100) NOT NULL,
  `address_postal` varchar(55) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_contact` varchar(55) NOT NULL,
  `customer_email` varchar(75) NOT NULL,
  `order_notes` longtext NOT NULL,
  `order_stotal` varchar(15) NOT NULL,
  `order_delivery` int(55) NOT NULL,
  `order_gst` varchar(15) NOT NULL,
  `order_discount` varchar(15) NOT NULL,
  `order_gtotal` varchar(15) NOT NULL,
  `order_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_buffet`, `order_date`, `order_buffet_price`, `dining_date`, `dining_time`, `order_pax`, `address_block`, `address_street`, `address_postal`, `customer_name`, `customer_contact`, `customer_email`, `order_notes`, `order_stotal`, `order_delivery`, `order_gst`, `order_discount`, `order_gtotal`, `order_status`) VALUES
(42, '8+1 Courses', '2019-06-01', '10.80', '2019-06-09', '17:30:00', 50, 'Block 582', 'Woodlands Drive 16', '730582', 'Vimal', '82918173', 'vimal@apstrix.com', 'Additional Napkins', '1385', 0, '87.255', '10', '1333.755', 3),
(43, '8+1 Courses', '2019-06-01', '10.80', '2019-06-16', '17:50:00', 65, 'Block 586', 'Woodlands Drive 62', '730586', 'Aadiv', '66562171', 'akhila@apstrix.com', 'Additional Sauces', '978', 0, '61.614', '10', '941.814', 2),
(47, '11+1 Courses', '2019-06-10', '15.50', '2019-06-13', '15:30:00', 65, '23 ', 'Wroodlands Industrial Park E1', '757741', 'Mok', '85859696', 'mok@bayfrontfood.sg', 'More Napkins and Trash Bags', '1202', 0, '67.312', '20', '1028.912', 3),
(48, '11+1 Courses', '2019-06-10', '15.50', '2019-06-15', '17:30:00', 65, '23 ', 'Woodlands Industrial Park E1', '757741', 'Boon', '85749636', 'davidboon@gmail.com', 'Additional Notes', '1568.9', 0, '109.823', '0', '1678.723', 3),
(50, '11+1 Courses', '2019-06-10', '15.50', '2019-06-15', '17:30:00', 65, '23 ', 'Woodlands Industrial Park E1', '757741', 'Boon David', '85749636', 'davidboon@gmail.com', 'Additional Notes', '1568.9', 0, '98.8407', '10', '1510.8507', 2),
(51, '11+1 Courses', '2019-06-11', '15.50', '2019-07-31', '18:59:00', 50, '23 Wroodlands Industrial ', 'Woodlands Industrial Park E1', '757741', 'Vimal', '82918173', 'vimal@apstrix.com', '', '821', 0, '51.723', '10', '790.623', 1),
(52, '9+1 Courses', '2019-06-29', '11.80', '2019-06-30', '17:30:00', 40, 'Casablanca', '27 Rosewwod Drive', '737677', 'Vimal', '82918173', 'vimalkarthik@apstrix.com', 'Plates, Spoons, Napkins and Trashbags', '1066', 0, '67.158', '10', '1026.558', 3),
(53, '8+1 Courses', '2019-06-30', '10.80', '2019-07-07', '21:00:00', 33, 'Casablanca', '27 Rosewwod Drive', '737677', 'Akhila', '82918173', 'akhilavimal@hotmail.com', 'Additional Trash Bags', '1098.9', 0, '61.5384', '20', '940.6584', 1),
(54, '9+1 Courses', '2019-06-30', '11.80', '2019-07-06', '20:00:00', 50, 'Block 582', 'Woodlands Drive 16730582', '730582', 'Ramya', '67', 'ramyaraj@gmail.com', 'Disposable Plates and Spoons', '1035.5', 0, '65.2365', '10', '997.1865', 3),
(55, '8+1 Courses', '2019-07-04', '10.80', '2019-07-14', '11:00:00', 35, 'BBQ Pit C5', 'Yishun Central 1, Northpark Residences', 'S768802', 'Jayzle Coballes', '97884212', 'jayzlecob@gmail.com', 'Food Ready to Serve @ 11am', '1074.5', 0, '75.215', '0', '1149.715', 1),
(56, '9+1 Courses', '2019-07-06', '11.80', '2019-07-08', '20:30:00', 35, '523A ', 'Tampines Central 7', '521523', 'Alan Chan', '87586369', 'alenchan@gmail.com', 'All good', '413', 40, '25.928', '20', '396.328', 0),
(57, '9+1 Courses', '2019-07-06', '11.80', '2019-07-08', '20:30:00', 35, '523A ', 'Tampines Central 7', '521523', 'Alan Chan', '87586369', 'alenchan@gmail.com', 'All good', '413', 0, '28.91', '0', '441.91', 0),
(58, '9+1 Courses', '2019-07-08', '11.80', '2019-07-08', '20:30:00', 35, '523A ', 'Tampines Central 7', '521523', 'Alan Chan', '87586369', 'alenchan@gmail.com', 'All good', '413', 0, '28.91', '0', '441.91', 0),
(62, '9+1 Courses', '2019-07-08', '11.80', '2019-07-15', '17:55:00', 35, '523A', 'Tampines Central 7', '521523', 'Xander Mok', '85848685', 'xander@akstech.com.sg', 'Order Testing', '413', 60, '33.11', '0', '506.11', 0),
(63, '8+1 Courses', '2019-08-23', '10.80', '2019-08-25', '17:00:00', 33, '582', 'Woodlands Drive 16', '730582', 'Mayuri Mohandas', '63685653', 'mayurim2909@gmail.com', 'Additional Napkins', '356.4', 0, '24.948', '0', '381.348', 1),
(64, '11+1 Courses', '2019-11-13', '15.50', '2019-11-14', '20:30:00', 100, '#03-470, Block 582', 'Woodlands Drive 16', '730582', 'Vishnu Karthik', '82918173', 'vimalkarthiksg@apstrix.com', 'Test order for Delivery Details', '3035', 60, '174.16', '20', '2662.16', 0),
(65, '9+1 Courses', '2019-11-15', '11.80', '2019-11-16', '17:30:00', 35, '#10-112, Block 586', 'Woodlands Drive 70', '733586', 'Krishnan', '85967485', 'krishnan@gmail.com', 'Additional is note is no applicable as it is a test order', '1066.4', 60, '78.848', '0', '1205.248', 0),
(66, '8+1 Courses', '2019-11-27', '10.80', '2019-11-28', '05:30:00', 10, '04-220, Block 38', 'Bedok Street 21', '650038', 'Chitra', '63685653', 'chitrashenoy@gmail.com', 'Test Order', '405', 30, '29.0325', '5', '443.7825', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_add_items`
--

CREATE TABLE `order_add_items` (
  `order_add_items_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_add_items_eng` varchar(55) NOT NULL,
  `order_add_items_pax` int(11) NOT NULL,
  `order_add_items_price` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_add_items`
--

INSERT INTO `order_add_items` (`order_add_items_id`, `order_id`, `order_add_items_eng`, `order_add_items_pax`, `order_add_items_price`) VALUES
(25, 42, 'Jumbo Prawns', 25, '9.20'),
(26, 42, 'Lobster', 30, '20.50'),
(27, 43, 'Jumbo Prawns', 30, '9.20'),
(28, 43, 'Lobster', 0, '20.50'),
(35, 47, 'Jumbo Prawns', 10, '9.20'),
(36, 47, 'Lobster', 5, '20.50'),
(37, 48, 'Jumbo Prawns', 12, '9.20'),
(38, 48, 'Lobster', 22, '20.50'),
(41, 50, 'Jumbo Prawns', 12, '9.20'),
(42, 50, 'Lobster', 22, '20.50'),
(43, 51, 'Jumbo Prawns', 5, '9.20'),
(44, 51, 'Lobster', 0, '20.50'),
(45, 52, 'Jumbo Prawns', 20, '9.20'),
(46, 52, 'Lobster', 20, '20.50'),
(47, 53, 'Jumbo Prawns', 25, '9.20'),
(48, 53, 'Lobster', 25, '20.50'),
(49, 54, 'Jumbo Prawns', 15, '9.20'),
(50, 54, 'Lobster', 15, '20.50'),
(51, 55, 'Jumbo Prawns', 20, '9.20'),
(52, 55, 'Lobster', 25, '20.50'),
(53, 57, 'Jumbo Prawns', 0, '9.20'),
(54, 57, 'Lobster', 0, '20.50'),
(55, 58, 'Jumbo Prawns', 0, '9.20'),
(56, 58, 'Lobster', 0, '20.50'),
(63, 62, 'Jumbo Prawns', 0, '9.20'),
(64, 62, 'Lobster', 0, '20.50'),
(65, 63, 'Jumbo Prawns', 0, '9.20'),
(66, 63, 'Lobster', 0, '20.50'),
(67, 64, 'Jumbo Prawns', 50, '9.20'),
(68, 64, 'Lobster', 50, '20.50'),
(69, 65, 'Jumbo Prawns', 22, '9.20'),
(70, 65, 'Lobster', 22, '20.50'),
(71, 66, 'Jumbo Prawns', 10, '9.20'),
(72, 66, 'Lobster', 10, '20.50');

-- --------------------------------------------------------

--
-- Table structure for table `order_courses`
--

CREATE TABLE `order_courses` (
  `order_courses_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `order_course_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_courses`
--

INSERT INTO `order_courses` (`order_courses_id`, `order_id`, `course_id`, `order_course_name`) VALUES
(90, 42, 73, 'Starters'),
(91, 42, 74, 'Main Course'),
(92, 42, 79, 'Drinks'),
(93, 43, 73, 'Starters'),
(94, 43, 74, 'Main Course'),
(95, 43, 79, 'Drinks'),
(105, 47, 83, 'Starters'),
(106, 47, 84, 'Main Course'),
(107, 47, 85, 'Drinks'),
(108, 48, 83, 'Starters'),
(109, 48, 84, 'Main Course'),
(110, 48, 85, 'Drinks'),
(114, 50, 83, 'Starters'),
(115, 50, 84, 'Main Course'),
(116, 50, 85, 'Drinks'),
(117, 51, 83, 'Starters'),
(118, 51, 84, 'Main Course'),
(119, 51, 85, 'Drinks'),
(120, 52, 80, 'Starters'),
(121, 52, 81, 'Main Course'),
(122, 52, 82, 'Drinks'),
(123, 53, 73, 'Starters'),
(124, 53, 74, 'Main Course'),
(125, 53, 79, 'Drinks'),
(126, 54, 80, 'Starters'),
(127, 54, 81, 'Main Course'),
(128, 54, 82, 'Drinks'),
(129, 55, 73, 'Starters'),
(130, 55, 74, 'Main Course'),
(131, 55, 79, 'Drinks'),
(132, 57, 80, 'Starters'),
(133, 57, 81, 'Main Course'),
(134, 57, 82, 'Drinks'),
(135, 58, 80, 'Starters'),
(136, 58, 81, 'Main Course'),
(137, 58, 82, 'Drinks'),
(147, 62, 80, 'Starters'),
(148, 62, 81, 'Main Course'),
(149, 62, 82, 'Drinks'),
(150, 63, 73, 'Starters'),
(151, 63, 74, 'Main Course'),
(152, 63, 79, 'Drinks'),
(153, 64, 83, 'Starters'),
(154, 64, 84, 'Main Course'),
(155, 64, 85, 'Drinks'),
(156, 65, 80, 'Starters'),
(157, 65, 81, 'Main Course'),
(158, 65, 82, 'Drinks'),
(159, 66, 73, 'Starters'),
(160, 66, 74, 'Main Course'),
(161, 66, 79, 'Drinks');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `order_items_id` int(11) NOT NULL,
  `order_items_eng` varchar(55) NOT NULL,
  `order_courses_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`order_items_id`, `order_items_eng`, `order_courses_id`) VALUES
(109, 'Crispy Chicken Wings', 90),
(110, 'Yang Chow Fried Rice', 91),
(111, 'Chicken Fried Rice', 91),
(112, 'Lime Cordial', 92),
(113, 'Crispy Chicken Wings', 93),
(114, 'Yang Chow Fried Rice', 94),
(115, 'Mee Goreng', 94),
(116, 'Orange Juice', 95),
(129, 'Yang Chow Fried Rice', 105),
(130, 'Chicken Fried Rice', 105),
(131, 'Mee Goreng', 106),
(132, 'Xing Zhao Bee Hoon', 106),
(133, 'Sauteed Fish Fillet', 106),
(134, 'Lime Cordial', 107),
(135, 'Yang Chow Fried Rice', 108),
(136, 'Steamed Fragrant White Rice', 108),
(137, 'Mee Goreng', 109),
(138, 'Sauteed Fish Fillet', 109),
(139, 'Sweet & Sour Chicken', 109),
(140, 'Lime Cordial', 110),
(147, 'Yang Chow Fried Rice', 114),
(148, 'Steamed Fragrant White Rice', 114),
(149, 'Mee Goreng', 115),
(150, 'Sauteed Fish Fillet', 115),
(151, 'Sweet & Sour Chicken', 115),
(152, 'Lime Cordial', 116),
(153, 'Yang Chow Fried Rice', 117),
(154, 'Chicken Fried Rice', 117),
(155, 'Mee Goreng', 118),
(156, 'Xing Zhao Bee Hoon', 118),
(157, 'Sweet & Sour Chicken', 118),
(158, 'Orange Juice', 119),
(159, 'Cheesy Sausage Balls', 120),
(160, 'Yang Chow Fried Rice', 121),
(161, 'Chicken Fried Rice', 121),
(162, 'Mee Goreng', 121),
(163, 'Lime Cordial', 122),
(164, 'Orange Juice', 122),
(165, 'Cheesy Sausage Balls', 123),
(166, 'Mee Goreng', 124),
(167, 'Chicken Fried Rice', 124),
(168, 'Lime Cordial', 125),
(169, 'Crispy Chicken Wings', 126),
(170, 'Yang Chow Fried Rice', 127),
(171, 'Chicken Fried Rice', 127),
(172, 'Mee Goreng', 127),
(173, 'Lime Cordial', 128),
(174, 'Orange Juice', 128),
(175, 'Crispy Chicken Wings', 129),
(176, 'Yang Chow Fried Rice', 130),
(177, 'Chicken Fried Rice', 130),
(178, 'Lime Cordial', 131),
(179, 'Cheesy Sausage Balls', 132),
(180, 'Yang Chow Fried Rice', 133),
(181, 'Chicken Fried Rice', 133),
(182, 'Mee Goreng', 133),
(183, 'Lime Cordial', 134),
(184, 'Orange Juice', 134),
(185, 'Cheesy Sausage Balls', 135),
(186, 'Yang Chow Fried Rice', 136),
(187, 'Chicken Fried Rice', 136),
(188, 'Mee Goreng', 136),
(189, 'Lime Cordial', 137),
(190, 'Orange Juice', 137),
(209, 'Cheesy Sausage Balls', 147),
(210, 'Yang Chow Fried Rice', 148),
(211, 'Chicken Fried Rice', 148),
(212, 'Mee Goreng', 148),
(213, 'Lime Cordial', 149),
(214, 'Orange Juice', 149),
(215, 'Crispy Chicken Wings', 150),
(216, 'Yang Chow Fried Rice', 151),
(217, 'Chicken Fried Rice', 151),
(218, 'Lime Cordial', 152),
(219, 'Chicken Fried Rice', 153),
(220, 'Steamed Fragrant White Rice', 153),
(221, 'Mee Goreng', 154),
(222, 'Sauteed Fish Fillet', 154),
(223, 'Sweet & Sour Chicken', 154),
(224, 'Lime Cordial', 155),
(225, 'Cheesy Sausage Balls', 156),
(226, 'Yang Chow Fried Rice', 157),
(227, 'Chicken Fried Rice', 157),
(228, 'Mee Goreng', 157),
(229, 'Lime Cordial', 158),
(230, 'Orange Juice', 158),
(231, 'Cheesy Sausage Balls', 159),
(232, 'Yang Chow Fried Rice', 160),
(233, 'Chicken Fried Rice', 160),
(234, 'Lime Cordial', 161);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL,
  `entity` varchar(50) NOT NULL,
  `entity_address` varchar(155) NOT NULL,
  `entity_postal` varchar(25) NOT NULL,
  `entity_email` varchar(55) NOT NULL,
  `entity_contact` varchar(25) NOT NULL,
  `entity_web` varchar(55) NOT NULL,
  `entity_timezone` varchar(25) NOT NULL,
  `entity_currency` varchar(15) NOT NULL,
  `entity_currency_symbol` varchar(5) NOT NULL,
  `entity_gst` int(11) NOT NULL,
  `entity_logo` varchar(15) NOT NULL,
  `entity_invoice_prefix` varchar(15) NOT NULL,
  `entity_invoice_terms` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settings_id`, `entity`, `entity_address`, `entity_postal`, `entity_email`, `entity_contact`, `entity_web`, `entity_timezone`, `entity_currency`, `entity_currency_symbol`, `entity_gst`, `entity_logo`, `entity_invoice_prefix`, `entity_invoice_terms`) VALUES
(1, 'Bayfront Catering Services', '20 Opal Crescent, Singapore', '328415', 'admin@bayfrontfood.sg', '91199333', 'http://bayfrontfood.com.sg', 'Asia/Singapore', 'SGD', '$', 1, 'logo.png', 'BF', '<p><strong>Buffet Information</strong></p>\r\n\r\n<ul>\r\n	<li>Complimentary full buffet line set-up with table cloth,skirting,disposable utensils and garbage bag will be provided.</li>\r\n	<li>Orders must be made at least 5 working days in advance prior to function date.\r\n	<ul>\r\n		<li>Menu items and time slots are subjected to availability.</li>\r\n		<li>You are kindly advised to place your order 2 weeks in advance to avoid any disappointment.</li>\r\n	</ul>\r\n	</li>\r\n	<li>A delivery fee of $50.00 (outside CBD area) and $60.00 (within CBD area) is applied for all buffet orders.</li>\r\n	<li>Delivery fee will be waived for orders more than $700.00</li>\r\n	<li>An additional surcharge of $50 will be applicable for flight of stairs for no lift landing area.</li>\r\n	<li>Food is best consumed within 3 hours of delivery for all menus with buffet set-up.</li>\r\n	<li>We reserve the right to change the dish of equivalent value without prior notice,subject to availability.</li>\r\n	<li>Cancellation Charges:\r\n	<ul>\r\n		<li>70% cancellation charges will be applicable for cancellation 2 days prior to event.</li>\r\n		<li>100% cancellation fees will be applicable for cancellation 1 day prior to event.</li>\r\n	</ul>\r\n	</li>\r\n	<li>Late collection:\r\n	<ul>\r\n		<li>$120.00 for collection from 9.30pm to 11.30pm.</li>\r\n		<li>$60.00 for collection on the next day of event.</li>\r\n	</ul>\r\n	</li>\r\n	<li>All prices are subjected to 7% GST.</li>\r\n</ul>\r\n\r\n<p><strong>Bento Information</strong></p>\r\n\r\n<ul>\r\n	<li>A delivery fee of $30 (outside CBD area) and $40 (within CBD area) is applied for all buffet orders.</li>\r\n	<li>Delivery fee will be waived for orders more than $300.00</li>\r\n	<li>Orders must be made at least 5 working days in advance prior to function date.Menu items and time slots are subjected to availability.\r\n	<ul>\r\n		<li><em>Note: You are kindly advised to place your order 2 weeks in advance to avoid any disappointment</em></li>\r\n	</ul>\r\n	</li>\r\n	<li>All prices are subjected to 7% GST.</li>\r\n</ul>\r\n\r\n<p><strong>Mini Buffet Information</strong></p>\r\n\r\n<ul>\r\n	<li>No buffet line set-up included.</li>\r\n	<li>Disposable trays and utensils will be provided.</li>\r\n	<li>A delivery fee of $30.00 is applied for all buffet orders.</li>\r\n	<li>Delivery fee will be waived for orders more than $400.00</li>\r\n	<li>All prices are subjected to 7% GST.</li>\r\n</ul>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(50) NOT NULL,
  `supplier_block` varchar(20) NOT NULL,
  `supplier_unit` varchar(15) NOT NULL,
  `supplier_street` varchar(50) NOT NULL,
  `supplier_postal` varchar(15) NOT NULL,
  `supplier_person` varchar(25) NOT NULL,
  `supplier_contact` varchar(15) NOT NULL,
  `supplier_email` varchar(50) NOT NULL,
  `supplier_total_supplies` varchar(25) NOT NULL,
  `supplier_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `supplier_name`, `supplier_block`, `supplier_unit`, `supplier_street`, `supplier_postal`, `supplier_person`, `supplier_contact`, `supplier_email`, `supplier_total_supplies`, `supplier_status`) VALUES
(1, 'Akstech', '23', '05-02', 'Woodlands Industrial Park E1', '757741', 'Low Weesuan', '82828282', 'weesuanlow@akstech.com.sg', '', 1),
(2, 'Apstrix Solutions', '16', '10-10', 'Kallang Road', '730582', 'Vimal Karthik', '82918173', 'vimalkarthik@apstrix.com', '', 1),
(4, 'Excel Hardware', 'Northlink Building', '03-54', '10 Admiralty Street', '757688', 'Kaden Choa', '45858585', 'kaden@excelhw.com.sg', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplies`
--

CREATE TABLE `supplies` (
  `supplies_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(30) NOT NULL,
  `item_qty_order` varchar(10) NOT NULL,
  `item_qty_supplied` varchar(10) NOT NULL,
  `supply_date` date NOT NULL,
  `item_expiry_date` date NOT NULL,
  `location_id` int(11) NOT NULL,
  `supply_total` varchar(20) NOT NULL,
  `supply_remarks` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplies`
--

INSERT INTO `supplies` (`supplies_id`, `supplier_id`, `supplier_name`, `item_id`, `item_name`, `item_qty_order`, `item_qty_supplied`, `supply_date`, `item_expiry_date`, `location_id`, `supply_total`, `supply_remarks`) VALUES
(79, 2, 'Apstrix Solutions', 29, 'Disposable Spoons', '250', '250', '2019-06-26', '0000-00-00', 1, '500', 'Invoice 2000'),
(80, 2, 'Apstrix Solutions', 30, 'Napkins', '150', '150', '2019-06-12', '0000-00-00', 4, '180', 'Invoice 2001'),
(81, 2, 'Apstrix Solutions', 32, 'Kampong Chicken', '30', '30', '2019-07-11', '2019-07-19', 1, '105', 'Invoice 2003'),
(82, 4, 'Excel Hardware', 34, 'Beef (Quality 2nd)', '25', '25', '2019-07-12', '2019-07-19', 1, '70', 'Invoice 2007'),
(83, 1, 'Akstech', 31, 'Prawn', '15', '15', '2019-08-27', '2019-08-30', 5, '82.5', 'Invoice 2890'),
(84, 1, 'Akstech', 30, 'Napkins', '200', '200', '2019-08-26', '0000-00-00', 3, '240', 'Invoice 2831');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `unit_id` int(11) NOT NULL,
  `unit_name` varchar(50) NOT NULL,
  `unit_short` varchar(50) NOT NULL,
  `unit_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`unit_id`, `unit_name`, `unit_short`, `unit_status`) VALUES
(1, 'Kilograms', 'Kg', 1),
(2, 'Packets', 'Pax', 1),
(3, 'Grams', 'Gm', 1),
(5, 'Pieces', 'Pcs', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1: Admin 2: Employee',
  `user_email` varchar(55) NOT NULL,
  `user_display` varchar(25) NOT NULL,
  `user_fullname` varchar(55) NOT NULL,
  `user_contact` varchar(20) NOT NULL,
  `user_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `user_type`, `user_email`, `user_display`, `user_fullname`, `user_contact`, `user_status`) VALUES
(1, 'admin', '123456789', 1, 'admin@bayfrontfood.sg', 'admin.png', 'Mok Nicholas', '63685653', 1),
(2, 'employee', '12345678', 2, 'employee@gmail.com', 'employee.png', 'Employee', '90024292', 1),
(3, 'dupeplay', '123456789', 1, 'vimalkarthik@apstrix.com', 'dupeplay.png', 'Vimal Karthik', '82918173', 1),
(5, 'benlim', '', 2, 'ben@akstech.com.sg', 'benlim.png', 'Ben Lim', '85858585', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addons`
--
ALTER TABLE `addons`
  ADD PRIMARY KEY (`addon_id`);

--
-- Indexes for table `buffet`
--
ALTER TABLE `buffet`
  ADD PRIMARY KEY (`buffet_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `courses_item`
--
ALTER TABLE `courses_item`
  ADD PRIMARY KEY (`courses_item_id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`inventory_id`);

--
-- Indexes for table `inventory_draws`
--
ALTER TABLE `inventory_draws`
  ADD PRIMARY KEY (`drawn_id`);

--
-- Indexes for table `inventory_items`
--
ALTER TABLE `inventory_items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `inventory_location`
--
ALTER TABLE `inventory_location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `master_category`
--
ALTER TABLE `master_category`
  ADD PRIMARY KEY (`master_cat_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_add_items`
--
ALTER TABLE `order_add_items`
  ADD PRIMARY KEY (`order_add_items_id`);

--
-- Indexes for table `order_courses`
--
ALTER TABLE `order_courses`
  ADD PRIMARY KEY (`order_courses_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`order_items_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settings_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `supplies`
--
ALTER TABLE `supplies`
  ADD PRIMARY KEY (`supplies_id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addons`
--
ALTER TABLE `addons`
  MODIFY `addon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `buffet`
--
ALTER TABLE `buffet`
  MODIFY `buffet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `courses_item`
--
ALTER TABLE `courses_item`
  MODIFY `courses_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `inventory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `inventory_draws`
--
ALTER TABLE `inventory_draws`
  MODIFY `drawn_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `inventory_items`
--
ALTER TABLE `inventory_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `inventory_location`
--
ALTER TABLE `inventory_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `master_category`
--
ALTER TABLE `master_category`
  MODIFY `master_cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `order_add_items`
--
ALTER TABLE `order_add_items`
  MODIFY `order_add_items_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `order_courses`
--
ALTER TABLE `order_courses`
  MODIFY `order_courses_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `order_items_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `supplies`
--
ALTER TABLE `supplies`
  MODIFY `supplies_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
